function onReady(ev) {
    document.querySelector('#search-input').focus();
    window.removeEventListener('DOMContentLoaded', onReady);
}
window.addEventListener('DOMContentLoaded', onReady);

function _showPost(post){ post.removeAttribute('hidden'); }
function _hidePost(post){ post.setAttribute('hidden', 'true'); }
function filterPosts(input) {
  var val = input.value;
  var searchTerms = val.toLowerCase().match(/[a-z0-9]+/g);
  var posts = document.querySelectorAll('.postlist .row');
  if (val && searchTerms && searchTerms.length){
    posts.forEach(function(post) {
      var slug = post.dataset['slug'];
      if (!slug) { return }
      function termMatchesSlug(term) { return slug.indexOf(term) > -1 };
      (searchTerms.every(termMatchesSlug) ? _showPost : _hidePost)(post);
    });
  } else {
    posts.forEach(_showPost);
  }
}
