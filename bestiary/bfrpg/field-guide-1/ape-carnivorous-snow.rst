.. title: Ape, Carnivorous Snow
.. slug: ape-carnivorous-snow
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Carnivorous Snow
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 6          |
+-----------------+------------+
| No. of Attacks: | 2 claws    |
+-----------------+------------+
| Damage:         | 1d6 claw   |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 2d6        |
+-----------------+------------+
| Save As:        | Fighter: 6 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 500        |
+-----------------+------------+

**Carnivorous** **Snow Apes** are a larger variety of carnivorous ape with shaggy snow-white fur. They have long fangs for killing and tearing flesh. Frost giants often breed snow apes and keep them as pets.

A carnivorous snow ape is difficult to see in ice or snow, and thus surprises on 1-4 on 1d6 in such conditions.
