.. title: Infernal, Ice Devil
.. slug: infernal-ice-devil
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Infernal, Ice Devil
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 18                  |
+-----------------+---------------------+
| Hit Dice:       | 3**                 |
+-----------------+---------------------+
| No. of Attacks: | 1 claw + special    |
+-----------------+---------------------+
| Damage:         | 1d3 claw + 1d4 cold |
+-----------------+---------------------+
| Movement:       | 30’ Fly 50’         |
+-----------------+---------------------+
| No. Appearing:  | 1d4                 |
+-----------------+---------------------+
| Save As:        | Fighter: 3          |
+-----------------+---------------------+
| Morale:         | 9                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 205                 |
+-----------------+---------------------+

An **Ice Devil** is a winged and clawed humanoid monster around 3 feet tall. Its hairless bodies are genderless, with blue skin so light as to be almost white.

Whenever an ice devil attacks, an additional 1d4 cold damage is dealt. An ice devil can breathe a cone of icy shards every 1d4 rounds, causing 1d4 cold damage to one creature within 10 feet of the ice devil. Additionally, the individual struck must save vs. Paralysis or suffer a -1 penalty to hit and damage for 1d6 rounds. Any large heat source nearby gives a +2 to the save. In addition, an ice devil can cast **magic** **missile** once per hour at a level equivalent to its HD (typically 3rd-level caster).

An ice devil is completely immune to cold-based damage, and it regenerates 2 hp per round when in icy or wintry conditions. Fire or heat-based attacks cause an additional 50% damage to an ice devil. Magical attacks, such as damaging spells, inflict only half damage upon an ice devil. This magical resistance causes magical weapons to lose the benefit of any damage bonus (although any to-hit bonuses still apply).
