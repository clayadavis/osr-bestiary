.. title: Grick
.. slug: grick
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Grick
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 2                       |
+-----------------+-------------------------+
| No. of Attacks: | 4 tentacles / 1 bite    |
+-----------------+-------------------------+
| Damage:         | 1d4 tentacles, 1d3 bite |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1, Wild 1d4             |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | V                       |
+-----------------+-------------------------+
| XP:             | 75                      |
+-----------------+-------------------------+

An adult **Grick** is a large snake-like creature weighing around 200 pounds and stretching about 8 feet long from the tip of its tentacles to the end of its tail. Its body coloration is uniformly dark with a pale underbelly, and its tentacles attach just behind its head; the tentacles are segmented like the body of an earthworm.

A grick hunts by hiding near high-traffic areas, using its natural coloration to blend into the shadows; when doing this, the grick surprises on a 1-3 on 1d6. When prey ventures near it lashes out with its tentacles. Its jaws are small and weak compared to its body mass, so rather than consume its kill immediately, a grick normally drags its victim back to its lair to be eaten at its leisure.

Multiple gricks do not fight in concert; rather, each attacks the prey closest to it, and breaks off the fight as soon as it can drag a dead or unconscious victim away.
