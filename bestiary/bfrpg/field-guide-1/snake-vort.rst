.. title: Snake, Vort
.. slug: snake-vort
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Snake, Vort
.. type: text
.. image:

+----------------+--------------+
| Armor Class:   | 15           |
+----------------+--------------+
| Hit Dice:      | 1+2          |
+----------------+--------------+
| No of Attacks: | 1            |
+----------------+--------------+
| Damage:        | 1d3 + poison |
+----------------+--------------+
| Movement:      | 50'          |
+----------------+--------------+
| No. Appearing: | 1d4          |
+----------------+--------------+
| Save As:       | Fighter: 1   |
+----------------+--------------+
| Morale:        | 7            |
+----------------+--------------+
| Treasure Type: | none         |
+----------------+--------------+
| XP:            | 25           |
+----------------+--------------+

A **Vort**, otherwise known as a **crested serpent**, is a large snake ranging from 7 to 9 feet in length. It is dark in color with a contrasting brightly-colored crest on its head.

A crested serpent is venomous. Small animals bitten by a crested serpent must save vs. Poison or die; larger creatures (anything kobold size or larger) will be rendered unconscious for 2d4 turns if the save is failed.
