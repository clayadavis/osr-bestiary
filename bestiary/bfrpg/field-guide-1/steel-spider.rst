.. title: Steel Spider
.. slug: steel-spider
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Steel Spider
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 18 †         |
+-----------------+--------------+
| Hit Dice:       | 5**          |
+-----------------+--------------+
| No. of Attacks: | 2 forelegs   |
+-----------------+--------------+
| Damage:         | 1d6 forelegs |
+-----------------+--------------+
| Movement:       | 40'          |
+-----------------+--------------+
| No. Appearing:  | 1            |
+-----------------+--------------+
| Save As:        | Fighter: 5   |
+-----------------+--------------+
| Morale:         | 12           |
+-----------------+--------------+
| Treasure Type:  | None         |
+-----------------+--------------+
| XP:             | 450          |
+-----------------+--------------+

A **Steel Spider** is a construct used by inventive Magic-users to guard his or her possessions. It is made of steel, with jeweled eyes (worth 100 gp each if removed). Its main body is one foot in diameter, with each leg two feet long. It is often painted black to blend in with its surroundings. It has Darkvision out to 120 feet.

A steel spider is often placed in ambush, and can only be detected on a 1 on 1d6 if it is hidden (even with Darkvision). It can leap 30 feet, and automatically gain surprise when making a leap attack if it has not already been detected. It attacks with sword-like forelegs.

As a construct, it is immune to **fear**, **charm**, and similar spells. It takes an additional 1d6 points of damage per hit from any attack that rusts metal. It only takes half damage from non-magical weapons. It can climb walls, and even hang upside down. It is resistant to magic, and gains a +4 bonus to saves vs. Wands or Spells.
