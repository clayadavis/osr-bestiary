.. title: Homunculus
.. slug: homunculus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Homunculus
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 14                  |
+-----------------+---------------------+
| Hit Dice:       | 2                   |
+-----------------+---------------------+
| No. of Attacks: | 1 bite + poison     |
+-----------------+---------------------+
| Damage:         | 1d4-1 bite + poison |
+-----------------+---------------------+
| Movement:       | 20'                 |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Fighter: 2          |
+-----------------+---------------------+
| Morale:         | 12                  |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 75                  |
+-----------------+---------------------+

A **Homunculus** is a miniature servant created by a wizard. It is a weak combatant but makes for an effective spy, messenger, or scout. A homunculus’s creator determines its precise features. A homunculus cannot speak, but the process of creating one links it telepathically with its creator.

It knows what its master knows and can convey to him or her everything it sees and hears (up to a distance of 1,500 feet). A homunculus never travels beyond this range willingly, though it can be moved forcibly. If this occurs, the creature does everything in its power to regain contact with its master. An attack that destroys a homunculus deals 2d10 points of damage to its master.

A homunculus will try to climb onto its victim and bite with its venomous fangs. On a failed save vs. Poison, the opponent will fall asleep for 6d6 minutes. If the master is slain the homunculus also dies, its body swiftly melting away into a pool of ichor.
