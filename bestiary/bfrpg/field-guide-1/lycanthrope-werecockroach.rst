.. title: Lycanthrope, Werecockroach*
.. slug: lycanthrope-werecockroach
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Lycanthrope, Werecockroach*
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 15 †                       |
+-----------------+----------------------------+
| Hit Dice:       | 3**                        |
+-----------------+----------------------------+
| No. of Attacks: | 1 bite or 1 weapon         |
+-----------------+----------------------------+
| Damage:         | 1d6 bite or by weapon      |
+-----------------+----------------------------+
| Movement:       | 50' Human Form 40' Fly 10' |
+-----------------+----------------------------+
| No. Appearing:  | 2d4, Wild 2d10, Lair 2d10  |
+-----------------+----------------------------+
| Save As:        | Fighter: 3*                |
+-----------------+----------------------------+
| Morale:         | 8                          |
+-----------------+----------------------------+
| Treasure Type:  | C                          |
+-----------------+----------------------------+
| XP:             | 205                        |
+-----------------+----------------------------+

A **Werecockroach** is a human (or occasionally other humanoid) who can transform into a gigantic cockroach. In human form, a werecockroach tends to be a thin, unkempt person, usually walking a bit hunched and typically fond of wearing long, brown overcoats or robes. A werecockroach dislikes sunlight and other bright lights, preferring to wander about at night or underground even when in human form. Regardless of current form, a werecockroach tends to scurry, stopping from time to time to sniff and look around.

A werecockroach can assume the form of an enormous, unsightly 6 foot long cockroach. When in this insect form, a werecockroach is able to fly, albeit clumsily and slowly, and attacks by biting. In addition to this insect form, a werecockroach can also assume an intermediate form (a "roachman"). In this form, the werecockroach cannot fly and looks generally humanoid in shape, but has insect mandibles protruding from its mouth, two thin, long antennae on its head, shiny black bug eyes, spikes sticking out of its limbs, and four dark brown wings on its back. The roachman form shares the insect form's immunity to normal weapons, but must use a weapon to attack instead of biting.

In any form, a werecockroach is immune to normal weapons, and one must use silver or magical weapons in order to harm it. A werecockroach has a bonus of +5 on saves vs. Poison, and is immune to most forms of disease, saving at +5 against any disease that the GM rules may harm them. However, a werecockroach has a weakness: it recoils from bright lights (as bright as sunlight or a **continual light** spell; twilight, torches, **light**, or lanterns won't affect one); a werecockroach must make a Morale check when exposed to such a light, with failure causing it to scurry away to the nearest dark place.
