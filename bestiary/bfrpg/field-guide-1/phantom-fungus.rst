.. title: Phantom Fungus
.. slug: phantom-fungus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Phantom Fungus
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 14         |
+-----------------+------------+
| Hit Dice:       | 2          |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 1d6 bite   |
+-----------------+------------+
| Movement:       | 20'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 2 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 75         |
+-----------------+------------+

While its normal form is completely invisible (even while attacking), when the **Phantom Fungus** is killed or somehow made visible it looks like a brown or greenish-brown mass with a cluster of nodules at its highest point. The nodule cluster atop the main mass serves as its sensory organs. The creature feeds and attacks with a gaping maw lined with rows of teeth. Four stumpy legs support the creature and allow it to move about its subterranean environment.

Anyone attacking a phantom fungus does so at -4 unless the attacker can detect invisible creatures.
