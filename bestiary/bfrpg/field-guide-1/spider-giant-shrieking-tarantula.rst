.. title: Spider, Giant Shrieking Tarantula
.. slug: spider-giant-shrieking-tarantula
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Spider, Giant Shrieking Tarantula
.. type: text
.. image:

+----------------+--------------------+
| Armor Class:   | 18                 |
+----------------+--------------------+
| Hit Dice:      | 7*                 |
+----------------+--------------------+
| No of Attacks: | 1 bite             |
+----------------+--------------------+
| Damage:        | 1d12 bite + poison |
+----------------+--------------------+
| Movement:      | 50'                |
+----------------+--------------------+
| No. Appearing: | 1                  |
+----------------+--------------------+
| Save As:       | Fighter: 7         |
+----------------+--------------------+
| Morale:        | 9                  |
+----------------+--------------------+
| Treasure Type: | None               |
+----------------+--------------------+
| XP:            | 735                |
+----------------+--------------------+

A **Shrieking Tarantula** is a rare, aberrant form of the regular giant tarantula. It is about the size of a horse and has red leg-joints. A shrieking tarantula is somewhat intelligent but is driven by its hunting instincts.

Its bite is deadly; those bitten must save vs. Poison or be forced to dance wildly. The dance lasts 2d10 rounds, during which time the victim has a -4 penalty on attacks and further saving throws. Thieves or characters with similar Thief-like abilities may not utilize such skills while dancing. Onlookers must save vs. Spells or begin dancing themselves; such “secondary” victims suffer the same penalties as above, but they will only dance for 2d4 rounds. During each round that those bitten are dancing, they must save vs. Poison again or take 1d4 points of damage. The secondary victims do not suffer this effect. **Neutralize** **poison** will cure the original victim, and **dispel** **magic** will stop the dance for all victims in the area of effect, whether original or secondary.

In addition to its normal attack, the shrieking tarantula may flick a cloud of urticating hairs from its abdomen at an enemy. This fills an area of a 20 foot radius, and persists for one round. Creatures caught within the area of effect suffer a painful, itching rash, and for the next turn must save vs. Poison each round. A failed save indicates that the victim spends that entire round scratching and screaming. These hairs may be used only once, as it takes a week to regrow.

Finally, the shrieking tarantula can emit a shriek; those within 10 feet must save vs. Paralyzation or be stunned and unable to act for 1 round. When hunting, the spider often stuns its prey before binding it in silken thread, thus rendering its prey helpless so it can be be fed upon later.
