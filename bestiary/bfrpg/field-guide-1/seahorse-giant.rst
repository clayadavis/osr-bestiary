.. title: Seahorse, Giant
.. slug: seahorse-giant
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Seahorse, Giant
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 1          |
+-----------------+------------+
| No. of Attacks: | 1 ram      |
+-----------------+------------+
| Damage:         | 1d4 ram    |
+-----------------+------------+
| Movement:       | Swim 80'   |
+-----------------+------------+
| No. Appearing:  | Wild 10d10 |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 6          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 25         |
+-----------------+------------+

A **Giant Seahorse** is not closely related to a hippocampus, but is in fact an actual seahorse, and grows almost to the size of a pony. Though it is not very strong or graceful, it is sometimes used by undersea races to carry or pull things.
