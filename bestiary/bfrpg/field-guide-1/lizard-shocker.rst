.. title: Lizard, Shocker
.. slug: lizard-shocker
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Lizard, Shocker
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 16            |
+-----------------+---------------+
| Hit Dice:       | 2*            |
+-----------------+---------------+
| No. of Attacks: | 1 bite        |
+-----------------+---------------+
| Damage:         | 1d4 bite      |
+-----------------+---------------+
| Movement:       | 40' Swim 20'  |
+-----------------+---------------+
| No. Appearing:  | 1d6, Lair 2d6 |
+-----------------+---------------+
| Save As:        | Fighter: 2    |
+-----------------+---------------+
| Morale:         | 7             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 100           |
+-----------------+---------------+

A **Shocker Lizard** has a pale gray or blue underside and a darker hue on its back. It is about 1 foot tall at the shoulder and weighs 25 pounds. This lizard is typically found in warm marshes.

Once per round, a shocker lizard can deliver an electrical shock to a single opponent within 5 feet. This attack will stun on a failed save vs. Death Ray. Whenever two or more shocker lizards are within 20 feet of each other, they can work together to create a lethal shock. This effect has a radius of 20 feet, centered on any one contributing lizard. The shock deals 2d8 electrical damage for each lizard contributing to it (maximum 12d8); a successful save vs. Death Ray reduces the damage by half.

A shocker lizard relies on its electrical abilities in combat and can automatically detect any electrical discharges within 100 feet. It tends to bite only after attempting to shock an opponent. A solitary lizard flees once it delivers its shocks, but if other shocker lizards are nearby, it will home in on its comrades’ discharges.
