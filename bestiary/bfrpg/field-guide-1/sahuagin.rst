.. title: Sahuagin
.. slug: sahuagin
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Sahuagin
.. type: text
.. image:

+-----------------+-------------------------------------------------------------+
| Armor Class:    | 16                                                          |
+-----------------+-------------------------------------------------------------+
| Hit Dice:       | 2+2*                                                        |
+-----------------+-------------------------------------------------------------+
| No. of Attacks: | 1 trident/1 bite, 2 talons/1 bite, or heavy crossbow or net |
+-----------------+-------------------------------------------------------------+
| Damage:         | 1d8 trident, 1d4 bite, 1d4 talon,1d4 bite, or other weapon  |
+-----------------+-------------------------------------------------------------+
| Movement:       | 30'                                                         |
+-----------------+-------------------------------------------------------------+
| No. Appearing:  | 1d8, Lair 1d6 x10, Wild 1d10+10                             |
+-----------------+-------------------------------------------------------------+
| Save As:        | Fighter: 2                                                  |
+-----------------+-------------------------------------------------------------+
| Morale:         | 7                                                           |
+-----------------+-------------------------------------------------------------+
| Treasure Type:  | Individuals N; Lair E, I, O                                 |
+-----------------+-------------------------------------------------------------+
| XP:             | 100                                                         |
+-----------------+-------------------------------------------------------------+

The **Sahuagin** are a predatory fish-man race that feature green coloration, darker along the back and lighter on the belly. An adult male sahuagin stands roughly 6 feet tall and weighs about 200 pounds. A sahuagin can sense movement underwater within a 30-foot radius. It speaks its own language, as well as Common and usually one other language. A sahuagin can survive out of the water for 1d8 hours before dying from exposure. A sahuagin tears with its feet as it strikes with its talons, or with a weapon. About half of any group of sahuagin are also armed with nets.

A sahuagin fully immersed in fresh water must succeed on a save vs. Poison or become fatigued. Even on a success, it must repeat the save attempt every 10 minutes it remains immersed. Abrupt exposure to bright light blinds a sahuagin for 1d4 rounds. On subsequent rounds, it is dazzled while operating in bright light.
