.. title: Thulid
.. slug: thulid
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Thulid
.. type: text
.. image:

+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Armor Class:    | 15                                                                                                                      |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Hit Dice:       | 1* to 8* (1** to 8** if a Magic-User)                                                                                   |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| No. of Attacks: | 1 weapon or special                                                                                                     |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Damage:         | By weapon or special                                                                                                    |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Movement:       | 40'                                                                                                                     |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| No. Appearing:  | 1d4                                                                                                                     |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Save As:        | Magic-User: 1 to 8                                                                                                      |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Morale:         | 7                                                                                                                       |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| Treasure Type:  | F                                                                                                                       |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+
| XP:             | 1 HD 37 (49); 2 HD 100 (125)3 HD 175 (205); 4 HD 280 (320)5 HD 405 (450); 6 HD 555 (610)7 HD 735 (800); 8 HD 945 (1015) |
+-----------------+-------------------------------------------------------------------------------------------------------------------------+

A **Thulid** is a highly intelligent man-like creature with a squid-like visage, having four to ten tentacles surrounding a beak-like mouth. Though omnivorous, a thulid prefers the brains of sentient creatures as food. It can read minds (as the **ESP** spell) and communicate with each other telepathically. It can speak Common, usually to command slaves.

A thulid uses its tentacles to extract the brain from a foe. It takes 1d4 turns for the tentacles to reach the brain, killing the victim. As this process takes a long time, it is not done during combat. In fact, most thulids are disinterested in physical combat, preferring to use their **mind blast** (see below), magic, or slave creatures for this purpose.

About one-quarter (25%) of thulids are Magic-Users (roll 1d8 for level). The experience point value of a magic-using thulid is greater than normal, and is shown in parentheses after the standard amount.

The thulid's **mind blast** is a cone of mental force with a 60' range and a diameter of 50' at the far end. This ability can be used at most one time per day per HD of the monster, and may not be used more often than every other round. A thulid can choose to either stun or kill those within the affected area. If the thulid chooses to stun, those in the area of effect must save vs. Spells or be rendered unconscious (as if by **sleep**) for 2d6 rounds. A killing blast allows a save vs. Death Ray, with failure resulting in immediate death. Mindless creatures and the undead are unaffected by this attack. Add +2 to the saving throw if the victim is more than 20' from the thulid, or +5 if more than 40' away.

A **helm of telepathy** adds an additional +4 to saving throws. Further, when such saves are successful, the attacking thulid is stunned for 1d4 turns.

When encountered away from their lair, a group of thulids will generally consist of at least one fully mature (8 HD) thulid, with the remainder having 2d4 HD each. See below for details on thulid growth and maturation.

Thulids are actually a strange sort of parasite. When a group of thulids are encountered, the GM should roll 1d20. If the result of this roll is equal to or less than the number encountered, one of the thulids is ready to spawn. Do not count thulids who have only a single HD, as these are not mature enough to reproduce.

If a thulid is ready to spawn, it will notify its brethren which of their opponents it wants to impregnate. The group will then attempt to render the target host unconscious (generally by using the stunning form of mind blast) while removing other opponents as expeditiously as possible. If the thulids prevail, the pregnant one will begin using its tentacles to penetrate the skull of the subdued opponent, but instead of extracting the brain, an egg will be laid. The subdued opponent will then be bound and carried off by the thulid party. Thulids seem to prefer male host bodies, for no apparent reason. Humans are preferred over elves, and elves over dwarves or halflings.

The egg hatches in 3d6 hours, but as the brain feels no sensation the victim will not realize this. In another 1d6 hours the victim will become confused (as if by the spell **confusion**), then in 2d6 more hours will fall into a coma. Up to this point, the condition is reversible with a **cure disease** spell, but after the coma begins, the growth of the infant thulid cannot be stopped in that way. In 3+1d6 days, the victim will suddenly awaken, still appearing normal but with the psyche (and mental blast power) of a thulid. The newly-spawned thulid can speak the thulid language as well as any languages formerly known by the host body, though little or none of that host's mind is left otherwise.

A "newborn" thulid normally has one HD. Over the course of the next year the new thulid will slowly transform, taking on a reddish skin tone and developing the distinctive thulid head; at this point the thulid gains its second HD. Each year thereafter, the thulid gains another HD, until the maximum of eight is reached. Only a few thulids have the capacity to advance beyond 8 HD.

If the victim is rescued from the thulids but the egg is not slain, the development will be exactly as given. When the victim awakens as a thulid, it will instinctively realize that it is not among its own kind and feign amnesia or other illness to avoid discovery until it can find its way underground and attempt to find its "people." It is aided in this by its telepathy, which can be used to scan for other thulids within a 5 mile radius.
