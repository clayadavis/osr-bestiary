.. title: Skeleton, Crimson Bones
.. slug: skeleton-crimson-bones
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Skeleton, Crimson Bones
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 13 (see below)    |
+-----------------+-------------------+
| Hit Dice:       | 2*                |
+-----------------+-------------------+
| No. of Attacks: | 1 punch or weapon |
+-----------------+-------------------+
| Damage:         | 1d6 or by weapon  |
+-----------------+-------------------+
| Movement:       | 50'               |
+-----------------+-------------------+
| No. Appearing:  | 2d4               |
+-----------------+-------------------+
| Save As:        | Fighter: 2        |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 100               |
+-----------------+-------------------+

A **Crimson Bones** is a special type of undead, created through a combination of alchemy and necromancy. It appears as a normal skeleton in all regards except it is blood-red in color. It obeys the will of its creator or greater undead beings. Each time a crimson bones is reduced to 0 hp it is destroyed as normal; however, 1d4 rounds later it will rise again with half its previous hp allotment (round up). It will continue to rise when struck down, unless targeted by a **bless** spell, doused with holy water, or otherwise subjected to holy powers as determined by the GM. A crimson bones will otherwise regenerate 1 hp per day.

Like an ordinary skeleton, it only takes half damage from edged weapons, and only a single point from arrows, bolts, or sling stones (plus any magical bonus). As with all undead, it can be Turned by a Cleric (as a zombie), and any destroyed in this way will not return. It is immune to **sleep**, **charm**, and **hold** spells. Being mindless, no form of mind reading is of any use against it. A crimson bones never fails morale and always fights until destroyed.
