.. title: Lerini
.. slug: lerini
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Lerini
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 13 (11)                       |
+-----------------+-------------------------------+
| Hit Dice:       | 1                             |
+-----------------+-------------------------------+
| No. of Attacks: | 1 bite or weapon              |
+-----------------+-------------------------------+
| Damage:         | 1d6 or by weapon              |
+-----------------+-------------------------------+
| Movement:       | 20' (unarmored 40'), Swim 30' |
+-----------------+-------------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 6d6       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1                    |
+-----------------+-------------------------------+
| Morale:         | 8                             |
+-----------------+-------------------------------+
| Treasure Type:  | D                             |
+-----------------+-------------------------------+
| XP:             | 25                            |
+-----------------+-------------------------------+

A **Lerini** is a lizard-like humanoid dwelling in swamps and warm, wet forests. It stands 4 to 5 feet tall, weighs 60 to 100 pounds, and has a thick, muscular tail measuring 2 to 3 feet in length. Its skin is covered with soft scales of green, bluish-green or yellowish-green color. Its eyes are large and its hair is thick and oily. A lerini tends to wear loosely-fitting robes or gowns that don't interfere with its tail's mobility.

A lerini is an avid swimmer, and can hold its breath up to 10 minutes. A lerini tends to go fishing or hunting in small, fast river-boats. It has its own tongue, but many also speak the language of lizard-men and sometimes other swamp-dwelling beings. A lerini is not necessarily aggressive, but will fight to defend itself.
