.. title: Will-o’-Wisp
.. slug: will-o-wisp
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Will-o’-Wisp
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 29          |
+-----------------+-------------+
| Hit Dice:       | 9** (AB +8) |
+-----------------+-------------+
| No. of Attacks: | 1 shock     |
+-----------------+-------------+
| Damage:         | 2d8 shock   |
+-----------------+-------------+
| Movement:       | Fly 50'     |
+-----------------+-------------+
| No. Appearing:  | 1d4         |
+-----------------+-------------+
| Save As:        | Fighter: 9  |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | U           |
+-----------------+-------------+
| XP:             | 1,225       |
+-----------------+-------------+

A **Will-o’-Wisp** is a faintly-glowing sphere of light that is yellow or white. It is easily mistaken for a lantern, especially in the foggy marshes and swamps where it typically resides. A will-o’-wisp’s body is a globe of spongy material about 1 foot across and weighs about 3 pounds. Its body sheds as much light as a torch. It has no vocal apparatus, but can vibrate to create a voice with a ghostly sound.

A will-o’-wisp usually avoids combat. When it is forced to fight, it emits small electrical shocks. A will-o’-wisp is immune to all spells except **magic** **missile**. A startled or frightened will-o’-wisp can extinguish its glow, effectively becoming invisible.
