.. title: Necrotic Ooze 
.. slug: necrotic-ooze
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Necrotic Ooze 
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 13                      |
+-----------------+-------------------------+
| Hit Dice:       | 3                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 slam                  |
+-----------------+-------------------------+
| Damage:         | 1d6 slam                |
+-----------------+-------------------------+
| Movement:       | 10'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 11                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 145                     |
+-----------------+-------------------------+

A **Necrotic Ooze** is an undead slime creature that resembles nothing more than a sickly mass of sticky, oozing yellow-white puss with pustules of running clear liquid on its exterior. It attacks with a tentacle-like pseudopod.

As with all undead, it can be Turned by a Cleric (as a wight), and is immune to **sleep, charm**, or **hold** spells. As it is mindless, mind reading is useless. It is likewise immune to disease and poison.

The GM should keep track of who is struck by one; after a fight is over, each stricken victim must save vs. Poison; if this save fails, the victim will suffer a rotting disease that deals 1d4 points of damage per day unless cured by **cure** **disease** (normal healing has no effect). If slain by the rotting disease, the victim will quickly turn into a necrotic ooze.
