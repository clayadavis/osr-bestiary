.. title: Ape, Chimpanzee
.. slug: ape-chimpanzee
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Chimpanzee
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 13            |
+-----------------+---------------+
| Hit Dice:       | 1+1           |
+-----------------+---------------+
| No. of Attacks: | 2 fists       |
+-----------------+---------------+
| Damage:         | 1d4 fist      |
+-----------------+---------------+
| Movement:       | 50'           |
+-----------------+---------------+
| No. Appearing:  | 1d4, Wild 2d8 |
+-----------------+---------------+
| Save As:        | Fighter: 1    |
+-----------------+---------------+
| Morale:         | 8             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 25            |
+-----------------+---------------+

Though short, a **Chimpanzee** can weigh as much as an adult human, but will be much stronger. While they are generally herbivorous, they will eat fresh meat when it's available, even hunting and killing small animals. Individuals are rarely aggressive, but they become bolder in groups; dominant older male chimps can be quite violent.
