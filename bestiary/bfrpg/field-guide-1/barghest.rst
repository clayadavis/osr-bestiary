.. title: Barghest*
.. slug: barghest
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Barghest*
.. type: text
.. image:

+-----------------+---------------------------------------+
| Armor Class:    | 16 †                                  |
+-----------------+---------------------------------------+
| Hit Dice:       | 6+3*                                  |
+-----------------+---------------------------------------+
| No. of Attacks: | 2 claws (humanoid) or 1 bite (dog)    |
+-----------------+---------------------------------------+
| Damage:         | 1d6 claw (humanoid) or 2d4 bite (dog) |
+-----------------+---------------------------------------+
| Movement:       | 40' (humanoid) or 60' (dog)           |
+-----------------+---------------------------------------+
| No. Appearing:  | 1d6, Wild 1d8                         |
+-----------------+---------------------------------------+
| Save As:        | Fighter: 6                            |
+-----------------+---------------------------------------+
| Morale:         | 10                                    |
+-----------------+---------------------------------------+
| Treasure Type:  | D                                     |
+-----------------+---------------------------------------+
| XP:             | 555                                   |
+-----------------+---------------------------------------+

A **Barghest** is an evil shape-changing fiend that hungers for the souls of mortals. A barghest may appear as a huge demonic black dog, or in a humanoid form nearly seven feet tall, resembling a wingless gargoyle.

A barghest never uses weapons, even in its humanoid form, preferring to feel the blood of its enemies run down its claws. It is tenacious; if a barghest fails its morale check and flees, it will return in 1d6 turns to attack again.

Anyone who meets the gaze of a barghest will feel the heat of the monster's stare; such characters must save vs. Paralysis or be paralyzed in terror for 1d6+1 turns (or until the barghest is slain). A character is deemed to have met the gaze of the barghest if he or she faces it in combat, or if the character is surprised by the monster. Fighting a barghest with gaze averted results in a penalty of -4 on all attack rolls. Those who succeed at the saving throw are immune to the monster's gaze for the remainder of the combat (at least one full turn at the minimum).

Although it is not undead, a barghest is inherently unholy and can be Turned by Clerics (as a wight). They can only be harmed by silver or magical weapons.

A barghest generally speaks Common as well as the languages of infernals, goblins, hobgoblins, and bugbears, and can communicate with wolves. One can sometimes be found ruling over goblins or hobgoblins, but most commonly a barghest haunts a lonely stretch of road, preying on travelers.
