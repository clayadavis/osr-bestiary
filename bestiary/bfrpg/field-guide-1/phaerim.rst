.. title: Phaerim
.. slug: phaerim
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Phaerim
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 15                       |
+-----------------+--------------------------+
| Hit Dice:       | 1d6 hp (1 HD)            |
+-----------------+--------------------------+
| No. of Attacks: | 1 weapon                 |
+-----------------+--------------------------+
| Damage:         | By weapon                |
+-----------------+--------------------------+
| Movement:       | 30' Fly 60'              |
+-----------------+--------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6 |
+-----------------+--------------------------+
| Save As:        | Fighter: 1 (Elf bonuses) |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | D                        |
+-----------------+--------------------------+
| XP:             | 25                       |
+-----------------+--------------------------+

The beautiful **Phaerim** is related to fey such as booka or pixies. The phaerim appear to be a smaller elf-like folk, except that each has a pair of wings similar to dragonflies or butterflies. Most phaerim encountered are female. A phaerim stands no taller than the average halfling (3 feet) but has a slight build, seldom being heavier than 40 pounds. Phaerim speak their own language and Elvish, and most know one or more fey languages common to dryads or pixies. Adventuring phaerim usually know Common as well.

Phaerim are normally quite reclusive. Similar to halflings, one can hide very effectively, being 90% undetectable so long as it remains still in a forested environment. Even indoors one is able to hide with 70% effectiveness.

A phaerim normally walks but can fly if unencumbered. One can fly for up to 10 rounds, but must remain grounded an equivalent amount of time after any flight. A lightly-encumbered phaerim can fly up to 5 rounds but must rest for 20 rounds. A phaerim takes half damage from falls due to its reduced weight and wings.
