.. title: Wolf, Giant (Winter & Worg)
.. slug: wolf-giant-winter-worg
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Wolf, Giant (Winter & Worg)
.. type: text
.. image:

+-----------------+----------------------+----------------+
|                 | Winter               | Worg           |
+=================+======================+================+
| Armor Class:    | 15                   | 14             |
+-----------------+----------------------+----------------+
| Hit Dice:       | 6*                   | 4              |
+-----------------+----------------------+----------------+
| No. of Attacks: | 1 bite or breath     | 1 bite         |
+-----------------+----------------------+----------------+
| Damage:         | 1d6 bite, 4d6 breath | 1d6 bite       |
+-----------------+----------------------+----------------+
| Movement:       | 50'                  | 50'            |
+-----------------+----------------------+----------------+
| No. Appearing:  | 1d4, Lair 1d6        | 1d4, Pack 1d6  |
+-----------------+----------------------+----------------+
| Save As:        | Fighter: 6           | Fighter: 4     |
+-----------------+----------------------+----------------+
| Morale:         | 7                    | 7              |
+-----------------+----------------------+----------------+
| Treasure Type:  | U in lair only       | U in lair only |
+-----------------+----------------------+----------------+
| XP:             | 555                  | 240            |
+-----------------+----------------------+----------------+

A **Winter Wolf** is a dangerous predator that grows about 8 feet long, stands about 4-1/2 feet at the shoulder, and weighs about 450 pounds. This wolf typically hunts in packs. Its size and formidable breath weapon allows it to hunt and kill creatures much larger than itself. A pack usually works to circle and knock down its prey.

The breath weapon of a winter wolf is a 15 foot cone that can be used once every 1d4 rounds for 4d6 cold damage; a save vs. Dragon Breath for half damage applies. The bite of a winter wolf also deals an extra 1d6 points of cold damage. An individual hit by a winter wolf's bite must save vs. Death Ray or be knocked to the ground prone. A winter wolf is immune to cold effects.

A **Worg** is a thoroughly evil creature with gray or black fur. It is 5 feet long and stands 3 feet tall at the shoulder, and weighs 300 pounds. More intelligent than its smaller cousin, a worg speaks its own language. Some can also speak Common and goblin.

Mated pairs or packs work together to bring down large game, while a lone worg usually chases down creatures smaller than itself. Both often use hit-and-run tactics to exhaust their quarry. A worg that hits with a bite attack can attempt to trip the opponent as a free action. The target of the trip attack must save vs. Death Ray or fall to the ground.
