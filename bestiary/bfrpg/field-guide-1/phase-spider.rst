.. title: Phase Spider
.. slug: phase-spider
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Phase Spider
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 15                |
+-----------------+-------------------+
| Hit Dice:       | 9+1 (AB +8)       |
+-----------------+-------------------+
| No. of Attacks: | 1 bite + poison   |
+-----------------+-------------------+
| Damage:         | 1d6 bite + poison |
+-----------------+-------------------+
| Movement:       | 40'               |
+-----------------+-------------------+
| No. Appearing:  | 1d4               |
+-----------------+-------------------+
| Save As:        | Fighter: 9        |
+-----------------+-------------------+
| Morale:         | 7                 |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 1,075             |
+-----------------+-------------------+

A **Phase Spider** is an aggressive predator that can shift quickly from an intangible state to attack opponents. When intangible, the phase spider is invisible, insubstantial, and capable of moving in any direction (even up or down), albeit at half-normal speed. As an intangible creature, it can move through solid objects, including living creatures. It can see and hear on the material plane. Sight and hearing on the material plane are limited to 60 feet. A typical phase spider’s body is 8 feet long and weighs about 700 pounds.

Once one locates its prey, it shifts to a tangible state to attack, attempting to catch its victim off-guard (+2 to hit). The phase spider automatically retreats back to intangible state at the beginning of the next round, before initiative is rolled.
