.. title: Cockroach, Giant Ghoul
.. slug: cockroach-giant-ghoul
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Cockroach, Giant Ghoul
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 16                           |
+-----------------+------------------------------+
| Hit Dice:       | 2**                          |
+-----------------+------------------------------+
| No. of Attacks: | 1 bite                       |
+-----------------+------------------------------+
| Damage:         | 1d6 bite +paralysis +disease |
+-----------------+------------------------------+
| Movement:       | 50'                          |
+-----------------+------------------------------+
| No. Appearing:  | 2d6                          |
+-----------------+------------------------------+
| Save As:        | Fighter: 2                   |
+-----------------+------------------------------+
| Morale:         | 12                           |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 125                          |
+-----------------+------------------------------+

Animated through the use of foul magics, a **Giant Ghoul Cockroach** is a ravenous monster, seeking to devour all flesh. Those bitten by this monstrosity must save vs. Paralysis or be paralyzed for 2d8 turns; elves are immune to this effect, just as with the paralysis of the ordinary ghoul. In addition to paralysis, the giant ghoul cockroach's bite may carry disease, much like a giant rat's bite. Any successful bite has a 5% chance of causing a disease.

A character who suffers one or more ghoul cockroach bites where the die roll indicates disease will sicken in 3d6 hours. The infected character will lose one point of Constitution per hour; after losing each point, the character is allowed a save vs. Death Ray (adjusted by the current Constitution bonus or penalty) to break the fever and end the disease. Any character reduced to zero Constitution is dead (see Constitution Point Losses in the **Encounter** section of the Basic Fantasy RPG Core Rules for details on regaining lost Constitution).

As with all undead, it can be Turned by a Cleric (as a ghoul), and is immune to **sleep**, **charm**, or **hold** spells. As it is mindless, no form of mind reading is of any use against it. A giant ghoul cockroach never fails morale, and always fights until destroyed.
