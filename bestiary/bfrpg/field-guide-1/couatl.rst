.. title: Couatl
.. slug: couatl
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Couatl
.. type: text
.. image:

+-----------------+---------------------------------------+
| Armor Class:    | 16                                    |
+-----------------+---------------------------------------+
| Hit Dice:       | 9+** (AB +8)                          |
+-----------------+---------------------------------------+
| No. of Attacks: | 1 bite / 1 constrict or spells/powers |
+-----------------+---------------------------------------+
| Damage:         | 1d3 bite+poison, 2d4 constriction     |
+-----------------+---------------------------------------+
| Movement:       | 20' Fly 60'                           |
+-----------------+---------------------------------------+
| No. Appearing:  | 1d2 Wild, Lair 1d6                    |
+-----------------+---------------------------------------+
| Save As:        | Fighter: 9+                           |
+-----------------+---------------------------------------+
| Morale:         | 12                                    |
+-----------------+---------------------------------------+
| Treasure Type:  | B, I                                  |
+-----------------+---------------------------------------+
| XP:             | 1,225                                 |
+-----------------+---------------------------------------+

The powerful and legendary **Couatl** is a large serpent with a pair of feathered wings; since it is a shape-changer, one rarely sees this form. They can speak Common, communicate freely with reptiles and avians, or may use telepathic communication with intelligent creatures. A couatl has keen senses, including paranormal, which gives it the equivalent of Darkvision with a 90 foot range. A couatl is a benevolent force of goodness, and is rarely aggressive unless first attacked.

When pressed into direct physical combat, a couatl will bite for 1d3 points of damage plus a deadly poison (save vs. Poison or die instantly). In addition, the couatl wraps about its foe, causing 2d4 points of damage from constriction each round. However, a couatl prefers to attack from the air, using spells or other powers.

A couatl casts spells as either a Magic-user (40%), Cleric (40%), or sometimes as both (20%), equivalent to their hit dice (9th level). In addition, any couatl can, at will, cast **detect** **evil**, **detect** **invisibility**, **detect** **magic**, **ESP**, and **read** **languages**. A couatl can also become **invisible** and insubstantial (ethereal). In this form, it moves at half speed in any direction, and can move through solid objects freely. A couatl can **teleport** twice per day.

A couatl can **polymorph** itself freely, and will not hesitate to change into another, more effective form in combat.
