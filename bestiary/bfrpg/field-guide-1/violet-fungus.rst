.. title: Violet Fungus
.. slug: violet-fungus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Violet Fungus
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 13                    |
+-----------------+-----------------------+
| Hit Dice:       | 2*                    |
+-----------------+-----------------------+
| No. of Attacks: | 1 tentacle            |
+-----------------+-----------------------+
| Damage:         | 1d6 tentacle + poison |
+-----------------+-----------------------+
| Movement:       | 10'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1d6                   |
+-----------------+-----------------------+
| Save As:        | Fighter: 2            |
+-----------------+-----------------------+
| Morale:         | 12                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 100                   |
+-----------------+-----------------------+

A **Violet Fungus** resembles a shrieker, and is often found growing among them. Its coloration ranges from purple to gray. In combat, a violet fungus flails about with its tentacles at any living creatures that come within its reach. The attacks of the fungus is poisonous; any creature hit must save vs. Poison or lose one point of Constitution. Any character reduced to zero Constitution dies. Constitution points lost to this poison will be regained at a rate of one per day.
