.. title: Rousirl
.. slug: rousirl
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Rousirl
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 2          |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 2d6 bite   |
+-----------------+------------+
| Movement:       | 60'        |
+-----------------+------------+
| No. Appearing:  | 1d6        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 6          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 75         |
+-----------------+------------+

The **Rousirl** is a massive rodent-like creature. Its stout body stands nearly shoulder height to most humans. Like rodents, it has large front teeth and powerful jaws that it uses for biting through wood and defense.
