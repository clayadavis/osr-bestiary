.. title: Infernal, Vega*
.. slug: infernal-vega
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Infernal, Vega*
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 17 ‡                            |
+-----------------+---------------------------------+
| Hit Dice:       | 9** (AB +8)                     |
+-----------------+---------------------------------+
| No. of Attacks: | 1 weapon or whip                |
+-----------------+---------------------------------+
| Damage:         | Weapon + 2, 2d6 Whip or Special |
+-----------------+---------------------------------+
| Movement:       | 60' Fly 150'                    |
+-----------------+---------------------------------+
| No. Appearing:  | 1                               |
+-----------------+---------------------------------+
| Save As:        | Fighter: 9                      |
+-----------------+---------------------------------+
| Morale:         | 10                              |
+-----------------+---------------------------------+
| Treasure Type:  | A                               |
+-----------------+---------------------------------+
| XP:             | 1,225                           |
+-----------------+---------------------------------+

A **Vega** is an extremely powerful infernal that resembles a winged humanoid figure made of fire and darkness. It is among the most dishonorable and cruel of all creatures. A vega is extremely skilled with its whip and on a successful hit can choose to pull a creature close enough to itself that the heat radiating from its body deals 3d6 points of damage. A vega is only affected by magical weapons and spells of 3rd level or greater, and take only half damage from fire, lightning, and cold. A vega can cast **cause fear**, **darkness**, **detect magic**, **detect invisibility**, **dispel magic**, and **telekinesis** at will.
