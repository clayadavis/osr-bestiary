.. title: Jack O'Lantern
.. slug: jack-o-lantern
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Jack O'Lantern
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 15            |
+-----------------+---------------+
| Hit Dice:       | 3*            |
+-----------------+---------------+
| No. of Attacks: | 1 breath      |
+-----------------+---------------+
| Damage:         | 2d6 fire      |
+-----------------+---------------+
| Movement:       | 20' Fly       |
+-----------------+---------------+
| No. Appearing:  | 1d6           |
+-----------------+---------------+
| Save As:        | Magic-User: 3 |
+-----------------+---------------+
| Morale:         | 9             |
+-----------------+---------------+
| Treasure Type:  | U             |
+-----------------+---------------+
| XP:             | 175           |
+-----------------+---------------+

A **Jack O’Lanterns** is a strange sentient pumpkin (or other such gourds or squash) with a lit candle inside it. That it was created by a mad wizard is obvious. Each individual jack o'lantern has a distinctive personality that is generally mirrored by the expression carved into its face. They are always encountered within a mile or two of the pumpkin patch where they originally grew. When a jack o'lantern is at rest or otherwise inert, it is impossible to tell from a normal fruit, as each can close its carved eyes, mouth, and other apertures and appear entirely ordinary.

A jack o’lantern can project fire from its openings; this breath weapon is in the form of a cone five feet wide at the base with a range of five feet in front of the creature. Anyone caught in the blaze suffers 2d6 points of fire damage; a save vs. Dragon Breath allows the victim to suffer only half damage. Furthermore, they can **fly** (as the spell, at a rate of 20' per round) at will.

This monster is a construct similar to a golem, and thus is immune to poison, fire, **charm**, and **sleep**. It takes double damage from ice or cold. Water or wind-based attacks may snuff the monster's candle, if it fails a saving throw vs. Death Ray. If a jack o'lantern's candle is snuffed, it becomes instantly dormant; re-lighting the candle will restore it to life.
