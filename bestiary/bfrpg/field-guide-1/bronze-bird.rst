.. title: Bronze Bird
.. slug: bronze-bird
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Bronze Bird
.. type: text
.. image:

+-----------------+-----------------------------------+
| Armor Class:    | 16                                |
+-----------------+-----------------------------------+
| Hit Dice:       | 1                                 |
+-----------------+-----------------------------------+
| No. of Attacks: | 1 beak/1 wings or 1 feather throw |
+-----------------+-----------------------------------+
| Damage:         | 1d4 beak, 1d6 wing or 1d4 throw   |
+-----------------+-----------------------------------+
| Movement:       | 30' Fly 120' Swim 30'             |
+-----------------+-----------------------------------+
| No. Appearing:  | Wild 1d10, Lair 10d10             |
+-----------------+-----------------------------------+
| Save As:        | Fighter: 1                        |
+-----------------+-----------------------------------+
| Morale:         | 7                                 |
+-----------------+-----------------------------------+
| Treasure Type:  | None                              |
+-----------------+-----------------------------------+
| XP:             | 25                                |
+-----------------+-----------------------------------+

A **Bronze Bird** resembles a crane or similar water fowl. It dwells in regions of extreme heat such as volcanic lake shores. Each feather shines like polished metal. Its feathers, beaks, and other exposed areas contain significant amounts of magical metals. The metal does not hinder it, and the bronze bird can move, fly, and otherwise behave normally.

A bronze bird has a high AC due to is metal feathering. In combat, the bronze bird attacks with its beak for 1d4 points of damage, and flailing its razor sharp wings at its opponent for 1d6 points of damage collectively. Alternatively, each bronze bird can throw a dagger-like feather up to 30 feet from its wings, dealing 1d4 points of damage; in flight the range is 90 feet if thrown down from above. It can only throw 2 such feather-daggers (one from each wing); the feathers regrow in 1d8 days. A bronze bird is immune to normal fire. It saves against very hot or magical fire at +1 and takes 1 less hit point of damage per die.

Once removed from the bird, the feathers become completely non-magical, but may fetch a few coins due to their intricate metalwork.
