.. title: Vermen
.. slug: vermen
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Vermen
.. type: text
.. image:

+----------------+-----------------------------+
| Armor Class:   | 16 (13)                     |
+----------------+-----------------------------+
| Hit Dice:      | 1+1                         |
+----------------+-----------------------------+
| No of Attacks: | 1 bite or 1 weapon          |
+----------------+-----------------------------+
| Damage:        | 1d3 bite or by weapon       |
+----------------+-----------------------------+
| Movement:      | 30'                         |
+----------------+-----------------------------+
| No. Appearing: | 1d6, Wild 2d10, Lair 2d6X10 |
+----------------+-----------------------------+
| Save As:       | Fighter: 1                  |
+----------------+-----------------------------+
| Morale:        | 5 (see below)               |
+----------------+-----------------------------+
| Treasure Type: | P each, E in lair           |
+----------------+-----------------------------+
| XP:            | 25                          |
+----------------+-----------------------------+

A **Vermen** is a medium-sized furred humanoid with a rodent-like head and tail. It has a long torso and limbs. It tends to slouch forward when walking upright, giving it a hunched appearance. A vermen’s fur varies in color, but all have red eyes. It wears clothing and armor that is soiled and patched together.

Individually or in small groups, these creatures are cowardly, but in larger groups they become much braver; any group of 16 or more will have a morale of 8. Any group of 20 or more will have a leader of at least 4 HD, and such a group will have a morale of 10.
