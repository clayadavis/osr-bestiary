.. title: Red Cap
.. slug: red-cap
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Red Cap
.. type: text
.. image:

+-----------------+-----------+
| Armor Class:    | 14 (14)   |
+-----------------+-----------+
| Hit Dice:       | 3*        |
+-----------------+-----------+
| No. of Attacks: | 1 weapon  |
+-----------------+-----------+
| Damage:         | By weapon |
+-----------------+-----------+
| Movement:       | 60'       |
+-----------------+-----------+
| No. Appearing:  | 1d4       |
+-----------------+-----------+
| Save As:        | Thief: 6  |
+-----------------+-----------+
| Morale:         | 8         |
+-----------------+-----------+
| Treasure Type:  | D         |
+-----------------+-----------+
| XP:             | 175       |
+-----------------+-----------+

A **Red Cap** is a murderous fey that preys upon travelers seeking refuge in ruins or abandoned castles. Distantly related to both goblins and fairies, it appears as a small ugly man with unkempt hair, red eyes, and wickedly crooked teeth. A red cap wears small iron shoes and a blood-red hat upon its head; its shoes magically enhance its speed, so that if somehow removed the red cap's movement rate is halved. These shoes are useless to anyone other than a red cap. A red cap prefers to attack only helpless targets, usually with a knife or other bladed weapon that will spill blood.

Within its lair a red cap has the abilities of a 6th-level Thief, including the sneak attack ability. Outside its lair a red cap's Thief abilities are only 3rd level.

A red cap can cast **sleep** once daily, which it uses to incapacitate those it intends to murder. When hard pressed, it will flee and hide, counting on its speed to evade any pursuers.

The hat of a red cap must be soaked in blood regularly, lest the being wither and fade away. This hat, even if inert for years, can summon a red cap if soaked in blood anew. Only total destruction of the hat guarantees a true end.
