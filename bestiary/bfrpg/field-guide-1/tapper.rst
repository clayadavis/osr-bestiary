.. title: Tapper*
.. slug: tapper
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Tapper*
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 15 †                       |
+-----------------+----------------------------+
| Hit Dice:       | 3                          |
+-----------------+----------------------------+
| No. of Attacks: | 1 punch or mining pick     |
+-----------------+----------------------------+
| Damage:         | 1d4 punch, 1d6 mining pick |
+-----------------+----------------------------+
| Movement:       | 30'                        |
+-----------------+----------------------------+
| No. Appearing:  | 1d6                        |
+-----------------+----------------------------+
| Save As:        | Fighter: 3                 |
+-----------------+----------------------------+
| Morale:         | 12                         |
+-----------------+----------------------------+
| Treasure Type:  | Special                    |
+-----------------+----------------------------+
| XP:             | 145                        |
+-----------------+----------------------------+

According to legend, sometimes dwarven miners who have been suffering a "dry spell" will finally find a rich mineral vein. A lusty greed will overcome them, and forsaking their health, they work themselves to death. Such dwarves, it is said, will rise from the dead as **tappers**.

Tappers are undead monsters which resemble desiccated zombies. They continue to wander their mine and the surrounding area, striking at the rock here and there. This tapping noise is unsettling as it echoes throughout the tunnels.

Tappers jealously guard their claim, attacking with mining picks or similar implements. As with all undead, they can be Turned by a Cleric (as a mummy), and are immune to **sleep, charm** or** hold** spells. Silver or magical weapons are needed to strike a tapper. A dwarf killed by a tapper will rise as one unless **bless** is cast upon its body.
