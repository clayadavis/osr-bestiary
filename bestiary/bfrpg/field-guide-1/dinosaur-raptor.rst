.. title: Dinosaur, Raptor
.. slug: dinosaur-raptor
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Dinosaur, Raptor
.. type: text
.. image:

+-----------------+-------------------------+-------------------------+
|                 | Deinonychus             | Velociraptor            |
+=================+=========================+=========================+
| Armor Class:    | 15                      | 15                      |
+-----------------+-------------------------+-------------------------+
| Hit Dice:       | 3                       | 1                       |
+-----------------+-------------------------+-------------------------+
| No. of Attacks: | 2 claws or 1 bite       | 1 claw or 1 bite        |
+-----------------+-------------------------+-------------------------+
| Damage:         | 1d6 claw, 1d8 bite      | 1d4 claw, 1d4 bite      |
+-----------------+-------------------------+-------------------------+
| Movement:       | 50'                     | 80'                     |
+-----------------+-------------------------+-------------------------+
| No. Appearing:  | 1d3, Wild 2d3, Lair 2d6 | 1d4, Wild 2d4, Lair 2d8 |
+-----------------+-------------------------+-------------------------+
| Save As:        | Fighter: 3              | Fighter: 1              |
+-----------------+-------------------------+-------------------------+
| Morale:         | 8                       | 8                       |
+-----------------+-------------------------+-------------------------+
| Treasure Type:  | None                    | None                    |
+-----------------+-------------------------+-------------------------+
| XP:             | 145                     | 25                      |
+-----------------+-------------------------+-------------------------+

A **Deinonychus** is a medium-sized feathered dinosaur, weighting approximately 150 pounds and reaching about 11 feet in length from nose to tail. It is an avid predator and a skilled pack-hunter. It attacks by either biting or leaping and using its formidable claws. Against large prey it will bite and hold on in order to use its claws for automatic damage each round; however while holding its AC drops to 11.

The **Velociraptor** is a small feathered dinosaur, weighing about 30 pounds and similar in size to a turkey. It acts and attacks in a similar manner to the deinonychus.
