.. title: Spider, Giant Diving Bell
.. slug: spider-giant-diving-bell
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Spider, Giant Diving Bell
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 15              |
+-----------------+-----------------+
| Hit Dice:       | 4*              |
+-----------------+-----------------+
| No. of Attacks: | 1 bite + poison |
+-----------------+-----------------+
| Damage:         | 1d8 bite        |
+-----------------+-----------------+
| Movement:       | 40' Swim 30'    |
+-----------------+-----------------+
| No. Appearing:  | 1d3, Lair 1     |
+-----------------+-----------------+
| Save As:        | Fighter: 4      |
+-----------------+-----------------+
| Morale:         | 8               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 280             |
+-----------------+-----------------+

A **Giant Diving Bell Spider** carries a supply of air with it around the hairs of its body and crafts an underwater lair filled with air. It is otherwise similar to other giant spiders.
