.. title: Frost Worm
.. slug: frost-worm
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Frost Worm
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 18                  |
+-----------------+---------------------+
| Hit Dice:       | 16** (AB +12)       |
+-----------------+---------------------+
| No. of Attacks: | 1 bite + cold       |
+-----------------+---------------------+
| Damage:         | 2d8 bite + 1d8 cold |
+-----------------+---------------------+
| Movement:       | 30'                 |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Fighter: 16         |
+-----------------+---------------------+
| Morale:         | 10                  |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 3520                |
+-----------------+---------------------+

A **Frost Worm** grows about 40 feet long, has two huge mandibles, and a strange orifice on its head that it uses to create a trilling sound during combat. It can burrow through ice and frozen earth but not stone. When moving through such hard materials, it leaves behind a usable tunnel about 5 feet in diameter.

A frost worm lurks under the snow, waiting for prey to come near. It begins an attack with the trill, which forces its prey to stand motionless, and then sets upon helpless prey with its bite. This trilling affects all creatures other than frost worms within a 100 foot radius. Creatures must save vs. Paralysis or be stunned for as long as the worm trills and for 1d4 rounds thereafter. However, if the victim is attacked or violently shaken, another saving throw is allowed. Once a creature has resisted or broken the effect, it cannot be affected again by that same frost worm’s trill for 24 hours.

The body of a frost worm generates intense cold, causing opponents to take an extra 1d8 points of cold damage every time the creature succeeds on a bite attack. Any creature attacking a frost worm unarmed or with non-magical weapons suffers this same cold damage each time one of its attacks hits.

A frost worm can breathe a 30 foot cone of frost, once per hour, for 15d6 cold damage. Those struck may save vs. Dragon Breath for half damage. Opponents held motionless by the frost worm’s trill cannot save.

When killed, a frost worm turns to ice and shatters in an explosion, dealing 20d6 points of damage to everything within 100 feet. A victim may save vs. Dragon Breath for half damage.
