.. title: Octopus Fungi
.. slug: octopus-fungi
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Octopus Fungi
.. type: text
.. image:

+-----------------+-----------------------------------------+
| Armor Class:    | 15                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 6                                       |
+-----------------+-----------------------------------------+
| No. of Attacks: | 1 tentacle or 1 tentacle + bite (at +4) |
+-----------------+-----------------------------------------+
| Damage:         | 1d6 tentacle, 1d10 bite                 |
+-----------------+-----------------------------------------+
| Movement:       | 0' (immobile)                           |
+-----------------+-----------------------------------------+
| No. Appearing:  | 1d12                                    |
+-----------------+-----------------------------------------+
| Save As:        | Fighter: 6                              |
+-----------------+-----------------------------------------+
| Morale:         | 12                                      |
+-----------------+-----------------------------------------+
| Treasure Type:  | None                                    |
+-----------------+-----------------------------------------+
| XP:             | 500                                     |
+-----------------+-----------------------------------------+

An **Octopus Fungi** appears similar to other giant fungi such as shriekers. If anyone tries to pass through it, it will "unfurl" into a single long tentacle and try to wrap around the victim and drag him or her back to its biting core. A normal attack roll is made; a hit deals 1d6 points of damage from the clawed grasping sucker and a victim must save vs. Dragon Breath, or be drug back to the core the next round. If pulled into the body, two attacks will be made (the tentacle and the mouth) with a +4 bonus each.
