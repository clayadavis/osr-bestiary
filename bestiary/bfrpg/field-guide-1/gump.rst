.. title: Gump
.. slug: gump
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Gump
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 14                          |
+-----------------+-----------------------------+
| Hit Dice:       | 4*                          |
+-----------------+-----------------------------+
| No. of Attacks: | 2 fists or 1 weapon         |
+-----------------+-----------------------------+
| Damage:         | 1d8+3 fists or by weapon +3 |
+-----------------+-----------------------------+
| Movement:       | 30'                         |
+-----------------+-----------------------------+
| No. Appearing:  | 1                           |
+-----------------+-----------------------------+
| Save As:        | Fighter: 4                  |
+-----------------+-----------------------------+
| Morale:         | 10                          |
+-----------------+-----------------------------+
| Treasure Type:  | C                           |
+-----------------+-----------------------------+
| XP:             | 280                         |
+-----------------+-----------------------------+

A **Gump** is a large and blubbery humanoid that stands just over a head taller than a human male, with a broad, jagged toothed grin and small deep-set eyes. Meeting the gaze of a gump is dangerous.

Anyone who meets the gaze of a gump must save vs. Paralysis at +2 or suffer the effects of a **hold** **person** spell for 1d4+1 rounds. Anyone facing a gump in combat is deemed to have met its gaze, as is anyone who is surprised by the monster. Those who attempt to fight a gump while averting their eyes suffer a -4 penalty on attack rolls. It is safe to view a gump's reflection in a mirror or other reflective surface; anyone using a mirror to fight a gump suffers a penalty of -2 to attack. Characters fighting a gump must make the saving throw each round if not taking measures to avoid the monster's gaze.

A gump communicates with an odd jibbering and slobbery voice that is difficult to understand. It can also speak Common and the languages of goblinoid creatures.
