.. title: Anubian
.. slug: anubian
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Anubian
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 14 (11)                       |
+-----------------+-------------------------------+
| Hit Dice:       | 1+1                           |
+-----------------+-------------------------------+
| No. of Attacks: | 1 punch or 1 weapon           |
+-----------------+-------------------------------+
| Damage:         | 1d4 punch or by weapon        |
+-----------------+-------------------------------+
| Movement:       | 40' unencumbered, usually 30' |
+-----------------+-------------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1                    |
+-----------------+-------------------------------+
| Morale:         | 8                             |
+-----------------+-------------------------------+
| Treasure Type:  | Q, R each; D, K in lair       |
+-----------------+-------------------------------+
| XP:             | 25                            |
+-----------------+-------------------------------+

**Anubians** are a noble race of desert dwelling humanoids with heads which resemble jackals. They are usually very distrustful of outsiders, but not normally hostile. They will, however, defend the scarce resources found within desert oasis refuges or lush river valleys. An anubian speaks its own language and writes using a complex system of hieroglyphs; few speak Common.

The statistics given are for standard warriors; one might also encounter additional civilian types who have 1-1 HD, AC 13, and a morale of 7. In addition, for every eight typical warriors, there is a leader type having 3+3 HD (145 XP) who grants a +1 morale bonus to those it commands. Anubians are fervently religious, and in addition to the leader types above, one will also find a priest with the abilities of a 3rd level Cleric for every leader type. When applicable, use hit dice appropriate for their class (for instance, anubian priests use d6 for hit dice).
