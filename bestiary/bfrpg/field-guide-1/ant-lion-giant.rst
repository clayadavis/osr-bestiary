.. title: Ant Lion, Giant
.. slug: ant-lion-giant
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ant Lion, Giant
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 4                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d10 bite               |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d6, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 240                     |
+-----------------+-------------------------+

The **Giant Ant Lion** is a gigantic predatory beetle about the size of a cow. It builds a network of underground tunnels with multiple trapdoors which are about nine feet in diameter above the tunnels. The trap doors are difficult to locate (normal trap detection rules), as they are camouflaged to look like the surrounding materials. When a victim reaches the center, the ground gives way, swirling downward like water down a drain and dragging the victim below. The depth of the drop is generally ten to twenty feet, and normal falling damage is inflicted. Then, of course, the monster will attack its prey.
