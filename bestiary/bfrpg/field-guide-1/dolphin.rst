.. title: Dolphin
.. slug: dolphin
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Dolphin
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 2          |
+-----------------+------------+
| No. of Attacks: | 1 ram      |
+-----------------+------------+
| Damage:         | 2d4 ram    |
+-----------------+------------+
| Movement:       | Swim 120'  |
+-----------------+------------+
| No. Appearing:  | Wild 2d10  |
+-----------------+------------+
| Save As:        | Fighter: 4 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 75         |
+-----------------+------------+

A **Dolphin** is an aquatic mammal that resembles a large fish. It is highly intelligent and usually acts friendly toward humans and humanoid creatures. Because it is a mammal, a dolphin must surface periodically to breathe air, though it can hold its breath for up to an hour of light activity (or two turns of strenuous action). A dolphin is frequently found in the company of mermaids.
