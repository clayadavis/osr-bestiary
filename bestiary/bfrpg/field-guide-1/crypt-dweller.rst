.. title: Crypt Dweller*
.. slug: crypt-dweller
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Crypt Dweller*
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 13 ‡                       |
+-----------------+----------------------------+
| Hit Dice:       | 2*                         |
+-----------------+----------------------------+
| No. of Attacks: | 2 claws or 1 weapon        |
+-----------------+----------------------------+
| Damage:         | 1d4 claw or by weapon type |
+-----------------+----------------------------+
| Movement:       | 60'                        |
+-----------------+----------------------------+
| No. Appearing:  | 1-2                        |
+-----------------+----------------------------+
| Save As:        | Fighter: 2                 |
+-----------------+----------------------------+
| Morale:         | 12                         |
+-----------------+----------------------------+
| Treasure Type:  | None                       |
+-----------------+----------------------------+
| XP:             | 100                        |
+-----------------+----------------------------+

A **Crypt Dweller** is an undead creature improperly buried or placed into a grave that has been desecrated or defiled. It resembles a zombie, and is often mistaken for one. A crypt dweller attacks with clawed hands, or sometimes with a weapon if one was entombed with the creature. Its main defense is that it can only be damaged by magical weapons or spells.

Strikes from normal weapons will only make the crypt dweller pause slightly, making it lose initiative on the following round. Like all undead, it can be Turned by Clerics (as a wight), and are immune to **sleep**, **charm**, and **hold** spells. No form of mind reading or mental contact is of any use against it. A crypt dweller always fights until destroyed.
