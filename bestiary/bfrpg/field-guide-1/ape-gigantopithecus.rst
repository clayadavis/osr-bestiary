.. title: Ape, Gigantopithecus
.. slug: ape-gigantopithecus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Gigantopithecus
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 7                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws / 1 bite        |
+-----------------+-------------------------+
| Damage:         | 1d8 claw, 1d8 bite      |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 7              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 670                     |
+-----------------+-------------------------+

**Gigantopithecus** are prehistoric gorillas, huge and powerful. It is a vegetarian like their lesser kin, but can be as vicious as the carnivorous varieties if provoked.
