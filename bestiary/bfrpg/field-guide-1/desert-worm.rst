.. title: Desert Worm
.. slug: desert-worm
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Desert Worm
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 14                  |
+-----------------+---------------------+
| Hit Dice:       | 4*                  |
+-----------------+---------------------+
| No. of Attacks: | 1 bite              |
+-----------------+---------------------+
| Damage:         | 1d10 bite + special |
+-----------------+---------------------+
| Movement:       | 40’ Burrow 30’      |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Fighter: 3          |
+-----------------+---------------------+
| Morale:         | 12                  |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 280                 |
+-----------------+---------------------+

A **Desert Worm** is a large carnivorous creature, with a round mouth ringed with several rows of sharp teeth. A desert worm has no eyes nor olfactory organs, but it can sense movement on the ground within 20 feet. It spends most of its life burrowing beneath the sands. A desert worm will eventually leave a potential victim alone if it no longer senses any movement within that area. On a natural 20 attack roll, a victim of up to dwarf size will be swallowed whole, suffering 1d6 points of damage per round thereafter. Only one such victim may be swallowed at a time. A swallowed creature can cut its way out by dealing 5 or more points of damage with a small edged weapon such as a dagger.
