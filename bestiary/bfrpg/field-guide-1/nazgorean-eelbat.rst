.. title: Nazgorean, Eelbat
.. slug: nazgorean-eelbat
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Nazgorean, Eelbat
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 14                               |
+-----------------+----------------------------------+
| Hit Dice:       | 1*                               |
+-----------------+----------------------------------+
| No. of Attacks: | 1 bite                           |
+-----------------+----------------------------------+
| Damage:         | 1d6 bite + 1d6/round blood drain |
+-----------------+----------------------------------+
| Movement:       | 5' Fly 70'                       |
+-----------------+----------------------------------+
| No. Appearing:  | 2d6                              |
+-----------------+----------------------------------+
| Save As:        | Fighter: 1                       |
+-----------------+----------------------------------+
| Morale:         | 10                               |
+-----------------+----------------------------------+
| Treasure Type:  | None                             |
+-----------------+----------------------------------+
| XP:             | 37                               |
+-----------------+----------------------------------+

An **Eelbat** looks like an eyeless eel with bat-like wings and gray slimy skin. Despite the lack of visual organs, it can sense its surroundings like a bat and it effectively has Darkvision out to 60 feet. It attacks by biting, and after a successful bite it will hang on and drain an additional 1d6 points of damage every round. An eelbat can only be removed by killing it; any attack on the creature while attached receives an attack bonus of +2, but any attack that misses will hit its victim instead.
