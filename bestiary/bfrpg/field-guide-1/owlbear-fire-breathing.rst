.. title: Owlbear, Fire-breathing
.. slug: owlbear-fire-breathing
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Owlbear, Fire-breathing
.. type: text
.. image:

+-----------------+--------------------------------------+
| Armor Class:    | 18                                   |
+-----------------+--------------------------------------+
| Hit Dice:       | 8*                                   |
+-----------------+--------------------------------------+
| No. of Attacks: | 2 claws/1 bite (1 hug) or 1 breath   |
+-----------------+--------------------------------------+
| Damage:         | 1d8 claw,1d8 bite, 2d8 hug, 4d6 fire |
+-----------------+--------------------------------------+
| Movement:       | 40' Fly 40' (10')                    |
+-----------------+--------------------------------------+
| No. Appearing:  | 1d4, Lair 1d4, Wild 1d4              |
+-----------------+--------------------------------------+
| Save As:        | Fighter: 8                           |
+-----------------+--------------------------------------+
| Morale:         | 9                                    |
+-----------------+--------------------------------------+
| Treasure Type:  | C x 2                                |
+-----------------+--------------------------------------+
| XP:             | 945                                  |
+-----------------+--------------------------------------+

A **Fire-breathing Owlbear** is the larger, more bestial cousin of the owlbear, sharing most of the same physical features. In addition to the large wings allowing flight, its coat tends to be closer to rust in color and its beak a bright orange. A full-grown fire-breathing owlbear stands nearly 10 feet tall and weighs over 2000 pounds.

A fire-breathing owlbear usually relies on its powerful claws and ferocious beak in combat. Like a normal bear, it must hit with both claws to deal hug damage. What makes it most fearsome is its breath attack. Each round, roll to determine which attack form is used (1d6, 1-2 indicates breath of fire). If the beast breathes fire, its victim may save vs. Dragon Breath for half damage. It may use this attack a total of 4 times per combat. After an hour of rest, the breath weapon is usable once again.
