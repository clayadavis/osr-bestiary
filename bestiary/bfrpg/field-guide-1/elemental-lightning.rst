.. title: Elemental, Lightning*
.. slug: elemental-lightning
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Elemental, Lightning*
.. type: text
.. image:

+-----------------+------------+--------------+--------------+
|                 | **Staff**  | **Device**   | **Spell**    |
+=================+============+==============+==============+
| Armor Class:    | 18 ‡       | 20 ‡         | 22 ‡         |
+-----------------+------------+--------------+--------------+
| Hit Dice:       | 8*         | 12* (AB +10) | 16* (AB +12) |
+-----------------+------------+--------------+--------------+
| No. of Attacks: | -- special --                            |
+-----------------+------------+--------------+--------------+
| Damage:         | 1d12       | 2d8          | 3d6          |
+-----------------+------------+--------------+--------------+
| Movement:       | Fly 120'                                 |
+-----------------+------------------------------------------+
| No. Appearing:  | -- special --                            |
+-----------------+------------+--------------+--------------+
| Save As:        | Fighter: 8 | Fighter: 12  | Fighter: 16  |
+-----------------+------------+--------------+--------------+
| Morale:         | -- 10 --                                 |
+-----------------+------------------------------------------+
| Treasure Type:  | -- None --                               |
+-----------------+------------+--------------+--------------+
| XP:             | 945        | 1,975        | 3,385        |
+-----------------+------------+--------------+--------------+

A **Lightning Elemental** resembles dark clouds, lit from within by flashes of lightning. It can magnetically draw metal items towards itself as if using **telekinesis**. It deals an extra 1d8 points of damage to creatures that are in contact with water or metal but not touching solid ground. A lightning elemental takes double damage when attacked by air or wind attacks (including air elementals). A lightning elemental can choose either to strike a single creature or create a mighty thunderclap. If the latter attack is used, all creatures within a 30 foot radius must save vs. Paralysis or be deafened for 1d8 turns.
