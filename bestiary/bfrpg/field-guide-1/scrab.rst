.. title: Scrab
.. slug: scrab
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Scrab
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 18                      |
+-----------------+-------------------------+
| Hit Dice:       | 5*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws                 |
+-----------------+-------------------------+
| Damage:         | 1d8 claw                |
+-----------------+-------------------------+
| Movement:       | 20' Swim 20'            |
+-----------------+-------------------------+
| No. Appearing:  | 1d2, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 405                     |
+-----------------+-------------------------+

The **Scrab** looks like a huge crab with a 6 foot diameter body. The scrab is protected by a natural form of **anti-magic** **shell**. In order to affect the scrab with a spell, the caster must make a normal attack roll; on a hit, the scrab is affected by the spell (but still receives a normal saving throw if the spell allows one). If the attack roll fails, the spell is reflected back onto the caster.
