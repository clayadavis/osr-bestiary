.. title: Ape, Girallon
.. slug: ape-girallon
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Girallon
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 16                    |
+-----------------+-----------------------+
| Hit Dice:       | 7                     |
+-----------------+-----------------------+
| No. of Attacks: | 4 claws / 1 bite      |
+-----------------+-----------------------+
| Damage:         | 1d6 claw, 1d8 bite    |
+-----------------+-----------------------+
| Movement:       | 40'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1, Wild 1d2, Lair 2d4 |
+-----------------+-----------------------+
| Save As:        | Fighter: 7            |
+-----------------+-----------------------+
| Morale:         | 10                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 670                   |
+-----------------+-----------------------+

**Girallons** are the savage, four-armed, magical cousins of the gorilla. An adult girallon is about 8 feet tall, broad-chested, and covered in thick fur. It weighs about 800 pounds. A solitary girallon usually conceals itself, attacking with surprise. When it spots or smells prey, it charges. A girallon picks up prey that is small enough to carry and withdraws, often vanishing into the trees before the victim’s companions can retaliate.

A girallon that hits with two or more claw attacks latches onto the opponent’s body and tears the flesh. This attack automatically deals an extra 2d4 points of damage.
