.. title: Red Slime
.. slug: red-slime
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Red Slime
.. type: text
.. image:

+-----------------+-----------------------------------------------------------------+
| Armor Class:    | 11 to 16                                                        |
+-----------------+-----------------------------------------------------------------+
| Hit Dice:       | 1* to 6*                                                        |
+-----------------+-----------------------------------------------------------------+
| No. of Attacks: | 1 slam                                                          |
+-----------------+-----------------------------------------------------------------+
| Damage:         | 1d4, 1d6, 1d8, 1d10, 1d12, or 2d8 per round by Hit Dice         |
+-----------------+-----------------------------------------------------------------+
| Movement:       | 10'                                                             |
+-----------------+-----------------------------------------------------------------+
| No. Appearing:  | 1                                                               |
+-----------------+-----------------------------------------------------------------+
| Save As:        | Fighter: 1 to 6                                                 |
+-----------------+-----------------------------------------------------------------+
| Morale:         | 12                                                              |
+-----------------+-----------------------------------------------------------------+
| Treasure Type:  | None                                                            |
+-----------------+-----------------------------------------------------------------+
| XP:             | 1* HD 37; 2* HD 100; 3* HD 175;4* HD 280; 5* HD 405 ; 6* HD 555 |
+-----------------+-----------------------------------------------------------------+

Distant relative of ochre jellies and green slimes, the **Red Slime** is an oozing mass that appears to be a red pool. One is easily mistaken for an ochre jelly, save for its color. This slimy creature is notorious for remaining still and hiding in holes and under ledges to surprise victims.

A red slime can extend a pseudopod up to 2 feet per HD. If its attack is successful, the target is ensnared, and the slime will feed off the victim, dealing damage automatically each round. Hit points drained from the victim are added to the red slime's current hit points; every 6 points thus added increases the monster's HD by one, to a maximum of 6 HD. Excess hit points drained after the monster reaches its maximum of 48 are ignored. Note that the damage roll increases as the monster grows in size.

An ensnared victim may attempt to escape by making an open doors check to break free. A red slime can only feed on living creatures, but can still make normal attacks against other monsters. A successful attack against a red slime may cause it to release a victim; the GM should roll 1d20, and if the die roll is equal to or less than the damage done to the red slime, the ensnared victim will be freed.

A red slime can also regenerate 1 hp per round, even if apparently slain. Damage from fire or acid will not be regenerated, and so this is the only way to permanently slay a red slime.
