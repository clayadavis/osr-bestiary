.. title: Linnorm
.. slug: linnorm
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Linnorm
.. type: text
.. image:

+-----------------+----------------------------------------+
| Armor Class:    | 17                                     |
+-----------------+----------------------------------------+
| Hit Dice:       | 7**                                    |
+-----------------+----------------------------------------+
| No. of Attacks: | 1 bite, 2 claws, or breath             |
+-----------------+----------------------------------------+
| Damage:         | 2d8 bite + poison, 1d10 claw or breath |
+-----------------+----------------------------------------+
| Movement:       | 40'                                    |
+-----------------+----------------------------------------+
| No. Appearing:  | 1d2                                    |
+-----------------+----------------------------------------+
| Save As:        | Fighter: 8                             |
+-----------------+----------------------------------------+
| Morale:         | 9                                      |
+-----------------+----------------------------------------+
| Treasure Type:  | E                                      |
+-----------------+----------------------------------------+
| XP:             | 800                                    |
+-----------------+----------------------------------------+

A **Linnorm**, like a wyvern, is distantly related to a dragon. A linnorm resembles a large, horned snake with a pair of clawed forelimbs. A linnorm can breathe a cloud of fetid gas much like a dragon's breath. A linnorm is immune to all poisons.
