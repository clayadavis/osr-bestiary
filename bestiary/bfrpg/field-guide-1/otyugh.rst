.. title: Otyugh
.. slug: otyugh
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Otyugh
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 17                               |
+-----------------+----------------------------------+
| Hit Dice:       | 6*                               |
+-----------------+----------------------------------+
| No. of Attacks: | 2 tentacles/1 bite + special     |
+-----------------+----------------------------------+
| Damage:         | 1d4 tentacle, 1d6 bite + special |
+-----------------+----------------------------------+
| Movement:       | 20'                              |
+-----------------+----------------------------------+
| No. Appearing:  | 1, Lair 1d3+1, Wild 1d2          |
+-----------------+----------------------------------+
| Save As:        | Fighter: 6                       |
+-----------------+----------------------------------+
| Morale:         | 9                                |
+-----------------+----------------------------------+
| Treasure Type:  | U                                |
+-----------------+----------------------------------+
| XP:             | 555                              |
+-----------------+----------------------------------+

An **Otyugh** stands on 4 large legs and has a body 8 feet in diameter, weighing about 500 pounds. Behind its large mouth are 3 tentacles. When so inclined, an otyugh can communicate verbally using the predominate language of the area, usually Common.

An otyugh attacks living creatures if it feels threatened or if it is hungry; otherwise it is content to remain hidden. An otyugh slashes and squeezes opponents with its tentacles, which it also uses to drag prey into its mouth. It deals automatic tentacle damage with a successful attack on every round that its prey is held. The bite of an otyugh is diseased and the recipient of the attack must save vs. Poison or contract **filth fever** (incubation period 1d3 days; 1d3 reduction of Dexterity and 1d3 reduction of reduction of Constitution).
