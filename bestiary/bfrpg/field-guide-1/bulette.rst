.. title: Bulette
.. slug: bulette
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Bulette
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 22                        |
+-----------------+---------------------------+
| Hit Dice:       | 9** (AB +8)               |
+-----------------+---------------------------+
| No. of Attacks: | 1 bite/2 claws or 4 claws |
+-----------------+---------------------------+
| Damage:         | 3d8 bite, 2d6 claw        |
+-----------------+---------------------------+
| Movement:       | 40' Burrow 10'            |
+-----------------+---------------------------+
| No. Appearing:  | 1d2                       |
+-----------------+---------------------------+
| Save As:        | Fighter: 9                |
+-----------------+---------------------------+
| Morale:         | 11                        |
+-----------------+---------------------------+
| Treasure Type:  | None                      |
+-----------------+---------------------------+
| XP:             | 1,225                     |
+-----------------+---------------------------+

The **Bulette** (pronounced Boo-lay) is a terrifying predator that lives only to eat. Almost 10 feet tall and 15 feet long, it resembles something akin to a gigantic armadillo crossed with a snapping turtle. A bulette moves by quickly burrowing through the earth, leaping out to attack those on the surface.

A bulette attacks anything it regards as edible, biting for 3d8 points of damage and clawing for 2d6 points of damage with each fore claw. It can leap 10 feet through the air and attack prey with all four claws but cannot bite with such an attack. If somehow one gets on top of a bulette, the area behind its head plates is only AC 15. It is effectively immune to most types of attacks while burrowing.

A bulette senses vibrations in the earth, and is able to sense positions and numbers of creatures with a range of 60 feet while burrowing.
