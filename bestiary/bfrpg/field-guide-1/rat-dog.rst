.. title: Rat Dog
.. slug: rat-dog
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Rat Dog
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 15              |
+-----------------+-----------------+
| Hit Dice:       | 3               |
+-----------------+-----------------+
| No. of Attacks: | 1 bite          |
+-----------------+-----------------+
| Damage:         | 1d6 bite        |
+-----------------+-----------------+
| Movement:       | 40'             |
+-----------------+-----------------+
| No. Appearing:  | 1d10, Wild 2d20 |
+-----------------+-----------------+
| Save As:        | Fighter: 3      |
+-----------------+-----------------+
| Morale:         | 9               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 145             |
+-----------------+-----------------+

A **Rat Dog** is the size of a large dog, with a rat-like face and long hairless tail. It runs and otherwise moves like a canine, and its front paws have opposable thumbs and are thus able to grasp objects. Despite the name, it is not clear if this creature is a rat, dog, or some sort of magical hybrid.

A rat dog's bite may carry disease, much like a giant rat's bite. A rat dog bite has a 5% chance of causing a disease. Anyone who suffers one or more bites that results in disease will sicken in 3d6 hours. The infected character will lose one point of Constitution per hour; after losing each point, the character is allowed a save vs. Death Ray (adjusted by their current Constitution bonus or penalty) to break the fever and end the disease. Any character reduced to zero Constitution is dead (see Constitution Point Losses in the **Encounter** section of the Basic Fantasy RPG Core Rules for details on regaining lost Constitution).
