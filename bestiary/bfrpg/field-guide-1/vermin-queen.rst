.. title: Vermin Queen
.. slug: vermin-queen
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Vermin Queen
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 13                    |
+-----------------+-----------------------+
| Hit Dice:       | 6**                   |
+-----------------+-----------------------+
| No. of Attacks: | 2 claws or weapon     |
+-----------------+-----------------------+
| Damage:         | 1d6 claw or weapon    |
+-----------------+-----------------------+
| Movement:       | 40' Fly 20'           |
+-----------------+-----------------------+
| No. Appearing:  | 1, Wild 1d3, Lair 1d3 |
+-----------------+-----------------------+
| Save As:        | Magic-User: 6         |
+-----------------+-----------------------+
| Morale:         | 9                     |
+-----------------+-----------------------+
| Treasure Type:  | F                     |
+-----------------+-----------------------+
| XP:             | 610                   |
+-----------------+-----------------------+

A **Vermin Queen** is a swarm of horrid, intelligent black beetles with the ability to assume the guise of a beautiful human or demihuman. It uses this disguise to waylay travelers in order to devour his or her flesh and steal their skin. When a vermin queen eats a living human, demihuman, or humanoid creature, another beetle is born to the swarm. When the swarm gets too big to comfortably fit into a human skin, half of it splits off and becomes a new vermin queen. The mother swarm typically deposits the daughter swarm in the body of its next victim.

A vermin queen can freely change between humanoid and swarm forms once per round. The touch of a vermin queen in either form paralyzes (much like a ghoul) and it never takes more than 1d4 points of damage from weapon attacks. While in swarm form, a vermin queen fights just like an insect swarm.
