.. title: Iron Snapper
.. slug: iron-snapper
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Iron Snapper
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 17                      |
+-----------------+-------------------------+
| Hit Dice:       | 6*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 2d8 bite                |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d2, Wild 1d2, Lair 1d2 |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 555                     |
+-----------------+-------------------------+

An **Iron Snapper** is a large, highly territorial serpent with grey scales and beak, orange underbelly, and thick armored plates on its back. Its bite is strong enough to tear through steel plates; as it must be, for iron is its primary food. Any time an iron snapper successfully bites an opponent with a natural attack roll of 19 or 20, it destroys any armor the opponent is wearing or breaks one object held by the opponent, preferring items made of iron or steel if possible.
