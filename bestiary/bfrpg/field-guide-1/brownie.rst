.. title: Brownie*
.. slug: brownie
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Brownie*
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 19 ‡ (also invisibility)     |
+-----------------+------------------------------+
| Hit Dice:       | 1**                          |
+-----------------+------------------------------+
| No. of Attacks: | 1 miniature weapon           |
+-----------------+------------------------------+
| Damage:         | 1d2                          |
+-----------------+------------------------------+
| Movement:       | 30'                          |
+-----------------+------------------------------+
| No. Appearing:  | 1                            |
+-----------------+------------------------------+
| Save As:        | Magic-User: 1 (with bonuses) |
+-----------------+------------------------------+
| Morale:         | 7                            |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 49                           |
+-----------------+------------------------------+

A **Brownie** is a small fey being closely related to pixies and sprites, as it is an elf-like creature about 2 feet tall. A brownie is able to shape-change at will into the form of a small deer, hawk, or an otter. A brownie is industrious and predisposed to tinkering with and fixing things. In all forms the brownie has Darkvision with a range of 60 feet. A brownie can speak Common as well as the languages of pixies and sprites.

In its natural humanoid form, a brownie attacks with its miniature weapon, sometimes with paralytic poison applied (save vs. Poison or be held for 2d4 rounds as per the **hold** **person** spell). In its other forms, see the relevant monster entry in the Core Rules; none of these other forms will have the poison attack. In addition to its weapon attack, a brownie has several magical qualities available in any of its forms. It can **detect** **magic** at will, become **invisible** at will, and once per day can cast **confusion** as a 7th level caster. A brownie can attack while completely invisible without disrupting the effect (generally each opponent must take a -4 penalty on attacks against the brownie).

Silver or magical weapons are required to strike a brownie. So long as one has at least 1 hit point remaining, it regenerates 1 hit point each round; however, if reduced below 1 hp a brownie will die like any other creature. A brownie saves against magic (including wands) with a +4 bonus, and has a +1 bonus with respect to paralysis or petrify saves.
