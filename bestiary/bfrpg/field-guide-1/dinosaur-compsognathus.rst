.. title: Dinosaur, Compsognathus
.. slug: dinosaur-compsognathus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Dinosaur, Compsognathus
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 12             |
+-----------------+----------------+
| Hit Dice:       | 1d4 hit points |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 1d2 bite       |
+-----------------+----------------+
| Movement:       | 40'            |
+-----------------+----------------+
| No. Appearing:  | Wild 1d10      |
+-----------------+----------------+
| Save As:        | Normal Man     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 10             |
+-----------------+----------------+

A **Compsognathus** is a tiny, chicken-sized prehistoric reptile that runs swiftly to catch insects and other small prey. Alone it isn’t much of a threat, but on occasion it has been known to gang up on a large creature.
