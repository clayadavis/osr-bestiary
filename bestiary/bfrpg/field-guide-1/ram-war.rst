.. title: Ram, War
.. slug: ram-war
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ram, War
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 13                    |
+-----------------+-----------------------+
| Hit Dice:       | 3                     |
+-----------------+-----------------------+
| No. of Attacks: | 2 horns or hooves     |
+-----------------+-----------------------+
| Damage:         | 1d6 horns, 1d4 hooves |
+-----------------+-----------------------+
| Movement:       | 40' Climb 20'         |
+-----------------+-----------------------+
| No. Appearing:  | Domestic only         |
+-----------------+-----------------------+
| Save As:        | Fighter: 3            |
+-----------------+-----------------------+
| Morale:         | 9                     |
+-----------------+-----------------------+
| Treasure Type:  | none                  |
+-----------------+-----------------------+
| XP:             | 145                   |
+-----------------+-----------------------+

Dwarves are known for their **War Rams**, which are specifically bred for strength and aggression. It attacks with either its great horns (needing a short run or charge) or with its hooves. Like the mundane breeds of ram, a war ram can negotiate rocky terrain with ease. The climbing speed listed above assumes its native terrain of rocky slopes; a war ram cannot climb walls, trees, or other objects in the traditional sense.
