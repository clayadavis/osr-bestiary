.. title: Spider, Giant Ice
.. slug: spider-giant-ice
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Spider, Giant Ice
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 16              |
+-----------------+-----------------+
| Hit Dice:       | 6*              |
+-----------------+-----------------+
| No. of Attacks: | 1 bite + poison |
+-----------------+-----------------+
| Damage:         | 1d10 bite       |
+-----------------+-----------------+
| Movement:       | 50'             |
+-----------------+-----------------+
| No. Appearing:  | 1d6             |
+-----------------+-----------------+
| Save As:        | Fighter: 6      |
+-----------------+-----------------+
| Morale:         | 12              |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 555             |
+-----------------+-----------------+

A **Giant Ice Spider** is a crystal clear spider adapted to icy environments. It is immune to cold-based attacks, but takes an additional point of damage (per die) from fire or heat effects. Rather than spin silk webs, it spins ice into web-like structures. When an ice spider holds perfectly still in its natural environment, it is so hard to see that it surprises on 1-4 on 1d6.

Besides a poisonous bite, one can spit its freezing cold saliva up to 30 feet, which can freeze instantly and ensnare a man-sized or smaller creature. The ice spider must hit with an attack, and the affected creature can break free with a 1 on 1d6 (modified by Strength bonus).
