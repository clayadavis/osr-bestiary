.. title: Guard Fern
.. slug: guard-fern
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Guard Fern
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 14                              |
+-----------------+---------------------------------+
| Hit Dice:       | 6*                              |
+-----------------+---------------------------------+
| No. of Attacks: | 3 thorn, acid, leaves           |
+-----------------+---------------------------------+
| Damage:         | 1d4 thorn, 3d8 acid, 1d8 leaves |
+-----------------+---------------------------------+
| Movement:       | 0' (immobile)                   |
+-----------------+---------------------------------+
| No. Appearing:  | Wild 1d6                        |
+-----------------+---------------------------------+
| Save As:        | Fighter: 6                      |
+-----------------+---------------------------------+
| Morale:         | 12                              |
+-----------------+---------------------------------+
| Treasure Type:  | None                            |
+-----------------+---------------------------------+
| XP:             | 555                             |
+-----------------+---------------------------------+

A **Guard Fern** is a huge, bushy plant. It is typically cultivated and placed to guard narrow features, such as a hallway or gap in a wall. It is immune to acids, and has a fire-retardant structure that reduces all fire-based damage by half (a successful saving throw means it takes no damage).

The guard fern is immobile but can attack in all directions. It reacts to any vibration and heat sources.

It has three attacks that intensify as targets move closer. At sixty to thirty feet, it can fire a spray of large, sharp thorns, each dealing 1d4 points of damage; each human-sized person might be hit by 1d4 thorns. At five to thirty feet, it sprays a shower of acid, dealing 3d8 points of damage with a save vs. Dragon Breath reducing damage by half. It can perform both of these attacks three times in any direction before it must grow more thorns or replenish the acid, regaining one attack in each category each day. Finally, it can lash out with sharp leaves at a single creature within five feet, dealing 1d8 points of damage on a hit.

Even if chopped to bits, a guard fern will grow back in 1d6+4 weeks. Only burning one or digging up the roots will kill it for good.
