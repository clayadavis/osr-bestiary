.. title: Dragon, Brown
.. slug: dragon-brown
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Dragon, Brown
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 17                              |
+-----------------+---------------------------------+
| Hit Dice:       | 6**                             |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 1d4 claws, 2d8 bite, 1d4 tail   |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80'                     |
+-----------------+---------------------------------+
| No. Appearing:  | 1 Wild 1 Lair 1d4               |
+-----------------+---------------------------------+
| Save As:        | Fighter: 6                      |
+-----------------+---------------------------------+
| Morale:         | 7                               |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 610                             |
+-----------------+---------------------------------+

A **Brown Dragon** is the smallest and weakest of the dragons, but is far craftier. It favors rocky cliff-sides and canyons for its home. It is a highly territorial but cowardly hermit, and even a mated pair will jealously separate their treasure from one another. A brown dragon prefers to ambush trespassers by blending into the rocks. Some larger ones have been known to accumulate boulders to drop from above onto unsuspecting adventurers.

A brown dragons' claws are especially well-suited to climbing sheer cliff surfaces, which it does as well as a Thief (level equivalent to the brown dragon's HD).

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
