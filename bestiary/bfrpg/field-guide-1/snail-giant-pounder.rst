.. title: Snail, Giant Pounder
.. slug: snail-giant-pounder
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Snail, Giant Pounder
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 2                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 tail blow or 1 bite   |
+-----------------+-------------------------+
| Damage:         | 1d4 tail blow, 1d2 bite |
+-----------------+-------------------------+
| Movement:       | 10'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6                     |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 75                      |
+-----------------+-------------------------+

A **Giant Pounder Snail** is often heard from a distance, pounding the ground with a hard bony appendage to locate hollows and burrows to break into and then engulf any living matter therein. The appendage is shaped much like a war hammer or mace. Its mouth is little more than rough grinding surfaces, and do little more than 1d2 points of damage, but if provoked they will bite.
