.. title: Orc, Snow
.. slug: orc-snow
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Orc, Snow
.. type: text
.. image:

+-----------------+---------------------------------------+
| Armor Class:    | 14 (11)                               |
+-----------------+---------------------------------------+
| Hit Dice:       | 1*                                    |
+-----------------+---------------------------------------+
| No. of Attacks: | 1 punch or weapon                     |
+-----------------+---------------------------------------+
| Damage:         | 1d8 or by weapon                      |
+-----------------+---------------------------------------+
| Movement:       | 40'                                   |
+-----------------+---------------------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6, Horde 10d20 |
+-----------------+---------------------------------------+
| Save As:        | Fighter: 1 (but see below)            |
+-----------------+---------------------------------------+
| Morale:         | 8                                     |
+-----------------+---------------------------------------+
| Treasure Type:  | D                                     |
+-----------------+---------------------------------------+
| XP:             | 37                                    |
+-----------------+---------------------------------------+

A **Snow Orc** is a white-haired relative of the normal orc, well-suited to cold mountain lairs. It appears even more bestial than its normal orcish brethren. Its feet are very wide and allows it to traverse snow with ease. A snow orc is so hairy that it can walk in a blizzard without additional clothes, though it prefers to wear armor and long capes.

A snow orc is never affected by cold weather, and treats snowy or icy terrain as road terrain for the sake of movement. While cold-based magic does affect it, it saves against it as a 10th-level Fighter rather than 1st-level. Like other orcs it has Darkvision to a range of 60 feet. It speaks the orcish tongue, but may also speak Common, Goblin, or even Giant.
