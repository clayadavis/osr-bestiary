.. title: Megatherium
.. slug: megatherium
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Megatherium
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 9 (AB +8)  |
+-----------------+------------+
| No. of Attacks: | 2 claws    |
+-----------------+------------+
| Damage:         | 1d10 claw  |
+-----------------+------------+
| Movement:       | 30'        |
+-----------------+------------+
| No. Appearing:  | 1d4 wild   |
+-----------------+------------+
| Save As:        | Fighter: 9 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 1,075      |
+-----------------+------------+

A **Megatherium** is a gigantic prehistoric ground sloth the size of an elephant. While a normal sloth is a vegetarian, a megatherium is omnivorous, sometimes hunting prey or scavenging. A megatherium can climb well, assuming the surface is strong enough to support its massive weight.
