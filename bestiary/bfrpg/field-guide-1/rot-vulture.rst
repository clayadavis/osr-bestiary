.. title: Rot Vulture
.. slug: rot-vulture
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Rot Vulture
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 12                       |
+-----------------+--------------------------+
| Hit Dice:       | 2                        |
+-----------------+--------------------------+
| No. of Attacks: | 1 flogging               |
+-----------------+--------------------------+
| Damage:         | 1d6 flogging             |
+-----------------+--------------------------+
| Movement:       | 10' Fly 120'             |
+-----------------+--------------------------+
| No. Appearing:  | 1d10 Wild 1d10 Lair 1d10 |
+-----------------+--------------------------+
| Save As:        | Fighter: 2               |
+-----------------+--------------------------+
| Morale:         | 10                       |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 75                       |
+-----------------+--------------------------+

A **Rot Vulture** is an undead carnivorous bird that has the same general appearance as a large, healthy vulture at a distance, but up close it has major defects such as missing eyes, torn beaks, and protruding broken bones. It stinks of horrible decay. It is ravenous and driven to attack. As with all undead, it can be Turned by a Cleric (as a zombie), and is immune to **sleep**, **charm**, and **hold** spells.

A rot vulture attacks with a combined pecking, clawing, and wing-flogging attack that is rolled with one attack roll. Some rot vultures have certain special abilities (1-2 on 1d6 indicates such). The GM can roll randomly or choose the effect, and may add more types of horrors.

1. It emits a horrid stench in a gaseous blast of gooey rotten matter when slain, causing anyone within ten feet to save vs. Poison or drop anything in his or her hands, fall to their knees, and wretch helplessly for 1d4 rounds.

2. It has bitten, and the victim must save vs. Poison or be inflicted by rot grubs.

3. Any victim must save vs. Poison or suffer the effects of a disease similar to a giant rat bite.

4. It shrieks in the face of its victim (one only) so loudly and horrifically he or she must save vs. Spells or suffer the effects of **cause** **fear** (reversed **remove** **fear**) for 1d6 rounds, as well as deafened for 1d6 hours.

5. A victim must save vs. Paralysis or be paralyzed for 2d6 rounds, just like a ghoul's touch (elves are likewise immune).

6. A victim must save vs. Spells or be inflicted by a horrid decaying odor for 1d6 days, attracting scavengers (at least one additional roll for wandering monsters during each interval). The stench also means a character's Charisma is reduced by 8 points (minimum score of 3). His or her chances of stealth is greatly reduced, and effectively impossible when facing creatures with a keen sense of smell.
