.. title: Blade Spirit*
.. slug: blade-spirit
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Blade Spirit*
.. type: text
.. image:

+-----------------+-----------------+--------------+
|                 | Common          | Greater      |
+=================+=================+==============+
| Armor Class:    | 17 ‡            | 19 ‡         |
+-----------------+-----------------+--------------+
| Hit Dice:       | 9 (AB +8)       | 12 (AB +10)  |
+-----------------+-----------------+--------------+
| No. of Attacks: | 3/2 (see below) | 2 weapons    |
+-----------------+-----------------+--------------+
| Damage:         | By weapon +4    | By weapon +6 |
+-----------------+-----------------+--------------+
| Movement:       | 30'             | 30'          |
+-----------------+-----------------+--------------+
| No. Appearing:  | 1               | 1            |
+-----------------+-----------------+--------------+
| Save As:        | Fighter: 9      | Fighter: 12  |
+-----------------+-----------------+--------------+
| Morale:         | 9               | 10           |
+-----------------+-----------------+--------------+
| Treasure Type:  | Special         | Special      |
+-----------------+-----------------+--------------+
| XP:             | 1075            | 1875         |
+-----------------+-----------------+--------------+

**Blade Spirits** are restless souls of warriors fallen on the battlefield. The body of a blade spirit appears as a rotting or desiccated form or sometimes seems to be assembled from various corpses, always carrying a distinctive melee weapon. The weapon itself is possessed with the undead spirit, which animates the form in order to continue its battles.

A blade spirit deals damage according to its weapon type with +4 added to the damage. Like most undead, a blade spirit is immune to poison, **charm** and **hold** spells. It may be Turned by a Cleric (as a wraith), but not destroyed permanently except as described below. A blade spirit can only be harmed by magical weapons.

Upon the defeat of a blade spirit, the animated body falls apart and the possessed sword goes dormant for 1d10 days. A **remove** **curse** spell cast on the weapon during this time will drive the spirit out of the weapon permanently; otherwise the spirit will begin reassembling a body.

A common blade spirit may make 3 weapon attacks every two rounds; this means one attack on every odd-numbered round, and two on every even-numbered round.

A **Greater Blade Spirit** is simply a more powerful spirit pursuing even greater ambitions. It deals more damage (+6 damage) and has magical weapons determined by the Game Master. They can be Turned by a Cleric (as a vampire), and the **dispel** **evil** spell is required to permanently destroy the spirit.
