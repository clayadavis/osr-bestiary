.. title: Bone Horror*
.. slug: bone-horror
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Bone Horror*
.. type: text
.. image:

+-----------------+--------------------------+---------------------------+
|                 | Common                   | Greater                   |
+=================+==========================+===========================+
| Armor Class:    | 14 ‡                     | 19 ‡                      |
+-----------------+--------------------------+---------------------------+
| Hit Dice:       | 4*                       | 12* (AB +10)              |
+-----------------+--------------------------+---------------------------+
| No. of Attacks: | 2 claws or 1 sting       | 2 claws or 1 sting        |
+-----------------+--------------------------+---------------------------+
| Damage:         | 1d6+3 claw or 1d4+poison | 1d10+4 claw or 1d6+poison |
+-----------------+--------------------------+---------------------------+
| Movement:       | 20' Fly 30'              | 30' Fly 40'               |
+-----------------+--------------------------+---------------------------+
| No. Appearing:  | Varies                   | Varies                    |
+-----------------+--------------------------+---------------------------+
| Save As:        | Cleric: 4                | Cleric: 12                |
+-----------------+--------------------------+---------------------------+
| Morale:         | 12                       | 12                        |
+-----------------+--------------------------+---------------------------+
| Treasure Type:  | None                     | None                      |
+-----------------+--------------------------+---------------------------+
| XP:             | 280                      | 1975                      |
+-----------------+--------------------------+---------------------------+

A **Bone Horror** is a large, vaguely humanoid creature constructed from bones and parts from several creatures, magically animated in service to its master. One has a massive zombie-like head flanked by twin skulls, large desiccated bat wings, and a gigantic scorpion-like tail. The bone horror attacks with its two sharp claws or its skeletal stinger; those struck by it must save vs. Poison or die.

Magical weapons, fire, or spells are required to damage a bone horror. One can be Turned by a Cleric (as a wight). As with other undead creatures, they are immune to **sleep**, **charm** or **hold** spells.

A **Greater Bone Horror** is simply a much larger and stronger version of the bone horror. The save against the greater bone horror's poison sting is made at a -4 penalty. Greater bone horrors are Turned as vampires.
