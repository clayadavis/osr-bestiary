.. title: Delver
.. slug: delver
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Delver
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 24                    |
+-----------------+-----------------------+
| Hit Dice:       | 18** (AB +12)         |
+-----------------+-----------------------+
| No. of Attacks: | 2 flippers            |
+-----------------+-----------------------+
| Damage:         | 1d6 flipper + special |
+-----------------+-----------------------+
| Movement:       | 30' Burrow 10'        |
+-----------------+-----------------------+
| No. Appearing:  | 1                     |
+-----------------+-----------------------+
| Save As:        | Fighter: 18           |
+-----------------+-----------------------+
| Morale:         | 11                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 4,320                 |
+-----------------+-----------------------+

A **Delver** resembles a cross between an enormous centipede and a slug. It is roughly 15 feet long and 12 feet tall. It has a huge mouth and slits for eyes. It has spongy flipper-like arms, each of which ends in six black digging nails. Its ability to sense vibrations gives the equivalent of Darkvision with a 60 foot range.

A delver produces a mucus-like slime that is highly corrosive. Merely touching it causes 2d6 points of damage to organic creatures. The slime deals 4d8 points of damage to metallic creatures or objects, while against stony creatures (including earth elementals) the slime causes 8d10 points of damage. It prefers to fight from its tunnel, which it uses to protect its flanks while lashing out with its two flippers, causing 1d6 points of damage each (plus the corrosive damage noted above). On the round following a successful hit, the victim takes 1d6 points of damage from the slime unless is it washed off with at least a quart of fluid. For metal or stone creatures, this damage is half of the noted corrosive damage listed (2d8 or 4d10 respectively). Anyone attacking a delver with natural weapons will take damage from the corrosive slime each time an attack succeeds unless they succeed on a save vs. Paralysis.

Each time a delver strikes, the individual's shield, armor, and clothing (in that order) may be destroyed. The victim must make a save vs. Paralysis for each item; any successful saving throw means subsequent items are unaffected. For example: a Fighter is struck by a delver; he fails his first saving throw, and his shield is destroyed. He succeeds at his second save, so his armor and clothing are safe... for this round, at least. Magic shields or armor will lose one “plus” each time they are damaged, instead of being destroyed outright.
