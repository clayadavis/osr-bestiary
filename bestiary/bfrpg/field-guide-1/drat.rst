.. title: Drat*
.. slug: drat
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Drat*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 11         |
+-----------------+------------+
| Hit Dice:       | 1d4*       |
+-----------------+------------+
| No. of Attacks: | Special    |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 30'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 7          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 13         |
+-----------------+------------+

The **Drat**, so-called because of the frequent expletives emitted in its vicinity, is so rare as to elicit skepticism with regards to its very existence. It appears to be a common rat, and can be encountered anywhere that a rat might be found. The drat would be little more than an annoyance if not for its special ability.

A drat has an aura of bad luck which covers a radius of 30 feet around it. Anyone besides the drat within the radius suffers a cumulative penalty of -1 per round on attack and saving throw rolls, while giving opponents (who are outside the radius or are immune, i.e. drats) a cumulative +1 bonus on attack rolls against affected creatures. The maximum penalty (or bonus) which may accrue is -6 (or +6).

There is no way to detect this effect, other than to attempt and fail at attacks or saving throws, and the drat does not even need to be visible. For instance, a drat could be peacefully sleeping on the other side of a wall from the adventurers and its sphere of influence will still affect them. The only surefire way to detect the presence of a drat is by noticing the ever increasing string of unlikely events beginning to happen. For instance, a very dexterous Thief will, after only a few minutes exposure, begin to bumble about so clumsily that he or she will soon trip over their own feet. It is essential that the GM be completely and scrupulously fair in the use of this monster.

Draugr

+-----------------+---------------+
| Armor Class:    | 17            |
+-----------------+---------------+
| Hit Dice:       | 9** (AB +8)   |
+-----------------+---------------+
| No. of Attacks: | 1 weapon      |
+-----------------+---------------+
| Damage:         | 1d10+3 weapon |
+-----------------+---------------+
| Movement:       | 30'           |
+-----------------+---------------+
| No. Appearing:  | 1             |
+-----------------+---------------+
| Save As:        | Fighter: 9    |
+-----------------+---------------+
| Morale:         | 11            |
+-----------------+---------------+
| Treasure Type:  | B, M          |
+-----------------+---------------+
| XP:             | 1,225         |
+-----------------+---------------+

A **Draugr** is the undead remains of an ancient king, generally found only in its ancient crypt. It appears as a skeleton wearing antique plate mail. A draugr usually wields a two-handed sword in combat. It can see invisible opponents.

Once per turn, a draugr can breathe a cone of ice out to 10 feet in front of it. Anyone caught in this cloud of frozen mist must save vs. Spells or be stunned (-2 to AC, in addition to losing any Dexterity and shield bonuses) and unable to act for one round. Spellcasters in the midst of casting a spell who fail their save lose the spell they were attempting to cast.

As with all undead, a draugr can be Turned by a Cleric (as a vampire), and is immune to **sleep**, **charm** or** hold** spells.
