.. title: Ram, Wild
.. slug: ram-wild
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ram, Wild
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 13            |
+-----------------+---------------+
| Hit Dice:       | 2             |
+-----------------+---------------+
| No. of Attacks: | 1 horns       |
+-----------------+---------------+
| Damage:         | 1d6 horns     |
+-----------------+---------------+
| Movement:       | 40' Climb 20' |
+-----------------+---------------+
| No. Appearing:  | Wild 2d4      |
+-----------------+---------------+
| Save As:        | Fighter: 2    |
+-----------------+---------------+
| Morale:         | 7             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 75            |
+-----------------+---------------+

A **Wild Ram** is a large herd animal common to high country. It has large curved horns which it uses to attack with great force. Few creatures can match a wild ram's ability to scramble about the rocky slopes it is native to. The climbing speed listed above assumes its native terrain of rocky slopes; a wild ram cannot climb walls, trees, or other objects in the traditional sense.
