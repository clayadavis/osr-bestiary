.. title: Derej Cobra
.. slug: derej-cobra
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Derej Cobra
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 13                            |
+-----------------+-------------------------------+
| Hit Dice:       | 1*                            |
+-----------------+-------------------------------+
| No. of Attacks: | 1 bite                        |
+-----------------+-------------------------------+
| Damage:         | 1d4 bite (special, see below) |
+-----------------+-------------------------------+
| Movement:       | 40'                           |
+-----------------+-------------------------------+
| No. Appearing:  | Special                       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1                    |
+-----------------+-------------------------------+
| Morale:         | 12                            |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 37                            |
+-----------------+-------------------------------+

A **Derej Cobra's** poison increases in virulence every time it hits. A derej cobra rolls 1d4 points of damage for its first hit, 1d6 for its second hit, and so on up to 1d12; after 1d12, the damage becomes 2d6, then 2d8, then 2d10. This increased damage potential is reduced 1 die level per turn after combat ends, to a minimum of 1d4.
