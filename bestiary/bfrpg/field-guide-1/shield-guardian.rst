.. title: Shield Guardian
.. slug: shield-guardian
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Shield Guardian
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 20 ‡         |
+-----------------+--------------+
| Hit Dice:       | 15+20** (+5) |
+-----------------+--------------+
| No. of Attacks: | 1 fist       |
+-----------------+--------------+
| Damage:         | 1d6 fist     |
+-----------------+--------------+
| Movement:       | 30'          |
+-----------------+--------------+
| No. Appearing:  | 1            |
+-----------------+--------------+
| Save As:        | Fighter: 9   |
+-----------------+--------------+
| Morale:         | 12           |
+-----------------+--------------+
| Treasure Type:  | B, O         |
+-----------------+--------------+
| XP:             | 3,100        |
+-----------------+--------------+

Created by powerful spellcasters, a **Shield Guardian** is a construct that protect its masters from harm. Each one is magically linked to a particular magical amulet and will protect its bearer at any cost. A shield guardian will always follow the amulet; if for some reason it and its amulet are separated by more than 100 feet, the construct will enter a dormant state, standing still and waiting impassively for the return of the amulet.

A shield guardian is 9 feet tall and weigh at least 1,200 pounds. A shield guardian cannot speak, but will understand commands given by its master. This construct has Darkvision with a range of 60 feet.

Note the attack bonus given above; a shield guardian is not good for much beyond defense. It is slow and straightforward in battle, bashing alternately with its heavy stone fists.
