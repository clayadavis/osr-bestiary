.. title: Satyr
.. slug: satyr
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Satyr
.. type: text
.. image:

+-----------------+--------------------------------------+
| Armor Class:    | 15                                   |
+-----------------+--------------------------------------+
| Hit Dice:       | 5*                                   |
+-----------------+--------------------------------------+
| No. of Attacks: | 1 head butt/dagger or shortbow       |
+-----------------+--------------------------------------+
| Damage:         | 1d6 head butt, 1d4 dagger or 1d6 bow |
+-----------------+--------------------------------------+
| Movement:       | 40'                                  |
+-----------------+--------------------------------------+
| No. Appearing:  | 1, Lair 1d10, Wild 1d6               |
+-----------------+--------------------------------------+
| Save As:        | Fighter: 5                           |
+-----------------+--------------------------------------+
| Morale:         | 10                                   |
+-----------------+--------------------------------------+
| Treasure Type:  | Individuals S; Lair I                |
+-----------------+--------------------------------------+
| XP:             | 405                                  |
+-----------------+--------------------------------------+

A **Satyr's** body has the upper half of a man and the lower half of a goat, with red or chestnut brown hair, and hooves and horns that are jet black. It speaks Sylvan, and most also speak Common. A satyr is related to its lesser kin (fauns and ibex), but are much rarer and more reclusive.

The keen senses of a satyr make it almost impossible to surprise one in the wild. Conversely, with its own natural grace and agility, a satyr can sneak up on travelers who are not carefully watching the surrounding wilderness. Once engaged in battle, an unarmed satyr attacks with a powerful head butt. A satyr expecting trouble is likely to also be armed with a bow and a dagger.

A satyr can play a variety of magical tunes on its pan pipes. Usually, only one satyr in a group carries this instrument. When it plays, all creatures within 60 foot (except other satyrs) must save vs. Spells or be affected by **charm person**, **sleep**, or **fear**. The satyr chooses the tune and its effect. In the hands of other beings, these pipes have no special powers. A creature that successfully saves against any of the pipe’s effects cannot be affected by the same instrument for 24 hours.
