.. title: Wolverine
.. slug: wolverine
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Wolverine
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 3                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite          |
+-----------------+-------------------------+
| Damage:         | 1d4+2 claw, 1d6 bite    |
+-----------------+-------------------------+
| Movement:       | 30’ Climb 10’ Burrow 1' |
+-----------------+-------------------------+
| No. Appearing:  | 1                       |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 9 or 12 (see below)     |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 145                     |
+-----------------+-------------------------+

The **Wolverine** is a furry animal with a squat, powerful body. Its strong forelimbs are armed with long claws for digging. The adult wolverine is about the size of a medium dog, with a length of 2 to 3 feet, a tail from 1 to 2 feet long, and weighs 20 to 35 pounds. The males are as much as 30 percent larger than the females.

In appearance, the wolverine resembles a small bear with a long tail. It has been known to give off a very strong, extremely unpleasant odor, giving rise to the nicknames "skunk bear" and "nasty cat." A wolverine that takes damage in combat flies into a berserk rage on its next turn, clawing and biting madly until either it or its opponent is dead. While in its rage it has +2 to hit but -2 to AC.

Wug

+-----------------+--------------------------+
| Armor Class:    | 12                       |
+-----------------+--------------------------+
| Hit Dice:       | 1                        |
+-----------------+--------------------------+
| No. of Attacks: | 1 weapon                 |
+-----------------+--------------------------+
| Damage:         | 1d8 or by weapon         |
+-----------------+--------------------------+
| Movement:       | 40'                      |
+-----------------+--------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6 |
+-----------------+--------------------------+
| Save As:        | Fighter: 2               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | E (Lair Only)            |
+-----------------+--------------------------+
| XP:             | 25                       |
+-----------------+--------------------------+

The **Wug** are a race of nasty, brutish humanoid frogs. They infest swamps and other waterways, regularly attacking other humanoids in search of loot and sacrifices. A wug resembles a bipedal frog with a wide mouth, bulging eyes, and splotchy green skin. An adult stands around 5 feet tall and weighs around 180 pounds; males and females are roughly the same size, and non-wugs cannot generally tell them apart. A wug generally goes about naked except for a belt to carry its knives and pouches. All wug tribes are led by a shaman. Wugs speak Common and their own croaking tongue.

A wug is a crafty and merciless warrior, throwing itself into battle with a zealous fanaticism. It will attempt to take prisoners when possible for later sacrifice. It prefers to attack from ambush and is cunning enough to use the terrain to its advantage.

Wugs are known to tame other amphibious monsters, in particular giant frogs and toads as well as prince frogs; such creatures might be encountered in a wug lair.
