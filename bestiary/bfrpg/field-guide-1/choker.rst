.. title: Choker
.. slug: choker
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Choker
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 17                  |
+-----------------+---------------------+
| Hit Dice:       | 3+3                 |
+-----------------+---------------------+
| No. of Attacks: | 1 choke             |
+-----------------+---------------------+
| Damage:         | 1d3 choke + special |
+-----------------+---------------------+
| Movement:       | 20'                 |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Fighter: 3          |
+-----------------+---------------------+
| Morale:         | 7                   |
+-----------------+---------------------+
| Treasure Type:  | U                   |
+-----------------+---------------------+
| XP:             | 145                 |
+-----------------+---------------------+

A **Choker** is a vicious little predator lurking underground, grabbing whatever prey happens by. Its hands and feet have spiny pads that help it grip almost any surface. It weighs about 35 pounds, is brown or mottled gray in color, and vaguely humanoid in shape. A choker likes to perch high, often at intersections, archways, wells, or staircases, reaching down to attack. It generally prefers to attack lone prey.

A choker deals 1d3 points of damage as it grabs its target, and continues to deal 1d3 points of damage each round by choking and tearing at its prey until its victim is dead or it is forced to release. Because it seizes its victim by the neck, a creature in the choker's grasp cannot speak or cast spells. A chokers is supernaturally quick, and always acts first in a combat round.
