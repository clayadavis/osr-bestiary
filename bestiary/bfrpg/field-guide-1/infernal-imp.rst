.. title: Infernal, Imp*
.. slug: infernal-imp
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Infernal, Imp*
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 19 ‡                |
+-----------------+---------------------+
| Hit Dice:       | 2**                 |
+-----------------+---------------------+
| No. of Attacks: | 1 tail stinger      |
+-----------------+---------------------+
| Damage:         | 1d4 stinger +poison |
+-----------------+---------------------+
| Movement:       | 20' Fly 60'         |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Cleric: 2           |
+-----------------+---------------------+
| Morale:         | 7                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 125                 |
+-----------------+---------------------+

An **Imp** is a diminutive, dark, bat-winged humanoid standing about 2 feet tall with a dagger-like tail stinger. It is able to change at-will into the form of a massive spider, raven, or giant rat, all with a devilish look. In all forms the imp has Darkvision with a range of 60 feet.

In its natural form, an imp attacks with its poisonous stinger; those struck must save vs. Poison or die suffering tremendous pain. In its other forms (see the relevant monster entry), it cannot use its poison attack. In addition to physical attacks, an imp has several magical qualities available in any of its forms. It can **detect magic** at-will, become **invisible** at-will, and once per day can **charm** **person** (as a 7th-level caster).

An imp is immune to poison, cold, fire, and electrical attacks. Silver or magical weapons, or spells, are required to strike an imp. So long as it has at least 1 hp remaining, it regenerates 1 hp each round; if reduced below 1 hp an imp will die like any other creature. An imps has a bonus of +4 on all saving throws against magic (including wands).
