.. title: Ankheg
.. slug: ankheg
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ankheg
.. type: text
.. image:

+-----------------+-------------------------------------------+
| Armor Class:    | 18                                        |
+-----------------+-------------------------------------------+
| Hit Dice:       | 5*                                        |
+-----------------+-------------------------------------------+
| No. of Attacks: | 1 bite + hold or 1 acid spit              |
+-----------------+-------------------------------------------+
| Damage:         | 2d6 bite + 2d6 per round or 5d6 (special) |
+-----------------+-------------------------------------------+
| Movement:       | 30' Burrow 20'                            |
+-----------------+-------------------------------------------+
| No. Appearing:  | 2d8, Wild 2d8, Lair 1                     |
+-----------------+-------------------------------------------+
| Save As:        | Fighter: 5                                |
+-----------------+-------------------------------------------+
| Morale:         | 7                                         |
+-----------------+-------------------------------------------+
| Treasure Type:  | None                                      |
+-----------------+-------------------------------------------+
| XP:             | 405                                       |
+-----------------+-------------------------------------------+

An **Ankheg** is a burrowing insect-like monster with six legs and a nasty disposition. It is about 10 feet long and weighs about 800 pounds. An ankheg usually lies 5 to 10 feet below the surface, until its antennae detect the approach of prey; it then burrows up to attack, surprising on a roll of 1-3 on 1d6. Clusters of ankhegs may share the same territory but do not cooperate; in particular, this means that morale should be checked for each individual separately..

If an ankheg hits with its bite attack, it will grab its prey and retreat down its tunnel, dragging the victim with it. The individual automatically takes bite damage each round, and may only attempt to break the hold as if doing an 'open doors' attempt (1 on d6, adding Strength bonus to range). Larger-than-man-sized opponents cannot be dragged underground, but the ankheg will still hold on to the victim and do damage every round as above.

In desperation, an ankheg can spit a line of acid; one will use this attack upon failing a morale check, and then flee on the next round if any opponents are still standing. This attack affects all opponents within a 30' long, 5' wide path, doing 5d6 points of damage; a successful saving throw vs. Dragon Breath will reduce this damage by half. An ankheg can use this attack at most once every six hours.
