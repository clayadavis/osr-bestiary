.. title: Tendriculos
.. slug: tendriculos
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Tendriculos
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 16                    |
+-----------------+-----------------------+
| Hit Dice:       | 9* (AB +8)            |
+-----------------+-----------------------+
| No. of Attacks: | 1 bite/2 tendrils     |
+-----------------+-----------------------+
| Damage:         | 2d8 bite, 1d6 tendril |
+-----------------+-----------------------+
| Movement:       | 20'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1                     |
+-----------------+-----------------------+
| Save As:        | Fighter: 9            |
+-----------------+-----------------------+
| Morale:         | 12                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 1,150                 |
+-----------------+-----------------------+

A **Tendriculos** is a voracious plant-like creature that can rear up to a height of 15 feet and weighs about 3,500 pounds. Prowling deep in forests or waiting in vegetated areas, a tendriculos attacks savagely, showing no fear.

A tendriculos can swallow whole any creature smaller than itself if it hits with a natural 20 on its bite attack. Once inside the plant’s mass, the opponent must succeed on a save vs. Paralysis each round or be paralyzed for 3d6 rounds by the tendriculos’s digestive juices, suffering 2d6 points of acid damage each round. A swallowed creature can cut its way out by using a light slashing or piercing weapon to inflict damage of at least half the monster's hit points to its interior (AC 14). If a victim escapes in this way, the hole he or she made closes instantly; any other swallowed opponents must cut their own way out.

If a tendriculos loses part of its body mass it can be regrown in 1d6 minutes. Holding the severed portion against the wound enables it to reattach in just one round.

Tentacled Horror

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 10+20* (AB +9)          |
+-----------------+-------------------------+
| No. of Attacks: | 5 tentacles + crush     |
+-----------------+-------------------------+
| Damage:         | 1d6 tentacle, 3d6 crush |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1                       |
+-----------------+-------------------------+
| Save As:        | Fighter: 10             |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 1390                    |
+-----------------+-------------------------+

A **Tentacled Horror** is roughly 50 feet in diameter and is composed of hundreds of thick wriggling tentacles. It has no discernible body beyond the mass of pseudopods and moves by undulating across the ground. It speaks no languages.

A tentacled horror lacks subtlety in combat, simply moving in and flailing away with its numerous tentacles. A successful hit with three or more tentacles indicates a crush attack for an additional 3d6 points of damage. Those who witness a tentacle horror for the first time must make a save vs. Spells or flee in stark terror for 1d20 rounds.

A tentacled horror is incredibly resilient; non-magical weapons only deal 1 point of damage, while magic weapons, fire, electricity, and acid only deal half damage. It is immune to **sleep**, **charm**, and **hold** spells.
