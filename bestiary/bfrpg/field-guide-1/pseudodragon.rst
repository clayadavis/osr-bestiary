.. title: Pseudodragon
.. slug: pseudodragon
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Pseudodragon
.. type: text
.. image:

+-----------------+-----------------------------------------+
| Armor Class:    | 18                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 2*                                      |
+-----------------+-----------------------------------------+
| No. of Attacks: | 1 bite or 1 sting + poison              |
+-----------------+-----------------------------------------+
| Damage:         | 1 HP bite, 1d3 stinger + poison (sleep) |
+-----------------+-----------------------------------------+
| Movement:       | 10' Fly 60'                             |
+-----------------+-----------------------------------------+
| No. Appearing:  | 1 Wild 1d2 Lair 1d4                     |
+-----------------+-----------------------------------------+
| Save As:        | Magic-User: 2                           |
+-----------------+-----------------------------------------+
| Morale:         | 7                                       |
+-----------------+-----------------------------------------+
| Treasure Type:  | None                                    |
+-----------------+-----------------------------------------+
| XP:             | 100                                     |
+-----------------+-----------------------------------------+

A **Pseudodragon** has a small body, about 1 foot long, with a 2 foot tail, and weighs 7 pounds. It can communicate telepathically with intelligent creatures, provided they are within 60 feet. It can also vocalize animal noises. A pseudodragon covest shiny but worthless objects such as broken glass.

In combat a pseudodragon can bite for 1 point of damage, but its principal weapon is its stinger-equipped tail, which deals 1d3 points of damage. The stinger delivers a poison that will cause the recipient to fall asleep for 1d3 hours on a failed save vs. Poison. It has keen senses and can locate creatures within 60 feet by various means. Even against invisible opponents, any penalties (such as when attacking) are halved for a pseudodragon.

A pseudodragon has a chameleon-like ability to change color and is able to hide very effectively; so long as it remains still, there is only a 10% chance it will be detected outdoors in forested environments. Even indoors it can hide with a 30% chance of detection.
