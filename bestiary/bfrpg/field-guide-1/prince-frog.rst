.. title: Prince Frog
.. slug: prince-frog
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Prince Frog
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 13                             |
+-----------------+--------------------------------+
| Hit Dice:       | 2*                             |
+-----------------+--------------------------------+
| No. of Attacks: | 2 claws/1 bite + special       |
+-----------------+--------------------------------+
| Damage:         | 1d4 claw, 1d6 bite + paralysis |
+-----------------+--------------------------------+
| Movement:       | 20' Leap 20'                   |
+-----------------+--------------------------------+
| No. Appearing:  | 1d4                            |
+-----------------+--------------------------------+
| Save As:        | Fighter: 2                     |
+-----------------+--------------------------------+
| Morale:         | 9                              |
+-----------------+--------------------------------+
| Treasure Type:  | None                           |
+-----------------+--------------------------------+
| XP:             | 100                            |
+-----------------+--------------------------------+

The creature humorously called a **Prince Frog** is not truly a frog at all. It is about the size of a large dog and is frog-shaped. A prince frog comes in a variety of bold, striking colors, usually two-toned. Prince frogs prefer damp, warm conditions; swamps and jungles are their native habitats.

It is the creature's eyes that led to its name. A prince frog has six eyes on stalks, arranged in a hexagonal pattern around the top of its head; these stalks are distinctly wider at the bottom than at the tip, giving the appearance of a crown. A prince frog can direct its eyes any way it wishes. Many eyes combined with an excellent sense of hearing means that a prince frog cannot generally be surprised. In addition, it is able to **detect** **invisible** continuously.

A prince frog is an aggressive hunter, willing to take on prey larger than itself. Its success at this can be attributed to its poisonous skin, the secretions of which cause any creature coming in contact with one to save vs. Poison or become comatose for 2d4 turns. During this period the victim will have strange and horrifying dreams. A comatose victim cannot be awakened by normal means until the duration has expired.

This poisonous effect should normally be checked for anytime a prince frog successfully attacks a character; in addition, the secretions remain fully potent for 1d4 turns after being separated from the creature. This means that weapons used to successfully attack a prince frog may cause the same effect if touched, though a bonus of +2 is allowed on the saving throw.

Some believe that a prince frog is a form of a Nazgorean monster, but its bright colors have caused most sages to discount this theory.
