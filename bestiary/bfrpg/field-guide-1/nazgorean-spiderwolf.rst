.. title: Nazgorean, Spiderwolf
.. slug: nazgorean-spiderwolf
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Nazgorean, Spiderwolf
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 14                    |
+-----------------+-----------------------+
| Hit Dice:       | 4**                   |
+-----------------+-----------------------+
| No. of Attacks: | 1 bite                |
+-----------------+-----------------------+
| Damage:         | 1d10 bite + paralysis |
+-----------------+-----------------------+
| Movement:       | 60'                   |
+-----------------+-----------------------+
| No. Appearing:  | 2d4 Wild, Lair 2d4    |
+-----------------+-----------------------+
| Save As:        | Fighter: 5            |
+-----------------+-----------------------+
| Morale:         | 10                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 320                   |
+-----------------+-----------------------+

A **Spiderwolf** is a species of huge wolf-like monsters having eight legs, six eyes, and no tail. It is hairless, with gray slimy skin. A typical spiderwolf stands 5 feet high at the shoulder and weighs almost 200 pounds. In the wild, this creature travels and hunts in packs.

A spiderwolf's bite paralyzes; any living creature bitten by a spiderwolf must save vs. Paralyzation or be paralyzed for 1d6 turns. It will then ignore that paralyzed creature and attack another opponent, turning its attention back to any paralyzed victims only after all opposition is quelled.
