.. title: Snail, Giant Barb
.. slug: snail-giant-barb
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Snail, Giant Barb
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 17                   |
+-----------------+----------------------+
| Hit Dice:       | 4                    |
+-----------------+----------------------+
| No. of Attacks: | 1 bite or 1 barb     |
+-----------------+----------------------+
| Damage:         | 1d8 bite, 1d4 barb   |
+-----------------+----------------------+
| Movement:       | 10'                  |
+-----------------+----------------------+
| No. Appearing:  | 1d4                  |
+-----------------+----------------------+
| Save As:        | Fighter: 4           |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 240, 280 (poisonous) |
+-----------------+----------------------+

A **Giant Barb Snail** is a slow, tedious creature that grows strong, dagger-sized teeth, which it can spit in times of duress. They are launched with extremely high pressure from its mouth, with a range of 30 feet and dealing 1d4 points of damage. Some 1 in 6 of these creatures are poisonous; half the time the poison is paralytic (paralyzes for 4d6 hours) or deadly (kills). Victims hit by a tooth (or bitten) must save vs. Poison or suffer the effect.
