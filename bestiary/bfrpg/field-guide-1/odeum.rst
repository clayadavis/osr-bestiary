.. title: Odeum*
.. slug: odeum
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Odeum*
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 16 ‡                     |
+-----------------+--------------------------+
| Hit Dice:       | 4*                       |
+-----------------+--------------------------+
| No. of Attacks: | 1 touch                  |
+-----------------+--------------------------+
| Damage:         | 1d6 touch + wisdom drain |
+-----------------+--------------------------+
| Movement:       | Fly 60'                  |
+-----------------+--------------------------+
| No. Appearing:  | 1                        |
+-----------------+--------------------------+
| Save As:        | Fighter: 4               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | C                        |
+-----------------+--------------------------+
| XP:             | 280                      |
+-----------------+--------------------------+

An **Odeum** is a foul undead spirit, the revenant of a person who was murderously insane. They exist only to inflict similar madness upon others, driving them to perform heinous acts. Unlike many other kinds of spectral undead, an odeum is not necessarily bound to a specific location; most can move about at will, though they are drawn to places tainted by great suffering and torment. An odeum knows and can speak the languages it knew in life, but being insane, its utterances may still be incomprehensible to those who hear it speak.

An odeum attacks the minds of living creatures, damaging them with its icy touch. In addition to normal damage done, any living creature touched by an odeum takes 1d4 points of Wisdom damage. A creature reduced to less than 3 points of Wisdom by this attack is driven insane and acts as if under the effects of a **confusion** spell until its Wisdom is restored to at least 3 points. (Assume that non-character monsters have a Wisdom of 9 for this purpose).

Instead of attacking, an odeum can attempt to possess a living creature using a magical ability similar to **magic** **jar** (as if cast by a 10th level Magic-User), except that no receptacle is required. The target is allowed a save vs. Spells modified its Wisdom bonus to resist this attack. A creature who successfully saves is immune to possession by that odeum for a full day; for this reason, an odeum may delay using this power until its victim has suffered several attacks, and thus has a Wisdom penalty. If this attack succeeds the odeum disappears into the target's body and assumes control.

Once an odeum has possessed a host, it has complete control of the host's actions, though the host remains awake and aware. Attacking a host creature will damage the victim, not the odeum; if the host dies, the odeum leaves its body and resumes its normal nebulous form.

Outside a host an odeum can only be struck by magical weapons and spells. Like all undead it is immune to **sleep**, **charm**, and **hold** spells. It can be Turned by a Cleric (as a wraith). If successfully Turned by a Cleric while possessing a host, the host is allowed a new saving throw, with success resulting the the expulsion of the odeum. An odeum may also be driven from a host by means of **dispel evil**.

An odeum will possess a host for as a long as possible, until the host is driven mad by the horrors the odeum forces it to perform. Only then will the odeum willingly abandon the host and seek out a new victim.
