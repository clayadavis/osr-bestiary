.. title: Ape, Bonobo
.. slug: ape-bonobo
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Bonobo
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 13             |
+-----------------+----------------+
| Hit Dice:       | 1-1            |
+-----------------+----------------+
| No. of Attacks: | 2 claws        |
+-----------------+----------------+
| Damage:         | 1d3 claw       |
+-----------------+----------------+
| Movement:       | 50'            |
+-----------------+----------------+
| No. Appearing:  | 1d6, Wild 2d10 |
+-----------------+----------------+
| Save As:        | Fighter: 1     |
+-----------------+----------------+
| Morale:         | 7              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 25             |
+-----------------+----------------+

**Bonobos** are closely related to common chimpanzees, but they are much rarer. A bonobo is a less aggressive and more social member of the ape family.
