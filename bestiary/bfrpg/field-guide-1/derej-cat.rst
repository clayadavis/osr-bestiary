.. title: Derej Cat
.. slug: derej-cat
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Derej Cat
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 13 (special, see below) |
+-----------------+-------------------------+
| Hit Dice:       | 1*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 claws                 |
+-----------------+-------------------------+
| Damage:         | 1d6 claws               |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | Special                 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 37                      |
+-----------------+-------------------------+

Each time a **Derej Cat** evades a strike (i.e. is attacked unsuccessfully) its armor class increases by 2 points. Its AC is reduced by 1 point per turn after combat ends, to a minimum of 13.
