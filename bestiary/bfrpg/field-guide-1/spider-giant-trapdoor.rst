.. title: Spider, Giant Trapdoor
.. slug: spider-giant-trapdoor
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Spider, Giant Trapdoor
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 15                             |
+-----------------+--------------------------------+
| Hit Dice:       | 4*                             |
+-----------------+--------------------------------+
| No. of Attacks: | 1 bite                         |
+-----------------+--------------------------------+
| Damage:         | 1d8 bite + poison or wrestling |
+-----------------+--------------------------------+
| Movement:       | 50'                            |
+-----------------+--------------------------------+
| No. Appearing:  | Wild 1d4                       |
+-----------------+--------------------------------+
| Save As:        | Fighter: 4                     |
+-----------------+--------------------------------+
| Morale:         | 8                              |
+-----------------+--------------------------------+
| Treasure Type:  | None                           |
+-----------------+--------------------------------+
| XP:             | 280                            |
+-----------------+--------------------------------+

A **Giant Trapdoor Spider** digs deep tunnels and covers the entrance with a trapdoor-like patch of silk covered in dirt, leaves, and other camouflage. It often lays out camouflaged sensory threads of silk. It springs out to attack passing creatures by detecting vibrations, trying to drag them inside its lair (see the wrestling rules in the **Encounters** section of the Basic Fantasy RPG Core Rules). The trap doors can be detected as secret doors, but the spider will usually attack long before the search can be completed. When lying in wait in its tunnel, it surprises on 1-4 on 1d6.
