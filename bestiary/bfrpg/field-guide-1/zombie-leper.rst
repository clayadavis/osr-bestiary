.. title: Zombie, Leper
.. slug: zombie-leper
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Zombie, Leper
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 13                           |
+-----------------+------------------------------+
| Hit Dice:       | 1*                           |
+-----------------+------------------------------+
| No. of Attacks: | 2 claws/1 bite               |
+-----------------+------------------------------+
| Damage:         | 1d3 claw, 1d3 bite + disease |
+-----------------+------------------------------+
| Movement:       | 30'                          |
+-----------------+------------------------------+
| No. Appearing:  | 1d12                         |
+-----------------+------------------------------+
| Save As:        | Fighter: 1                   |
+-----------------+------------------------------+
| Morale:         | 9                            |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 37                           |
+-----------------+------------------------------+

A **Leper Zombie** shuffles toward its prey with grim purpose and hatred. More agile than other types of zombies, it is far more deadly as it carries a horrible disease that resembles leprosy.

It does not seek to consume living folk, but instead slay them. A leper zombie may be Turned by a Cleric (as a ghoul) and is immune to **sleep**, **charm**, and **hold** spells.

Humanoids bitten by leper zombies may be infected with **zombie leprosy**. Each time a humanoid is bitten or clawed, there is a 10% (cumulative per bite and blow) chance of the infection being passed. The afflicted humanoid is allowed to save vs. Death Ray; if the save is failed, the humanoid dies in 3 days. An afflicted humanoid who dies of zombie leprosy rises as a leper zombie at midnight of the following day.

Equipment, arms, and armor of one slain by a leper zombie (or used to destroy a leper zombie) carries a 5% chance of transmitting the disease each day. The infection can be removed from gear by washing in holy water, heating with fire, or casting **bless** on each item.
