.. title: Snail, Giant Cone
.. slug: snail-giant-cone
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Snail, Giant Cone
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 16                     |
+-----------------+------------------------+
| Hit Dice:       | 3*                     |
+-----------------+------------------------+
| No. of Attacks: | 1 dart                 |
+-----------------+------------------------+
| Damage:         | 1d6 dart + paralysis   |
+-----------------+------------------------+
| Movement:       | 10'                    |
+-----------------+------------------------+
| No. Appearing:  | 1d3, 1d3 Wild Lair 1d3 |
+-----------------+------------------------+
| Save As:        | Fighter: 3             |
+-----------------+------------------------+
| Morale:         | 8                      |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 175                    |
+-----------------+------------------------+

A **Giant Cone Snail** is extremely well-camouflaged and highly toxic. It has a natural long and narrow hypodermic tube (a modified tooth) to inject poison, and can launch it up to 20 feet. Stricken victims must save vs. Poison or be paralyzed for 2d6 hours. It swallows prey whole once succumbed. Each snail has but a single tooth to launch, and if this attack is not successful, the creature will retreat inside its shell. This gives it a bonus of +6 to its AC. The snail will then simply wait for any enemy combatants to give up and leave. A giant cone snail generally will not attack groups of creatures which outnumber it, but may lay in wait for a single opponent to become isolated and thus vulnerable.
