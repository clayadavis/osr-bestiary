.. title: Flying Man-of-War
.. slug: flying-man-of-war
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Flying Man-of-War
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 11         |
+-----------------+------------+
| Hit Dice:       | 2          |
+-----------------+------------+
| No. of Attacks: | Special    |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | Fly 40'    |
+-----------------+------------+
| No. Appearing:  | 3d4        |
+-----------------+------------+
| Save As:        | Fighter: 2 |
+-----------------+------------+
| Morale:         | 7          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 75         |
+-----------------+------------+

A **Flying Man-Of-Wars** is a horse-sized flying jellyfish with a distinctive crest like a sail. Its body is filled with hot air, allowing it to float about 20 feet off the ground, with its tentacles hanging below. Any creature touched by its tentacles takes only 1 damage, but must save vs. Paralysis or be stunned for 1d4 rounds due to its paralyzing venom. The flying man-of-war will then pull itself down to its prey (if it weighs more than 100lbs) or lift its prey up to its body and begin digesting it, dealing 1d6 points of damage per round.

If a flying man-of-war takes at least 6 points of damage from a single attack, its envelope is punctured and it swiftly falls to the ground, immobile; its tentacles will collapse in a 10' radius around it and remain poisonous.
