.. title: Sea Hag
.. slug: sea-hag
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Sea Hag
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 14           |
+-----------------+--------------+
| Hit Dice:       | 4+1          |
+-----------------+--------------+
| No. of Attacks: | 2 daggers    |
+-----------------+--------------+
| Damage:         | 1d4+4 dagger |
+-----------------+--------------+
| Movement:       | 30' Swim 40' |
+-----------------+--------------+
| No. Appearing:  | 1            |
+-----------------+--------------+
| Save As:        | Fighter: 4   |
+-----------------+--------------+
| Morale:         | 8            |
+-----------------+--------------+
| Treasure Type:  | F            |
+-----------------+--------------+
| XP:             | 240          |
+-----------------+--------------+

A **Sea Hag** is a horrible creature with an equally hideous appearance. The sea hag is found in the water of seas or overgrown lakes, and appears as an old crone whose bent shape belies her power and swiftness.

A sea hag is not subtle and prefers a direct approach to combat. It usually remains in hiding until it can affect as many foes as possible with its horrific appearance. The sight of a sea hag is so revolting that anyone who sets eyes upon one must succeed on a save vs. Spells or instantly be weakened, reducing his or her Strength by 2d6 points for 1d6 turns. This damage cannot reduce a victim’s Strength score below 0, but anyone reduced to 0 falls to the ground helpless. Creatures that are affected by this power or that successfully save against it cannot be affected again for 24 hours.

A sea hag will attack with two daggers in melee combat. It gains +4 to damage due to its supernatural strength.

Three times per day, a sea hag can focus its evil gaze upon any single creature within 10 feet. The target must succeed on a save vs. Poison or be paralyzed for three days (75%) or die (25%). **Remove** **curse** or **dispel** **evil** can restore sanity sooner. Creatures with immunity to fear effects are not affected.
