.. title: Ettin
.. slug: ettin
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ettin
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 18             |
+-----------------+----------------+
| Hit Dice:       | 13 (AB +10)    |
+-----------------+----------------+
| No. of Attacks: | 2 weapons      |
+-----------------+----------------+
| Damage:         | by weapon type |
+-----------------+----------------+
| Movement:       | 30'            |
+-----------------+----------------+
| No. Appearing:  | 1d4            |
+-----------------+----------------+
| Save As:        | Fighter: 13    |
+-----------------+----------------+
| Morale:         | 7              |
+-----------------+----------------+
| Treasure Type:  | J (Y in lair)  |
+-----------------+----------------+
| XP:             | 2,175          |
+-----------------+----------------+

An **Ettin** is a vicious and unpredictable hunter. An ettin rarely bathes, resulting in grimy and dirty skin resembling a thick, gray hide. An adult ettin is about 13 feet tall. It has no language of its own, but speaks a pidgin of orc and giant. Creatures that can speak any of these languages can understand only bits and pieces of an ettin’s speech.

Although an ettin is not very intelligent, it is a cunning fighter, preferring to ambush victims rather than charge into a fair fight. An ettin typically wields a spear in each hand, adding +4 to damage rolls due to its great strength. Because each arm is controlled by one head, the ettin does not suffer penalties for attacking with two weapons.

An ettin is almost never surprised. The GM rolls once for each head, and only if both heads fail will the monster suffer surprise.
