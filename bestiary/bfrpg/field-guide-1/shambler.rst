.. title: Shambler
.. slug: shambler
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Shambler
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 20                      |
+-----------------+-------------------------+
| Hit Dice:       | 13** (AB +10)           |
+-----------------+-------------------------+
| No. of Attacks: | 2 slam + constrict      |
+-----------------+-------------------------+
| Damage:         | 2d6 slam, 4d6 constrict |
+-----------------+-------------------------+
| Movement:       | 20' Swim 20'            |
+-----------------+-------------------------+
| No. Appearing:  | 1                       |
+-----------------+-------------------------+
| Save As:        | Fighter: 13             |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | B, O                    |
+-----------------+-------------------------+
| XP:             | 2,395                   |
+-----------------+-------------------------+

A **Shambler** appears to be a heap of rotting vegetation. It is actually an intelligent, carnivorous plant with sensory organs located in its upper body. A shambler’s body has an 8 foot girth and is about 6 feet tall when the creature stands erect. It weighs about 3,800 pounds. It is typically encountered in temperate marshes or other warm wetlands, but it may also be found in other areas where rotting vegetation can accumulate in large quantities.

A shambler batters its opponents with two huge arm-like appendages. If a shambler hits with both slam attacks, it will automatically constrict for 4d6 points of damage on the next and subsequent rounds. Because it has only a plant's intelligence, it will fight to the death. It will constrict an opponent until it is dead before moving on to another foe.

A shambler takes no damage from electricity; any electrical attack against a shambler will heal 1d6 hp instead.
