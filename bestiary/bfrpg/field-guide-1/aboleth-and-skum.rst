.. title: Aboleth (and Skum)
.. slug: aboleth-and-skum
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Aboleth (and Skum)
.. type: text
.. image:

+-----------------+---------------+------------------------------------+
|                 | Aboleth       | Skum                               |
+=================+===============+====================================+
| Armor Class:    | 16            | 13                                 |
+-----------------+---------------+------------------------------------+
| Hit Dice:       | 8**           | 2*                                 |
+-----------------+---------------+------------------------------------+
| No. of Attacks: | 4 tentacles   | 1 bite / 2 claws, or 1 weapon      |
+-----------------+---------------+------------------------------------+
| Damage:         | 1d6 tentacles | 2d6 bite / 1d4 claws, or by weapon |
+-----------------+---------------+------------------------------------+
| Movement:       | 10' Swim 60'  | 20' Swim 40'                       |
+-----------------+---------------+------------------------------------+
| No. Appearing:  | 1, Lair 1d3+1 | 1d4+1, Wild 1d4+1, Lair 1d10+5     |
+-----------------+---------------+------------------------------------+
| Save As:        | Magic-User: 8 | Fighter: 2                         |
+-----------------+---------------+------------------------------------+
| Morale:         | 9             | 8 or 12                            |
+-----------------+---------------+------------------------------------+
| Treasure Type:  | H             | B                                  |
+-----------------+---------------+------------------------------------+
| XP:             | 1015          | 100                                |
+-----------------+---------------+------------------------------------+

The **Aboleth** are an ancient race of fish-like amphibians, usually found lurking in subterranean waters. One resembles a huge, slimy fish, with three large eyes and four long, sticky tentacles arranged around its mouth. An aboleth secretes an oily, foul-smelling slime, polluting the water where the creature lurks.

A blow from an aboleth’s tentacle deals 1d6 points of damage. Any living creature hit by a tentacle must save vs. Paralysis or begin to transform over the next 1d4+1 turns. The skin gradually becomes a translucent, slimy membrane. An afflicted creature must remain moistened with fresh water or suffer 1d12 points of damage every turn. A **cure** **disease** or **remove curse** spell cast before the transformation is complete will restore an afflicted creature to normal. After the transformation is complete, only a **heal** spell can reverse it.

An aboleth can cast **ventriloquism**, **phantasmal** **force** and **hallucinatory** **terrain** at will, as long as these illusions appear within a range of 60 feet of the creature.

Up to three times per day, an aboleth can attempt to enslave any one living creature within 30 feet. The target must save vs. Spells or be utterly dominated by the aboleth's mental power. An enslaved creature will obey the telepathic commands of the aboleth. Such a creature can attempt a new save vs. Spells every 24 hours to break free, or can be freed by a **remove** **curse** spell. The control is also broken if the aboleth dies or is separated from its slave by more than a mile.

The slime an aboleth secretes allows a living creature (generally its slaves) to breathe underwater for the next 3 hours, but for the same duration the affected creature can no longer breathe air; such a creature suffocates in 2d6 minutes if removed from water. Continuous and repeated exposure to the slime slowly transforms the creature into a **skum**. The transformation takes about a month, and once complete the creature is forever a slave to the aboleth.

**Skum** are hapless humanoid creatures transformed by aboleths as their servants. A skum resembles a horrific combination of fish and humanoid. It has a slimy, scaly skin and a finned tail used for swimming. A skum attacks with its teeth and razor-sharp claws, or with any weapon provided by its master. Skum have **darkvision** with a range of 60'. They have the same breathing capabilities (and limitations) described above.

In the presence of its aboleth master, a skum becomes totally fearless, having a Morale of 12. If the aboleth master dies its skum enter a frenzied rage, attacking any creature in sight and seeking additional victims when those nearby have been vanquished.
