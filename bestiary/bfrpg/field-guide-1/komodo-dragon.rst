.. title: Komodo Dragon
.. slug: komodo-dragon
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Komodo Dragon
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 2*                 |
+-----------------+--------------------+
| No. of Attacks: | 1 bite             |
+-----------------+--------------------+
| Damage:         | 1d6 bite + disease |
+-----------------+--------------------+
| Movement:       | 30'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d6           |
+-----------------+--------------------+
| Save As:        | Fighter: 2         |
+-----------------+--------------------+
| Morale:         | 7                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 100                |
+-----------------+--------------------+

A **Komodo Dragon** is a huge lizard about twice the weight of an adult human. It is an aggressive carnivore that hunts by ambush, and has a toxic bite that can kill within hours. This giant lizard has a keen sense of smell, and readily tracks dead or dying prey. A komodo dragon attacks with its bite for 1d6 points of damage. Its mouth is filled with virulent microbes and toxins that can quickly overwhelm a victim. These poisons cause 1d8 points of damage per hour, plus the loss of 2 points of Constitution unless a save vs. Poison is made; this save is made each hour until successful, or the victim is dead.
