.. title: Clockwork Skeleton
.. slug: clockwork-skeleton
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Clockwork Skeleton
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 13                |
+-----------------+-------------------+
| Hit Dice:       | 2+2               |
+-----------------+-------------------+
| No. of Attacks: | 1 punch or weapon |
+-----------------+-------------------+
| Damage:         | 1d6 or by weapon  |
+-----------------+-------------------+
| Movement:       | 40'               |
+-----------------+-------------------+
| No. Appearing:  | 2d6               |
+-----------------+-------------------+
| Save As:        | Fighter: 2        |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 75                |
+-----------------+-------------------+

The **Clockwork Skeleton** is a skeletal construct made of bronze and powered by a strange arrangement of pulleys, coils, wires, and gears. It has rudimentary brain matrices limited to knowing a patrol area, responding to alarms, knowing friend from foe, and standing guard. It will not automatically attack unless what it is guarding is disturbed. Unlike a normal animated skeleton, a clockwork skeleton is vulnerable to edged weapons due to the nature of the wires and pulleys that make up its mechanics. Crucially, this creature is not an undead monster (despite its obvious resemblance) and thus cannot be Turned.

Like any construct, a clockwork skeleton is immune to **sleep**, **charm**, and **hold** spells. As it is mindless in the traditional sense, no form of mind reading is of any use against it. It never fails morale and always fights until destroyed.

A clockwork skeleton is vulnerable to electrical attacks; in addition to normal damage done, all clockwork skeletons within 40 feet react as if being Turned by a Cleric of the same level as the caster. Roll on the clerical Turning table for normal skeletons: a result of “T” will cause the clockwork skeletons to move about randomly without attacking, while “D” inflicts an additional 1d8 points of damage per level of the caster.
