.. title: Ogre Mage
.. slug: ogre-mage
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ogre Mage
.. type: text
.. image:

+-----------------+---------------------------------------------+
| Armor Class:    | 17                                          |
+-----------------+---------------------------------------------+
| Hit Dice:       | 5+2**                                       |
+-----------------+---------------------------------------------+
| No. of Attacks: | 1 weapon (+3 to hit) or spells              |
+-----------------+---------------------------------------------+
| Damage:         | 2d6+3 (large weapon), by weapon +3,or spell |
+-----------------+---------------------------------------------+
| Movement:       | 40' Fly 40'                                 |
+-----------------+---------------------------------------------+
| No. Appearing:  | 1, Lair 1d6                                 |
+-----------------+---------------------------------------------+
| Save As:        | Magic-User: 6                               |
+-----------------+---------------------------------------------+
| Morale:         | 10                                          |
+-----------------+---------------------------------------------+
| Treasure Type:  | E                                           |
+-----------------+---------------------------------------------+
| XP:             | 450                                         |
+-----------------+---------------------------------------------+

An **Ogre Mage** is a humanoid standing between 9 and 10 feet tall, has 1d4 horns, and 2 large tusks. It is highly intelligent and speaks Common and ogrish, among other languages.

An ogre mage is very strong (18 Strength), granting it +3 to hit and damage with melee weapons.

An ogre mage may cast **fly**, **invisibility**, **darkness**, and **polymorph** **self** at-will (one choice per round) as often as desired. Once per day it can cast **charm** **person** and **sleep**. Also once per day it may produce a special cone of cold which is 60 feet long and 12 feet diameter at the base, inflicting 12d4 points of damage (save vs. Spells for half damage). An ogre magi regenerates 1 hp per round.
