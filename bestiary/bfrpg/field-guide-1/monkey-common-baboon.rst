.. title: Monkey, Common & Baboon
.. slug: monkey-common-baboon
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Monkey, Common & Baboon
.. type: text
.. image:

+-----------------+----------------+------------+
|                 | Common         | Baboon     |
+=================+================+============+
| Armor Class:    | 16             | 13         |
+-----------------+----------------+------------+
| Hit Dice:       | 1d4 hit points | 1          |
+-----------------+----------------+------------+
| No. of Attacks: | 1 bite         | 1 bite     |
+-----------------+----------------+------------+
| Damage:         | 1d3 bite       | 1d4 bite   |
+-----------------+----------------+------------+
| Movement:       | 50'            | 40'        |
+-----------------+----------------+------------+
| No. Appearing:  | 3d6 Wild       | 3d6 Wild   |
+-----------------+----------------+------------+
| Save As:        | Fighter: 1     | Fighter: 1 |
+-----------------+----------------+------------+
| Morale:         | 7              | 8          |
+-----------------+----------------+------------+
| Treasure Type:  | None           | None       |
+-----------------+----------------+------------+
| XP:             | 10             | 25         |
+-----------------+----------------+------------+

A **Common Monkey** weighs about 30-40 pounds. They are usually shy, but if one or more of them becomes comfortable around humans they can be quite a nuisance. This smaller type of monkey is generally inconsequential to adventurers.

A **Baboon** (the natural form of the baboon, as opposed to the rock baboon in the Basic Fantasy RPG Core Rules) weighs 60-90 pounds. It lives in packs and often hunts other small game. It will face down predators on occasion. An ordinary baboon does not normally present a threat to adventurers, but if cornered or threatened it will become hostile.
