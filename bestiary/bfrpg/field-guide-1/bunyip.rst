.. title: Bunyip
.. slug: bunyip
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Bunyip
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 15           |
+-----------------+--------------+
| Hit Dice:       | 6            |
+-----------------+--------------+
| No. of Attacks: | 1 bite       |
+-----------------+--------------+
| Damage:         | 1d10+2 bite  |
+-----------------+--------------+
| Movement:       | 40' Swim 20' |
+-----------------+--------------+
| No. Appearing:  | 1            |
+-----------------+--------------+
| Save As:        | Fighter: 6   |
+-----------------+--------------+
| Morale:         | 9            |
+-----------------+--------------+
| Treasure Type:  | None         |
+-----------------+--------------+
| XP:             | 500          |
+-----------------+--------------+

A **Bunyip** is a large carnivorous lake-dwelling creature, with a dog-like face, large tusks, sturdy webbed feet, short otter-like fur, and a body much like that of a great bear. It may be found in lakes and rivers in the remote wilderness, and also in underground pools and lakes. A bunyip is very aggressive and will usually attack anyone who wanders into its territorial waters.
