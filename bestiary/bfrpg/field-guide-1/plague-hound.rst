.. title: Plague Hound
.. slug: plague-hound
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Plague Hound
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite + special        |
+-----------------+-------------------------+
| Damage:         | 2d4 bite + paralysis    |
+-----------------+-------------------------+
| Movement:       | 50'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d8, Wild 2d8, Lair 3d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

A **Plague Hound** is an undead canine with an infliction similar to a ghoul or ghast. It appears as a ravenous beast with patches of fur or skin sloughing off.

Those bitten by a plague hound must save vs. Paralyze or be paralyzed for 2d8 turns; elves are immune to this effect. The plague hound's bite also carries the ghoul fever affliction, but it is even more virulent. Each bite has a 10% cumulative chance of infecting the victim with ghoul fever (roll once per bitten character, after the encounter is over, at 10% per each bite; for example, a character bitten three times has a 30% likelihood of being infected). If afflicted, the victim must save vs. Death Ray (at a penalty of -4) or die within a day, only to rise at the next sunset as a ghoul. Any dog or wolf will return as a plague hound. A plague hounds can be Turned by a Cleric (as a wight) and it shares the common undead traits of immunity to **sleep**, **charm**, and **hold** spells.
