.. title: Mohrg
.. slug: mohrg
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Mohrg
.. type: text
.. image:

+-----------------+---------------------------------------+
| Armor Class:    | 23                                    |
+-----------------+---------------------------------------+
| Hit Dice:       | 14** (AB +11)                         |
+-----------------+---------------------------------------+
| No. of Attacks: | 1 slam or 1 tongue                    |
+-----------------+---------------------------------------+
| Damage:         | 1d6 slam or special                   |
+-----------------+---------------------------------------+
| Movement:       | 30'                                   |
+-----------------+---------------------------------------+
| No. Appearing:  | Wild 1d4, Lair 1d4 (plus 5d4 zombies) |
+-----------------+---------------------------------------+
| Save As:        | Fighter: 14                           |
+-----------------+---------------------------------------+
| Morale:         | 12                                    |
+-----------------+---------------------------------------+
| Treasure Type:  | None                                  |
+-----------------+---------------------------------------+
| XP:             | 2,730                                 |
+-----------------+---------------------------------------+

A **Mohrg** is the animated corpse of a mass murderer or other heinous villain. The average mohrg is 5 to 6 feet tall and weighs about 120 pounds. A mohrg attacks by striking enemies with its fists, but it may also lash out with its tongue. Whomever the tongue touches must save vs. Paralysis or become paralyzed for 6d4 rounds. Unlike zombies, mohrgs move at full speed and have no penalty to initiative.

As with all undead, they may be Turned by a Cleric (as a vampire), and are immune to **sleep**, **charm**, or **hold** magic. Creatures killed by a mohrg rise after 1d4 days as ordinary zombies under the mohrg’s control.
