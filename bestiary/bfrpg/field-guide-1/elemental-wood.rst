.. title: Elemental, Wood*
.. slug: elemental-wood
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Elemental, Wood*
.. type: text
.. image:

+-----------------+------------+--------------+--------------+
|                 | **Staff**  | **Device**   | **Spell**    |
+=================+============+==============+==============+
| Armor Class:    | 18 ‡       | 20 ‡         | 22 ‡         |
+-----------------+------------+--------------+--------------+
| Hit Dice:       | 8*         | 12* (AB +10) | 16* (AB +12) |
+-----------------+------------+--------------+--------------+
| No. of Attacks: | 1 punch or stomp                         |
+-----------------+------------+--------------+--------------+
| Damage:         | 1d12       | 2d8          | 3d6          |
+-----------------+------------+--------------+--------------+
| Movement:       | 40' Fly 30'                              |
+-----------------+------------------------------------------+
| No. Appearing:  | -- special --                            |
+-----------------+------------+--------------+--------------+
| Save As:        | Fighter: 8 | Fighter: 12  | Fighter: 16  |
+-----------------+------------+--------------+--------------+
| Morale:         | -- 10 --                                 |
+-----------------+------------------------------------------+
| Treasure Type:  | -- None --                               |
+-----------------+------------+--------------+--------------+
| XP:             | 945        | 1,975        | 3,385        |
+-----------------+------------+--------------+--------------+

A **Wood Elemental** superficially resembles a treant, but closer inspection reveals that rather than a single tree it is made up of dozens of interlinked trees. A wood elemental takes double damage from fire attacks of any kind, and does 1d8 points of extra damage to creatures in contact with vegetation (including weapons or shields made mainly of wood), as well as to earthen or stone structures.
