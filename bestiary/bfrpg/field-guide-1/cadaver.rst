.. title: Cadaver
.. slug: cadaver
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Cadaver
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 17                  |
+-----------------+---------------------+
| Hit Dice:       | 6**                 |
+-----------------+---------------------+
| No. of Attacks: | 1 punch or by spell |
+-----------------+---------------------+
| Damage:         | 1d6 punch           |
+-----------------+---------------------+
| Movement:       | 40'                 |
+-----------------+---------------------+
| No. Appearing:  | 1d4                 |
+-----------------+---------------------+
| Save As:        | Fighter: 6          |
+-----------------+---------------------+
| Morale:         | 8                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 610                 |
+-----------------+---------------------+

The conditions that create a **Cadaver** is unknown, but it's rumored it arises in areas of dungeons or ruins that have been rich in undead for long periods of time. A cadaver is a corporeal undead creature bearing a physical resemblance to a ghoul. It is not exceptionally smart but shouldn't be underestimated. A cadavers is believed to subsist by eating the flesh of other undead creatures.

A cadavers attacks with powerful blows from its fists. As with all undead, it can be Turned by a Cleric (as a mummy), and is immune to **sleep**, **charm** or **hold** spells. It has all the powers and spells of an 8th-level Cleric, including the power to Turn undead. The spells the cadaver uses will typically include: 1st level: **cure light wounds***, **curse**, and/or **darkness**; 2nd level: **silence 15ft radius**; 3rd level: **bestow curse**, **speak with dead**; 4th level: **animate dead** and/or **dispel magic**. Healing spells cast by a cadaver can even heal undead creatures.
