.. title: Bog Crone 
.. slug: bog-crone
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Bog Crone 
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 15              |
+-----------------+-----------------+
| Hit Dice:       | 5+2**           |
+-----------------+-----------------+
| No. of Attacks: | 2 claws         |
+-----------------+-----------------+
| Damage:         | 1d4+2 claw      |
+-----------------+-----------------+
| Movement:       | 30' Swim 40'    |
+-----------------+-----------------+
| No. Appearing:  | 1               |
+-----------------+-----------------+
| Save As:        | Fighter: 5      |
+-----------------+-----------------+
| Morale:         | 10              |
+-----------------+-----------------+
| Treasure Type:  | C; E, N in lair |
+-----------------+-----------------+
| XP:             | 450             |
+-----------------+-----------------+

**Bog Crones** are loathsome and repugnant fey who dwell in marshlands, swamps, and other watery environs. It stands near 7 feet tall hunched over. It has sickly blue-white skin, long wet black hair, and jaundiced eyes. As a race they are cunning and cruel, preferring trickery over direct combat. A bog crone is a master potion maker, and its huts and caves are usually festooned with potions of all varieties. As its name suggests, a bog crone prefers to live in swampy, overgrown environs. It is also aquatic and can breathe underwater. It speaks Common and Elvish.

A bog crone uses the terrain of its marshy homes to its advantage. It is capable of moving in near-complete silence while in swampy terrain, surprising opponents on 1-4 on 1d6. A favored tactic of a bog crone is to surprise a single opponent and drag them away into a deep pool of water, where it then drowns the individual. A bog crone exudes an aura of unwholesomeness. Near its lair animals grow sick and die, plants wither, and water turns foul. This aura of evil even effects magic; any healing spell cast upon a target that is standing within 30 feet of the crone only heals half the normal amount.
