.. title: Great Orb of Eyes
.. slug: great-orb-of-eyes
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Great Orb of Eyes
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 19                             |
+-----------------+--------------------------------+
| Hit Dice:       | 12* (AB +10)                   |
+-----------------+--------------------------------+
| No. of Attacks: | 1d4 rays or spell-like ability |
+-----------------+--------------------------------+
| Damage:         | By ray or spell                |
+-----------------+--------------------------------+
| Movement:       | Fly 30'                        |
+-----------------+--------------------------------+
| No. Appearing:  | 1 Wild, 1 Lair                 |
+-----------------+--------------------------------+
| Save As:        | Magic-User: 12                 |
+-----------------+--------------------------------+
| Morale:         | 9                              |
+-----------------+--------------------------------+
| Treasure Type:  | None                           |
+-----------------+--------------------------------+
| XP:             | 1,975                          |
+-----------------+--------------------------------+

A **Great Orb of Eyes** is a living mass of pulsing and ever-moving eyes. It is highly intelligent but is unable to communicate vocally. A great orb of eyes can see in all directions, making it nearly impossible to surprise. It has Darkvision out to 120 feet, and with concentration may **detect** **magic** or **detect** **invisible** (see below). In combat a great orb of eyes usually levitates high, trying to avoid melee combat. From this vantage, it fires its eye rays, preferring to **cause** **fear**, **hold**, or **charm** as many foes as possible. Each round it fires 1d4 rays, each at a different target. If the die roll is greater than the number of possible targets, the extra rays are lost. The GM may roll to determine which rays fires or choose those with the most destructive effect(s).

**Great Orb of Eyes Rays**

1. Death: target must save vs. Death Ray or die.

2. Draining: target takes 3d6 points of damage. He or she may save vs. Spells for half damage. The great orb of eyes heals half that many hit points.

3. Fear: target is affected by **cause** **fear** (reversed **remove** **fear**) spell, as cast by a 12th-level Cleric.

4. Charm: target is affected by **charm** **monster** spell, as cast by a 12th-level Magic-user.

5. Hold: target is affected by **hold** **monster** spell, as cast by a 12th-level Magic-user.

6. Blinding: target is affected by **cause** **blindness** (reversed **remove** **blindness**) spell, as cast by a 12th-level Cleric.

A great orb of eyes rarely ever uses its death ray unless its very life depends on it, instead preferring to drain foes for sustenance. A great orb of eyes can also cast **telekinesis** three times per day (as a 12th-level caster). Charmed individuals become slaves, providing any necessary manual labor, and a great orb of eyes can communicate telepathically with any such charmed being. When their usefulness fades, these slaves are drained for sustenance.
