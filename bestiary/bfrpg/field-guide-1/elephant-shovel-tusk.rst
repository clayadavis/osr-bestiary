.. title: Elephant, Shovel Tusk
.. slug: elephant-shovel-tusk
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Elephant, Shovel Tusk
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 20                      |
+-----------------+-------------------------+
| Hit Dice:       | 10 (AB +9)              |
+-----------------+-------------------------+
| No. of Attacks: | 1 tusk or trample       |
+-----------------+-------------------------+
| Damage:         | 1d10 tusk, 4d10 trample |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | Wild 1d12               |
+-----------------+-------------------------+
| Save As:        | Fighter: 10             |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 1,300                   |
+-----------------+-------------------------+

The **Shovel Tusk Elephant** is a prehistoric relative of the modern elephants; they have a variety of forms depending on the exact era and region. All varieties have extended lower jaws tipped with a shovel-like plate or tusk. Shovel tusk elephants vary in size; the statistics given are for a larger specimen such as a bull.
