.. title: Boglin
.. slug: boglin
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Boglin
.. type: text
.. image:

+-----------------+-----------------------------------------------+
| Armor Class:    | 11                                            |
+-----------------+-----------------------------------------------+
| Hit Dice:       | 1* to 5*                                      |
+-----------------+-----------------------------------------------+
| No. of Attacks: | 1 weapon or spell                             |
+-----------------+-----------------------------------------------+
| Damage:         | 1d6 or by weapon, or per spell                |
+-----------------+-----------------------------------------------+
| Movement:       | 30'                                           |
+-----------------+-----------------------------------------------+
| No. Appearing:  | 1d4 (see below)                               |
+-----------------+-----------------------------------------------+
| Save As:        | Magic-User: 2 to 10 (see below)               |
+-----------------+-----------------------------------------------+
| Morale:         | 8                                             |
+-----------------+-----------------------------------------------+
| Treasure Type:  | R                                             |
+-----------------+-----------------------------------------------+
| XP:             | 1 HD 37; 2 HD 100; 3 HD 1754 HD 280; 5 HD 405 |
+-----------------+-----------------------------------------------+

Occasionally a goblin is born different, developing a bluish tint to its skin during its childhood; such a creature is called a** Boglin**. An adult boglin has the abilities of a Magic-user of a level equal to twice the monster's hit dice. In any given goblin lair, there is a 10% chance that 1d4 boglins are present. Larger goblin societies, such as a goblin city or major fortress, will almost always have at least 1d4 boglins. They are almost never encountered alone.

A boglin receives a bonus of +4 on saves vs. any sort of magic.
