.. title: Cloaker
.. slug: cloaker
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Cloaker
.. type: text
.. image:

+-----------------+-----------------------------------+
| Armor Class:    | 19                                |
+-----------------+-----------------------------------+
| Hit Dice:       | 6**                               |
+-----------------+-----------------------------------+
| No. of Attacks: | 1 bite / 1 tail + special (crush) |
+-----------------+-----------------------------------+
| Damage:         | 1d6 bite, 1d6 tail                |
+-----------------+-----------------------------------+
| Movement:       | 10' Fly 40'                       |
+-----------------+-----------------------------------+
| No. Appearing:  | 1d3                               |
+-----------------+-----------------------------------+
| Save As:        | Fighter: 6                        |
+-----------------+-----------------------------------+
| Morale:         | 7                                 |
+-----------------+-----------------------------------+
| Treasure Type:  | C                                 |
+-----------------+-----------------------------------+
| XP:             | 610                               |
+-----------------+-----------------------------------+

When resting or lying in wait, a **Cloaker** is almost impossible to distinguish from dark surroundings. A cloaker has glowing eyes, needle sharp fangs, and a whip-like tail. It has an 8-foot wingspan and weighs about 100 pounds. Cloakers are generally found in dark places.

A cloaker lies in wait, surprising on a roll of 1-4 on 1d6. It will bite and wrap itself around the target, causing damage equal to 20 minus the victim's unadjusted, shield-less AC; creatures with AC 20 or higher will suffer no damage. Dexterity offers no protection against this attack, but magical armor bonuses do. The cloaker's tail attack cannot be used on an enveloped victim, but will be applied to those attempting to assist. Attacks against a cloaker that has enveloped a victim will do half damage to the cloaker and half to the trapped victim.
