.. title: Ape, Gorilla
.. slug: ape-gorilla
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Gorilla
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 14                     |
+-----------------+------------------------+
| Hit Dice:       | 4                      |
+-----------------+------------------------+
| No. of Attacks: | 1 maul / 1 bite        |
+-----------------+------------------------+
| Damage:         | 1d6 maul, 1d6 bite     |
+-----------------+------------------------+
| Movement:       | 40'                    |
+-----------------+------------------------+
| No. Appearing:  | 1d6, wild 2d4,lair 2d4 |
+-----------------+------------------------+
| Save As:        | Fighter: 4             |
+-----------------+------------------------+
| Morale:         | 7                      |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 240                    |
+-----------------+------------------------+

**Gorillas** are generally herbivores; it is aggressive mainly in defense of its family group. Otherwise it tends to avoid adventurers.
