.. title: Nazgorean
.. slug: nazgorean
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Nazgorean
.. type: text
.. image:

The term Nazgorean refers to any of a group of monstrous otherworldly creatures believed to be from a realm or dimension called Nazgor. Little is known about this realm or its inhabitants, but all that have been encountered so far share a few common traits.

In general, a being from Nazgor has a grayish cast to its skin, which is usually wet and slimy. It suffers damage when exposed to sunlight and/or dry air. Sunlight alone causes 1d4 points of damage per hour, as does dry air; exposure to both causes 1d8 points of damage per hour unless the being can periodically wet its skin and move into areas of shadow or darkness.

Nazgoreans are truly alien, so much so that their brains are effectively immune to all forms of **sleep**, **charm,** or **hold** magic. Attempting to read the mind of a Nazgorean (via **ESP**, for example) causes the character who made the attempt to save vs. Spells or become **confused** (as the spell) for 2d6 rounds. Those which are apparently sentient cannot learn any normal language, nor is it generally possible for normal characters or creatures to learn their language (if indeed they have one; none have ever been witnessed engaging in any sort of conversation). Finally, they cannot perform magic in any normal way; even magic items that normally work for any character or creature will not function in their hands.
