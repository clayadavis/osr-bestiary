.. title: Lich*
.. slug: lich
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Lich*
.. type: text
.. image:

+-----------------+---------------------------------------+
| Armor Class:    | 19+‡                                  |
+-----------------+---------------------------------------+
| Hit Dice:       | 10+** (AB +9)                         |
+-----------------+---------------------------------------+
| No. of Attacks: | 1 touch, weapon, or spell             |
+-----------------+---------------------------------------+
| Damage:         | 1d8 touch+drain, by weaponor by spell |
+-----------------+---------------------------------------+
| Movement:       | 30'                                   |
+-----------------+---------------------------------------+
| No. Appearing:  | 1                                     |
+-----------------+---------------------------------------+
| Save As:        | Magic-User or Cleric: by hit dice     |
+-----------------+---------------------------------------+
| Morale:         | 8                                     |
+-----------------+---------------------------------------+
| Treasure Type:  | G                                     |
+-----------------+---------------------------------------+
| XP:             | 1,480                                 |
+-----------------+---------------------------------------+

A **Lich** is a former Magic-user or Cleric (of at least 10th level with all spells and powers intact) who used dark magic to prolong its life into a state of undeath. A lich initially appears rather ghoulish or zombie-like, but after some time its body degrades and it begins to appear skeletal. In spite of its great powers, a lich will act to preserve itself by any means it has at its disposal. It knows the value and function of all magical items in its lair, and will use them to their greatest effect.

Simply encountering a lich for the first time is so terrifying that the subject must save vs. Spells or flee for 2d6 rounds. A lich's gaze is also terrifying; effective up to 30 feet, the affected target must save vs. Spells or be paralyzed in fright for 2d4 rounds. A lich prefers to attack with spells from a distance. A lich that hits a living target with its touch deals 1d8 points of damage and drains the victim 1d4 points of Constitution while healing itself an equal amount of damage.

The Constitution damage is permanent, a result of physical and psychic drain. Due to longevity and/or vitality traits, elves can ignore the first 10 points of Constitution drain; dwarves the first 4 points, and halflings the first 2 points. Lost Constitution can be regained at a rate of 1 point per casting of a **restoration** spell; nothing else (except a **wish**) can restore Constitution lost to a lich. If a character's Constitution falls to 0, he or she dies immediately, but will rise the following round as a lesser wight. Use statistics for the wight (in the Basic Fantasy RPG Core Rules), but instead of energy drain, the lesser wight deals 1d4 points of damage plus 1 point of Constitution damage. Those whom were drained and subsequently defeated are permanently dead and cannot be **raised** (but may still be **reincarnated**).

A lich can only be hit by magical weapons or spells. Like a normal skeleton, it takes only half damage from edged weapons, and only a single point from arrows, bolts, or sling stones (plus any magical bonus). As with all undead, it can be Turned by a Cleric (as vampire, but with a -6 penalty on the check), and is immune to **sleep**, **charm**, or **hold** spells. Despite having Magic-user or Clerical levels, the lich uses d8 for rolling hp like other monsters.

A lich's life-force is kept safe within an object called a phylactery, often hidden and protected. This allows the lich to persist even when its physical form is destroyed; in that event, the monster's physical form will slowly regenerate at a rate of 1 hp per hour. In order to completely destroy a lich, its phylactery must be located and destroyed; however, only very powerful magic or catastrophic natural damage (a **disintegrate** spell, a **wish**, or throwing it into an active volcano) can actually damage the object.
