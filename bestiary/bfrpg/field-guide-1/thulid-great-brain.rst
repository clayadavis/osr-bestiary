.. title: Thulid, Great Brain
.. slug: thulid-great-brain
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Thulid, Great Brain
.. type: text
.. image:

+----------------+---------------+
| Armor Class:   | 11            |
+----------------+---------------+
| Hit Dice:      | 9** (AB +8)   |
+----------------+---------------+
| No of Attacks: | Special       |
+----------------+---------------+
| Damage:        | Special       |
+----------------+---------------+
| Movement:      | 1'            |
+----------------+---------------+
| No. Appearing: | 1             |
+----------------+---------------+
| Save As:       | Magic-User: 9 |
+----------------+---------------+
| Morale:        | 7             |
+----------------+---------------+
| Treasure Type: | H             |
+----------------+---------------+
| XP:            | 1225          |
+----------------+---------------+

A **Great Brain** looks like a monstrous, immobile brain ringed by many dexterous but weak tentacles, which it uses both as manipulators and to drag itself around. The exact relationship of the great brain to thulids is unknown. A great brain does not speak, but if it has need for it, can telepathically communicate with any creature of greater than animal intelligence.

Although slow and physically weak, a great brain possesses great magical power, and it shares the thulid's ability to use a mental blast with the same effects.

Up to three times per day, a great brain can attempt to enslave any one living creature within 30 feet, which must save vs. Spells or be utterly dominated by the brain. An enslaved creature obeys the brain’s telepathic commands. Such a creature can attempt a new save vs. Spells every 24 hours to break free, or can be freed by a **remove** **curse** spell. The control is also broken if the brain dies or is separated from the slave by more than a mile. A great brain can also **teleport** once per day with no risk of failure. They can psychically sense living creatures in a 100 foot radius, and can use the following spells at will: **charm** **monster**, **hold** **monster**, **confusion**, and **telekinesis**.
