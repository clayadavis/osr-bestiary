.. title: Poludnitsa
.. slug: poludnitsa
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Poludnitsa
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 15          |
+-----------------+-------------+
| Hit Dice:       | 3*          |
+-----------------+-------------+
| No. of Attacks: | 1 scythe    |
+-----------------+-------------+
| Damage:         | 1d8 scythe  |
+-----------------+-------------+
| Movement:       | 40'         |
+-----------------+-------------+
| No. Appearing:  | 1, Lair 1d6 |
+-----------------+-------------+
| Save As:        | Fighter: 3  |
+-----------------+-------------+
| Morale:         | 8           |
+-----------------+-------------+
| Treasure Type:  | D           |
+-----------------+-------------+
| XP:             | 175         |
+-----------------+-------------+

A **Poludnitsa**, sometimes called "Lady Midday", is a cruel fey who appears as a young, beautiful woman dressed in white and holding a scythe. She enjoys engaging in conversation, asking complicated and difficult riddles. If one answers correctly, she might tell them a great secret (spoken in riddles, of course). If a wrong answer is given, however, she will cause the one answering to suffer a heat stroke; the victim must save vs. Spells or take a -4 penalty to all attack rolls and ability checks for the next 2d6 hours.

A poludnitsa is usually mischievous rather than outright evil, but if threatened she will attempt to behead her foes with her scythe.
