.. title: Ape, Orangutan
.. slug: ape-orangutan
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ape, Orangutan
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 13              |
+-----------------+-----------------+
| Hit Dice:       | 2 or 3          |
+-----------------+-----------------+
| No. of Attacks: | 1 maul          |
+-----------------+-----------------+
| Damage:         | 1d4 or 1d6      |
+-----------------+-----------------+
| Movement:       | 40'             |
+-----------------+-----------------+
| No. Appearing:  | 2d6 or 1-2      |
+-----------------+-----------------+
| Save As:        | Fighter: 2 or 3 |
+-----------------+-----------------+
| Morale:         | 8               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 75 or 145       |
+-----------------+-----------------+

**Orangutans** are usually heavier than humans (averaging 225 to 250 pounds or so) and stronger. The statistics for a 2 hit die individual represent adult females or younger males, while the 3 hit die statistics are for larger mature males; there will usually only be one such in a group of orangutans.

Orangutans are shy creatures and would generally prefer to be left alone, but they are physically powerful and may be very dangerous if cornered or provoked.
