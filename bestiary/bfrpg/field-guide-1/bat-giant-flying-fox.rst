.. title: Bat, Giant Flying Fox
.. slug: bat-giant-flying-fox
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Bat, Giant Flying Fox
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 14                |
+-----------------+-------------------+
| Hit Dice:       | 3                 |
+-----------------+-------------------+
| No. of Attacks: | 1 bite            |
+-----------------+-------------------+
| Damage:         | 1d6 bite          |
+-----------------+-------------------+
| Movement:       | 10' Fly 60' (10') |
+-----------------+-------------------+
| No. Appearing:  | 1d10, Wild 2d20   |
+-----------------+-------------------+
| Save As:        | Fighter: 3        |
+-----------------+-------------------+
| Morale:         | 9                 |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 145               |
+-----------------+-------------------+

**Giant Flying Foxes** are a special variety of giant bats. It is similar in appearance to some jungle fruit bats, only much larger and carnivorous. It has typical bat senses, having a natural sonar that grants it Darkvision to a range of 90 feet.

A giant flying fox has a wingspan over 15 feet and weighs over 200 pounds. Its bite may carry disease, much like a giant rat's bite. Any bite has a 5% chance of causing a disease. A character who suffers one or more bites where the die roll indicates disease will sicken in 3d6 hours. The infected character will lose one point of Constitution per hour; after losing each point, the character is allowed a save vs. Death Ray (adjusted by the current Constitution bonus or penalty) to break the fever and end the disease. Any character reduced to zero Constitution is dead (see Constitution point losses in the **Encounter** section of the Core Rules for details on regaining lost Constitution).Unknown Author2013-01-18T06:57:28Bat, Giant Flying FoxR. Kevin SmootArt: Vintage Copyright Free
