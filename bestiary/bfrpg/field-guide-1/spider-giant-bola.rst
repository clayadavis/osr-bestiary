.. title: Spider, Giant Bola
.. slug: spider-giant-bola
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Spider, Giant Bola
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 15              |
+-----------------+-----------------+
| Hit Dice:       | 4*              |
+-----------------+-----------------+
| No. of Attacks: | 1 bite + poison |
+-----------------+-----------------+
| Damage:         | 1d8 bite        |
+-----------------+-----------------+
| Movement:       | 50'             |
+-----------------+-----------------+
| No. Appearing:  | 1d3, Lair 1     |
+-----------------+-----------------+
| Save As:        | Fighter: 4      |
+-----------------+-----------------+
| Morale:         | 8               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 280             |
+-----------------+-----------------+

A **Giant Bola Spider** forms a long thread of silk, attaches a net of webbing to the end, and then throws it at its target. If its attack succeeds the victim is entangled, and the spider begins to drag the victim in. Entangled individuals may break free on a roll of 1 on 1d6 (modified by Strength bonus, as with a check to open doors). Any small or medium creature thus entangled will be drawn to the spider in 1 round, and then bitten with a bonus of +4 on the monster's attack roll.
