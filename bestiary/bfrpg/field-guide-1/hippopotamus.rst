.. title: Hippopotamus
.. slug: hippopotamus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Hippopotamus
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 17                    |
+-----------------+-----------------------+
| Hit Dice:       | 6                     |
+-----------------+-----------------------+
| No. of Attacks: | 1 bite or 1 trample   |
+-----------------+-----------------------+
| Damage:         | 2d6 bite, 4d6 trample |
+-----------------+-----------------------+
| Movement:       | 40' Swim 30'          |
+-----------------+-----------------------+
| No. Appearing:  | Wild 3d10             |
+-----------------+-----------------------+
| Save As:        | Fighter: 6            |
+-----------------+-----------------------+
| Morale:         | 9                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 500                   |
+-----------------+-----------------------+

A **Hippopotamus** is a massive herbivore dwelling in tropical and sub-tropical swamps, lakes, and rivers. While it feeds on various herbs and weeds, it is territorial, aggressive, and hot-tempered, and is likely to attack anyone who encroaches into its territory. Not only can it trample its foes with its enormous weight, but a hippopotamus also has long, sharp teeth that can deliver a devastating bite.
