.. title: Stalag
.. slug: stalag
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Stalag
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 17         |
+-----------------+------------+
| Hit Dice:       | 1          |
+-----------------+------------+
| No. of Attacks: | 1 stab     |
+-----------------+------------+
| Damage:         | 1d6 stab   |
+-----------------+------------+
| Movement:       | 10'        |
+-----------------+------------+
| No. Appearing:  | 3d6        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 25         |
+-----------------+------------+

The **Stalag** looks very much like a stalactite, hanging from the ceiling of a natural cave. It senses the sounds and warmth of living creatures nearby, and attacks by dropping onto the victim. Due to its camouflage and its silent attack, it surprises on 1-5 on 1d6.
