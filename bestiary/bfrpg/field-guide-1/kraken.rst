.. title: Kraken
.. slug: kraken
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Kraken
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 20                     |
+-----------------+------------------------+
| Hit Dice:       | 36** (+16)             |
+-----------------+------------------------+
| No. of Attacks: | 10 tentacles or 1 bite |
+-----------------+------------------------+
| Damage:         | 7d6 tentacle, 4d6 bite |
+-----------------+------------------------+
| Movement:       | 40' Swim               |
+-----------------+------------------------+
| No. Appearing:  | 1                      |
+-----------------+------------------------+
| Save As:        | Fighter: 20            |
+-----------------+------------------------+
| Morale:         | 11                     |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 18,450                 |
+-----------------+------------------------+

A **Kraken** is possibly the largest known creature, with a body 150 feet long and 10 barbed tentacles that can reach an additional 500 feet. Its beak-like mouth is located where the tentacles meet the lower portion of its body. It usually stays in the deepest parts of the oceans, but will come to the surface for prey.

A kraken strikes its opponents with its barbed tentacles, then grab and crush its victims within its huge jaws. Once an opponent has been hit, the kraken wraps a tentacle around the victim and automatically inflicts 7d6 points of damage each round. When six of the tentacles are wrapped around a ship, the kraken may crush for 4d6 points of damage to the vessel each round. Victims caught in the kraken’s tentacles attack at -4. If a tentacle takes 60 points of damage it's severed. Severed tentacles will regrow in 1d10+10 days.

A kraken can jet backward once per round at a speed of 280 feet in a straight line. When a kraken has lost 5 of its tentacles or 50% of its hit points, it will emit a cloud of jet-black ink in a 100-foot cubic area. This can be repeated once per hour. The cloud provides total concealment, which the kraken will use to escape. Creatures within the cloud are automatically blinded.
