.. title: Behir
.. slug: behir
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Behir
.. type: text
.. image:

+-----------------+------------------------------------------------+
| Armor Class:    | 20                                             |
+-----------------+------------------------------------------------+
| Hit Dice:       | 13** (AB +10)                                  |
+-----------------+------------------------------------------------+
| No. of Attacks: | 1 bite, constriction + 6 claws, or breath      |
+-----------------+------------------------------------------------+
| Damage:         | 2d10 bite, 1d8 constrict, 1d4 claws, or breath |
+-----------------+------------------------------------------------+
| Movement:       | 40'                                            |
+-----------------+------------------------------------------------+
| No. Appearing:  | 1d2                                            |
+-----------------+------------------------------------------------+
| Save As:        | Fighter: 13                                    |
+-----------------+------------------------------------------------+
| Morale:         | 9                                              |
+-----------------+------------------------------------------------+
| Treasure Type:  | L                                              |
+-----------------+------------------------------------------------+
| XP:             | 2,395                                          |
+-----------------+------------------------------------------------+

The **Behir** is a serpentine monster that can slither like a snake or use its dozen legs to move with considerable speed. A behir is around 40 feet long and weighs about 4,000 pounds. The coloration of a behir ranges from ultramarine to deep blue with bands of gray-brown. A behirs often knows the common language of the region.

A behir will bite its foe and then coil around it. On following rounds, the behir causes 1d8 points of constriction damage and rakes at the victim with 6 of its claws for 1d4 points of damage each. Alternatively, a behir can swallow whole a small or medium-sized creature that it has bitten. The swallowed creature takes 1d8 points of damage each round. The swallowed creature may attempt to cut its way out using a small edged weapon such as a dagger to deal 20 points of damage to the behir's insides (AC 15). The behir may swallow multiple creatures and each must cut their own way out. A behir can breathe forth a bolt of lightning once every 10 rounds, dealing 7d6 points of damage to all in its path (20 ft. long x 5 ft. wide). Those struck may save vs. Dragon Breath for half damage.
