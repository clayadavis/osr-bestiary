.. title: Chelonian
.. slug: chelonian
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Chelonian
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 14, 17 Rear (13, 17 Rear)     |
+-----------------+-------------------------------+
| Hit Dice:       | 1                             |
+-----------------+-------------------------------+
| No. of Attacks: | 1 bite or weapon              |
+-----------------+-------------------------------+
| Damage:         | 1d6 bite or by weapon+special |
+-----------------+-------------------------------+
| Movement:       | 30' Swim 30'                  |
+-----------------+-------------------------------+
| No. Appearing:  | 1d8, Wild 5d8, Lair 5d8       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1 (+2 Poison saves)  |
+-----------------+-------------------------------+
| Morale:         | 8                             |
+-----------------+-------------------------------+
| Treasure Type:  | D                             |
+-----------------+-------------------------------+
| XP:             | 25                            |
+-----------------+-------------------------------+

Inhabiting rivers and swampy regions, a **Chelonian** is a race of reptilian humanoids bearing resemblance to long-necked snapping turtles. Is it normally content to remain within its own small societies, but on occasion a more adventurous individual can be found. A chelonian is protected by thick scaly skin, as well as a shell-like growth that covers their backside except for its thick tail. A chelonian stands about 5 feet tall. However, its neck can stretch out to make it up to 8 feet tall for very short periods.

Chelonians have their own language, and adventuring chelonians always know Common as well. Chelonians are natural enemies to **Lizard Men**, often competing fiercely for the same resources.

A chelonian's thick skin grants it a base AC of 13, and its back is especially tough (AC 17). Use these figures unless armor worn grants better AC.

A chelonian has a vicious bite, causing 1d6 points of damage. It can choose to either attack with a bite or by weapon, but when utilizing a weapon and roll a natural 20 on attack, it can roll for a bite attack as well. If the chelonian finds itself in a grapple (wrestling), the chelonian gets a free bite attack roll each round at +2 to hit. A chelonian is resistant to poisons, and has a +2 bonus on those saves.

A chelonian has a natural swim speed of 30 feet while unencumbered (it cannot swim while wearing armor or encumbered), and it can hold its breath twice as long as the normal rules state. Its underwater vision is also twice as effective as the other races. A chelonian has the ability to submerge with just its eyes and nostrils above the surface of water. When it is able to employ this maneuver, a chelonian can surprise others with a roll of 1-4 on 1d6.
