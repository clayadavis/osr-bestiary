.. title: Elemental, Metal*
.. slug: elemental-metal
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Elemental, Metal*
.. type: text
.. image:

+-----------------+------------+--------------+--------------+
|                 | **Staff**  | **Device**   | **Spell**    |
+=================+============+==============+==============+
| Armor Class:    | 18 ‡       | 20 ‡         | 22 ‡         |
+-----------------+------------+--------------+--------------+
| Hit Dice:       | 8*         | 12* (AB +10) | 16* (AB +12) |
+-----------------+------------+--------------+--------------+
| No. of Attacks: | 1 punch, stomp, or special               |
+-----------------+------------+--------------+--------------+
| Damage:         | 1d12       | 2d8          | 3d6          |
+-----------------+------------+--------------+--------------+
| Movement:       | 30'                                      |
+-----------------+------------------------------------------+
| No. Appearing:  | -- special --                            |
+-----------------+------------+--------------+--------------+
| Save As:        | Fighter: 8 | Fighter: 12  | Fighter: 16  |
+-----------------+------------+--------------+--------------+
| Morale:         | -- 10 --                                 |
+-----------------+------------------------------------------+
| Treasure Type:  | -- None --                               |
+-----------------+------------+--------------+--------------+
| XP:             | 945        | 1,975        | 3,385        |
+-----------------+------------+--------------+--------------+

A **Metal Elemental** resembles lithe figures made of molten metal. It is able to shape its extremities into cruel blades. Despite its appearance, a metal elemental is normally cool to the touch. Metal armor affords no protection against a metal elemental, and indeed it deals an additional 1d8 points of damage to creatures, vehicles, or structures that are made of or in direct contact with some form of metal. Lightning attacks deal double damage to it. A metal elemental cannot cross a body of water greater than its own height.
