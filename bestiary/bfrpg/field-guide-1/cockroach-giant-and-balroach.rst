.. title: Cockroach, Giant, and Balroach
.. slug: cockroach-giant-and-balroach
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Cockroach, Giant, and Balroach
.. type: text
.. image:

+-----------------+-------------------------+---------------------+
|                 | Giant                   | Balroach            |
+=================+=========================+=====================+
| Armor Class:    | 15                      | 18                  |
+-----------------+-------------------------+---------------------+
| Hit Dice:       | 1*                      | 5**                 |
+-----------------+-------------------------+---------------------+
| No. of Attacks: | 1 bite                  | 1 bite              |
+-----------------+-------------------------+---------------------+
| Damage:         | 1d4 bite                | 1d8 bite            |
+-----------------+-------------------------+---------------------+
| Movement:       | 50'                     | 40'                 |
+-----------------+-------------------------+---------------------+
| No. Appearing:  | 1d4, Wild 2d4, Lair 2d8 | 1, Wild 1, Lair 1d6 |
+-----------------+-------------------------+---------------------+
| Save As:        | Fighter: 1*             | Fighter: 5*         |
+-----------------+-------------------------+---------------------+
| Morale:         | 6                       | 9                   |
+-----------------+-------------------------+---------------------+
| Treasure Type:  | None                    | G                   |
+-----------------+-------------------------+---------------------+
| XP:             | 37                      | 450                 |
+-----------------+-------------------------+---------------------+

A **Giant Cockroach** is a massive and particularly disgusting version of the common cockroach. It can reach up to 2 feet in length (not including antennae) and weighs about 40 pounds. A giant cockroach feeds on decomposing material and will defend its nest and territory. While a giant cockroach saves as a 1st-level Fighter in most situations, it saves as a 10th-level Cleric vs. Poison, and is immune to most disease-based attacks.

The dreaded **Balroach** is a variety of giant cockroach larger than a horse. While it is an omnivore and scavenger rather than a predator, it will also fiercely defend its lair.

The balroach is immune to all poisons, diseases, or similar types of afflictions. Besides its powerful mandibulae, its disgusting appearance and horrible smell offers itself protection; any being that comes within 20 feet of a balroach must save vs. Poison or suffer from a severe nausea, causing a -4 penalty to all attack rolls, saving throws and ability checks, and a -20% penalty to all Thief abilities.
