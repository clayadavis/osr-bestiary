.. title: Dinosaur, Ankylosaurus
.. slug: dinosaur-ankylosaurus
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Dinosaur, Ankylosaurus
.. type: text
.. image:

+-----------------+------------------------------------+
| Armor Class:    | 21                                 |
+-----------------+------------------------------------+
| Hit Dice:       | 9 or 9* (for Paleocinthus) (AB +8) |
+-----------------+------------------------------------+
| No. of Attacks: | 1 tail                             |
+-----------------+------------------------------------+
| Damage:         | 3d6 tail                           |
+-----------------+------------------------------------+
| Movement:       | 30'                                |
+-----------------+------------------------------------+
| No. Appearing:  | 1d3, Wild 1d4+1                    |
+-----------------+------------------------------------+
| Save As:        | Fighter: 9                         |
+-----------------+------------------------------------+
| Morale:         | 8                                  |
+-----------------+------------------------------------+
| Treasure Type:  | None                               |
+-----------------+------------------------------------+
| XP:             | 1,075 (Paleocinthus 1,150)         |
+-----------------+------------------------------------+

The **Ankylosaurus** weighs about 8,000 pounds, most of this weight due to its armor plating, side spines, and great, knobby tail. If attacked or threatened, it lashes out with its tail, delivering blows of considerable force. A related species, **Paleocinthus**, has even more armor plating (AC 24) and a spiked tail (3d6 points of damage).
