.. title: Grimlock
.. slug: grimlock
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Grimlock
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 15                              |
+-----------------+---------------------------------+
| Hit Dice:       | 2                               |
+-----------------+---------------------------------+
| No. of Attacks: | 1 battleaxe                     |
+-----------------+---------------------------------+
| Damage:         | 1d8 battleaxe                   |
+-----------------+---------------------------------+
| Movement:       | 30'                             |
+-----------------+---------------------------------+
| No. Appearing:  | 1d4, Wild 1d10+10, Lair 1d6 x10 |
+-----------------+---------------------------------+
| Save As:        | Fighter: 2                      |
+-----------------+---------------------------------+
| Morale:         | 7                               |
+-----------------+---------------------------------+
| Treasure Type:  | D                               |
+-----------------+---------------------------------+
| XP:             | 75                              |
+-----------------+---------------------------------+

A **Grimlock** is a muscular humanoid with gray skin. It is blind, but its exceptional senses of smell and hearing allow it to notice foes nearby. As a result, it usually shuns ranged weapons and rushes to attack, brandishing stone battleaxes.

A grimlock can sense all foes within 40 feet as a sighted creature would. Beyond that range, treat all targets as having total concealment. A grimlock is susceptible to sound- and scent-based attacks, however, and is affected normally by loud noises, sonic spells (such as **silence 15'** **radius**) and overpowering odors. Negating a grimlock’s sense of smell or hearing reduces its ability to fight. If both of these senses are negated, a grimlock is effectively blinded. It is immune to gaze attacks, visual effects, illusions, and other attacks that rely on sight.
