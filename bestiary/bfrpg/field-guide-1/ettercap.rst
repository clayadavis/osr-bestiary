.. title: Ettercap
.. slug: ettercap
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Ettercap
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 6+1                     |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite + poison |
+-----------------+-------------------------+
| Damage:         | 1d3 claw, 1d8 bite      |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d2                     |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 500                     |
+-----------------+-------------------------+

An **Ettercap's** appearance is a cross between a grossly-bloated spider and a humanoid. It is often found in the company of 2-4 large spiders. An ettercap is about 6 feet tall, weighs about 200 pounds, and speaks Common.

An ettercap is not a brave creature, but its cunning traps often ensure that the enemy never draws a weapon. When an ettercap does engage its enemies, it attacks with its keen-edged claws and venomous bite. It usually will not come within melee reach of any foe that is still able to move. The poison of an ettercap paralyzes its foe on a missed saving throw vs. Poison. This paralysis will wear off naturally in 1d6+6 turns.

An ettercap can throw a web eight times per day to entangle enemies. To hit, the ettercap must make an attack roll against AC10 + the target's Dexterity modifier (and any magic modifier). If the ettercap hits, the target is entangled. An entangled creature takes a -2 penalty on attack rolls and a -4 penalty on Dexterity. The web has a maximum range of 50 feet and is effective against targets of up to Medium size. The web anchors the target in place, preventing any movement. An entangled creature can burst the web as per the **web** spell.

An ettercap can also create sheets of sticky webbing up to 60 square feet. They usually position these to snare flying creatures, but can also try to trap prey on the ground. Approaching creatures may stumble into it and become trapped. An ettercap can determine the exact location of any creature touching its web.
