.. title: Skeleton, Pitch
.. slug: skeleton-pitch
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Skeleton, Pitch
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 15                          |
+-----------------+-----------------------------+
| Hit Dice:       | 6*                          |
+-----------------+-----------------------------+
| No. of Attacks: | 1 punch or weapon + special |
+-----------------+-----------------------------+
| Damage:         | 1d6 or by weapon +special   |
+-----------------+-----------------------------+
| Movement:       | 40'                         |
+-----------------+-----------------------------+
| No. Appearing:  | 1d4                         |
+-----------------+-----------------------------+
| Save As:        | Fighter: 6                  |
+-----------------+-----------------------------+
| Morale:         | 12                          |
+-----------------+-----------------------------+
| Treasure Type:  | B                           |
+-----------------+-----------------------------+
| XP:             | 555                         |
+-----------------+-----------------------------+

A **Pitch Skeleton** is a skeletal undead that is covered in a black oily or tarry substance, giving it a slick and blackened appearance. Unlike other skeletons, a pitch skeleton has a low level of intelligence and evil intent; they will use basic strategies in combat, such as changing opponents after a successful strike so as to ignite as many of their enemies as possible.

When a pitch skeleton strikes in combat, a thick black substance is left behind which promptly ignites, causing 1d6 points of fire damage on the round following the successful attack. On the next round the fire does 1d4 points of damage, then 1d2 points, and finally a 1 point on the fifth round. Additional successful strikes from the pitch skeleton restarts this process at 1d6 points of damage on the next round.

Like other skeletons, a pitch skeleton takes only half damage from edged weapons, and only a single point from arrows, bolts, and sling stones (plus any magical bonus). A pitch skeleton is completely immune to fire-based attacks. As with all undead, it can be Turned by a Cleric (as a wraith), and is immune to **sleep**, **charm**, and **hold** magic.
