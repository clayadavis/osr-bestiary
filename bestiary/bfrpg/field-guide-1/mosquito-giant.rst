.. title: Mosquito, Giant
.. slug: mosquito-giant
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Mosquito, Giant
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 11                               |
+-----------------+----------------------------------+
| Hit Dice:       | 1d4*                             |
+-----------------+----------------------------------+
| No. of Attacks: | 1 bite                           |
+-----------------+----------------------------------+
| Damage:         | 1d3 bite + 1d3/round blood drain |
+-----------------+----------------------------------+
| Movement:       | 50'                              |
+-----------------+----------------------------------+
| No. Appearing:  | Wild 2d6x10                      |
+-----------------+----------------------------------+
| Save As:        | Fighter: 1                       |
+-----------------+----------------------------------+
| Morale:         | 9                                |
+-----------------+----------------------------------+
| Treasure Type:  | None                             |
+-----------------+----------------------------------+
| XP:             | 13                               |
+-----------------+----------------------------------+

**Giant Mosquitos** swarm victims by the dozen, with 1d6+1 attacking each living creature they encounter. A successful attack causes 1d3 points of damage and results in the insect attaching itself to the victim, draining 1d3 hp per round until the mosquito is sated (having caused damage at least equal to three times its own total hp) or killed. To aid in feeding, it injects an anti-coagulant; a victim will bleed another 1d3 hp for 1d3 rounds after feeding or killed. Any healing spell or potion will stop the bleeding, as will taking a round to bind the wound.
