.. title: Badger, Giant
.. slug: badger-giant
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Badger, Giant
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 17          |
+-----------------+-------------+
| Hit Dice:       | 10 (AB +9)  |
+-----------------+-------------+
| No. of Attacks: | 1 bite      |
+-----------------+-------------+
| Damage:         | 4d4 bite    |
+-----------------+-------------+
| Movement:       | 50'         |
+-----------------+-------------+
| No. Appearing:  | 1d4         |
+-----------------+-------------+
| Save As:        | Fighter: 10 |
+-----------------+-------------+
| Morale:         | 9           |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 1,300       |
+-----------------+-------------+

**Giant Badgers** are quite simply gigantic versions of normal badgers. They are squat and broad, and they are fast burrowers. A giant badger is sometimes domesticated by a giant, but even a tame one is untrustworthy and may attack its master.
