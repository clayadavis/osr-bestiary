.. title: Armorollo
.. slug: armorollo
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Armorollo
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 15                 |
+-----------------+--------------------+
| Hit Dice:       | 1                  |
+-----------------+--------------------+
| No. of Attacks: | 1 claw             |
+-----------------+--------------------+
| Damage:         | 1d4 claw           |
+-----------------+--------------------+
| Movement:       | 60'                |
+-----------------+--------------------+
| No. Appearing:  | 1d4+3, Wild 1d10+3 |
+-----------------+--------------------+
| Save As:        | Fighter: 1         |
+-----------------+--------------------+
| Morale:         | 7                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 25                 |
+-----------------+--------------------+

The **Armorollo** is most commonly found in the open grassland. When curled up it resembles a rock, as its top portion is covered in thick rock-colored plates. The armorollo is a pack creature and are never found in groups of less than 4. It moves by rolling across the ground at high speed, springing open at the last moment to attack with its sharp claws. The armorollo can also emit a large cloud of thick white smoke once per day (filling 30 cubic feet) that serves to conceal its movements. Lastly, the armorollo emits a piercing shriek that helps find its prey by following the echo.

Multiple armorollos will work together to attack. Some will stop to emit smoke, others will shriek, and the remainder attack through the smoke guided by the shriek. When fighting as a group, armorollos have a +3 attack bonus.
