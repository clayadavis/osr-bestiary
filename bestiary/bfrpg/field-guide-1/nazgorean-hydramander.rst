.. title: Nazgorean, Hydramander
.. slug: nazgorean-hydramander
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Nazgorean, Hydramander
.. type: text
.. image:

+-----------------+----------------------------------------------------------+
| Armor Class:    | 18 to 21                                                 |
+-----------------+----------------------------------------------------------+
| Hit Dice:       | 8 to 12 (AB +10)                                         |
+-----------------+----------------------------------------------------------+
| No. of Attacks: | 5 to 8 bites                                             |
+-----------------+----------------------------------------------------------+
| Damage:         | 2d6 bite                                                 |
+-----------------+----------------------------------------------------------+
| Movement:       | 20' (10')                                                |
+-----------------+----------------------------------------------------------+
| No. Appearing:  | 1, Lair 1                                                |
+-----------------+----------------------------------------------------------+
| Save As:        | Fighter: 8 to 12                                         |
+-----------------+----------------------------------------------------------+
| Morale:         | 11                                                       |
+-----------------+----------------------------------------------------------+
| Treasure Type:  | None                                                     |
+-----------------+----------------------------------------------------------+
| XP:             | 8 HD 825; 9 HD 1075; 10 HD 1,30011 HD 1,575; 12 HD 1,875 |
+-----------------+----------------------------------------------------------+

A **Hydramander** is a multi-headed creature from the dimension of Nazgor. The body of a hydramander is similar to the body of a hydra; however, the hydramander has no eyes. It depends on sound to locate prey (treat as Darkvision out to 60 feet). Thus, a silent opponent is as good as invisible to the monster.

The individual heads of a hydramander may be attacked; 12 points of damage must be dealt to disable each head.
