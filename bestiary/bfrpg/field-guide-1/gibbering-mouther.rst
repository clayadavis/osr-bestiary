.. title: Gibbering Mouther
.. slug: gibbering-mouther
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Gibbering Mouther
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 19                 |
+-----------------+--------------------+
| Hit Dice:       | 9** (AB +8)        |
+-----------------+--------------------+
| No. of Attacks: | 6 bites + special  |
+-----------------+--------------------+
| Damage:         | 1d6 bite + special |
+-----------------+--------------------+
| Movement:       | 10' Swim 20'       |
+-----------------+--------------------+
| No. Appearing:  | 1                  |
+-----------------+--------------------+
| Save As:        | Fighter: 9         |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 1225               |
+-----------------+--------------------+

A **Gibbering Mouther** is a horrible creature seemingly drawn from a lunatic’s nightmares. It has the fluid body of an amoeba, with eyes and toothy mouths constantly appearing and disappearing all over its body.

As soon as a mouther spots something edible, it begins a constant gibbering. All creatures (other than mouthers) within 60 feet must save vs. Paralysis or be affected as though by a **confusion** spell for 1d2 rounds. A gibbering mouther may fire a stream of spittle at one opponent within 30 feet. The mouther makes an attack against AC 10 + Dex bonus of the target; if it hits, it deals 1d4 points of acid damage, and the target must save vs. Poison or be blinded for 1d4 rounds. Any creature bit by 3 or more of its mouths will be engulfed on the next round. The mouther will be able to make 12 subsequent bite attacks on the engulfed creature. A swallowed creature can cut its way out by dealing 5 points of damage with a small edged weapon such as a dagger.
