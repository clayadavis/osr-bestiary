.. title: Otter (Common and Giant)
.. slug: otter-common-and-giant
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Otter (Common and Giant)
.. type: text
.. image:

+-----------------+--------------+----------------+
|                 | Common       | Giant          |
+=================+==============+================+
| Armor Class:    | 16           | 16 (see below) |
+-----------------+--------------+----------------+
| Hit Dice:       | ½ (1d4 hp)   | 5              |
+-----------------+--------------+----------------+
| No. of Attacks: | 1 bite       | 1 bite         |
+-----------------+--------------+----------------+
| Damage:         | 1d2 bite     | 2d6 bite       |
+-----------------+--------------+----------------+
| Movement:       | 30' Swim 50' | 30' Swim 50'   |
+-----------------+--------------+----------------+
| No. Appearing:  | 1d4+2        | 1d3+1          |
+-----------------+--------------+----------------+
| Save As:        | Fighter: 1   | Fighter: 5     |
+-----------------+--------------+----------------+
| Morale:         | 7            | 8              |
+-----------------+--------------+----------------+
| Treasure Type:  | None         | L              |
+-----------------+--------------+----------------+
| XP:             | 10           | 360            |
+-----------------+--------------+----------------+

An **Otter** is a small or medium-sized semi-aquatic mammal with a long body. They are fast, maneuverable swimmers, and are playful and social.

A **Giant Otter** is similar in most ways but is much larger, being 10 to 15 feet in length, with a vicious bite. It is fairly intelligent and sometimes has small valuables in its den.
