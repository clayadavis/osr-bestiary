.. title: Allip
.. slug: allip
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Allip
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 15                     |
+-----------------+------------------------+
| Hit Dice:       | 6**                    |
+-----------------+------------------------+
| No. of Attacks: | 1 touch + special      |
+-----------------+------------------------+
| Damage:         | energy drain (1 level) |
+-----------------+------------------------+
| Movement:       | Fly 30'                |
+-----------------+------------------------+
| No. Appearing:  | 1d4, Lair 1d6          |
+-----------------+------------------------+
| Save As:        | Fighter: 6             |
+-----------------+------------------------+
| Morale:         | 12                     |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 610                    |
+-----------------+------------------------+

An **Allip** is the spectral remains of someone driven to suicide by a madness that afflicted it in life. An allip is not entirely mindless, but it is quite insane.

The insane babbling of an allip causes all creatures within 60 feet to save vs. Paralysis or stop and stare blankly, unable to move, attack, or defend. Anyone who saves successfully will be unaffected by the allip's babble for 24 hours. An allip's touch does no direct physical damage, but instead drains one energy level; further, the allip regenerates 1d6 hit points for each level drained.

As with all undead, an allip can be Turned by a Cleric (as a mummy), and are immune to **sleep**, **charm** or **hold** spells. Anyone using mind-reading magic against one will suffer energy drain just as if touched. An allip is insanely fearless and always fights until destroyed.
