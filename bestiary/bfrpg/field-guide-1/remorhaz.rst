.. title: Remorhaz
.. slug: remorhaz
.. date: 2019-11-08 13:50:17.320434
.. tags:
.. description: Remorhaz
.. type: text
.. image:

+-----------------+---------------------------------------------------------------------------------------------+
| Armor Class:    | 20                                                                                          |
+-----------------+---------------------------------------------------------------------------------------------+
| Hit Dice:       | 7* to 14* (AB +11)                                                                          |
+-----------------+---------------------------------------------------------------------------------------------+
| No. of Attacks: | 1 bite                                                                                      |
+-----------------+---------------------------------------------------------------------------------------------+
| Damage:         | 4d6 bite (7-8hd), 5d6(9-12hd),6d6(13-14hd)                                                  |
+-----------------+---------------------------------------------------------------------------------------------+
| Movement:       | 30' Burrow 20'                                                                              |
+-----------------+---------------------------------------------------------------------------------------------+
| No. Appearing:  | 1                                                                                           |
+-----------------+---------------------------------------------------------------------------------------------+
| Save As:        | Fighter: by HD                                                                              |
+-----------------+---------------------------------------------------------------------------------------------+
| Morale:         | 11                                                                                          |
+-----------------+---------------------------------------------------------------------------------------------+
| Treasure Type:  | D (see below)                                                                               |
+-----------------+---------------------------------------------------------------------------------------------+
| XP:             | 7 HD 735; 8 HD 945; 9 HD 1150;10 HD 1,390; 11 HD 1,670;12 HD 1,975; 13 HD 2,285;14 HD 2,615 |
+-----------------+---------------------------------------------------------------------------------------------+

A **Remorhaz** is light blue in color but pulses with a reddish glow from the heat its body produces. It is a little more than 20 feet long and about 5 feet wide, and weighs about 10,000 pounds. A remorhaze hides under the snow and ice until it hears movement above it, attacking with surprise.

On an attack roll of 20, the remorhaz swallows prey of small or medium size whole. Once inside, the opponent takes 2d8 points of bludgeoning damage plus 8d6 points of fire damage per round from the remorhaz’s gizzard. A swallowed creature can cut its way out by using a light slashing or piercing weapon to deal 25 points of damage to the gizzard (AC 15). Once the victim exits, muscular action closes the hole; another swallowed opponent must cut his or her own way out.

An enraged remorhaz generates heat so intense that anything touching its body takes points of 8d6 fire damage. Creatures striking a remorhaz with natural or unarmed attacks are subject to this damage, but creatures striking with melee weapons are not. This heat can melt or char weapons; any non-magical weapon will be destroyed on a hit. Magical weapons are allowed a save vs. Dragon Breath to avoid destruction, adding any relevant attack bonus to the roll. Treasure recovered will probably be heat resistant, for obvious reasons.
