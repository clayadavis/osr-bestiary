.. title: Lizard, Giant Horned Chameleon
.. slug: lizard-giant-horned-chameleon
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Lizard, Giant Horned Chameleon
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 18                 |
+-----------------+--------------------+
| Hit Dice:       | 5                  |
+-----------------+--------------------+
| No. of Attacks: | 1 tongue or 1 bite |
+-----------------+--------------------+
| Damage:         | grab or 2d6        |
+-----------------+--------------------+
| Movement:       | 40' (10')          |
+-----------------+--------------------+
| No. Appearing:  | 1d3, Wild 1d6      |
+-----------------+--------------------+
| Save As:        | Fighter: 4         |
+-----------------+--------------------+
| Morale:         | 7                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 360                |
+-----------------+--------------------+

Giant horned chameleons average 8' to 10' in length. They are typically green, but can change color to blend into their surroundings, allowing them to surprise prey on 1-4 on 1d6. Giant horned chameleon have very long tongues, able to spring out up to 20' forward; the sticky muscular ball on the end grabs on to the chameleon's prey, and the chameleon then drags the prey to its mouth, doing bite damage automatically on the following round (and all subsequent rounds, until the chameleon is killed or fails a morale check, or until the prey is dead).

The horns of the giant horned chameleon are used only in mating rituals, not in combat.
