.. title: Pixie
.. slug: pixie
.. date: 2019-11-08 13:50:36.109736
.. tags: fey, flying, humanoid
.. description: Pixies are winged fairies often found in forested areas, although they are usually invisible.
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 17                            |
+-----------------+-------------------------------+
| Hit Dice:       | 1*                            |
+-----------------+-------------------------------+
| No. of Attacks: | 1 dagger                      |
+-----------------+-------------------------------+
| Damage:         | 1d4                           |
+-----------------+-------------------------------+
| Movement:       | 30' Fly 60'                   |
+-----------------+-------------------------------+
| No. Appearing:  | 2d4, Wild 10d4, Lair 10d4     |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1 (with Elf bonuses) |
+-----------------+-------------------------------+
| Morale:         | 7                             |
+-----------------+-------------------------------+
| Treasure Type:  | R, S                          |
+-----------------+-------------------------------+
| XP:             | 37                            |
+-----------------+-------------------------------+

Pixies are winged fairies often found in forested areas. They wear bright clothing, often including a cap and shoes with curled and pointed toes. A pixie stands about 2-1/2 feet tall and weighs about 30 pounds.

**A pixie can become invisible at will, as many times per day as it wishes, and can attack while remaining invisible.** Anyone attacking an invisible pixie does so with an attack penalty of -4 unless the attacker can somehow detect invisible creatures. Pixies may ambush their foes while invisible; if they do so, they surprise on 1-5 on 1d6.

Pixies are whimsical, enjoying nothing so much as a good joke or prank, especially at the expense of a “big person” like a human or demi-human.

Pixies can only fly for 3 turns maximum before requiring rest of at least one turn, during which time the pixie may walk at normal speed but may not fly.
