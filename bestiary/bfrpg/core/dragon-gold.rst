.. title: Dragon, Gold
.. slug: dragon-gold
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying
.. description: Dragon, Gold
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 22                              |
+-----------------+---------------------------------+
| Hit Dice:       | 11** (+9)                       |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 2d4/2d4/6d6 or breath/2d4       |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80' (20')               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4             |
+-----------------+---------------------------------+
| Save As:        | Fighter: 11 (as Hit Dice)       |
+-----------------+---------------------------------+
| Morale:         | 10                              |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 1,765                           |
+-----------------+---------------------------------+

Gold dragons usually parley before fighting. Those having spellcasting ability make heavy use of spells in combat. Among their favorites are **cloudkill**, **sleep**, and **slow**.

All gold dragons have the power to assume human form at will (in a manner equivalent to the spell **polymorph self**, but performed at will).

Unlike many other dragons, gold dragons are not cruel and do not seek to kill for pleasure. Many tales are told of gold dragons offering assistance to adventurers. They are, however, every bit as avaricious as any dragon; adventurers in need of gold need not bother asking for a loan.

Gold dragons are immune to all poisons, as well as normal fire. They suffer only half damage from magical fire.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
