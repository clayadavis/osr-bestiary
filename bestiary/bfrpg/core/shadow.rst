.. title: Shadow*
.. slug: shadow
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Shadow*
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 13 ‡                        |
+-----------------+-----------------------------+
| Hit Dice:       | 2*                          |
+-----------------+-----------------------------+
| No. of Attacks: | 1 touch                     |
+-----------------+-----------------------------+
| Damage:         | 1d4 + 1 point Strength loss |
+-----------------+-----------------------------+
| Movement:       | 30'                         |
+-----------------+-----------------------------+
| No. Appearing:  | 1d10, Wild 1d10, Lair 1d10  |
+-----------------+-----------------------------+
| Save As:        | Fighter: 2                  |
+-----------------+-----------------------------+
| Morale:         | 12                          |
+-----------------+-----------------------------+
| Treasure Type:  | F                           |
+-----------------+-----------------------------+
| XP:             | 100                         |
+-----------------+-----------------------------+

A shadow can be difficult to see in dark or gloomy areas but stands out starkly in brightly illuminated places. They lurk in dark places, waiting for living prey to happen by. A shadow is 5 to 6 feet tall and is weightless. Shadows cannot speak intelligibly. Despite their strange nature and appearance, shadows are not **undead** monsters, and thus cannot be Turned by a Cleric.

A shadow's attack does 1d4 damage (from cold) and drains 1 point of Strength from the victim. Victims reduced to 2 or fewer points of Strength collapse and become unable to move; those reduced to 0 Strength die and rise as shadows a day later (at nightfall). Otherwise, Strength points lost to shadows are recovered at a rate of 1 point per turn.

Due to their incorporeal nature, shadows cannot be harmed by non-magical weapons.
