.. title: Bear, Grizzly (or Brown)
.. slug: bear-grizzly-or-brown
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Bear, Grizzly (or Brown)
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 14                    |
+-----------------+-----------------------+
| Hit Dice:       | 5                     |
+-----------------+-----------------------+
| No. of Attacks: | 2 claws/1 bite + hug  |
+-----------------+-----------------------+
| Damage:         | 1d4/1d4/1d8 + 2d8 hug |
+-----------------+-----------------------+
| Movement:       | 40'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1, Wild 1d4, Lair 1d4 |
+-----------------+-----------------------+
| Save As:        | Fighter: 5            |
+-----------------+-----------------------+
| Morale:         | 8                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 360                   |
+-----------------+-----------------------+

These massive carnivores weigh more than 1,800 pounds and stand nearly 9 feet tall when they rear up on their hind legs. They are bad-tempered and territorial.
