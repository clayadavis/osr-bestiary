.. title: Golem, Stone*
.. slug: golem-stone
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Golem, Stone*
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 25 ‡          |
+-----------------+---------------+
| Hit Dice:       | 14** (+11)    |
+-----------------+---------------+
| No. of Attacks: | 1 + special   |
+-----------------+---------------+
| Damage:         | 3d8 + special |
+-----------------+---------------+
| Movement:       | 20' (10')     |
+-----------------+---------------+
| No. Appearing:  | 1             |
+-----------------+---------------+
| Save As:        | Fighter: 7    |
+-----------------+---------------+
| Morale:         | 12            |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 2,730         |
+-----------------+---------------+

This golem has a humanoid body made from stone. A stone golem is 9 feet tall and weighs around 2,000 pounds. Its body is frequently stylized to suit its creator. For example, it might look like it is wearing armor, with a particular symbol carved on the breastplate, or have designs worked into the stone of its limbs.

Stone golems are formidable opponents, being physically powerful and difficult to harm. A stone golem can use a **slow** effect, as the spell, once every other round; a save vs. Spells is allowed to resist. The effect has a range of 10 feet and a duration of 2d6 rounds.

A **stone to flesh** spell may be used to weaken the monster. The spell does not actually change the golem’s structure, but for one full round after being affected, the golem is vulnerable to normal weapons. The stone golem is allowed a save vs. Spells to resist this effect.
