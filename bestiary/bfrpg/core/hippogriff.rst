.. title: Hippogriff
.. slug: hippogriff
.. date: 2019-11-08 13:50:36.109736
.. tags: flying, monstrosity
.. description: Hippogriff
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 15                       |
+-----------------+--------------------------+
| Hit Dice:       | 3                        |
+-----------------+--------------------------+
| No. of Attacks: | 2 claws/1 bite           |
+-----------------+--------------------------+
| Damage:         | 1d6/1d6/1d10             |
+-----------------+--------------------------+
| Movement:       | 60' (10') Fly 120' (10') |
+-----------------+--------------------------+
| No. Appearing:  | Wild 2d8                 |
+-----------------+--------------------------+
| Save As:        | Fighter: 3               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 145                      |
+-----------------+--------------------------+

Hippogriffs resemble large ying horses with the forefront of a bird of prey. A typical hippogriff is 9 feet long, has a wingspan of 20 feet, and weighs 1,000 pounds.

A hippogriff avoids the territories and civilizations of other creatures, dwelling in extreme altitudes. **Griffons** sometimes prey upon them, and hippogriffs will generally attack griffons on sight if they have a numerical advantage.

Hippogriffs are omnivorous, entering combat only as defense, save for those times a griffon is met. They are prized as flying mounts since, unlike griffons, they are relatively safe around horses; note that it is still necessary to raise one in captivity in order to use it as a mount. A light load for a hippogriff is up to 400 pounds; a heavy load, up to 900 pounds.
