.. title: Stirge
.. slug: stirge
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying
.. description: Stirge
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 13                          |
+-----------------+-----------------------------+
| Hit Dice:       | 1*                          |
+-----------------+-----------------------------+
| No. of Attacks: | 1 bite                      |
+-----------------+-----------------------------+
| Damage:         | 1d4 + 1d4/round blood drain |
+-----------------+-----------------------------+
| Movement:       | 10' Fly 60'                 |
+-----------------+-----------------------------+
| No. Appearing:  | 1d10, Wild 3d12, Lair 3d12  |
+-----------------+-----------------------------+
| Save As:        | Fighter: 1                  |
+-----------------+-----------------------------+
| Morale:         | 9                           |
+-----------------+-----------------------------+
| Treasure Type:  | D                           |
+-----------------+-----------------------------+
| XP:             | 37                          |
+-----------------+-----------------------------+

A stirge’s coloration ranges from rust-red to reddish-brown, with a dirty yellow underside. The proboscis is pink at the tip, fading to gray at its base. A stirge’s body is about 1 foot long, with a wingspan of about 2 feet. It weighs about 1 pound.

If a stirge hits a creature, it attaches and drains blood equal to 1d4 damage per round (on its Initiative). The creature can only be removed by killing it; any attack on the creature receives an attack bonus of +2, but any attack that misses hits the victim instead.
