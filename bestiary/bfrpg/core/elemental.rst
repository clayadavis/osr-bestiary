.. title: Elemental*
.. slug: elemental
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental
.. description: Elemental*
.. type: text
.. image:

Elementals are incarnations of the elements that compose existence.

It is possible to summon an elemental by one of three means: By the use of a *staff*, or of a *device*, or by casting a *spell*. For each elemental type, separate statistics are provided for each of these three categories.

Due to their highly magical nature, elementals cannot be harmed by non-magical weapons.
