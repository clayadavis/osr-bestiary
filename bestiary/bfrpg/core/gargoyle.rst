.. title: Gargoyle*
.. slug: gargoyle
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Gargoyle*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 ‡                    |
+-----------------+-------------------------+
| Hit Dice:       | 4**                     |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite/1 horn   |
+-----------------+-------------------------+
| Damage:         | 1d4/1d4/1d6/1d4         |
+-----------------+-------------------------+
| Movement:       | 30' Fly 50' (15')       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 11                      |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 320                     |
+-----------------+-------------------------+

Gargoyles are demonic-looking winged humanoid monsters with gray stone-like skin. They are often mistaken for winged stone statues, for they can remain still indefinitely without moving. Gargoyles use this disguise to ambush their foes, surprising on 1-4 on 1d6 if their foes do not otherwise suspect them. They are cruel monsters, inflicting pain on other creatures for the sole purpose of enjoyment.

Gargoyles require no food, water, or air. Due to their highly magical nature, they can only be harmed by magical weapons.
