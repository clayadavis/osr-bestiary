.. title: Water Termite, Giant
.. slug: water-termite-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, insect
.. description: Water Termite, Giant
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 13                            |
+-----------------+-------------------------------+
| Hit Dice:       | 1 to 4                        |
+-----------------+-------------------------------+
| No. of Attacks: | 1 spray                       |
+-----------------+-------------------------------+
| Damage:         | Stun                          |
+-----------------+-------------------------------+
| Movement:       | Swim 30'                      |
+-----------------+-------------------------------+
| No. Appearing:  | Wild 1d4                      |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1 to 4 (as Hit Dice) |
+-----------------+-------------------------------+
| Morale:         | 10                            |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 25 - 240                      |
+-----------------+-------------------------------+

Giant water termites vary from 1' to 5' in length. They attack using a noxious spray with a range of 5' which stuns the target for a full turn on a hit; a save vs. Poison is allowed to avoid the effect. A stunned character can neither move nor take action for the remainder of the current round and all of the next one.

However, the primary concern regarding these monsters is the damage they can do to boats and ships. Each creature can do 2d4 points of damage to a ship's hull per round (no roll required) for a number of rounds equal to 1d4 plus the creature's hit dice total; after this time, the monster is full. They eat noisily.

These creatures are found in fresh and salt water as well as in swamps. The freshwater variety tend to be smaller, 1-2 hit dice, the saltwater variety 3-4 hit dice, and those found in swamps range from 2-3 hit dice.
