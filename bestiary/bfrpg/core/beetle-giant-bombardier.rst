.. title: Beetle, Giant Bombardier
.. slug: beetle-giant-bombardier
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Beetle, Giant Bombardier
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 2*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite + special        |
+-----------------+-------------------------+
| Damage:         | 1d6 + special           |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d8, Wild 2d6, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 100                     |
+-----------------+-------------------------+

Giant bombardier beetles have red head and thorax sections and black abdomens. They are 3 to 4 feet long. In combat, a giant bombardier beetle bites opponents in front of it, and sprays a cone of very hot and noxious gases from a nozzle in the rearmost tip of the abdomen. This toxic blast causes 2d6 points of damage to all within a cone 10' long and 10' wide at the far end (a save vs. Death Ray for half damage is allowed). A giant bombardier beetle can use this spray attack up to five times per day, but no more often than once per three rounds. Faced with opponents attacking from just one direction, a giant bombardier beetle may choose to turn away and use the spray attack rather than biting.

Giant bombardier beetles, like most beetles, have more or less the same visual acuity in all directions, and thus suffer no penalty to Armor Class when attacked from behind.
