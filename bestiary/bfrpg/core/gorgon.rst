.. title: Gorgon
.. slug: gorgon
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Gorgon
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 19                   |
+-----------------+----------------------+
| Hit Dice:       | 8*                   |
+-----------------+----------------------+
| No. of Attacks: | 1 gore or 1 breath   |
+-----------------+----------------------+
| Damage:         | 2d6 or petrification |
+-----------------+----------------------+
| Movement:       | 40' (10')            |
+-----------------+----------------------+
| No. Appearing:  | Wild 1d4             |
+-----------------+----------------------+
| Save As:        | Fighter: 8           |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 945                  |
+-----------------+----------------------+

Gorgons are magical monsters resembling bulls made of iron. Their breath can turn living creatures to stone; it covers an area 60' long by 10' wide, and can be used as many times per day as the monster has hit dice, but no more often than every other round. A save vs. Petrification is allowed to resist.

A typical gorgon stands over 6 feet tall at the shoulder, measures 8 feet from snout to tail, and weighs about 4,000 pounds. Gorgons are nothing if not aggressive. They attack intruders on sight, attempting to gore or petrify them. There is no way to calm these furious creatures, and they are impossible to domesticate.
