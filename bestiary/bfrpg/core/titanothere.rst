.. title: Titanothere
.. slug: titanothere
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Titanothere
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 15                  |
+-----------------+---------------------+
| Hit Dice:       | 12 (+10)            |
+-----------------+---------------------+
| No. of Attacks: | 1 butt or 1 trample |
+-----------------+---------------------+
| Damage:         | 2d6 or 3d8          |
+-----------------+---------------------+
| Movement:       | 40' (10')           |
+-----------------+---------------------+
| No. Appearing:  | Wild 1d6            |
+-----------------+---------------------+
| Save As:        | Fighter: 8          |
+-----------------+---------------------+
| Morale:         | 7                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 1,875               |
+-----------------+---------------------+

A titanothere is a huge prehistoric animal that resembles the rhinoceros; adults average 10' tall and 13' long. They have large, forked horns rather than the pointed horns of rhinos. Like rhinos, they are herd animals, and males aggressively defend the herd; females only enter combat if the male(s) are defeated or the attackers are very numerous. If a single titanothere is encountered, it will be a rogue male; they are bad tempered and prone to attacking smaller creatures that enter their territory.
