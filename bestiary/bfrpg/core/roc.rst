.. title: Roc
.. slug: roc
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying
.. description: Roc
.. type: text
.. image:

+-----------------+-------------+--------------+------------------+
|                 | **Normal**  | **Large**    | **Giant**        |
+=================+=============+==============+==================+
| Armor Class:    | 18          | 18           | 18               |
+-----------------+-------------+--------------+------------------+
| Hit Dice:       | 6           | 12 (+10)     | 32 (+16)         |
+-----------------+-------------+--------------+------------------+
| No. of Attacks: | 2 claws/1 bite                                |
+-----------------+-------------+--------------+------------------+
| Damage:         | 1d6/1d6/2d6 | 1d8/1d8/2d10 | 3d6/3d6/6d6      |
+-----------------+-------------+--------------+------------------+
| Movement:       | 20' Fly 160' (10')                            |
+-----------------+-------------+--------------+------------------+
| No. Appearing:  | Wild 1d12   | Wild 1d8     | Wild 1           |
+-----------------+-------------+--------------+------------------+
| Save As:        | Fighter: 6  | Fighter: 12  | Fighter: 20at +5 |
+-----------------+-------------+--------------+------------------+
| Morale:         | 8           | 9            | 10               |
+-----------------+-------------+--------------+------------------+
| Treasure Type:  | I           | I            | I                |
+-----------------+-------------+--------------+------------------+
| XP:             | 500         | 1,875        | 14,250           |
+-----------------+-------------+--------------+------------------+

Rocs are birds similar to eagles, but even a “normal” roc is huge, being about 9 feet long and having a wingspan of 24 feet. Large rocs are about 18 feet long and have wingspans of around 48 feet; giant rocs average 30 feet long and have massive wingspans of around 80 feet. A roc’s plumage is either dark brown or golden from head to tail. Like most birds, the males have the brighter plumage, with females being duller in color and thus more easily hidden (if anything so large can even be hidden, that is).

A light load for a normal roc is 150 pounds, while a heavy load is 300 pounds. Obviously only the smallest characters can hope to ride upon a normal roc. For a large roc, a light load is up to 600 pounds and a heavy load up to 1200. Giant rocs can easily lift up to 3000 pounds, and are heavily loaded when carrying up to 6000 pounds. Tales of giant rocs carrying off full-grown elephants are somewhat exaggerated, but note that a young elephant would be reasonable prey for these monstrous birds.

A roc attacks from the air, swooping earthward to snatch prey in its powerful talons and carry it off for itself and its young to devour. Any successful hit with both claw (talon) attacks against a single creature results in that creature being carried off, unless of course the creature is too large for the roc to carry. While being carried, the victim will not be further attacked, so as to be as “fresh” as possible when given to the hatchlings (or consumed by the roc itself if it is solitary). A solitary roc is typically hunting and will attack any man-sized or larger creature that appears edible. A mated pair of rocs attack in concert, fighting to the death to defend their nests or hatchlings.
