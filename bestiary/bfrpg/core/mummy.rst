.. title: Mummy*
.. slug: mummy
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Mummy*
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 17 ‡ (see below)  |
+-----------------+-------------------+
| Hit Dice:       | 5**               |
+-----------------+-------------------+
| No. of Attacks: | 1 touch + disease |
+-----------------+-------------------+
| Damage:         | 1d12 + disease    |
+-----------------+-------------------+
| Movement:       | 20'               |
+-----------------+-------------------+
| No. Appearing:  | 1d4, Lair 1d12    |
+-----------------+-------------------+
| Save As:        | Fighter: 5        |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | D                 |
+-----------------+-------------------+
| XP:             | 450               |
+-----------------+-------------------+

Mummies are **undead** monsters, linen-wrapped preserved corpses animated through the auspices of dark desert gods best forgotten. Most mummies are 5 to 6 feet tall and weigh about 120 pounds.

As they are undead, mummies are immune to **sleep**, **charm** and **hold** magic. They can only be injured by spells, fire, or magical weapons; furthermore, magic weapons do only half damage, while any sort of fire-based attack does double damage. Those injured by mummy attacks will contract **mummy rot**, a disease that prevents normal or magical healing; a **cure disease** spell must be applied to the victim before he or she may again regain hit points.
