.. title: Golem, Iron*
.. slug: golem-iron
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Golem, Iron*
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 25 ‡           |
+-----------------+----------------+
| Hit Dice:       | 17** (+12)     |
+-----------------+----------------+
| No. of Attacks: | 1 + special    |
+-----------------+----------------+
| Damage:         | 4d10 + special |
+-----------------+----------------+
| Movement:       | 20' (10')      |
+-----------------+----------------+
| No. Appearing:  | 1              |
+-----------------+----------------+
| Save As:        | Fighter: 9     |
+-----------------+----------------+
| Morale:         | 12             |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 3,890          |
+-----------------+----------------+

This golem has a humanoid body made from iron. An iron golem can be fashioned in any manner, just like a stone golem (see below), although it almost always displays armor of some sort. Its features are much smoother than those of a stone golem. Iron golems sometimes carry a short sword in one hand. An iron golem is 12 feet tall and weighs about 5,000 pounds. An iron golem cannot speak or make any vocal noise, nor does it have any distinguishable odor. It moves with a ponderous but smooth gait. Each step causes the floor to tremble unless it is on a thick, solid foundation.

Iron golems can exhale a cloud of poisonous gas which fills a 10-foot cube and persists for 1 round. Those within the area of effect must save vs. Dragon Breath or die. This ability can be used up to 3 times per day.

A magical attack that deals electricity damage slows an iron golem (as the **slow** spell) for 3 rounds, with no saving throw. A magical attack that deals fire damage breaks any slow effect on the golem and heals 1 point of damage for each 3 full points of damage the attack would otherwise deal. If the amount of healing would cause the golem to exceed its full normal hit points, the excess is ignored. For example, an iron golem hit by a fireball gains back 6 hit points if the damage total is 19 points. An iron golem is affected normally by rust attacks, such as that of a rust monster, suffering 2d6 points of damage for each hit (with no saving throw normally allowed).
