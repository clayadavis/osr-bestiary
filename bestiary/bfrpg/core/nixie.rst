.. title: Nixie
.. slug: nixie
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, fey
.. description: Nixie
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 16                   |
+-----------------+----------------------+
| Hit Dice:       | 1*                   |
+-----------------+----------------------+
| No. of Attacks: | 1 dagger             |
+-----------------+----------------------+
| Damage:         | 1d4                  |
+-----------------+----------------------+
| Movement:       | 40' Swim 40'         |
+-----------------+----------------------+
| No. Appearing:  | Wild 2d20, Lair 2d20 |
+-----------------+----------------------+
| Save As:        | Fighter: 2           |
+-----------------+----------------------+
| Morale:         | 6                    |
+-----------------+----------------------+
| Treasure Type:  | B                    |
+-----------------+----------------------+
| XP:             | 37                   |
+-----------------+----------------------+

Nixies are small water fairies. As far as anyone knows, all nixies are female. Most nixies are slim and comely, with lightly scaled, pale green skin and dark green hair. They often twine shells and pearl strings in their hair and dress in wraps woven from colorful seaweed. Nixies prefer not to leave their lakes. A nixie stands about 4 feet tall and weighs about 45 pounds.

Ten or more nixies can work together to cast a powerful charm (similar to **charm person**). The charm lasts one year (unless dispelled). A save vs. Spells is allowed to resist. Each nixie can cast **water breathing** once per day, with a duration of one day. Finally, a group of nixies will often have a school of giant bass living nearby who can be called to their aid (see **Fish, Giant Bass** for details).

Nixies are fey creatures, and thus unpredictable. However, they are rarely malicious, attacking only when they feel threatened.
