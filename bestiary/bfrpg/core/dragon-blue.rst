.. title: Dragon, Blue
.. slug: dragon-blue
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying
.. description: Dragon, Blue
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 20                              |
+-----------------+---------------------------------+
| Hit Dice:       | 9** (+8)                        |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 1d8/1d8/3d8 or breath/1d8       |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80' (15')               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4             |
+-----------------+---------------------------------+
| Save As:        | Fighter: 9 (as Hit Dice)        |
+-----------------+---------------------------------+
| Morale:         | 9                               |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 1,225                           |
+-----------------+---------------------------------+

Blue dragons love to soar in the hot desert air, usually flying in the daytime when temperatures are highest. Some nearly match the color of the desert sky and use this coloration to their advantage. Their vibrant color makes blue dragons easy to spot in barren desert surroundings. However, they often burrow into the sand so only part of their heads are exposed, waiting until opponents come within 100 feet to spring out and attack (surprising on a roll of 1-4 on 1d6 in this case).

Blue dragons lair in vast underground caverns, where they also store their treasure. Although they collect anything that looks valuable, they are most fond of gems, especially sapphires. Blue dragons are evil monsters, though not so fierce as red dragons. They particularly enjoy tricking intelligent prey into entering their lairs or passing by their hiding places to be ambushed and killed; usually one member of a party attacked by a blue dragon will be left alive for a while, and the dragon will play with that person as a cat plays with a mouse.

Blue dragons are immune to normal lightning, and suffer only half damage from magical lightning.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
