.. title: Cockatrice
.. slug: cockatrice
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Cockatrice
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 5**                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 beak + special        |
+-----------------+-------------------------+
| Damage:         | 1d6 + petrification     |
+-----------------+-------------------------+
| Movement:       | 30' Fly 60' (10')       |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d8, Lair 1d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 5              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | D                       |
+-----------------+-------------------------+
| XP:             | 450                     |
+-----------------+-------------------------+

A cockatrice is a strange creature, appearing to be a chicken (hen or rooster) with a long serpentine neck and tail; the neck is topped by a more or less normal looking chicken head.

A male cockatrice has wattles and a comb, just like a rooster. Females, much rarer than males, differ only in that they have no wattles or comb. A cockatrice weighs about 25 pounds. A cockatrice is no more intelligent than any animal, but they are bad-tempered and prone to attack if disturbed.

Anyone touched by a cockatrice, or who touches one (even if gloved), must save vs. Petrification or be turned to stone.
