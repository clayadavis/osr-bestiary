.. title: Dryad
.. slug: dryad
.. date: 2019-11-08 13:50:36.109736
.. tags: fey
.. description: Dryad
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 15            |
+-----------------+---------------+
| Hit Dice:       | 2*            |
+-----------------+---------------+
| No. of Attacks: | 1             |
+-----------------+---------------+
| Damage:         | 1d4           |
+-----------------+---------------+
| Movement:       | 40'           |
+-----------------+---------------+
| No. Appearing:  | Lair 1d6      |
+-----------------+---------------+
| Save As:        | Magic-User: 4 |
+-----------------+---------------+
| Morale:         | 6             |
+-----------------+---------------+
| Treasure Type:  | D             |
+-----------------+---------------+
| XP:             | 100           |
+-----------------+---------------+

Dryads are female nature spirits; each is mystically bound to a single, enormous oak tree and must never stray more than 300 yards from it. Any who do become ill and die within 4d6 hours. A dryad’s oak does not radiate magic. A dryad lives as long as her tree, and dies when the tree dies; likewise, if the dryad is killed, her tree dies also.

A dryad’s delicate features are much like a female elf's, though her flesh is like bark or fine wood, and her hair is like a canopy of leaves that changes color with the seasons. Although they are generally solitary, up to seven dryads have been encountered in one place on rare occasions.

Shy, intelligent, and resolute, dryads are as elusive as they are alluring -- they avoid physical combat and are rarely seen unless they wish to be. If threatened, or in need of an ally, a dryad can charm (as the spell **charm person**), attempting to gain control of the attacker(s) who could help the most against the rest. Any attack on her tree, however, provokes the dryad into a frenzied defense.
