.. title: Boar
.. slug: boar
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Boar
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 3          |
+-----------------+------------+
| No. of Attacks: | 1 tusk     |
+-----------------+------------+
| Damage:         | 2d4        |
+-----------------+------------+
| Movement:       | 50' (10')  |
+-----------------+------------+
| No. Appearing:  | Wild 1d6   |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 9          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 145        |
+-----------------+------------+

Though not carnivores, these wild swine are bad-tempered and usually charge anyone who disturbs them. Note that “boar” refers specifically to the male of the species, but females are equally large and fierce.

A boar is covered in coarse, grayish-black fur. Adults are about 4 feet long and 3 feet high at the shoulder.
