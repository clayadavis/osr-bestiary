.. title: Rust Monster*
.. slug: rust-monster
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Rust Monster*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 18         |
+-----------------+------------+
| Hit Dice:       | 5*         |
+-----------------+------------+
| No. of Attacks: | 1 antenna  |
+-----------------+------------+
| Damage:         | special    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1d4        |
+-----------------+------------+
| Save As:        | Fighter: 5 |
+-----------------+------------+
| Morale:         | 7          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 405        |
+-----------------+------------+

The hide of these creatures varies in color from a yellowish tan underside to a rust-red upper back. A rust monster’s prehensile antennae can rust metals on contact. The typical rust monster measures 5 feet long and 3 feet high, weighing 200 pounds.

A rust monster's touch transforms metal objects into rust (or verdigris, or other oxides as appropriate). Non-magical metal attacked by a rust monster, or that touches the monster (such as a sword used to attack it), is instantly ruined. A non-magical metal weapon used to attack the monster does half damage before being destroyed. Magic weapons or armor lose one “plus” each time they make contact with the rust monster; this loss is permanent.

The metal oxides created by this monster are its food; a substantial amount of metal dropped in its path may cause it to cease pursuit of metal-armored characters. Use a morale check to determine this.
