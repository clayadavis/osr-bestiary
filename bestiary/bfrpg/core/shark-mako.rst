.. title: Shark, Mako
.. slug: shark-mako
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Shark, Mako
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 4          |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 2d6        |
+-----------------+------------+
| Movement:       | Swim 80'   |
+-----------------+------------+
| No. Appearing:  | Wild 2d6   |
+-----------------+------------+
| Save As:        | Fighter: 4 |
+-----------------+------------+
| Morale:         | 7          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 240        |
+-----------------+------------+

Mako sharks are fast-moving predators found in temperate and tropical seas. They average 9' to 13' in length and weigh up to 1,750 pounds. Mako sharks are known for their ability to leap out of the water; they are able to leap up to 20' in the air.
