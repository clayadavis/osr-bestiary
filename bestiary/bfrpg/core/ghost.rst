.. title: Ghost*
.. slug: ghost
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Ghost*
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 20 ‡           |
+-----------------+----------------+
| Hit Dice:       | 10* (+9)       |
+-----------------+----------------+
| No. of Attacks: | 1 touch/1 gaze |
+-----------------+----------------+
| Damage:         | 1d8 + special  |
+-----------------+----------------+
| Movement:       | 30'            |
+-----------------+----------------+
| No. Appearing:  | 1              |
+-----------------+----------------+
| Save As:        | Fighter: 10    |
+-----------------+----------------+
| Morale:         | 10             |
+-----------------+----------------+
| Treasure Type:  | E, N, O        |
+-----------------+----------------+
| XP:             | 1,390          |
+-----------------+----------------+

Ghosts are the spectral remnants of intelligent beings who, for one reason or another, cannot rest easily in their graves. A ghost normally resembles the form it had in life, but sometimes the spiritual form is altered. For instance, the ghost of someone who believed he or she was evil might look a bit demonic. Because they are incorporeal, ghosts may be hit only by magical weapons.

Seeing a ghost is so terrible that the victim must save vs. Spells or flee for 2d6 rounds. A character or creature who successfully saves vs. a given ghost's **fear** attack may not be so affected by that ghost again, but of course may still be affected by another.

A ghost that hits a living target with its touch attack does 1d8 points of damage, and at the same time regenerates the same number of hit points. In addition, the victim loses 1 Constitution point. Elves and dwarves (and other long-lived creatures such as dragons) are allowed a saving throw vs. Death Ray to resist this effect, which must be rolled on each hit. Characters who lose Constitution appear to have aged. If a ghost is fighting a living creature which does not have a Constitution score, the GM should assign whatever score he or she sees fit.

Lost Constitution can be regained at a rate of one point per casting of **restoration**; nothing else (except a **wish**) can restore Constitution lost to a ghost. If a character's Constitution falls to 0, he or she dies permanently and cannot be **raised** (but still may be **reincarnated**).

Once per turn, a ghost can use **telekinesis** (as the spell) as if it were a 10th level Magic-User.

Instead of attacking, a ghost may attempt to possess a living creature. This ability is similar to a **magic jar** spell (as if cast by a 10th level Magic-User), except that it does not require a receptacle. To use this ability, the ghost must be able to move into the target (so it is possible to outrun it). The target can resist the attack with a successful save vs. Spells. A creature that successfully saves is immune to being possessed by that ghost for 24 hours. If the save fails, the ghost enters the target's body and controls it; control may be maintained until the ghost chooses to leave the victim's body, or until it is driven out by means of a **remove curse** spell. While it is possessing a living creature, a ghost may not use any of its special abilities.
