.. title: Green Slime*
.. slug: green-slime
.. date: 2019-11-08 13:50:36.109736
.. tags: ooze
.. description: Green Slime*
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | hit only by fire or cold |
+-----------------+--------------------------+
| Hit Dice:       | 2**                      |
+-----------------+--------------------------+
| No. of Attacks: | 1 special                |
+-----------------+--------------------------+
| Damage:         | special                  |
+-----------------+--------------------------+
| Movement:       | 1'                       |
+-----------------+--------------------------+
| No. Appearing:  | 1                        |
+-----------------+--------------------------+
| Save As:        | Fighter: 2               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 125                      |
+-----------------+--------------------------+

Green slime devours flesh and organic materials on contact and is even capable of dissolving metal given enough time. Bright green, wet, and sticky, it clings to walls, floors, and ceilings in patches, reproducing as it consumes organic matter. It drops from walls and ceilings when it detects movement (and possible food) below. Green slime cannot grow in sunlight; even the indirect sunlight of a dense forest will stunt it and prevent it from spreading, and direct sunlight will kill green slime outright within a turn.

On the first round of contact, the slime can be scraped off a creature (most likely destroying the scraping device), but after that it must be frozen, burned, or cut away (dealing the same damage to both the victim and the slime). A **cure disease** spell will destroy a patch of green slime. It does not harm stone or enchanted metal, but can dissolve normal metal or enchanted wood in a turn and normal wood in 2d4 rounds.

If not destroyed or scraped off within 6+1d4 rounds, the victim will be completely transformed into green slime; such a character or creature cannot be retrieved by any magic short of a **wish**.
