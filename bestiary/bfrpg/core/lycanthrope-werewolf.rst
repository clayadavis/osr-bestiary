.. title: Lycanthrope, Werewolf*
.. slug: lycanthrope-werewolf
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid, shapechanger
.. description: Lycanthrope, Werewolf*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 2d4                     |
+-----------------+-------------------------+
| Movement:       | 60' Human Form 40'      |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d6, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

Werewolves in human form have no distinguishing traits. They may be found anywhere humans are found. They are ferocious predators, equally willing to eat animal or human flesh.
