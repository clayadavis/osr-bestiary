.. title: Ant, Giant
.. slug: ant-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Ant, Giant
.. type: text
.. image:

+-----------------+---------------------------------------+
| Armor Class:    | 17                                    |
+-----------------+---------------------------------------+
| Hit Dice:       | 4                                     |
+-----------------+---------------------------------------+
| No. of Attacks: | 1 bite                                |
+-----------------+---------------------------------------+
| Damage:         | 2d6                                   |
+-----------------+---------------------------------------+
| Movement:       | 60' (10')                             |
+-----------------+---------------------------------------+
| No. Appearing:  | 2d6, Wild 2d6, Lair 4d6               |
+-----------------+---------------------------------------+
| Save As:        | Fighter: 4                            |
+-----------------+---------------------------------------+
| Morale:         | 7 on first sighting, 12 after engaged |
+-----------------+---------------------------------------+
| Treasure Type:  | U or special                          |
+-----------------+---------------------------------------+
| XP:             | 240                                   |
+-----------------+---------------------------------------+

Giant ants are among the hardiest and most adaptable vermin. Soldiers and workers are about 5 to 6 feet long, while queens can grow to a length of 9 feet. Giant ants may be red or black; there is no statistical difference between them. Though relatively shy when first encountered, once combat begins they will fight to the death. They are known to collect shiny things, and so will sometimes have a small amount of treasure in their lair.

Giant ants may occasionally mine shiny metals such as gold or silver; one in three (1-2 on 1d6) giant ant lairs will contain 1d100 x 1d100 gp value in relatively pure nuggets.
