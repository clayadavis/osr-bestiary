.. title: Dragon, Black
.. slug: dragon-black
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying
.. description: Dragon, Black
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 18                              |
+-----------------+---------------------------------+
| Hit Dice:       | 7**                             |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 1d6/1d6/2d10 or breath/1d6      |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80' (15')               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4             |
+-----------------+---------------------------------+
| Save As:        | Fighter: 7 (as Hit Dice)        |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 800                             |
+-----------------+---------------------------------+

Black dragons prefer to ambush their targets, using their surroundings as cover. When fighting in heavily forested swamps and marshes, they try to stay in the water or on the ground; trees and leafy canopies limit their aerial maneuverability. When outmatched, a black dragon attempts to fly out of sight, so as not to leave tracks, and hide in a deep pond or bog. Black dragons are more cruel than white dragons, but are still motivated mostly by the urge to live, breed and collect valuable items.

Black dragons often choose to hide underwater, leaving only part of the head above the waterline, and leap up suddenly when prey comes within 100' (surprising on a roll of 1-4 on 1d6 in this case).

Black dragons are immune to all forms of acid. A black dragon may hold its breath up to three turns while lying in wait underwater.

**Black Dragon Age Table**

+-----------------+-----+-----+-----+------+------+------+------+
| Age Category    | 1   | 2   | 3   | 4    | 5    | 6    | 7    |
+-----------------+-----+-----+-----+------+------+------+------+
| Hit Dice        | 4   | 5   | 6   | 7    | 8    | 9    | 10   |
+-----------------+-----+-----+-----+------+------+------+------+
| Attack Bonus    | +4  | +5  | +6  | +7   | +8   | +8   | +9   |
+-----------------+-----+-----+-----+------+------+------+------+
| Breath Weapon   | Acid (Line)                                 |
+-----------------+-----+-----+-----+------+------+------+------+
| Length          | -   | 70' | 80' | 90'  | 95'  | 100' | 100' |
+-----------------+-----+-----+-----+------+------+------+------+
| Width           | -   | 25' | 30' | 30'  | 35'  | 40'  | 45'  |
+-----------------+-----+-----+-----+------+------+------+------+
| Chance/Talking  | 0%  | 15% | 20% | 25%  | 35%  | 50%  | 60%  |
+-----------------+-----+-----+-----+------+------+------+------+
| Spells by Level |     |     |     |      |      |      |      |
+-----------------+-----+-----+-----+------+------+------+------+
| Level 1         | -   | 1   | 2   | 4    | 4    | 4    | 4    |
+-----------------+-----+-----+-----+------+------+------+------+
| Level 2         | -   | -   | -   | -    | 1    | 2    | 3    |
+-----------------+-----+-----+-----+------+------+------+------+
| Level 3         | -   | -   | -   | -    | -    | 1    | 2    |
+-----------------+-----+-----+-----+------+------+------+------+
| Claw            | 1d4 | 1d4 | 1d6 | 1d6  | 1d6  | 1d8  | 1d8  |
+-----------------+-----+-----+-----+------+------+------+------+
| Bite            | 2d4 | 2d6 | 2d8 | 2d10 | 2d10 | 2d10 | 2d12 |
+-----------------+-----+-----+-----+------+------+------+------+
| Tail            | 1d4 | 1d4 | 1d4 | 1d6  | 1d6  | 1d8  | 1d8  |
+-----------------+-----+-----+-----+------+------+------+------+
