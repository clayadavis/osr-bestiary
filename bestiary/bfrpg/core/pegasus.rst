.. title: Pegasus
.. slug: pegasus
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Pegasus
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 15                       |
+-----------------+--------------------------+
| Hit Dice:       | 4                        |
+-----------------+--------------------------+
| No. of Attacks: | 2 hooves                 |
+-----------------+--------------------------+
| Damage:         | 1d6/1d6                  |
+-----------------+--------------------------+
| Movement:       | 80' (10') Fly 160' (10') |
+-----------------+--------------------------+
| No. Appearing:  | Wild 1d12                |
+-----------------+--------------------------+
| Save As:        | Fighter: 2               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 240                      |
+-----------------+--------------------------+

The pegasus is a magnificent winged horse. Though highly prized as aerial steeds, pegasi are wild and shy creatures not easily tamed. A typical pegasus stands 6 feet high at the shoulder, weighs 1,500 pounds, and has a wingspan of 20 feet. A light load for a pegasus is up to 400 pounds; a heavy load, up to 900 pounds.
