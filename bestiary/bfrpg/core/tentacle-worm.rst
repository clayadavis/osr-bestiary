.. title: Tentacle Worm
.. slug: tentacle-worm
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Tentacle Worm
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 13            |
+-----------------+---------------+
| Hit Dice:       | 3*            |
+-----------------+---------------+
| No. of Attacks: | 6 tentacles   |
+-----------------+---------------+
| Damage:         | paralysis     |
+-----------------+---------------+
| Movement:       | 40'           |
+-----------------+---------------+
| No. Appearing:  | 1d3, Lair 1d3 |
+-----------------+---------------+
| Save As:        | Fighter: 3    |
+-----------------+---------------+
| Morale:         | 9             |
+-----------------+---------------+
| Treasure Type:  | B             |
+-----------------+---------------+
| XP:             | 175           |
+-----------------+---------------+

Tentacle worms appear to be giant worms of some sort, averaging 6 to 8 feet long. Their heads are pasty white or grey, but their bodies vary from livid pink or purple to deep green in color. Their tentacles splay out from around the creature's “neck.” Some sages believe they are the larval form of some other monster, but this has never been proven.

A tentacle worm can attack as many as three adjacent opponents. Those hit must save vs. Paralysis or be paralyzed 2d4 turns. No matter how many of a tentacle worm's attacks hit an opponent in a given round, only one saving throw is required in each such round.

If all opponents of a tentacle worm are rendered paralyzed, it will begin to feed upon the paralyzed victims, doing 1 point of damage every 1d8 rounds until the victim is dead; if other paralyzed victims are still alive, the worm is 50% likely to move on immediately to another still-living victim. Otherwise, it continues to eat the corpse of the slain victim for 1d4 turns.
