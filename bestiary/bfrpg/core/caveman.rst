.. title: Caveman
.. slug: caveman
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid
.. description: Caveman
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 12                         |
+-----------------+----------------------------+
| Hit Dice:       | 2                          |
+-----------------+----------------------------+
| No. of Attacks: | 1 weapon                   |
+-----------------+----------------------------+
| Damage:         | 1d8 or weapon + 1          |
+-----------------+----------------------------+
| Movement:       | 40'                        |
+-----------------+----------------------------+
| No. Appearing:  | 1d10, Wild 10d4, Lair 10d4 |
+-----------------+----------------------------+
| Save As:        | Fighter: 2                 |
+-----------------+----------------------------+
| Morale:         | 7                          |
+-----------------+----------------------------+
| Treasure Type:  | C                          |
+-----------------+----------------------------+
| XP:             | 75                         |
+-----------------+----------------------------+

Cavemen are a species closely related to humans; they are shorter and stockier, and much more heavily muscled. They do not all actually live in caves. Whether they are actually less intelligent than “normal” humans or not is a matter of debate, but it is true that they do not have the facility for language as other human, demi-human and humanoid races.
