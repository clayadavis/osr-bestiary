.. title: Dinosaur, Stegosaurus
.. slug: dinosaur-stegosaurus
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Dinosaur, Stegosaurus
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 17                         |
+-----------------+----------------------------+
| Hit Dice:       | 11 (+9)                    |
+-----------------+----------------------------+
| No. of Attacks: | 1 tail/1 bite or 1 trample |
+-----------------+----------------------------+
| Damage:         | 2d8/1d6 or 2d8             |
+-----------------+----------------------------+
| Movement:       | 20' (15')                  |
+-----------------+----------------------------+
| No. Appearing:  | Wild 1d4                   |
+-----------------+----------------------------+
| Save As:        | Fighter: 6                 |
+-----------------+----------------------------+
| Morale:         | 7                          |
+-----------------+----------------------------+
| Treasure Type:  | None                       |
+-----------------+----------------------------+
| XP:             | 1,575                      |
+-----------------+----------------------------+

Although fearsome looking, the stegosaurus is actually a peaceable creature and will only fight in self-defense, either biting, trampling, or using its spiked tail, depending on where the opponent is standing in relation to the dinosaur. A stegosaurus can't use its tail and bite attacks against the same creature in the same round.
