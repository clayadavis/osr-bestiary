.. title: Living Statue, Stone
.. slug: living-statue-stone
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Living Statue, Stone
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 16            |
+-----------------+---------------+
| Hit Dice:       | 5*            |
+-----------------+---------------+
| No. of Attacks: | 2 lava sprays |
+-----------------+---------------+
| Damage:         | 2d6/2d6       |
+-----------------+---------------+
| Movement:       | 20'           |
+-----------------+---------------+
| No. Appearing:  | 1d3           |
+-----------------+---------------+
| Save As:        | Fighter: 5    |
+-----------------+---------------+
| Morale:         | 12            |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 405           |
+-----------------+---------------+

A stone living statue attacks by spraying molten rock from its fingertips. The range of the spray is 5'.
