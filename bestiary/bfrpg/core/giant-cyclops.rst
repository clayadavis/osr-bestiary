.. title: Giant, Cyclops
.. slug: giant-cyclops
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Cyclops
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 15 (13)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 13* (+10)                       |
+-----------------+---------------------------------+
| No. of Attacks: | 1 giant club or 1 rock (thrown) |
+-----------------+---------------------------------+
| Damage:         | 3d10 or 3d6                     |
+-----------------+---------------------------------+
| Movement:       | 20' Unarmored 30'               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1d4, Lair 1d4           |
+-----------------+---------------------------------+
| Save As:        | Fighter: 13                     |
+-----------------+---------------------------------+
| Morale:         | 9                               |
+-----------------+---------------------------------+
| Treasure Type:  | E plus 1d8x1000 gp              |
+-----------------+---------------------------------+
| XP:             | 2,285                           |
+-----------------+---------------------------------+

A cyclops is a one-eyed giant. Huge and brutish, they most resemble hill giants, and even dress in the same “style,” layers of crudely prepared hides with the fur left on, unwashed and unrepaired.

They are reclusive and unfriendly to almost all of the smaller races.

A cyclops can throw a large rock up to 200' for 3d6 points of damage, but they aim poorly and thus suffer an attack penalty of -2. Once per year, a cyclops can cast the spell **bestow curse** (the reverse of the spell **remove curse**).
