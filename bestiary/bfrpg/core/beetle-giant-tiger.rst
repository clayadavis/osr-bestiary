.. title: Beetle, Giant Tiger
.. slug: beetle-giant-tiger
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Beetle, Giant Tiger
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 17                      |
+-----------------+-------------------------+
| Hit Dice:       | 3+1                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 2d6                     |
+-----------------+-------------------------+
| Movement:       | 60' (10')               |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | U                       |
+-----------------+-------------------------+
| XP:             | 145                     |
+-----------------+-------------------------+

Giant tiger beetles are predatory monsters around 5 feet long. Their carapaces tend to be dark brown with lighter brown striped or spotted patterns, but there are many variations.

They are fast runners, depending on their speed to run down prey, and they willingly prey on any creature of man size or smaller. Like most beetles, they have more or less the same visual acuity in all directions, and thus suffer no penalty to Armor Class when attacked from behind.
