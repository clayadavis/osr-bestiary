.. title: Rot Grub
.. slug: rot-grub
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Rot Grub
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 10         |
+-----------------+------------+
| Hit Dice:       | 1 hp       |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | special    |
+-----------------+------------+
| Movement:       | 5'         |
+-----------------+------------+
| No. Appearing:  | 5d4        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 10         |
+-----------------+------------+

Rot grubs are 1-inch long vermin found in carrion, dung, and other such garbage and organic material. Their skin color is white or brown. When a living creature contacts an area (dung heap, offal, etc) infested with rot grubs, the grubs will attack if they can come in contact the victim’s skin. A rot grub secretes an anesthetic when it bites and will burrow into the flesh. A burrowing grub can be noticed if the victim succeeds at a Wisdom check. If successful, the victim sees strange rippling beneath his skin. If failed, the creature does not notice the grubs. During the first two rounds, a burrowing rot grub can be killed by applying fire to the infested skin or by cutting open the infested skin with any slashing weapon. Either method deals 2d6 points of damage to the victim, but kills the grubs. After the second round, only a cure disease can kill the grubs as they burrow to the victim’s heart and devour it in 1d3 turns.
