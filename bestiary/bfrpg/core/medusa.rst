.. title: Medusa
.. slug: medusa
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Medusa
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 12                         |
+-----------------+----------------------------+
| Hit Dice:       | 4**                        |
+-----------------+----------------------------+
| No. of Attacks: | 1 snakebite + gaze         |
+-----------------+----------------------------+
| Damage:         | 1d6+poison + petrification |
+-----------------+----------------------------+
| Movement:       | 30'                        |
+-----------------+----------------------------+
| No. Appearing:  | 1d3, Wild 1d4, Lair 1d4    |
+-----------------+----------------------------+
| Save As:        | Fighter: 4                 |
+-----------------+----------------------------+
| Morale:         | 8                          |
+-----------------+----------------------------+
| Treasure Type:  | F                          |
+-----------------+----------------------------+
| XP:             | 320                        |
+-----------------+----------------------------+

A medusa appears to be a human female with vipers growing from her head instead of hair. The gaze of a medusa will petrify any creature who meets it unless a save vs. Petrification is made. In general, any creature surprised by the medusa will meet its gaze. Those who attempt to fight the monster while averting their eyes suffer penalties of -4 on attack rolls and -2 to AC. It is safe to view a medusa's reflection in a mirror or other reflective surface; anyone using a mirror to fight a medusa suffers a penalty of -2 to attack and no penalty to AC. If a medusa sees its own reflection, it must save vs. Petrification itself; a petrified medusa is no longer able to petrify others, but the face of a medusa continues to possess the power to petrify even after death otherwise. Medusae instinctively avoid mirrors or other reflective surfaces, even drinking with their eyes closed, but if an attacker can manage to surprise the monster with a mirror she may see her reflection.

Further, the snakes growing from her head are poisonous (save vs. Poison or die in one turn). They attack as a group, not individually, once per round for 1d6 damage (plus the poison).

A medusa often wears garments that enhance its body while hiding its face behind a hood or veil. A typical medusa is 5 to 6 feet tall and about the same weight as a human.

Medusae are shy and reclusive, owing no doubt to the fact that, once the lair of one is found, any humans living nearby will not rest until she is slain. They are hateful creatures, however, and will seek to destroy as many humans as they can without being discovered.
