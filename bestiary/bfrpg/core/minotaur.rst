.. title: Minotaur
.. slug: minotaur
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Minotaur
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 14 (12)                   |
+-----------------+---------------------------+
| Hit Dice:       | 6                         |
+-----------------+---------------------------+
| No. of Attacks: | 1 gore/1 bite or 1 weapon |
+-----------------+---------------------------+
| Damage:         | 1d6/1d6 or by weapon + 2  |
+-----------------+---------------------------+
| Movement:       | 30' Unarmored 40'         |
+-----------------+---------------------------+
| No. Appearing:  | 1d6, Wild 1d8, Lair 1d8   |
+-----------------+---------------------------+
| Save As:        | Fighter: 6                |
+-----------------+---------------------------+
| Morale:         | 11                        |
+-----------------+---------------------------+
| Treasure Type:  | C                         |
+-----------------+---------------------------+
| XP:             | 500                       |
+-----------------+---------------------------+

Minotaurs are huge bull-headed humanoid monsters. A minotaur stands more than 7 feet tall and weighs about 700 pounds. Most minotaurs are very aggressive, and fly into a murderous rage if provoked or hungry. Although minotaurs are not especially intelligent, they possess innate cunning and logical ability. They never become lost, and can track enemies with 85% accuracy. They gain +2 to damage when using melee weapons due to their great Strength. Minotaurs often wear toughened hides for armor.
