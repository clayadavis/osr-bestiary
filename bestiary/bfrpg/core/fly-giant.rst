.. title: Fly, Giant
.. slug: fly-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: flying, insect
.. description: Fly, Giant
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 14            |
+-----------------+---------------+
| Hit Dice:       | 2             |
+-----------------+---------------+
| No. of Attacks: | 1 bite        |
+-----------------+---------------+
| Damage:         | 1d8           |
+-----------------+---------------+
| Movement:       | 30' Fly 60'   |
+-----------------+---------------+
| No. Appearing:  | 1d6, Wild 2d6 |
+-----------------+---------------+
| Save As:        | Fighter: 2    |
+-----------------+---------------+
| Morale:         | 8             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 75            |
+-----------------+---------------+

Giant flies look much like ordinary houseflies, but are about 3' long. Some are banded yellow and black, and are thus mistaken for giant bees. Giant flies are predators; after killing prey, they will sometimes lay eggs in the remains.
