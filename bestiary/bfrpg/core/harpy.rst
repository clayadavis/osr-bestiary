.. title: Harpy
.. slug: harpy
.. date: 2019-11-08 13:50:36.109736
.. tags: flying, monstrosity
.. description: Harpy
.. type: text
.. image:

+-----------------+------------------------------------+
| Armor Class:    | 13                                 |
+-----------------+------------------------------------+
| Hit Dice:       | 2*                                 |
+-----------------+------------------------------------+
| No. of Attacks: | 2 claws/1 weapon + special         |
+-----------------+------------------------------------+
| Damage:         | 1d4/1d4/1d6 or by weapon + special |
+-----------------+------------------------------------+
| Movement:       | 20' Fly 50' (10')                  |
+-----------------+------------------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 2d4            |
+-----------------+------------------------------------+
| Save As:        | Fighter: 2                         |
+-----------------+------------------------------------+
| Morale:         | 7                                  |
+-----------------+------------------------------------+
| Treasure Type:  | C                                  |
+-----------------+------------------------------------+
| XP:             | 100                                |
+-----------------+------------------------------------+

A harpy looks like a giant vulture bearing the torso and face of a human female. They are able to attack with their claws as well as with normal weapons, but their most insidious ability is their song. When a harpy sings, all creatures (other than harpies) within a 300' radius must succeed on a save vs. Spells or become **charmed**. The same harpy’s song cannot affect a creature that successfully saves again for 24 hours. A charmed victim walks toward the harpy, taking the most direct route available. If the path leads into a dangerous area (through ame, off a cliff, or the like), that creature is allowed a second saving throw to resist the charm. Charmed creatures can take no actions other than to defend themselves. A victim within reach of the harpy offers no resistance to the monster’s attacks. The effect continues for as long as the harpy sings, and for one round thereafter.
