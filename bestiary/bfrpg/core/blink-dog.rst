.. title: Blink Dog
.. slug: blink-dog
.. date: 2019-11-08 13:50:36.109736
.. tags: fey
.. description: Blink Dog
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d6                     |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 6                       |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

The blink dog is an intelligent canine that has a limited teleportation ability; they are able to teleport up to 120' at will. Blink dogs may teleport immediately after attacking, thus possibly avoiding being attacked. In particular, a blink dog may teleport next to an opponent, attack, and teleport away in the same round; the victim would need to strike on the same Initiative number as the blink dog in order to attack it in melee.

Blink dogs hunt in packs, teleporting in a seemingly random fashion until they surround their prey, allowing some of them to get the bonus for attacking from behind.

Blink dogs are medium-sized dogs, typically light brown in color and short haired, though other varieties are rumored to exist. They have their own language, a mixture of barks, yaps, whines, and growls that can transmit complex information. They are usually shy, avoiding a fight if possible, but they hate **displacers** and will generally attack them on sight.
