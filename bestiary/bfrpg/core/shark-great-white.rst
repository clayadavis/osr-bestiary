.. title: Shark, Great White
.. slug: shark-great-white
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Shark, Great White
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 19             |
+-----------------+----------------+
| Hit Dice:       | 8              |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 2d10           |
+-----------------+----------------+
| Movement:       | Swim 60' (10') |
+-----------------+----------------+
| No. Appearing:  | Wild 1d4       |
+-----------------+----------------+
| Save As:        | Fighter: 8     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 875            |
+-----------------+----------------+

Great white sharks range from 12' to 15' in length on the average, though specimens ranging up to 30' in length have been reported. They are apex predators. Great white sharks have the ability to sense the electromagnetic fields of living creatures, allowing them to find prey even when light or water clarity are poor, and are able to smell blood at great distances.
