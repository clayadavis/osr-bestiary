.. title: Bat (and Bat, Giant)
.. slug: bat-and-bat-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying
.. description: Bat (and Bat, Giant)
.. type: text
.. image:

+-----------------+-----------------------------+--------------------------+
|                 | Bat                         | Giant Bat                |
+=================+=============================+==========================+
| Armor Class:    | 14                          | 14                       |
+-----------------+-----------------------------+--------------------------+
| Hit Dice:       | 1 Hit Point                 | 2                        |
+-----------------+-----------------------------+--------------------------+
| No. of Attacks: | 1 special                   | 1 bite                   |
+-----------------+-----------------------------+--------------------------+
| Damage:         | Confusion                   | 1d4                      |
+-----------------+-----------------------------+--------------------------+
| Movement:       | 30' Fly 40'                 | 10' Fly 60' (10')        |
+-----------------+-----------------------------+--------------------------+
| No. Appearing:  | 1d100,Wild 1d100,Lair 1d100 | 1d10,Wild 1d10,Lair 1d10 |
+-----------------+-----------------------------+--------------------------+
| Save As:        | Normal Man                  | Fighter: 2               |
+-----------------+-----------------------------+--------------------------+
| Morale:         | 6                           | 8                        |
+-----------------+-----------------------------+--------------------------+
| Treasure Type:  | None                        | None                     |
+-----------------+-----------------------------+--------------------------+
| XP:             | 10                          | 75                       |
+-----------------+-----------------------------+--------------------------+

Bats are nocturnal flying mammals. The statistics presented here describe small, insectivorous bats. They have a natural sonar that allows them to operate in total darkness; for game purposes, treat this ability as Darkvision.

A group of normal-sized bats has no effective attack (at least in terms of doing damage) but can confuse those in the area, flying around apparently randomly. For every ten bats in the area, one creature can be confused; such a creature will suffer a penalty of -2 on all attack and saving throw rolls while the bats remain in the area.

A giant bat has a wingspan of 15 feet and weighs about 200 pounds. They have the same sensory abilities as normal-sized bats, but being much larger, they are able to attack adventurers; many are carnivorous, making such attacks likely.
