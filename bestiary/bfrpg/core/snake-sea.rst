.. title: Snake, Sea
.. slug: snake-sea
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Snake, Sea
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 14           |
+-----------------+--------------+
| Hit Dice:       | 3*           |
+-----------------+--------------+
| No. of Attacks: | 1 bite       |
+-----------------+--------------+
| Damage:         | 1 + poison   |
+-----------------+--------------+
| Movement:       | 10' Swim 30' |
+-----------------+--------------+
| No. Appearing:  | Wild 1d8     |
+-----------------+--------------+
| Save As:        | Fighter: 3   |
+-----------------+--------------+
| Morale:         | 7            |
+-----------------+--------------+
| Treasure Type:  | None         |
+-----------------+--------------+
| XP:             | 175          |
+-----------------+--------------+

Sea snakes are relatively small; the largest varieties rarely exceed 6' in length. They have relatively small heads, and are very stealthy in the water. Their bite does so little damage that the creature bitten has only a 50% chance to notice the attack, but their poison is terribly strong, such that any creature bitten must save vs. Poison at a penalty of -4 or die.

Fortunately, sea snakes rarely attack; only if molested (grabbed, stepped on, etc.) will they do so. They are very clumsy when out of the water.
