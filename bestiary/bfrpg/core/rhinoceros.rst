.. title: Rhinoceros
.. slug: rhinoceros
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Rhinoceros
.. type: text
.. image:

+-----------------+------------+-------------+
|                 | Black      | Woolly      |
+=================+============+=============+
| Armor Class:    | 17         | 19          |
+-----------------+------------+-------------+
| Hit Dice:       | 8          | 12 (+10)    |
+-----------------+------------+-------------+
| No. of Attacks: | 1 butt or 1 trample      |
+-----------------+------------+-------------+
| Damage:         | 2d6 or 2d8 | 2d8 or 2d12 |
+-----------------+------------+-------------+
| Movement:       | 40' (15')  | 40' (15')   |
+-----------------+------------+-------------+
| No. Appearing:  | Wild 1d12  | Wild 1d8    |
+-----------------+------------+-------------+
| Save As:        | Fighter: 6 | Fighter: 8  |
+-----------------+------------+-------------+
| Morale:         | 6          | 6           |
+-----------------+------------+-------------+
| Treasure Type:  | None       | None        |
+-----------------+------------+-------------+
| XP:             | 875        | 1,875       |
+-----------------+------------+-------------+

The rhinoceros is infamous for its bad temper and willingness to charge intruders.

The statistics presented here are based on the African black rhino, which is 6 to 14 feet long, 3 to 6 feet high at the shoulder, and weighs up to 6,000 pounds. These statistics can describe any herbivore of similar size and similar natural weapons (antlers, horns, tusks, or the like).

The woolly rhinoceros is a prehistoric beast with long fur, found in primitive “lost world” areas in colder territories. They behave much as the black rhino does.
