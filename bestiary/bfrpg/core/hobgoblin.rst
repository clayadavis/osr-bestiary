.. title: Hobgoblin
.. slug: hobgoblin
.. date: 2019-11-08 13:50:36.109736
.. tags: goblinoid, humanoid
.. description: Hobgoblin
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14 (11)                 |
+-----------------+-------------------------+
| Hit Dice:       | 1                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon                |
+-----------------+-------------------------+
| Damage:         | 1d8 or by weapon        |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | Q, R each; D, K in lair |
+-----------------+-------------------------+
| XP:             | 25                      |
+-----------------+-------------------------+

Hobgoblins are larger cousins of goblins, being about the same size as humans. Their hair color ranges from dark reddish-brown to dark gray. They have dark orange or red-orange skin. Large males have blue or red noses. Hobgoblins' eyes are yellowish or dark brown, while their teeth are yellow. Their garments tend to be brightly colored, often blood red with black-tinted leather. Their weaponry is kept polished and in good repair. They wear toughened hides and carry wooden shields for armor. As with most goblinoids, they have Darkvision with a 30' range.

Hobgoblins are cruel and calculating warriors, always looking to exploit those weaker than themselves. They have a strong grasp of strategy and tactics and are capable of carrying out sophisticated battle plans. Under the leadership of a skilled strategist or tactician, their discipline can prove a deciding factor. Hobgoblins hate elves and attack them first in preference over other opponents.

One out of every six hobgoblins will be a warrior of 3 Hit Dice (145 XP). Regular hobgoblins gain a +1 bonus to their morale if they are led by a warrior. In hobgoblin lairs, one out of every twelve will be a chieftain of 5 Hit Dice (360 XP) in chainmail with an Armor Class of 15 (11) and a movement of 20', having a +1 bonus to damage due to strength. In lairs of 30 or greater, there will be a hobgoblin king of 7 Hit Dice (670 XP), adding a shield for an Armor Class of 16 (11) (movement is still 20') having a +2 bonus to damage. In the lair, hobgoblins never fail a morale check as long as the king is alive. In addition, a lair has a chance equal to 1-2 on 1d6 of a shaman being present (or 1-3 on 1d6 if a hobgoblin king is present), and 1 on 1d6 of a witch or warlock. A shaman is equivalent to a hobgoblin warrior statistically, but has Clerical abilities at level 1d6+1. A witch or warlock is equivalent to a regular hobgoblin, but has Magic-User abilities of level 1d6.
