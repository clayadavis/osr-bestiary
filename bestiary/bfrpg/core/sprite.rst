.. title: Sprite
.. slug: sprite
.. date: 2019-11-08 13:50:36.109736
.. tags: fey
.. description: Sprite
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 15                               |
+-----------------+----------------------------------+
| Hit Dice:       | 1d4 Hit Points *                 |
+-----------------+----------------------------------+
| No. of Attacks: | 1 dagger or 1 spell              |
+-----------------+----------------------------------+
| Damage:         | 1d4 or by spell                  |
+-----------------+----------------------------------+
| Movement:       | 20' Fly 60'                      |
+-----------------+----------------------------------+
| No. Appearing:  | 3d6, Wild 3d6, Lair 5d8          |
+-----------------+----------------------------------+
| Save As:        | Magic-User: 4 (with Elf bonuses) |
+-----------------+----------------------------------+
| Morale:         | 7                                |
+-----------------+----------------------------------+
| Treasure Type:  | S                                |
+-----------------+----------------------------------+
| XP:             | 13                               |
+-----------------+----------------------------------+

Sprites are reclusive fey creatures, looking like tiny elves just a foot tall with dragonfly-like wings. They go out of their way to fight evil and ugliness and to protect their homelands. Sprites fight their opponents with spell-like abilities and pint-sized weaponry. They prefer ambushes and other trickery over direct confrontation.

Five sprites acting together can cast **remove curse**, or its reversed form **bestow curse**, once per day. The latter spell is often used as an attack.
