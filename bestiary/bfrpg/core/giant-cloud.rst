.. title: Giant, Cloud
.. slug: giant-cloud
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Cloud
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 19 (13)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 12+3* (+10)                     |
+-----------------+---------------------------------+
| No. of Attacks: | 1 giant weapon or 1 thrown rock |
+-----------------+---------------------------------+
| Damage:         | 6d6 or 3d6                      |
+-----------------+---------------------------------+
| Movement:       | 20' Unarmored 40' (10')         |
+-----------------+---------------------------------+
| No. Appearing:  | 1d2, Wild 1d3, Lair 1d3         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 12                     |
+-----------------+---------------------------------+
| Morale:         | 10                              |
+-----------------+---------------------------------+
| Treasure Type:  | E plus 1d12x1000 gp             |
+-----------------+---------------------------------+
| XP:             | 1,975                           |
+-----------------+---------------------------------+

Cloud giants’ skin ranges in color from milky white to light sky blue. They have hair of silvery white or brass, and their eyes are iridescent blue. Adult males are about 18 feet tall and weigh about 5,000 pounds. Females are slightly shorter and lighter. Cloud giants can live to be 400 years old.

Cloud giants dress in the finest clothing available and wear jewelry. To many, appearance indicates station: The better the clothes and the finer the jewelry, the more important the wearer. They also appreciate music, and most can play one or more instruments (the harp is a favorite). Like most giants, they are suspicious of the smaller races, but cloud giants do not usually prey upon them, preferring instead to demand tribute from humans, demi-humans, or humanoids living nearby.

Cloud giants fight in well-organized units, using carefully developed battle plans. They prefer to fight from a position above their opponents. Cloud giants can throw large stones up to 200' for 3d6 points of damage each. Also, 5% of cloud giants have the abilities of a Magic-User of level 2 to 8 (2d4). A favorite tactic is to circle the enemies, barraging them with rocks while the giants with magical abilities confound them with spells. In battle, cloud giants wear finely crafted, intricately engraved plate mail.
