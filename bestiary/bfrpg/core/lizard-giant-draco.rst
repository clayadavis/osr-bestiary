.. title: Lizard, Giant Draco
.. slug: lizard-giant-draco
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying
.. description: Lizard, Giant Draco
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 15                               |
+-----------------+----------------------------------+
| Hit Dice:       | 4+2                              |
+-----------------+----------------------------------+
| No. of Attacks: | 1 bite                           |
+-----------------+----------------------------------+
| Damage:         | 1d10                             |
+-----------------+----------------------------------+
| Movement:       | 40' Fly 70' (20', and see below) |
+-----------------+----------------------------------+
| No. Appearing:  | 1d4, Wild 1d8                    |
+-----------------+----------------------------------+
| Save As:        | Fighter: 3                       |
+-----------------+----------------------------------+
| Morale:         | 7                                |
+-----------------+----------------------------------+
| Treasure Type:  | None                             |
+-----------------+----------------------------------+
| XP:             | 240                              |
+-----------------+----------------------------------+

Giant draco lizards are able to extend their ribs and connected skin to form a sort of wing, allowing them to fly for short distances (no more than three rounds, and ascending is impossible). An average giant draco lizard is 8' long, including its nearly 3' long tail. They are fierce predators.
