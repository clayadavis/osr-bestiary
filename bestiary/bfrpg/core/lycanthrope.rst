.. title: Lycanthrope*
.. slug: lycanthrope
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid, shapechanger
.. description: Lycanthrope*
.. type: text
.. image:

Lycanthropes are humans who can transform themselves into animals. In its natural form, a lycanthrope looks like any other human, though those who have been afflicted for a long time tend to acquire features reminiscent of their animal forms. In animal form, a lycanthrope resembles a powerful version of the normal animal, but on close inspection, its eyes (which often glow red in the dark) show a faint spark of unnatural intelligence.

Lycanthropy is spread like a disease. Any human who loses half or more of his or her hit points due to lycanthrope bite and/or claw attacks will subsequently contract the same form of lycanthropy in 3d6 days. For demi-humans and humanoids, contracting the disease is fatal in the same time period. A **cure disease** cast before the onset is complete will stop the progress of the disease, but once the time has elapsed, the transformation is permanent.

In animal form, lycanthropes may be hit only by silver or magical weapons.
