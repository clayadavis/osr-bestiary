.. title: Dinosaur, Deinonychus
.. slug: dinosaur-deinonychus
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Dinosaur, Deinonychus
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 3                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d8                     |
+-----------------+-------------------------+
| Movement:       | 50'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d3, Wild 2d3, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 145                     |
+-----------------+-------------------------+

The Deinonychus (sometimes mistakenly called a "Velociraptor") is a medium-sized feathered dinosaur weighting approximately 150 pounds and reaching about 11 feet of length (tail included). It is an avid predator and a skilled pack-hunter; its warm blood, aerodynamic build and vicious maw allow it to feed on larger but more primitive dinosaurs.
