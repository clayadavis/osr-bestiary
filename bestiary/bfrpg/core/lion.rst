.. title: Lion
.. slug: lion
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Lion
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 14             |
+-----------------+----------------+
| Hit Dice:       | 5              |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d6/1d6/1d10   |
+-----------------+----------------+
| Movement:       | 50'            |
+-----------------+----------------+
| No. Appearing:  | Wild 1d8       |
+-----------------+----------------+
| Save As:        | Fighter: 5     |
+-----------------+----------------+
| Morale:         | 9              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 360            |
+-----------------+----------------+

The statistics presented here describe a male African lion, which is 5 to 8 feet long and weighs 330 to 550 pounds. Females are slightly smaller but use the same statistics.
