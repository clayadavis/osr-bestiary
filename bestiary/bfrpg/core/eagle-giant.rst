.. title: Eagle, Giant
.. slug: eagle-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying
.. description: Eagle, Giant
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 15             |
+-----------------+----------------+
| Hit Dice:       | 4              |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d6/1d6/1d8    |
+-----------------+----------------+
| Movement:       | 10' fly 90'    |
+-----------------+----------------+
| No. Appearing:  | 2d6            |
+-----------------+----------------+
| Save As:        | Fighter: 4     |
+-----------------+----------------+
| Morale:         | 7              |
+-----------------+----------------+
| Treasure Type:  | Nil            |
+-----------------+----------------+
| XP:             | 240            |
+-----------------+----------------+

A typical giant eagle stands about 10 feet tall, has a wingspan of up to 20 feet, and resembles its smaller cousins in nearly every way except size. It weighs about 500 pounds. Many giant eagles are intelligent creatures and speak Common.

A giant eagle typically attacks from a great height, diving earthward at tremendous speed. When it cannot dive, it uses its powerful talons and slashing beak to strike at its target’s head and eyes.

A solitary giant eagle is typically hunting or patrolling in the vicinity of its nest and generally ignores creatures that do not appear threatening. A mated pair attacks in concert, making repeated diving attacks to drive away intruders, and fights to the death to defend their nest or hatchlings.
