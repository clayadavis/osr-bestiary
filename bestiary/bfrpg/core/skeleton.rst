.. title: Skeleton
.. slug: skeleton
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Skeleton
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 13 (see below)   |
+-----------------+------------------+
| Hit Dice:       | 1                |
+-----------------+------------------+
| No. of Attacks: | 1 weapon         |
+-----------------+------------------+
| Damage:         | 1d6 or by weapon |
+-----------------+------------------+
| Movement:       | 40'              |
+-----------------+------------------+
| No. Appearing:  | 3d6, Wild 3d10   |
+-----------------+------------------+
| Save As:        | Fighter: 1       |
+-----------------+------------------+
| Morale:         | 12               |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 25               |
+-----------------+------------------+

Skeletons are mindless **undead** created by an evil Magic-User or Cleric, generally to guard a tomb or treasure hoard, or to act as guards for their creator. They take only ½ damage from edged weapons, and only a single point from arrows, bolts or sling stones (plus any magical bonus). As with all undead, they can be **Turned** by a Cleric, and are immune to **sleep, charm** or **hold** magic. As they are mindless, no form of mind reading is of any use against them. Skeletons never fail morale, and thus always fight until destroyed.
