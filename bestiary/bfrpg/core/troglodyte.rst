.. title: Troglodyte
.. slug: troglodyte
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid
.. description: Troglodyte
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 15             |
+-----------------+----------------+
| Hit Dice:       | 2              |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d4/1d4/1d4    |
+-----------------+----------------+
| Movement:       | 40'            |
+-----------------+----------------+
| No. Appearing:  | 1d8, Lair 5d8  |
+-----------------+----------------+
| Save As:        | Fighter: 2     |
+-----------------+----------------+
| Morale:         | 9              |
+-----------------+----------------+
| Treasure Type:  | A              |
+-----------------+----------------+
| XP:             | 75             |
+-----------------+----------------+

Troglodytes are very intelligent lizardlike humanoid creatures. They have large red eyes and spiny “combs” on their legs, head, and arms. They normally stand 5 to 6 feet tall. They can change color at will, and 50% of the time a group can blend into the environment well enough to surprise on a roll of 1-5 on 1d6. Furthermore, they gain a +2 attack bonus during any surprise round due to their excellent ambush skills.

Troglodytes secrete a smelly oil that keeps their scaly skin supple. All mammals (including, of course, all the standard character races) find the scent repulsive, and those within 10 feet of the Troglodyte must make a saving throw versus poison. Those failing the save suffer a -2 penalty to attack rolls while they remain within range of the Troglodyte. Getting out of range negates the penalty, but renewed exposure reinstates the penalty. The results of the original save last a full 24 hours, after which a new save must be rolled.

Troglodytes are very hostile, attacking equal or weaker non-troglodyte groups on sight. They prefer to attack with surprise, depending on their color-changing ability for this.

One out of every eight troglodytes will be a warrior of 4 Hit Dice (240 XP) that gains a +1 bonus to damage due to Strength. Troglodytes gain a +1 bonus to their morale if they are led by a warrior. In lairs of 24 or more, there will be a troglodyte leader of 6 Hit Dice (500 XP) with an Armor Class of 17 and having a +2 bonus to damage due to Strength. In the lair, troglodytes never fail a morale check as long as the leader is alive.
