.. title: Rhagodessa, Giant
.. slug: rhagodessa-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Rhagodessa, Giant
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 4                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 legs/1 bite           |
+-----------------+-------------------------+
| Damage:         | grab/grab/2d8           |
+-----------------+-------------------------+
| Movement:       | 50'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | U                       |
+-----------------+-------------------------+
| XP:             | 240                     |
+-----------------+-------------------------+

The rhagodessa is related to both spiders and scorpions, though it is not properly either. Rhagodessas have “pedipalps,” an elongated extra pair of legs in front that have sticky pads on them for capturing prey.

Giant rhagodessas are the size of a pony. Those found in desert terrain are generally marked in yellow, red, and brown, while those found underground may be black or white in color (those found in the deepest caverns are always white). Like spiders, they can climb walls, but they are unable to cross ceilings or otherwise climb entirely upside down.

A hit by a leg does no damage, but the victim is stuck fast, and will be drawn to the rhagodessa's mouth the next round and automatically hit for 2d8 damage. The rhagodessa will not use its bite attack against a foe it has not captured in this way, and neither will it attack more than one foe with its legs. If threatened, a rhagodessa which has captured a victim will attempt to withdraw to consume its prey in peace.
