.. title: Owlbear
.. slug: owlbear
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Owlbear
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 5                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite + 1 hug  |
+-----------------+-------------------------+
| Damage:         | 1d8/1d8/1d8 + 2d8       |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 5              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 360                     |
+-----------------+-------------------------+

Owlbears appear to be bears with owlish faces, including a large, sharp beak. An owlbear’s coat ranges in color from brown-black to yellowish brown; its beak is a dull ivory color. A full-grown male can stand as tall as 8 feet and weigh up to 1,500 pounds. Adventurers who have survived encounters with the creature often speak of the bestial madness in its red-rimmed eyes.

Owlbears fight much as do bears, but are more aggressive (as noted above). As with normal bears, an owlbear must hit with both claws in order to do the listed “hug” damage.
