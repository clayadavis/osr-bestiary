.. title: Golem, Bronze*
.. slug: golem-bronze
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Golem, Bronze*
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 20 ‡             |
+-----------------+------------------+
| Hit Dice:       | 20** (+13)       |
+-----------------+------------------+
| No. of Attacks: | 1 fist + special |
+-----------------+------------------+
| Damage:         | 3d10 + special   |
+-----------------+------------------+
| Movement:       | 80' (10')        |
+-----------------+------------------+
| No. Appearing:  | 1                |
+-----------------+------------------+
| Save As:        | Fighter:10       |
+-----------------+------------------+
| Morale:         | 12               |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 5,650            |
+-----------------+------------------+

These golems resemble statues made of bronze; unlike natural bronze statues, they never turn green from verdigris. A bronze golem is 10 feet tall and weighs about 4,500 pounds. A bronze golem cannot speak or make any vocal noise, nor does it have any distinguishable odor. It moves with a ponderous but smooth gait. Each step causes the floor to tremble unless it is on a thick, solid foundation.

The interior of a bronze golem is molten metal. Creatures hit by one in combat suffer an additional 1d10 damage from the heat (unless resistant to heat or fire). If one is hit in combat, molten metal spurts out, spraying the attacker for 2d6 damage. A save vs. Death Ray is allowed to avoid the metal spray.

When a bronze golem enters combat, there is a cumulative 1% chance each round that its elemental spirit will break free. Such a golem will go on a rampage, attacking the nearest living creature or smashing some object smaller than itself if no creature is within reach, then moving on to cause more destruction. The golem’s creator, if within 60 feet, can try to regain control by speaking firmly and persuasively to the golem; he or she must make a save vs. Spells to succeed at this, and at least 1 round of time is required for each check. It takes 1 round of inactivity by the golem to reset the chance it will go berserk to 0%.
