.. title: Elemental, Water*
.. slug: elemental-water
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, elemental
.. description: Elemental, Water*
.. type: text
.. image:

+-----------------+------------+-------------+-------------+
|                 | **Staff**  | **Device**  | **Spell**   |
+=================+============+=============+=============+
| Armor Class:    | 18 ‡       | 20 ‡        | 22 ‡        |
+-----------------+------------+-------------+-------------+
| Hit Dice:       | 8*         | 12* (+10)   | 16* (+12)   |
+-----------------+------------+-------------+-------------+
| No. of Attacks: | 1          | 1           | 1           |
+-----------------+------------+-------------+-------------+
| Damage:         | 1d12       | 2d8         | 3d6         |
+-----------------+------------+-------------+-------------+
| Movement:       | -- 20' (15') Swim 60' --               |
+-----------------+----------------------------------------+
| No. Appearing:  | -- special --                          |
+-----------------+------------+-------------+-------------+
| Save As:        | Fighter: 8 | Fighter: 12 | Fighter: 16 |
+-----------------+------------+-------------+-------------+
| Morale:         | -- 10 --                               |
+-----------------+----------------------------------------+
| Treasure Type:  | -- None --                             |
+-----------------+------------+-------------+-------------+
| XP:             | 945        | 1,975       | 3,385       |
+-----------------+------------+-------------+-------------+

Water elementals resemble roiling waves of water, which seem to fall upon any creature attacked, only to reform the next round. They take double damage when attacked with air or wind attacks (including air elementals). A water elemental cannot move more than 60' from a body of water. They do an extra 1d8 points of damage against creatures, vehicles, or structures which are in the water.
