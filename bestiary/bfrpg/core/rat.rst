.. title: Rat
.. slug: rat
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Rat
.. type: text
.. image:

+-----------------+--------------------------+-------------------------+
|                 | Normal                   | Giant                   |
+=================+==========================+=========================+
| Armor Class:    | 11                       | 13                      |
+-----------------+--------------------------+-------------------------+
| Hit Dice:       | 1 Hit Point              | 1d4 Hit Points          |
+-----------------+--------------------------+-------------------------+
| No. of Attacks: | 1 bite per pack          | 1 bite                  |
+-----------------+--------------------------+-------------------------+
| Damage:         | 1d6 + disease            | 1d4 + disease           |
+-----------------+--------------------------+-------------------------+
| Movement:       | 20' Swim 10'             | 40' Swim 20'            |
+-----------------+--------------------------+-------------------------+
| No. Appearing:  | 5d10,Wild 5d10,Lair 5d10 | 3d6,Wild 3d10,Lair 3d10 |
+-----------------+--------------------------+-------------------------+
| Save As:        | Normal Man               | Fighter: 1              |
+-----------------+--------------------------+-------------------------+
| Morale:         | 5                        | 8                       |
+-----------------+--------------------------+-------------------------+
| Treasure Type:  | None                     | C                       |
+-----------------+--------------------------+-------------------------+
| XP:             | 360*                     | 10                      |
+-----------------+--------------------------+-------------------------+

These omnivorous rodents thrive almost anywhere. Normal rats attack as a swarm; each point of damage done to the swarm reduces their numbers by one animal.

Giant rats are scavengers, but will attack to defend their nests and territories. A giant rat can grow to be up to 4 feet long and weigh over 50 pounds. A single giant rat, or a small group of up to four, will generally be shy, but larger packs attack fearlessly, biting and chewing with their sharp incisors.

Any rat bite has a 5% chance of causing a disease. A character who suffers one or more rat bites where the die roll indicates disease will sicken in 3d6 hours. The infected character will lose one point of Constitution per hour; after losing each point, the character is allowed a save vs. Death Ray (adjusted by the current Constitution bonus or penalty) to break the fever and end the disease. Any character reduced to zero Constitution is dead. See **Constitution Point Losses** in the **Encounter** section for details on regaining lost Constitution.

* Note: The XP award for normal rats is for driving away or killing an entire pack of normal size. If the adventurers are forced to flee, the GM should award 3 XP per rat slain.
