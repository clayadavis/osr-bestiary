.. title: Bear, Cave
.. slug: bear-cave
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Bear, Cave
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 7                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite + hug    |
+-----------------+-------------------------+
| Damage:         | 1d8/1d8/2d6 + 2d8 hug   |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d2, Wild 1d2, Lair 1d2 |
+-----------------+-------------------------+
| Save As:        | Fighter: 7              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 670                     |
+-----------------+-------------------------+

These monstrous bears are even larger than grizzly bears. They are ferocious killers, attacking almost anything of equal or smaller size.
