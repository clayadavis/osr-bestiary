.. title: Living Statue, Crystal
.. slug: living-statue-crystal
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Living Statue, Crystal
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 16         |
+-----------------+------------+
| Hit Dice:       | 3          |
+-----------------+------------+
| No. of Attacks: | 2 fists    |
+-----------------+------------+
| Damage:         | 1d6/1d6    |
+-----------------+------------+
| Movement:       | 30'        |
+-----------------+------------+
| No. Appearing:  | 1d6        |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 145        |
+-----------------+------------+

Crystal living statues have no particular special powers, unlike those made of iron or stone.
