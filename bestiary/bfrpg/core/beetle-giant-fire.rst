.. title: Beetle, Giant Fire
.. slug: beetle-giant-fire
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Beetle, Giant Fire
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 1+2                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 2d4                     |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d8, Wild 2d6, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 25                      |
+-----------------+-------------------------+

These luminous nocturnal insects are prized by miners and adventurers. They have two glands, one above each eye, that produce a red glow. The glands’ luminosity persists for 1d6 days after removal from the beetle, illuminating a roughly circular area with a 10-foot radius.

Giant fire beetles are about 2 feet long. They are normally timid but will fight if cornered. Like most beetles, they have more or less the same visual acuity in all directions, and thus suffer no penalty to Armor Class when attacked from behind.
