.. title: Wight*
.. slug: wight
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Wight*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 3*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 touch                 |
+-----------------+-------------------------+
| Damage:         | Energy drain (1 level)  |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d8, Lair 1d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | B                       |
+-----------------+-------------------------+
| XP:             | 175                     |
+-----------------+-------------------------+

A wight’s appearance is a weird and twisted reflection of the form it had in life. A wight is about the height and weight of a human. Wights do not possess any of the abilities they had in life.

If a wight touches (or is touched by) a living creature, that living creature suffers one level of **energy drain**. No saving throw is allowed. Striking a wight with a weapon does not count as “touching” it.

*Any humanoid slain by a wight becomes a wight* by the next sunset (but not less than 12 hours later). Wight spawn are under the command of the wight that created them and remain enslaved until its death.

Like all undead, wights may be Turned by Clerics and are *immune to sleep, charm and hold* magics. Wights are *harmed only by silver or magical weapons*, and *take only half damage from burning oil*.
