.. title: Giant, Stone
.. slug: giant-stone
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Stone
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 17 (15)                       |
+-----------------+-------------------------------+
| Hit Dice:       | 9 (+8)                        |
+-----------------+-------------------------------+
| No. of Attacks: | 1 stone club or 1 thrown rock |
+-----------------+-------------------------------+
| Damage:         | 3d6 or 3d6                    |
+-----------------+-------------------------------+
| Movement:       | 30' Unarmored 40'             |
+-----------------+-------------------------------+
| No. Appearing:  | 1d2, Wild 1d6, Lair 1d6       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 9                    |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | E plus 1d8x1000 gp            |
+-----------------+-------------------------------+
| XP:             | 1,075                         |
+-----------------+-------------------------------+

Stone giants prefer thick leather garments, dyed in shades of brown and gray to match the stone around them. Adults are about 12 feet tall and weigh about 1,500 pounds. Stone giants can live to be 800 years old.

A stone giant can throw large stones up to 300' for 3d6 damage.

Stone giants are reclusive, but they will defend their territory (typically in rocky mountainous terrain) against any who trespass therein.
