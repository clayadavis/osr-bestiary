.. title: Shrieker
.. slug: shrieker
.. date: 2019-11-08 13:50:36.109736
.. tags: plant
.. description: Shrieker
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 3          |
+-----------------+------------+
| No. of Attacks: | Special    |
+-----------------+------------+
| Damage:         | None       |
+-----------------+------------+
| Movement:       | 5'         |
+-----------------+------------+
| No. Appearing:  | 1d8        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 145        |
+-----------------+------------+

A shrieker is a large (3' to 5' tall and about the same size across), semi-mobile fungus that emits a loud noise as a defense mechanism when disturbed. Shriekers live in dark, subterranean places. They come in several shades of purple.

A shrieker has no means of attack. Instead, it lures monsters to its vicinity by emitting a loud noise. Movement or a light source within 10 feet of a shrieker causes the fungus to emit a piercing sound that lasts for 1d3 rounds. The sound attracts nearby creatures that are disposed to investigate it. Some creatures that live near shriekers learn that the fungus’ noise means there is food nearby. In game terms, the GM should roll a wandering monster check each round that a shrieker shrieks.
