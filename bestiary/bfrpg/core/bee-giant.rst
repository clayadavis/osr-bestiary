.. title: Bee, Giant
.. slug: bee-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: flying, insect
.. description: Bee, Giant
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 13                      |
+-----------------+-------------------------+
| Hit Dice:       | 1d4hp*                  |
+-----------------+-------------------------+
| No. of Attacks: | 1 sting                 |
+-----------------+-------------------------+
| Damage:         | 1d4 + poison            |
+-----------------+-------------------------+
| Movement:       | 10' Fly 50'             |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 5d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | Special                 |
+-----------------+-------------------------+
| XP:             | 13                      |
+-----------------+-------------------------+

Although many times larger, growing to a length of about a foot, giant bees behave generally the same as their smaller cousins. Giant bees are usually not aggressive except when defending themselves or their hive. Those stung by a giant bee must save vs. Poison or die. A giant bee that successfully stings another creature pulls away, leaving its stinger in the creature. The bee then dies.
