.. title: Snake, Python
.. slug: snake-python
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Snake, Python
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 5*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite/1 constrict      |
+-----------------+-------------------------+
| Damage:         | 1d4/2d4                 |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d3, Wild 1d3, Lair 1d3 |
+-----------------+-------------------------+
| Save As:        | Fighter: 5              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 405                     |
+-----------------+-------------------------+

After a successful bite attack, a python will wrap itself around the victim (in the same round), constricting for 2d4 damage plus an additional 2d4 per round thereafter. The hold may be broken on a roll of 1 on 1d6 (add the victim's Strength bonus to the range, so a Strength of 16 would result in a range of 1-3 on 1d6); breaking the hold takes a full round.
