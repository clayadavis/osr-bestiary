.. title: Giant, Frost
.. slug: giant-frost
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Frost
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 17 (13)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 10+1* (+9)                      |
+-----------------+---------------------------------+
| No. of Attacks: | 1 giant weapon or 1 thrown rock |
+-----------------+---------------------------------+
| Damage:         | 4d6 or 3d6                      |
+-----------------+---------------------------------+
| Movement:       | 20' Unarmored 40' (10')         |
+-----------------+---------------------------------+
| No. Appearing:  | 1d2, Wild 1d4, Lair 1d4         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 10                     |
+-----------------+---------------------------------+
| Morale:         | 9                               |
+-----------------+---------------------------------+
| Treasure Type:  | E plus 1d10x1000 gp             |
+-----------------+---------------------------------+
| XP:             | 1,390                           |
+-----------------+---------------------------------+

Frost giants have pale, almost white skin. A frost giant’s hair can be light blue or dirty yellow, and its eyes usually match its hair color. Frost giants dress in skins and pelts, along with any jewelry they own. Frost giant warriors add chain shirts and metal helmets decorated with horns or feathers.

An adult male is about 15 feet tall and weighs about 2,800 pounds. Females are slightly shorter and lighter, but otherwise identical with males. Frost giants can live to be 250 years old.

Frost giants are, first and foremost, cunning. They dislike the smaller races as much as any giant, but rather than attacking outright they will try to use their advantages to convince those weaker than them to submit. If faced with a stronger force, frost giants will parley or withdraw if possible, attacking only if victory seems assured.

A frost giant can throw large stones up to 200' for 3d6 damage. Frost giants are immune to all ice or cold-based attacks.
