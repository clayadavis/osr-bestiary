.. title: Assassin Vine
.. slug: assassin-vine
.. date: 2019-11-08 13:50:36.109736
.. tags: plant
.. description: Assassin Vine
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 15            |
+-----------------+---------------+
| Hit Dice:       | 6             |
+-----------------+---------------+
| No. of Attacks: | 1 + special   |
+-----------------+---------------+
| Damage:         | 1d8 + special |
+-----------------+---------------+
| Movement:       | 5'            |
+-----------------+---------------+
| No. Appearing:  | 1d4+1         |
+-----------------+---------------+
| Save As:        | Fighter: 6    |
+-----------------+---------------+
| Morale:         | 12            |
+-----------------+---------------+
| Treasure Type:  | U             |
+-----------------+---------------+
| XP:             | 500           |
+-----------------+---------------+

The assassin vine is a semi-mobile plant found in temperate forests that collects its own grisly fertilizer by grabbing and crushing animals and depositing the carcasses near its roots.

Because it can lie very still indeed, an assassin vine surprises on a roll of 1-4 on 1d6. A successful hit inflicts 1d8 points of damage, and the victim becomes entangled, suffering an additional 1d8 points of damage thereafter. A victim may attempt to escape by rolling a saving throw vs. Death Ray with Strength bonus added; this is a full action, so the victim may not attempt this and also perform an attack. The plant will continue to crush its victim until one or the other is dead or the victim manages to escape.

An assassin vine can move about, albeit very slowly, but generally only does so to seek new hunting grounds. They have no visual organs but can sense foes within 30 feet by sound and vibration.

A mature plant consists of a main vine, about 20 feet long. Smaller vines up to 5 feet long branch off from the main vine about every 6 inches. These small vines bear clusters of leaves, and in late summer they produce bunches of small fruits that resemble wild grapes. The fruit is tough and has a hearty but bitter flavor. Assassin vine berries make a heady wine.

A subterranean version of the assassin vine grows near hot springs, volcanic vents, and other sources of thermal energy. These plants have thin, wiry stems and gray leaves shot through with silver, brown, and white veins so that they resemble mineral deposits. An assassin vine growing underground usually generates enough offal to support a thriving colony of mushrooms and other fungi, which spring up around the plant and help conceal it.
