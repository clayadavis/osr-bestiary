.. title: Gelatinous Cube
.. slug: gelatinous-cube
.. date: 2019-11-08 13:50:36.109736
.. tags: ooze
.. description: Gelatinous Cube
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 12              |
+-----------------+-----------------+
| Hit Dice:       | 4*              |
+-----------------+-----------------+
| No. of Attacks: | 1               |
+-----------------+-----------------+
| Damage:         | 2d4 + paralysis |
+-----------------+-----------------+
| Movement:       | 20'             |
+-----------------+-----------------+
| No. Appearing:  | 1               |
+-----------------+-----------------+
| Save As:        | Fighter: 2      |
+-----------------+-----------------+
| Morale:         | 12              |
+-----------------+-----------------+
| Treasure Type:  | V               |
+-----------------+-----------------+
| XP:             | 280             |
+-----------------+-----------------+

The nearly transparent gelatinous cube travels slowly along dungeon corridors and cave floors, absorbing carrion, creatures, and trash. Inorganic material remains trapped and visible inside the cube’s body. A typical gelatinous cube is ten feet on a side and weighs about 15,000 pounds; however, smaller specimens have been reported.

A gelatinous cube attacks by slamming its body into its prey. It is capable of lashing out with a pseudopod, but usually engulfs foes. Any character hit by a gelatinous cube must save vs. Paralyzation or be paralyzed for 2d4 turns.

Any treasure indicated will be visible inside the creature, which must be slain if the treasure is to be recovered.
