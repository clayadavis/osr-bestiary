.. title: Lycanthrope, Weretiger*
.. slug: lycanthrope-weretiger
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid, shapechanger
.. description: Lycanthrope, Weretiger*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 17 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 5*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite          |
+-----------------+-------------------------+
| Damage:         | 1d6/1d6/2d6             |
+-----------------+-------------------------+
| Movement:       | 50' Human Form 40'      |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 5              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 405                     |
+-----------------+-------------------------+

Weretigers are humans that can transform into tigers. In human form, they tend to be tall, trim, and very agile. They tend to live and hunt close to human settlements, and are excellent trackers (5 in 6 chance to track prey in either form). Weretigers will typically only attack if provoked.
