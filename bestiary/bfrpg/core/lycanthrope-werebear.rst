.. title: Lycanthrope, Werebear*
.. slug: lycanthrope-werebear
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid, shapechanger
.. description: Lycanthrope, Werebear*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 18 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 6*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite + hug    |
+-----------------+-------------------------+
| Damage:         | 2d4/2d4/2d8 + 2d8       |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 555                     |
+-----------------+-------------------------+

Werebears are humans that can transform into large bears. When in human form, they typically appear as well-muscled, imposing figures, with an abundance of thick hair. Werebears typically dwell in deep forests, far from civilization. They are distrustful of those that they do not know, but will ferociously defend those that they have befriended.
