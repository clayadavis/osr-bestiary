.. title: Zombie
.. slug: zombie
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Zombie
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 12 (see below)   |
+-----------------+------------------+
| Hit Dice:       | 2                |
+-----------------+------------------+
| No. of Attacks: | 1 weapon         |
+-----------------+------------------+
| Damage:         | 1d8 or by weapon |
+-----------------+------------------+
| Movement:       | 20'              |
+-----------------+------------------+
| No. Appearing:  | 2d4, Wild 4d6    |
+-----------------+------------------+
| Save As:        | Fighter: 2       |
+-----------------+------------------+
| Morale:         | 12               |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 75               |
+-----------------+------------------+

Zombies are the **undead** corpses of humanoid creatures. They are deathly slow, but they move silently, are very strong and must be literally hacked to pieces to “kill” them. They take only half damage from blunt weapons, and only a single point from arrows, bolts or sling stones (plus any magical bonus). A zombie never has Initiative and always acts last in any given round. Like all undead, they may be Turned by Clerics and are immune to **sleep**, **charm** and **hold** magics. As they are mindless, no form of mind reading is of any use against them. Zombies never fail morale checks, and thus always fight until destroyed.
