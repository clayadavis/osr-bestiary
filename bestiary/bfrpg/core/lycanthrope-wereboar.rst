.. title: Lycanthrope, Wereboar*
.. slug: lycanthrope-wereboar
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid, shapechanger
.. description: Lycanthrope, Wereboar*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 2d6                     |
+-----------------+-------------------------+
| Movement:       | 50' Human Form 40'      |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

Wereboars in human form tend to be a stocky, muscular individuals of average height. They dress in simple garments that are easy to remove, repair, or replace. In either form, wereboars are as aggressive and ferocious as normal boars.
