.. title: Living Statue, Iron
.. slug: living-statue-iron
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Living Statue, Iron
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 18                |
+-----------------+-------------------+
| Hit Dice:       | 4*                |
+-----------------+-------------------+
| No. of Attacks: | 2 fists           |
+-----------------+-------------------+
| Damage:         | 1d8/1d8 + special |
+-----------------+-------------------+
| Movement:       | 10'               |
+-----------------+-------------------+
| No. Appearing:  | 1d4               |
+-----------------+-------------------+
| Save As:        | Fighter: 4        |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 280               |
+-----------------+-------------------+

If struck by a non-magical metal (even partially metal) weapon, the weapon may become stuck in the monster. If this happens, it cannot be removed until the statue is “killed.” The wielder is allowed a save vs. Spells to avoid this.
