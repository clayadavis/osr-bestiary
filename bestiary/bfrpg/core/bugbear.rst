.. title: Bugbear
.. slug: bugbear
.. date: 2019-11-08 13:50:36.109736
.. tags: goblinoid, humanoid
.. description: Bugbear
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 15 (13)                    |
+-----------------+----------------------------+
| Hit Dice:       | 3+1                        |
+-----------------+----------------------------+
| No. of Attacks: | 1 weapon                   |
+-----------------+----------------------------+
| Damage:         | 1d8+1 or by weapon +1      |
+-----------------+----------------------------+
| Movement:       | 30' Unarmored 40'          |
+-----------------+----------------------------+
| No. Appearing:  | 2d4, Wild 5d4, Lair 5d4    |
+-----------------+----------------------------+
| Save As:        | Fighter: 3                 |
+-----------------+----------------------------+
| Morale:         | 9                          |
+-----------------+----------------------------+
| Treasure Type:  | Q, R each; B, L, M in lair |
+-----------------+----------------------------+
| XP:             | 145                        |
+-----------------+----------------------------+

Bugbears look like huge, hairy goblins, standing about 6 feet tall. Their eyes are usually a darkish brown color and they move very quietly. They are wild and relatively fearless, and bully smaller humanoids whenever possible.

Bugbears prefer to ambush opponents if they can. When hunting, they often send scouts ahead of the main group. Bugbear attacks are coordinated, and their tactics are sound if not brilliant. They are able to move in nearly complete silence, surprising opponents on 1-3 on 1d6. In order to remain silent, they must wear only leather or hide armor, as indicated in the Armor Class scores above. Bugbears receive a +1 bonus on damage due to their great Strength. As with most goblinoid monsters, they have Darkvision with a 30' range.

One out of every eight bugbears will be a hardened warrior of 4+4 Hit Dice (240 XP), with a +2 bonus to damage. In lairs of 16 or more bugbears, there will be a chieftain of 6+6 Hit Dice (500 XP), with a +3 bonus to damage. Bugbears gain a +1 bonus to their morale if they are led by a hardened warrior or chieftain. In the lair, bugbears never fail a morale check as long as the chieftain is alive. In addition, there is a 2 in 6 chance that a shaman will be present in a lair. A shaman is equal to an ordinary bugbear statistically, but possesses 1d4+1 levels of Clerical abilities.
