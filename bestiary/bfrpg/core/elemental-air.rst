.. title: Elemental, Air*
.. slug: elemental-air
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental, flying
.. description: Elemental, Air*
.. type: text
.. image:

+-----------------+------------+-------------+-------------+
|                 | **Staff**  | **Device**  | **Spell**   |
+=================+============+=============+=============+
| Armor Class:    | 18 ‡       | 20 ‡        | 22 ‡        |
+-----------------+------------+-------------+-------------+
| Hit Dice:       | 8*         | 12* (+10)   | 16* (+12)   |
+-----------------+------------+-------------+-------------+
| No. of Attacks: | -- special --                          |
+-----------------+------------+-------------+-------------+
| Damage:         | 1d12       | 2d8         | 3d6         |
+-----------------+------------+-------------+-------------+
| Movement:       | -- Fly 120' --                         |
+-----------------+----------------------------------------+
| No. Appearing:  | -- special --                          |
+-----------------+------------+-------------+-------------+
| Save As:        | Fighter: 8 | Fighter: 12 | Fighter: 16 |
+-----------------+------------+-------------+-------------+
| Morale:         | -- 10 --                               |
+-----------------+----------------------------------------+
| Treasure Type:  | -- None --                             |
+-----------------+------------+-------------+-------------+
| XP:             | 945        | 1,975       | 3,385       |
+-----------------+------------+-------------+-------------+

Air elementals resemble “dust devils,” that is, small whirlwinds, but they are much more powerful. Air elementals take double damage when attacked by earth-based attacks (including by earth elementals). An air elemental may choose either to attack a single opponent, thus receiving one attack per round at the listed damage, or may choose to knock all opponents in a 5' radius to the ground; if the latter attack is used, all creatures of 2 hit dice or less must save vs. Death Ray or fall prone. Creatures of 3 or more levels or hit dice are not so affected. Air elementals do an additional 1d8 points of damage against creatures or vehicles which are airborne.
