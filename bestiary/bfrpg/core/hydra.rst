.. title: Hydra
.. slug: hydra
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Hydra
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 16 to 23          |
+-----------------+-------------------+
| Hit Dice:       | 5 to 12 (+10)     |
+-----------------+-------------------+
| No. of Attacks: | 5 to 12 bites     |
+-----------------+-------------------+
| Damage:         | 1d10 per bite     |
+-----------------+-------------------+
| Movement:       | 40' (10')         |
+-----------------+-------------------+
| No. Appearing:  | 1, Wild 1, Lair 1 |
+-----------------+-------------------+
| Save As:        | Fighter: 5 to 12  |
+-----------------+-------------------+
| Morale:         | 9                 |
+-----------------+-------------------+
| Treasure Type:  | B                 |
+-----------------+-------------------+
| XP:             | 360 - 1,875       |
+-----------------+-------------------+

Hydras are reptile-like monsters with multiple heads. They are gray-brown to dark brown, with a light yellow or tan underbelly. The eyes are amber and the teeth are yellow-white. Hydras are about 20 feet long and weigh about 4,000 pounds. They are bad-tempered and territorial, but not particularly cunning.

A hydra may be slain by damage in the normal fashion; however, most who fight them choose to strike at their heads. If a character using a melee weapon chooses to strike at a particular head, and succeeds in doing 8 points of damage, that head is disabled (severed or severely damaged) and will not be able to attack anymore. Such damage also applies to the monster's total hit points, of course.

Some hydras live in the ocean; use the given movement as a swimming rate rather than walking in this case. A very few hydras can breathe fire; those that have this ability can emit a flame 10' wide and 20' long one time per head per day. This attack will be used about one time in three (1-2 on 1d6) if it is available; roll for each head which is attacking. Each such attack does 3d6 damage, with a save vs. Dragon Breath reducing the amount by half.
