.. title: Dragon, Red
.. slug: dragon-red
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying
.. description: Dragon, Red
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 21                              |
+-----------------+---------------------------------+
| Hit Dice:       | 10** (+9)                       |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 1d8/1d8/4d8 or breath/1d8       |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80' (20')               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4             |
+-----------------+---------------------------------+
| Save As:        | Fighter: 10 (as Hit Dice)       |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 1,480                           |
+-----------------+---------------------------------+

Because red dragons are so confident, they seldom pause to appraise an adversary. On spotting a target, they make a snap decision whether to attack, using one of many strategies worked out ahead of time. A red dragon lands to attack small, weak creatures with its claws and bite rather than obliterating them with its breath weapon, so as not to destroy any treasure they might be carrying.

Red dragons are cruel monsters, actively seeking to hunt, torment, kill and consume intelligent creatures. They are often said to prefer women and elves, but in truth a red dragon will attack almost any creature less powerful than itself.

Red dragons are immune to normal fire, and suffer only half damage from magical fire.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
