.. title: Ochre Jelly*
.. slug: ochre-jelly
.. date: 2019-11-08 13:50:36.109736
.. tags: ooze
.. description: Ochre Jelly*
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 12 (only hit by fire or cold) |
+-----------------+-------------------------------+
| Hit Dice:       | 5*                            |
+-----------------+-------------------------------+
| No. of Attacks: | 1 pseudopod                   |
+-----------------+-------------------------------+
| Damage:         | 2d6                           |
+-----------------+-------------------------------+
| Movement:       | 10'                           |
+-----------------+-------------------------------+
| No. Appearing:  | 1                             |
+-----------------+-------------------------------+
| Save As:        | Fighter: 5                    |
+-----------------+-------------------------------+
| Morale:         | 12                            |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 405                           |
+-----------------+-------------------------------+

Ochre jellies are ochre-colored amorphous monsters, similar to the gray ooze in appearance. An ochre jelly can grow to a diameter of up to 10 feet and a thickness of about 6 inches. A typical specimen weighs about 700 pounds.

Ochre jellies can only be hit (damaged) by fire or cold. Attacks with weapons or electricity/lightning cause the creature to divide into 1d4+1 smaller jellies of 2 hit dice apiece. If divided, the resulting smaller jellies do 1d6 damage with each hit.
