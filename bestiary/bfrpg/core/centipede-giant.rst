.. title: Centipede, Giant
.. slug: centipede-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Centipede, Giant
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 11                      |
+-----------------+-------------------------+
| Hit Dice:       | 1d4 Hit Points*         |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | poison                  |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 2d4, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Normal Man              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 13                      |
+-----------------+-------------------------+

Giant centipedes are larger versions of the normal sort, being 2 to 3 feet long. Centipedes are fast-moving, predatory, venomous arthropods, having long segmented bodies with exoskeletons. They prefer to live in underground areas, shadowy forested areas, and other places out of direct sunlight; however, there are desert-dwelling varieties that hide under the sand waiting for prey to wander by.

Giant centipedes tend to attack anything that resembles food, biting with their jaws and injecting their poison. Those bitten by a giant centipede must save vs. Poison at +2 or die.
