.. title: Squid, Giant
.. slug: squid-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Squid, Giant
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 17                    |
+-----------------+-----------------------+
| Hit Dice:       | 6                     |
+-----------------+-----------------------+
| No. of Attacks: | 8 tentacles/1 bite    |
+-----------------+-----------------------+
| Damage:         | 1d4 per tentacle/1d10 |
+-----------------+-----------------------+
| Movement:       | Swim 40'              |
+-----------------+-----------------------+
| No. Appearing:  | Wild 1d4              |
+-----------------+-----------------------+
| Save As:        | Fighter: 6            |
+-----------------+-----------------------+
| Morale:         | 8                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 500                   |
+-----------------+-----------------------+

These voracious creatures can have bodies more than 20 feet long and attack almost anything they meet. Their tentacles are studded with barbs and sharp-edged suckers. In order to bite a creature, the giant squid must hit with at least two tentacles first.

If a giant squid fails a morale check, it will squirt out a cloud of black “ink” 30' in diameter and then jet away at twice normal speed for 3d8 rounds.
