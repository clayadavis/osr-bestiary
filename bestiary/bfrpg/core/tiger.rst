.. title: Tiger
.. slug: tiger
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Tiger
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 6                  |
+-----------------+--------------------+
| No. of Attacks: | 2 claws/1 bite     |
+-----------------+--------------------+
| Damage:         | 1d6/1d6/2d6        |
+-----------------+--------------------+
| Movement:       | 50'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d3, Lair 1d3 |
+-----------------+--------------------+
| Save As:        | Fighter: 6         |
+-----------------+--------------------+
| Morale:         | 9                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 500                |
+-----------------+--------------------+

These great cats stand more than 3 feet tall at the shoulder and are about 9 feet long. They weigh from 400 to 600 pounds.
