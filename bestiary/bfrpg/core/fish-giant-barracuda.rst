.. title: Fish, Giant Barracuda
.. slug: fish-giant-barracuda
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Fish, Giant Barracuda
.. type: text
.. image:

+-----------------+------------+----------------+
|                 | **Huge**   | **Giant**      |
+=================+============+================+
| Armor Class:    | 16         | 15             |
+-----------------+------------+----------------+
| Hit Dice:       | 5          | 9 (+8)         |
+-----------------+------------+----------------+
| No. of Attacks: | 1 bite     | 1 bite         |
+-----------------+------------+----------------+
| Damage:         | 2d6        | 2d8+1          |
+-----------------+------------+----------------+
| Movement:       | Swim 60'   | Swim 60' (10') |
+-----------------+------------+----------------+
| No. Appearing:  | Wild 2d4   | Wild 1         |
+-----------------+------------+----------------+
| Save As:        | Fighter: 5 | Fighter: 9     |
+-----------------+------------+----------------+
| Morale:         | 8          | 10             |
+-----------------+------------+----------------+
| Treasure Type:  | None       | None           |
+-----------------+------------+----------------+
| XP:             | 360        | 1,075          |
+-----------------+------------+----------------+

Barracuda are predatory fish found in salt water. Huge barracudas are about 12' long, while giant specimens can exceed 20'. They have elongated bodies, pointed heads and prominent jaws. Their bodies are covered with smooth scales, typically blue, gray or silver in color. They have extremely keen eyesight and are surprised only on a 1 on 1d6. Due to the quickness of their attack, barracudas are themselves capable of surprising on 1-3 on 1d6 and gain a +2 bonus to Initiative.

Giant barracudas always appear singly and are 50% likely to break off the attack after 1d4 rounds if they haven't killed their prey. Both kinds are attracted to shiny objects.
