.. title: Ape, Carnivorous
.. slug: ape-carnivorous
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Ape, Carnivorous
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 4                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws                 |
+-----------------+-------------------------+
| Damage:         | 1d4/1d4                 |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 240                     |
+-----------------+-------------------------+

These powerful creatures resemble gorillas but are far more aggressive; though they are actually omnivores, they prefer meat, and they kill and eat anything they can catch. An adult male carnivorous ape is 5-1/2 to 6 feet tall and weighs 300 to 400 pounds.
