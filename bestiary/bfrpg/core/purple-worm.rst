.. title: Purple Worm
.. slug: purple-worm
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Purple Worm
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 16                               |
+-----------------+----------------------------------+
| Hit Dice:       | 11* (+9) to 20* (+13)            |
+-----------------+----------------------------------+
| No. of Attacks: | 1 bite/1 sting                   |
+-----------------+----------------------------------+
| Damage:         | 2d8/1d8+poison                   |
+-----------------+----------------------------------+
| Movement:       | 20' (15')                        |
+-----------------+----------------------------------+
| No. Appearing:  | 1d2, Wild 1d4                    |
+-----------------+----------------------------------+
| Save As:        | Fighter: 6 to 10 (½ of Hit Dice) |
+-----------------+----------------------------------+
| Morale:         | 10                               |
+-----------------+----------------------------------+
| Treasure Type:  | None                             |
+-----------------+----------------------------------+
| XP:             | 1,670 – 5,450                    |
+-----------------+----------------------------------+

Purple worms are gigantic subterranean monsters; they are rarely found above ground. The body of a mature purple worm is 5-8 feet in diameter and 60-100 feet long, weighing about 40,000 pounds.

The creature has a poisonous stinger in its tail; those injured by it must save vs. Poison or die. Note that the purple worm's movement is less than the monster's length, so that, if attacking from out of a tunnel, it might not be able to use the stinger for several rounds.

Any time a purple worm successfully bites a man-sized or smaller opponent with a natural roll of 19 or 20, the opponent has been swallowed, and will suffer 3d6 damage per round afterward due to being digested. A character who has been swallowed can only effectively attack with small cutting or stabbing weapons such as dagger or shortsword.
