.. title: Snake, Giant Rattlesnake
.. slug: snake-giant-rattlesnake
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Snake, Giant Rattlesnake
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 2*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d8 + poison            |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d2, Wild 1d2, Lair 1d2 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 100                     |
+-----------------+-------------------------+

Giant rattlesnakes are simply much enlarged versions of the normal rattlesnake (see **pit vipers**, below, for details). They average 14' to 20' in length at adulthood.
