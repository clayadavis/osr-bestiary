.. title: Spider, Giant Crab
.. slug: spider-giant-crab
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Spider, Giant Crab
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 13                      |
+-----------------+-------------------------+
| Hit Dice:       | 2*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d8 + poison            |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 100                     |
+-----------------+-------------------------+

Crab spiders are ambush predators, hiding using various forms of camouflage and leaping out to bite their surprised prey. Giant crab spiders are horribly enlarged, being around 3' in length. They can change color slowly (over the course of a few days), taking on the overall coloration of their preferred lair or ambush location. After this change is complete, the spider is able to surprise potential prey on 1-4 on 1d6 when in that preferred location. Anyone bitten by a giant crab spider must save vs. Poison or die.
