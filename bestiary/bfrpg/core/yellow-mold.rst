.. title: Yellow Mold
.. slug: yellow-mold
.. date: 2019-11-08 13:50:36.109736
.. tags: plant
.. description: Yellow Mold
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | Can always be hit |
+-----------------+-------------------+
| Hit Dice:       | 2*                |
+-----------------+-------------------+
| No. of Attacks: | Special           |
+-----------------+-------------------+
| Damage:         | See below         |
+-----------------+-------------------+
| Movement:       | 0                 |
+-----------------+-------------------+
| No. Appearing:  | 1d8               |
+-----------------+-------------------+
| Save As:        | Normal Man        |
+-----------------+-------------------+
| Morale:         | N/A               |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 100               |
+-----------------+-------------------+

If disturbed, a patch of this mold bursts forth with a cloud of poisonous spores. Each patch covers from 10 to 25 square feet; several patches may grow adjacent to each other, and will appear to be a single patch in this case. Each patch can emit a cloud of spores once per day. All within 10 feet of the mold will be affected by the spores and must save vs. Death Ray or take 1d8 points of damage per round for 6 rounds. Sunlight renders yellow mold dormant.
