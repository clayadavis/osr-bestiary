.. title: Basilisk
.. slug: basilisk
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Basilisk
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 6**                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite/1 gaze           |
+-----------------+-------------------------+
| Damage:         | 1d10/petrification      |
+-----------------+-------------------------+
| Movement:       | 20' (10')               |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | F                       |
+-----------------+-------------------------+
| XP:             | 610                     |
+-----------------+-------------------------+

A basilisk is an eight-legged reptilian monster that petrifies living creatures with a mere gaze. A basilisk usually has a dull brown body with a yellowish underbelly. Some specimens sport a short, curved horn atop the nose. An adult basilisk’s body grows to about 6 feet long, not including its tail, which can reach an additional length of 5 to 7 feet. The creature weighs about 300 pounds.

Any living creature which meets the gaze of the basilisk must save vs. Petrification or be turned to stone instantly. In general, any creature surprised by the basilisk will meet its gaze. Those who attempt to fight the monster while averting their eyes suffer penalties of -4 to attack and -2 to AC. It is possible to use a mirror to fight the monster, in which case the penalties are -2 to attack and no penalty to AC. If a basilisk sees its own reflection in a mirror it must save vs. Petrification or be turned to stone; a petrified basilisk loses its power to petrify. Basilisks instinctively avoid mirrors or other reflective surfaces, even drinking with their eyes closed, but if an attacker can manage to surprise the monster with a mirror it may see its reflection.
