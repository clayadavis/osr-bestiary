.. title: Wraith*
.. slug: wraith
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Wraith*
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 15 ‡                         |
+-----------------+------------------------------+
| Hit Dice:       | 4**                          |
+-----------------+------------------------------+
| No. of Attacks: | 1 touch                      |
+-----------------+------------------------------+
| Damage:         | 1d6 + energy drain (1 level) |
+-----------------+------------------------------+
| Movement:       | Fly 80'                      |
+-----------------+------------------------------+
| No. Appearing:  | 1d4, Lair 1d6                |
+-----------------+------------------------------+
| Save As:        | Fighter: 4                   |
+-----------------+------------------------------+
| Morale:         | 12                           |
+-----------------+------------------------------+
| Treasure Type:  | E                            |
+-----------------+------------------------------+
| XP:             | 320                          |
+-----------------+------------------------------+

Wraiths are incorporeal creatures born of evil and darkness. In some cases, the grim silhouette of a wraith might appear armored or outfitted with weapons. This appearance does not affect the creature’s AC or combat abilities but only reflects the shape it had in life.

Like all undead, they may be Turned by Clerics and are immune to **sleep**, **charm** and **hold** magics. Due to their incorporeal nature, they cannot be harmed by non-magical weapons.
