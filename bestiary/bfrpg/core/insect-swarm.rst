.. title: Insect Swarm
.. slug: insect-swarm
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Insect Swarm
.. type: text
.. image:

+-----------------+--------------------------------------------------------+
| Armor Class:    | Immune to normal weapons, including most magical types |
+-----------------+--------------------------------------------------------+
| Hit Dice:       | 2* to 4*                                               |
+-----------------+--------------------------------------------------------+
| No. of Attacks: | 1 swarm                                                |
+-----------------+--------------------------------------------------------+
| Damage:         | 1d3 (double against no armor)                          |
+-----------------+--------------------------------------------------------+
| Movement:       | 10' Fly 20'                                            |
+-----------------+--------------------------------------------------------+
| No. Appearing:  | 1 swarm, Wild 1d3 swarms                               |
+-----------------+--------------------------------------------------------+
| Save As:        | N/A                                                    |
+-----------------+--------------------------------------------------------+
| Morale:         | 11                                                     |
+-----------------+--------------------------------------------------------+
| Treasure Type:  | None                                                   |
+-----------------+--------------------------------------------------------+
| XP:             | 100 - 280                                              |
+-----------------+--------------------------------------------------------+

An insect swarm is not a single creature; rather, it is a large group of ordinary flying or crawling insects moving as a unit. In general, a swarm fills a volume equal to three 10' cubes, though it is possible for a swarm to become more compact in order to move through a small doorway or narrow corridor. If the swarm consists of crawling insects, it covers three 10' squares and the flying movement above is ignored.

Any living creature within the volume or area of the swarm suffers 1d3 points of damage each round. Damage rolls are doubled if the victim is unarmored (for creatures which do not wear armor, any creature having less than Armor Class 15 is considered unarmored).

Damage is reduced to a single point per round for three rounds if the character manages to exit the swarm. It is possible to “ward off” the insects by swinging a weapon, shield, or other similar-sized object around, and in this case also damage is reduced to 1 point per round. If a lit torch is used in this way, the swarm takes 1d4 damage per round. Weapons, even magic weapons, do not harm an insect swarm. An entire swarm can be affected by a **sleep** spell. Smoke can be used to drive a swarm away (if the swarm moves away from the victim(s) due to smoke, the damage stops immediately). Finally, a victim who dives into water will take damage for only one more round.
