.. title: Kobold
.. slug: kobold
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid
.. description: Kobold
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 13 (11)                   |
+-----------------+---------------------------+
| Hit Dice:       | 1d4 Hit Points            |
+-----------------+---------------------------+
| No. of Attacks: | 1 weapon                  |
+-----------------+---------------------------+
| Damage:         | 1d4 or by weapon          |
+-----------------+---------------------------+
| Movement:       | 20' Unarmored 30'         |
+-----------------+---------------------------+
| No. Appearing:  | 4d4, Wild 6d10, Lair 6d10 |
+-----------------+---------------------------+
| Save As:        | Normal Man                |
+-----------------+---------------------------+
| Morale:         | 6                         |
+-----------------+---------------------------+
| Treasure Type:  | P, Q each; C in lair      |
+-----------------+---------------------------+
| XP:             | 10                        |
+-----------------+---------------------------+

Kobolds are small, dog-faced reptilian humanoids. A kobold is 2 to 2½ feet tall and weighs 35 to 45 pounds. They prefer ranged combat, closing only when they can see that their foes have been weakened. Whenever they can, kobolds set up ambushes near trapped areas. They aim to drive enemies into the traps, where other kobolds wait to pour flaming oil over them, shoot them, or drop poisonous vermin onto them. Kobolds have Darkvision with a range of 60', and suffer a -1 penalty to attack rolls in bright sunlight or within the radius of **light** spells. Kobolds typically wear leather armor in battle.

One out of every six kobolds will be a warrior of 1 Hit Dice (25 XP). Kobolds gain a +1 bonus to their morale if they are led by a warrior. In kobold lairs, one out of every twelve will be a chieftain of 2 Hit Dice (75 XP) with an Armor Class of 14 (11) and having a +1 bonus to damage due to strength. In lairs of 30 or greater, there will be a kobold king of 3 Hit Dice (145 XP) who wears chain mail with an Armor Class of 15 (11) and a movement of 10', and who has a +1 bonus to damage. In the lair, kobolds never fail a morale check as long as the kobold king is alive. In addition, a lair has a chance equal to 1 on 1d6 of a shaman being present (or 1-2 on 1d6 if a kobold king is present). A shaman is equivalent to a regular kobold statistically, but has Clerical abilities at level 1d4+1.

Kobolds are cunning foes. They see all larger races as enemies, and are thus likely to be hostile when encountered. However, they are naturally cowardly, and prefer to avoid combat, leading enemies into ambushes or traps rather than facing them directly. Sometimes kobold tribes build and inhabit extensive dungeon areas filled with deadly traps which only they know how to avoid.
