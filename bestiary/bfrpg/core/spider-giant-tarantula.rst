.. title: Spider, Giant Tarantula
.. slug: spider-giant-tarantula
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Spider, Giant Tarantula
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d8 + poison            |
+-----------------+-------------------------+
| Movement:       | 50'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d3, Wild 1d3, Lair 1d3 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

Giant tarantulas are huge, hairy spiders, about the size of a pony. They run down their prey much as wolves do. The bite of the giant tarantula is poisonous; those bitten must save vs. Poison or be forced to dance wildly. The dance lasts 2d10 rounds, during which time the victim has a -4 penalty on attack and saving throw rolls. If the victim is a Thief, he or she cannot use any Thief abilities while dancing. Onlookers must save vs. Spells or begin dancing themselves; such “secondary” victims suffer the same penalties as above, but they will only dance for 2d4 rounds.

Each round the original victim dances, he or she must save vs. Poison again or take 1d4 points of damage. Secondary victims do not suffer this effect.

**Neutralize poison** will cure the original victim, and **dispel magic** will stop the dance for all victims in the area of effect, whether they are original or secondary.
