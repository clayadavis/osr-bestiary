.. title: Efreeti*
.. slug: efreeti
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental, flying
.. description: Efreeti*
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 21 ‡              |
+-----------------+-------------------+
| Hit Dice:       | 10* (+9)          |
+-----------------+-------------------+
| No. of Attacks: | 1                 |
+-----------------+-------------------+
| Damage:         | 2d8 or special    |
+-----------------+-------------------+
| Movement:       | 30' Fly 80' (10') |
+-----------------+-------------------+
| No. Appearing:  | 1                 |
+-----------------+-------------------+
| Save As:        | Fighter: 15       |
+-----------------+-------------------+
| Morale:         | 12 (9)            |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 1,390             |
+-----------------+-------------------+

The efreet (singular efreeti) are humanoid creatures from the Elemental Plane of Fire. An efreeti in its natural form stands about 12 feet tall and weighs about 2,000 pounds. Efreet are malicious by nature. They love to mislead, befuddle, and confuse their foes. They do this for enjoyment as much as for tactical reasons.

Note that the 12 morale reflects an efreeti's absolute control over its own fear, but does not indicate that the creature will throw its life away easily. Use the “9” figure to determine whether an outmatched efreeti decides to leave a combat.

Efreet have a number of magical powers, which can be used at will (that is, without needing magic words or gestures): become **invisible**, with unlimited uses per day; assume **gaseous form**, as the potion, up to one hour per day; **create illusions**, as the spell **phantasmal force** but including sound as well as visual elements, three times per day; **create flame**, with unlimited uses; and create a **wall of fire** (as the spell), once per day. Create flame allows the efreet to cause a flame to appear in its hand or otherwise on its person at will; it behaves as desired by the efreet, becoming as large as a torchflame or as small as a candle, and ignites flammable material just as any ordinary flame does. The flame can be thrown as a weapon with a range of up to 60', causing 1d8 points of damage on a successful hit. The efreet can create another flame, and throw it as well if desired, once per round.

Efreet may assume the form of a column of fire at will, with no limit as to the number of times per day this power may be used; an efreeti in flame-form fights as if it were a fire elemental.

Due to their highly magical nature, efreet cannot be harmed by non-magical weapons. They are immune to normal fire, and suffer only half damage from magical fire attacks.
