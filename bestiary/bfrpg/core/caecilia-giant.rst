.. title: Caecilia, Giant
.. slug: caecilia-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, beast
.. description: Caecilia, Giant
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 14                           |
+-----------------+------------------------------+
| Hit Dice:       | 6*                           |
+-----------------+------------------------------+
| No. of Attacks: | 1 bite + swallow on 19/20    |
+-----------------+------------------------------+
| Damage:         | 1d8 + 1d8/round if swallowed |
+-----------------+------------------------------+
| Movement:       | 20' (10')                    |
+-----------------+------------------------------+
| No. Appearing:  | 1d3, Lair 1d3                |
+-----------------+------------------------------+
| Save As:        | Fighter: 3                   |
+-----------------+------------------------------+
| Morale:         | 9                            |
+-----------------+------------------------------+
| Treasure Type:  | B                            |
+-----------------+------------------------------+
| XP:             | 555                          |
+-----------------+------------------------------+

Caecilia are carnivorous, legless amphibians; they strongly resemble earthworms, but they have bony skeletons and sharp teeth. Caecilia live entirely underground. The giant variety grows up to 30' long and frequently are found in caverns or dungeons. They are nearly blind, but caecilia are very sensitive to sound and vibrations, and are able to find their prey regardless of light or the absence thereof.

A caecilia can swallow a single small humanoid or demi-human (such as a goblin or halfling) whole. On a natural attack roll of 19 or 20, such a victim has been swallowed (assuming that roll does actually hit the victim). A swallowed victim suffers 1d8 damage per round, and may only attack from the inside with a small cutting or stabbing weapon such as a dagger. While the inside of the caecilia is easier for the victim to hit, fighting while swallowed is more difficult, so no modifiers to the attack roll are applied.

Once a caecilia has swallowed an opponent, it will generally attempt to disengage from combat, going to its lair to rest and digest its meal.
