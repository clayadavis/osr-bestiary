.. title: Whale, Killer
.. slug: whale-killer
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Whale, Killer
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 17             |
+-----------------+----------------+
| Hit Dice:       | 6              |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 2d10           |
+-----------------+----------------+
| Movement:       | Swim 80' (10') |
+-----------------+----------------+
| No. Appearing:  | Wild 1d6       |
+-----------------+----------------+
| Save As:        | Fighter: 6     |
+-----------------+----------------+
| Morale:         | 10             |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 500            |
+-----------------+----------------+

These ferocious creatures are about 30 feet long. Killer whales, also called “orca” (both singular and plural), are strikingly marked in black and white, with prominent white patches that resemble eyes. Their real eyes are much smaller and located away from the fake eye-spots.

Killer whales eat fish, squid, seals, and other whales, but are not above consuming a meal of human or demi-human fare.
