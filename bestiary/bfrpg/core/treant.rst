.. title: Treant
.. slug: treant
.. date: 2019-11-08 13:50:36.109736
.. tags: plant
.. description: Treant
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 19                 |
+-----------------+--------------------+
| Hit Dice:       | 8*                 |
+-----------------+--------------------+
| No. of Attacks: | 2 fists            |
+-----------------+--------------------+
| Damage:         | 2d6/2d6            |
+-----------------+--------------------+
| Movement:       | 20'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d8, Lair 1d8 |
+-----------------+--------------------+
| Save As:        | Fighter: 8         |
+-----------------+--------------------+
| Morale:         | 9                  |
+-----------------+--------------------+
| Treasure Type:  | C                  |
+-----------------+--------------------+
| XP:             | 945                |
+-----------------+--------------------+

A treant is a large, roughly humanoid tree-man. Treants have leaves of deep green in the spring and summer. In the fall and winter the leaves change to yellow, orange, or red, but they rarely fall out. A treant’s legs fit together when closed to look like the trunk of a tree, and a motionless treant is nearly indistinguishable from a tree. A treant is about 30 feet tall, with a “trunk” about 2 feet in diameter. It weighs about 4,500 pounds.

Treants speak their own language, plus Common and Elvish. Most also can manage a smattering of just about all other humanoid tongues, at least enough to say “Get away from my trees!” Treants prefer to watch potential foes carefully before attacking. They often charge suddenly from cover to trample the despoilers of forests. If sorely pressed, they animate trees as reinforcements.

A treant can animate trees within 180' at will, controlling up to two trees at a time. It takes one round for a normal tree to uproot itself. Thereafter it moves at a speed of 10' and fights as a treant in all respects. Such trees lose their ability to move if the treant that animated them is incapacitated or moves out of range.
