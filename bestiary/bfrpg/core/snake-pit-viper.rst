.. title: Snake, Pit Viper
.. slug: snake-pit-viper
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Snake, Pit Viper
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 1*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d4 + poison            |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 37                      |
+-----------------+-------------------------+

Pit vipers are highly venomous snakes. There are many varieties ranging in size from 2' to 12' at adulthood; the statistics above are for an “average” variety which reaches about 9' in length.

Those bitten by a pit viper must save vs. Poison or die.

Pit vipers are named for the thermally sensitive “pits” between their eyes and nostrils. These are used to detect birds, mammals, and lizards, the natural prey of these snakes. Note that, even though lizards are cold-blooded, pit vipers can still sense them because their temperature will often be slightly higher or lower than their surroundings.

Rattlesnakes are a variety of pit viper; in addition to the details given above, a rattlesnake has a rattle (from which it gets its name) at the end of its tail. The rattle is used to warn away larger creatures.
