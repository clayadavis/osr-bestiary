.. title: Urgoblin
.. slug: urgoblin
.. date: 2019-11-08 13:50:36.109736
.. tags: goblinoid, humanoid
.. description: Urgoblin
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 14 (11)                       |
+-----------------+-------------------------------+
| Hit Dice:       | 2*                            |
+-----------------+-------------------------------+
| No. of Attacks: | 1 weapon                      |
+-----------------+-------------------------------+
| Damage:         | 1d8 or by weapon              |
+-----------------+-------------------------------+
| Movement:       | 30' Unarmored 40'             |
+-----------------+-------------------------------+
| No. Appearing:  | Special                       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 2                    |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | Q, R, S each; special in lair |
+-----------------+-------------------------------+
| XP:             | 100                           |
+-----------------+-------------------------------+

These creatures appear to be normal **hobgoblins**, but urgoblins are actually a mutant subspecies. Urgoblins are able to regenerate much as do **trolls** (with the same limitations). All urgoblins are male; if an urgoblin mates with a female hobgoblin, any offspring will also be male, but only one in four such offspring will share their father's gifts. Like hobgoblins, urgoblins wear toughened hides and carry wooden shields into battle, blending in perfectly.

Some hobgoblin tribes consider urgoblins an abomination, and kill them whenever they can be identified. Other hobgoblin tribes employ them as bodyguards for the chieftain, and accord them great honor. There are even rumors of a tribe entirely made up of urgoblins, with kidnapped hobgoblin females as their mates; reportedly they slit the throats of all infants born to their mates, so that only those who have the power of regeneration survive.
