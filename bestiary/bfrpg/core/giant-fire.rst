.. title: Giant, Fire
.. slug: giant-fire
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Fire
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 17 (13)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 11+2* (+9)                      |
+-----------------+---------------------------------+
| No. of Attacks: | 1 giant weapon or 1 thrown rock |
+-----------------+---------------------------------+
| Damage:         | 5d6 or 3d6                      |
+-----------------+---------------------------------+
| Movement:       | 20' Unarmored 40' (10')         |
+-----------------+---------------------------------+
| No. Appearing:  | 1d2, Wild 1d3, Lair 1d3         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 11                     |
+-----------------+---------------------------------+
| Morale:         | 9                               |
+-----------------+---------------------------------+
| Treasure Type:  | E plus 1d10x1000 gp             |
+-----------------+---------------------------------+
| XP:             | 1,670                           |
+-----------------+---------------------------------+

An adult male fire giant is 14 feet tall, has a chest that measures 9 feet around, and weighs about 3,200 pounds. Females are slightly shorter and lighter. Fire giants can live to be 350 years old. Fire giants wear sturdy cloth or leather garments colored red, orange, yellow, or black. Warriors wear helmets and half-plate armor of blackened steel.

Fire giants are unfriendly to almost all other human, demi-human, and humanoid races, though they sometimes subjugate nearby humanoid races to act as their servants.

A fire giant can throw large stones up to 200' for 3d6 damage. Fire giants are immune to all fire-based attacks.
