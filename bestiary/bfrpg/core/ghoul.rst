.. title: Ghoul
.. slug: ghoul
.. date: 2019-11-08 13:50:36.109736
.. tags: undead
.. description: Ghoul
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 14                              |
+-----------------+---------------------------------+
| Hit Dice:       | 2*                              |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite                  |
+-----------------+---------------------------------+
| Damage:         | 1d4/1d4/1d4, all plus paralysis |
+-----------------+---------------------------------+
| Movement:       | 30'                             |
+-----------------+---------------------------------+
| No. Appearing:  | 1d6, Wild 2d8, Lair 2d8         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 2                      |
+-----------------+---------------------------------+
| Morale:         | 9                               |
+-----------------+---------------------------------+
| Treasure Type:  | B                               |
+-----------------+---------------------------------+
| XP:             | 100                             |
+-----------------+---------------------------------+

Ghouls are **undead** monsters which eat the flesh of dead humanoids to survive. They are vile, disgusting carrion-eaters, but are more than willing to kill for food. Those slain by ghouls will generally be stored until they begin to rot before the ghouls will actually eat them.

Those hit by a ghoul’s bite or claw attack must save vs. Paralyzation or be paralyzed for 2d8 turns. Elves are immune to this paralysis. Ghouls try to attack with surprise whenever possible, striking from behind tombstones and bursting from shallow graves; when these methods are employed, they are able to surprise opponents on 1-3 on 1d6. Like all undead, they may be Turned by Clerics and are immune to **sleep**, **charm** and **hold** magics.

Humanoids bitten by ghouls may be infected with ghoul fever. Each time a humanoid is bitten, there is a 5% chance of the infection being passed. The afflicted humanoid is allowed to save vs. Death Ray; if the save is failed, the humanoid dies within a day.

An afflicted humanoid who dies of ghoul fever rises as a ghoul at the next midnight. A humanoid who becomes a ghoul in this way retains none of the knowledge or abilities he or she possessed in life. The newly-risen ghoul is not under the control of any other ghouls, but hungers for the flesh of the living and behaves like any other ghoul in all respects.
