.. title: Displacer
.. slug: displacer
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Displacer
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 16            |
+-----------------+---------------+
| Hit Dice:       | 6*            |
+-----------------+---------------+
| No. of Attacks: | 2 blades      |
+-----------------+---------------+
| Damage:         | 1d8/1d8       |
+-----------------+---------------+
| Movement:       | 50'           |
+-----------------+---------------+
| No. Appearing:  | 1d4, Wild 1d4 |
+-----------------+---------------+
| Save As:        | Fighter: 6    |
+-----------------+---------------+
| Morale:         | 8             |
+-----------------+---------------+
| Treasure Type:  | D             |
+-----------------+---------------+
| XP:             | 555           |
+-----------------+---------------+

Displacers are blue-black, catlike monsters with strange bladed winglike arms extending from their shoulders. The blades are carried folded back like wings, but the Displacer swings the blades around in front to attack.

The real power and danger of the Displacer is its power of **displacement**, which causes the monster's apparent location to shift around constantly over a range of 3' from the monster's true location. This is a form of illusion, but a powerful form that cannot be seen through even by those who know the secret.

Any character fighting a Displacer for the first time will miss his or her first strike regardless of the die roll. Thereafter, all attacks against displacers will be at a penalty of -2 to the attack roll. This is not cumulative with the penalty for fighting blind. Some monsters, such as bats, do not depend on vision to fight and thus may be able to perceive the monster's true location and fight without penalty.
