.. title: Golem, Amber*
.. slug: golem-amber
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Golem, Amber*
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 21 ‡           |
+-----------------+----------------+
| Hit Dice:       | 10* (+9)       |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 2d6/2d6/2d10   |
+-----------------+----------------+
| Movement:       | 60'            |
+-----------------+----------------+
| No. Appearing:  | 1              |
+-----------------+----------------+
| Save As:        | Fighter: 5     |
+-----------------+----------------+
| Morale:         | 12             |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 1,390          |
+-----------------+----------------+

Amber golems are generally built to resemble lions or other great cats. They are able to detect invisible creatures or objects within 60', and can track with 95% accuracy through any terrain type.

A magical attack that deals electricity damage heals 1 point of damage for every 3 full points of damage the attack would otherwise deal. For example, an amber golem hit by a **lightning bolt** for 20 points of damage is instead healed up to 6 points. If the amount of healing would cause the golem to exceed its full normal hit points, the excess is ignored.
