.. title: Elemental, Fire*
.. slug: elemental-fire
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental, flying
.. description: Elemental, Fire*
.. type: text
.. image:

+-----------------+------------+-------------+-------------+
|                 | **Staff**  | **Device**  | **Spell**   |
+=================+============+=============+=============+
| Armor Class:    | 18 ‡       | 20 ‡        | 22 ‡        |
+-----------------+------------+-------------+-------------+
| Hit Dice:       | 8*         | 12* (+10)   | 16* (+12)   |
+-----------------+------------+-------------+-------------+
| No. of Attacks: | 1          | 1           | 1           |
+-----------------+------------+-------------+-------------+
| Damage:         | 1d12       | 2d8         | 3d6         |
+-----------------+------------+-------------+-------------+
| Movement:       | -- 40' Fly 30' --                      |
+-----------------+----------------------------------------+
| No. Appearing:  | -- special --                          |
+-----------------+------------+-------------+-------------+
| Save As:        | Fighter: 8 | Fighter: 12 | Fighter: 16 |
+-----------------+------------+-------------+-------------+
| Morale:         | -- 10 --                               |
+-----------------+----------------------------------------+
| Treasure Type:  | -- None --                             |
+-----------------+------------+-------------+-------------+
| XP:             | 945        | 1,975       | 3,385       |
+-----------------+------------+-------------+-------------+

Fire elementals are simply flames, which may appear generally humanoid for brief moments when they attack. Fire elementals take double damage when attacked by water (including water elementals). They cannot cross a body of water wider than their own diameter. They do an additional 1d8 points of damage against creatures which are cold or icy in nature.

Remember that a fire elemental is constantly burning; such a creature may easily start fires if it moves into an area containing items which burn easily, such as dry wood, paper, or oil. No specific rules are given for such fires, but the GM is directed to the rules for burning oil for an example of fire damage.
