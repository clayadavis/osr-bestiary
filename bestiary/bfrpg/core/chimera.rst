.. title: Chimera
.. slug: chimera
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Chimera
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 16                            |
+-----------------+-------------------------------+
| Hit Dice:       | 9** (+8)                      |
+-----------------+-------------------------------+
| No. of Attacks: | 2 claws/3 heads + special     |
+-----------------+-------------------------------+
| Damage:         | 1d4/1d4/2d4/2d4/3d4 + special |
+-----------------+-------------------------------+
| Movement:       | 40' (10') Fly 60' (15')       |
+-----------------+-------------------------------+
| No. Appearing:  | 1d2, Wild 1d4, Lair 1d4       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 9                    |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | F                             |
+-----------------+-------------------------------+
| XP:             | 1,225                         |
+-----------------+-------------------------------+

Chimeras are strange creatures having a lion's body with the heads of a lion, a goat, and a dragon, and the wings of a dragon. A chimera is about 5 feet tall at the shoulder, nearly 10 feet long, and weighs about 4,000 pounds. A chimera’s dragon head might be black, blue, green, red, or white, and has the same type of breath weapon as that sort of dragon. Regardless of type, the dragon's head breathes a 50' long cone with a 10' wide end, for 3d6 points of damage; victims may save vs. Dragon Breath for one-half damage.

Chimeras are cruel and voracious. They can speak Dragon but seldom bother to do so, except when toadying to more powerful creatures.
