.. title: Gnome
.. slug: gnome
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid
.. description: Gnome
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 15 (11)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 1                               |
+-----------------+---------------------------------+
| No. of Attacks: | 1 weapon                        |
+-----------------+---------------------------------+
| Damage:         | 1d6 or by weapon                |
+-----------------+---------------------------------+
| Movement:       | 20' Unarmored 40'               |
+-----------------+---------------------------------+
| No. Appearing:  | 1d8, Wild 5d8, Lair 5d8         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 1 (with Dwarf bonuses) |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | D                               |
+-----------------+---------------------------------+
| XP:             | 25                              |
+-----------------+---------------------------------+

Gnomes stand 3 to 3½ feet tall and weigh 40 to 45 pounds. Their skin color ranges from dark tan to woody brown, their hair is fair, and their eyes can be any shade of blue. Males usually wear short, carefully trimmed beards.

Gnomes generally wear leather or earth tones, though they decorate their clothes with intricate stitching or fine jewelry. Gnomes reach adulthood at about age 40, and they live about 350 years. They have Darkvision with a 30' range. When attacked in melee by creatures larger than man-sized, gnomes gain a +1 bonus to their Armor Class. Outdoors in their preferred forest terrain, they are able to hide very effectively; so long as they remain still there is only a 20% chance they will be detected. If one or more gnomes who are successfully hiding attack from ambush, they surprise their foes on 1-4 on 1d6.

Gnomes speak their own language, Gnomish, and many know the language of the dwarves. Most gnomes who travel outside gnome lands (as traders or tinkers) know Common, while warriors in gnome settlements usually learn Goblin. Gnomes encountered in the wilderness are likely to be unfriendly, but not hostile. They tolerate dwarves but dislike most other humanoid races. When forced to interact with other races, a gnome will generally be recalcitrant, unless offered a significant amount of treasure.

Most gnomes encountered outside their home are warriors; the statistics above are for such. In the lair, for every warrior there will be an average of three civilians having 1-1 Hit Dice and Armor Class 11; such gnomes have Morale of 7. One out of every eight gnome warriors will be a sergeant having 3 Hit Dice (145 XP). Gnomes gain a +1 bonus to their morale if they are led by a sergeant. Both warriors and sergeants commonly wear chainmail. In gnomish communities, one out of every sixteen warriors will be a captain of 5 Hit Dice (360 XP) with an Armor Class of 16 (11), adding a shield. In addition, in communities of 35 or greater, there will be a king of 7 Hit Dice (670 XP), with an Armor Class of 18 (11), in plate mail and carrying a shield, having a +1 bonus damage due to strength. In their community, gnomes never fail a morale check as long as the king is alive. There is a chance equal to 1-4 on 1d6 that a community will have a Cleric of level 1d6+1, and 1-2 on 1d6 of a Magic-User of level 1d6. Gnomish Clerics and Magic-Users are equivalent to regular gnomish warriors statistically.
