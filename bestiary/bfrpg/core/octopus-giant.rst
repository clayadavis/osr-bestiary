.. title: Octopus, Giant
.. slug: octopus-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Octopus, Giant
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 19                   |
+-----------------+----------------------+
| Hit Dice:       | 8                    |
+-----------------+----------------------+
| No. of Attacks: | 8 tentacles/1 bite   |
+-----------------+----------------------+
| Damage:         | 1d4 per tentacle/1d6 |
+-----------------+----------------------+
| Movement:       | Swim 30'             |
+-----------------+----------------------+
| No. Appearing:  | Wild 1d2             |
+-----------------+----------------------+
| Save As:        | Fighter: 8           |
+-----------------+----------------------+
| Morale:         | 7                    |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 875                  |
+-----------------+----------------------+

These creatures are aggressive and territorial hunters, with arms reaching 10 feet or more in length. Their tentacles are studded with barbs and sharp-edged suckers. In order to bite a creature, the giant octopus must hit with at least two tentacles first.

If a giant octopus fails a morale check, it will squirt out a cloud of black “ink” 40' in diameter and then jet away at twice normal speed for 2d6 rounds.
