.. title: Wolf
.. slug: wolf
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Wolf
.. type: text
.. image:

+-----------------+-----------------------+-----------------------+
|                 | **Normal**            | **Dire**              |
+=================+=======================+=======================+
| Armor Class:    | 13                    | 14                    |
+-----------------+-----------------------+-----------------------+
| Hit Dice:       | 2                     | 4                     |
+-----------------+-----------------------+-----------------------+
| No. of Attacks: | 1 bite                | 1 bite                |
+-----------------+-----------------------+-----------------------+
| Damage:         | 1d6                   | 2d4                   |
+-----------------+-----------------------+-----------------------+
| Movement:       | 60'                   | 50'                   |
+-----------------+-----------------------+-----------------------+
| No. Appearing:  | 2d6,Wild 3d6,Lair 3d6 | 1d4,Wild 2d4,Lair 2d4 |
+-----------------+-----------------------+-----------------------+
| Save As:        | Fighter: 2            | Fighter: 4            |
+-----------------+-----------------------+-----------------------+
| Morale:         | 8                     | 9                     |
+-----------------+-----------------------+-----------------------+
| Treasure Type:  | None                  | None                  |
+-----------------+-----------------------+-----------------------+
| XP:             | 75                    | 240                   |
+-----------------+-----------------------+-----------------------+

Wolves are pack hunters known for their persistence and cunning. They prefer to surround and flank a foe when they can.

Dire wolves are efficient pack hunters that will kill anything they can catch. Dire wolves are generally mottled gray or black, about 9 feet long and weighing some 800 pounds.
