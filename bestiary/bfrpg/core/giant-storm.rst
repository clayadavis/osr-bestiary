.. title: Giant, Storm
.. slug: giant-storm
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Storm
.. type: text
.. image:

+-----------------+------------------------------------+
| Armor Class:    | 19 (13)                            |
+-----------------+------------------------------------+
| Hit Dice:       | 15** (+11)                         |
+-----------------+------------------------------------+
| No. of Attacks: | 1 giant weapon or 1 lightning bolt |
+-----------------+------------------------------------+
| Damage:         | 8d6 or 15d6                        |
+-----------------+------------------------------------+
| Movement:       | 30' Unarmored 50' (10')            |
+-----------------+------------------------------------+
| No. Appearing:  | 1, Wild 1d3, Lair 1d3              |
+-----------------+------------------------------------+
| Save As:        | Fighter: 15                        |
+-----------------+------------------------------------+
| Morale:         | 10                                 |
+-----------------+------------------------------------+
| Treasure Type:  | E plus 1d20x1000 gp                |
+-----------------+------------------------------------+
| XP:             | 3,100                              |
+-----------------+------------------------------------+

Adult storm giants are about 21 feet tall and weigh about 12,000 pounds. They can live to be 600 years old. Most storm giants have pale skin and dark hair. Very rarely, storm giants have violet skin. Violet-skinned storm giants have deep violet or blue-black hair with silvery gray or purple eyes.

Storm giants generally dress in short, loose tunic belted at the waist, sandals or bare feet, and a headband. They wear a few pieces of simple but finely crafted jewelry, anklets (favored by barefoot giants), rings, or circlets being most common. They live quiet, reflective lives and spend their time musing about the world, composing and playing music, and tilling their land or gathering food.

Storm giants prefer to attack first with their **lightning bolts** (which work just as the spell does, and can be used once per five rounds; a save vs. Spells reduces damage to half). Also, 10% of storm giants have the abilities of a Magic-User of level 2 to 12 (2d6). In battle, they wear well-crafted and well-cared-for plate mail.

Unlike most other giants, storm giants have been known to befriend humans, elves, or dwarves.
