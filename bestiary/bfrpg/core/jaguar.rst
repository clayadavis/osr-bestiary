.. title: Jaguar
.. slug: jaguar
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Jaguar
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 16             |
+-----------------+----------------+
| Hit Dice:       | 4              |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d4/1d4/2d4    |
+-----------------+----------------+
| Movement:       | 70' Swim 30'   |
+-----------------+----------------+
| No. Appearing:  | 1d2, Wild 1d6  |
+-----------------+----------------+
| Save As:        | Fighter: 4     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 240            |
+-----------------+----------------+

These great cats are about 8 to 9 feet long (from nose to tail-tip) and weigh about 165 pounds. Unlike other great cats, they enjoy swimming and often hunt near rivers or lakes. Jaguars kill with their powerful bite, preferring to deliver a fatal wound to the skull of their prey.
