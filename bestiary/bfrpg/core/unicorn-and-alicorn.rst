.. title: Unicorn (and Alicorn)
.. slug: unicorn-and-alicorn
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity
.. description: Unicorn (and Alicorn)
.. type: text
.. image:

+-----------------+-----------------------------------+-----------------+
|                 | Unicorn                           | Alicorn         |
+=================+===================================+=================+
| Armor Class:    | 19                                | 19              |
+-----------------+-----------------------------------+-----------------+
| Hit Dice:       | 4*                                | 4*              |
+-----------------+-----------------------------------+-----------------+
| No. of Attacks: | 2 hooves/1 horn (+3 attack bonus) | 2 hooves/1 horn |
+-----------------+-----------------------------------+-----------------+
| Damage:         | 1d8/1d8/1d6+3                     | 2d4/2d4/2d6     |
+-----------------+-----------------------------------+-----------------+
| Movement:       | 80'                               | 70'             |
+-----------------+-----------------------------------+-----------------+
| No. Appearing:  | Wild 1d6                          | Wild 1d8        |
+-----------------+-----------------------------------+-----------------+
| Save As:        | Fighter: 8                        | Fighter: 6      |
+-----------------+-----------------------------------+-----------------+
| Morale:         | 7                                 | 9               |
+-----------------+-----------------------------------+-----------------+
| Treasure Type:  | None                              | None            |
+-----------------+-----------------------------------+-----------------+
| XP:             | 280                               | 280             |
+-----------------+-----------------------------------+-----------------+

Unicorns are horselike creatures having a single spirally-twisted horn in the middle of the forehead. A typical adult unicorn grows to 8 feet in length, stands 5 feet high at the shoulder, and weighs 1,200 pounds. Females are slightly smaller and slimmer than males. A unicorn has deep sea-blue, violet, brown, or fiery gold eyes. Males sport a white beard.

Unicorns normally attack only when defending themselves or their forests. They either charge, impaling foes with their horns like lances, or strike with their hooves. The horn is a +3 magic weapon, though its power fades if removed from the unicorn.

Three times per day a unicorn can cast **cure light wounds** by a touch of its horn. Once per day a unicorn can transport itself 360' (as the spell **dimension door**), and can carry a full load (possibly including a rider) while doing so. A light load for a unicorn is up to 300 pounds; a heavy load, up to 550 pounds.

An Alicorn resembles a unicorn in all details, save that they always have yellow, orange or red eyes, and (if one gets close enough to see) pronounced, sharp canine teeth. Alicorns are as evil as unicorns are good, using their razor-sharp horns and clawlike hooves as weapons. They attack any weaker creatures for the sheer pleasure of killing, but will try to avoid stronger parties.

Alicorns cannot heal or transport themselves by magic as unicorns do. However, alicorns may become invisible at will, exactly as if wearing a **ring of invisibility**.
