.. title: Troll
.. slug: troll
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Troll
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 6*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite          |
+-----------------+-------------------------+
| Damage:         | 1d6/1d6/1d10            |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d8, Wild 1d8, Lair 1d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 10 (8)                  |
+-----------------+-------------------------+
| Treasure Type:  | D                       |
+-----------------+-------------------------+
| XP:             | 555                     |
+-----------------+-------------------------+

A typical adult troll stands 9 feet tall and weighs 500 pounds. A troll’s rubbery hide is moss green, mottled green and gray, or putrid gray. The hair is usually greenish black or iron gray. They appear sexless if examined, and their method of reproduction is a mystery. Trolls walk upright but hunched forward with sagging shoulders. Their gait is uneven, and when they run, their arms dangle and drag along the ground. For all this seeming awkwardness, trolls are very agile.

Trolls have the power of regeneration; they heal 1 hit point of damage each round after being injured. A troll reduced to 0 hit points is not dead, but only disabled for 2d6 rounds, at which point it will regain 1 hit point. Note that the troll may “play dead” until it has regenerated further. Damage from fire and acid cannot be regenerated, and must heal at the normal rate; a troll can only be killed by this sort of damage. The lower morale rating (in parentheses) is used when the troll faces attackers armed with fire or acid. If a troll loses a limb or body part, the lost portion regrows in one turn; or, the creature can reattach the severed member instantly by holding it to the stump.

Trolls are hateful creatures, reveling in combat and bloodshed. Though trolls could easily use a variety of weapons, they much prefer the sensation of flesh being rent by their teeth and claws.
