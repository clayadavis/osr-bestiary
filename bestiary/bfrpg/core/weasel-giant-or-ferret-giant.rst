.. title: Weasel, Giant (or Ferret, Giant)
.. slug: weasel-giant-or-ferret-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Weasel, Giant (or Ferret, Giant)
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 17                      |
+-----------------+-------------------------+
| Hit Dice:       | 5                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite + hold           |
+-----------------+-------------------------+
| Damage:         | 2d4 + 2d4 per round     |
+-----------------+-------------------------+
| Movement:       | 50'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 5              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | V                       |
+-----------------+-------------------------+
| XP:             | 360                     |
+-----------------+-------------------------+

Giant weasels resemble their more normally sized cousins, having long bodies, short legs, and pointed, toothy snouts. They are predatory animals, hunting those creatures smaller than themselves.

Weasels of all sorts are cunning, crafty hunters, and surprise their prey on 1-3 on 1d6. Once a giant weasel bites a living creature, it hangs on, rending with its teeth each round until the victim or the weasel is dead, or until the weasel fails a morale check (rolled normally) in which case it will let go of its victim and flee.

There are many varieties of normal-sized weasel, including several which are called ferrets; in some territories, the giant weasel is thus called a giant ferret. The only distinction is that those which are tamed are always called ferrets, though not all giant ferrets are tame. Various humanoid races as well as some fairy creatures are known to tame giant ferrets for use as guards or war-animals.
