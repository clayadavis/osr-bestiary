.. title: Hangman Tree
.. slug: hangman-tree
.. date: 2019-11-08 13:50:36.109736
.. tags: plant
.. description: Hangman Tree
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 16                       |
+-----------------+--------------------------+
| Hit Dice:       | 5                        |
+-----------------+--------------------------+
| No. of Attacks: | 4 limbs plus strangle    |
+-----------------+--------------------------+
| Damage:         | 1d6 per limb or strangle |
+-----------------+--------------------------+
| Movement:       | 0                        |
+-----------------+--------------------------+
| No. Appearing:  | Wild 1                   |
+-----------------+--------------------------+
| Save As:        | Fighter: 4               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 360                      |
+-----------------+--------------------------+

Hangman trees are horrible, semi-animate creatures that fertilize themselves with dead bodies. A hangman tree has four animated limbs that can wrap around the necks of living creatures that pass beneath, strangling for 1d6 points of damage per round. These limbs are arranged evenly around the tree in most cases, and generally no more than one limb can attack any single creature at a time.

The roots of this tree are also animated; they do not attack, but they do pull dead bodies below the surface of the ground for “digestion.”
