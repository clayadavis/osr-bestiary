.. title: Spider, Giant Black Widow
.. slug: spider-giant-black-widow
.. date: 2019-11-08 13:50:36.109736
.. tags: insect
.. description: Spider, Giant Black Widow
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 3*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 2d6 + poison            |
+-----------------+-------------------------+
| Movement:       | 20' Web 40'             |
+-----------------+-------------------------+
| No. Appearing:  | 1d3, Wild 1d3, Lair 1d3 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 175                     |
+-----------------+-------------------------+

The giant black widow spider is a much enlarged version of the ordinary black widow; a full-grown male has a leg-span of 2 feet, while an adult female will be 3' or more across. Despite the size difference, both genders are statistically equal. Both genders are marked with an orange “hourglass” on the abdomen.

The venom of the giant black widow is strong, such that those bitten must save vs. Poison at a penalty of -2 or die. Giant black widow spiders spin strong, sticky, nearly invisible webs, usually across passageways or cave entrances, or sometimes between trees in the wilderness; those who stumble into these webs become stuck, and must roll to escape just as if opening a door. Any character stuck in such a web cannot effectively cast spells or use a weapon.
