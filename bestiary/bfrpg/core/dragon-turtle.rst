.. title: Dragon Turtle
.. slug: dragon-turtle
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, dragon
.. description: Dragon Turtle
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 22                       |
+-----------------+--------------------------+
| Hit Dice:       | 30**                     |
+-----------------+--------------------------+
| No. of Attacks: | 2 claws/1 bite or breath |
+-----------------+--------------------------+
| Damage:         | 2d8/2d8/10d6 or 30d8     |
+-----------------+--------------------------+
| Movement:       | 10' (10') Swim 30' (15') |
+-----------------+--------------------------+
| No. Appearing:  | Wild 1                   |
+-----------------+--------------------------+
| Save As:        | Fighter: 20 at +5        |
+-----------------+--------------------------+
| Morale:         | 10                       |
+-----------------+--------------------------+
| Treasure Type:  | H                        |
+-----------------+--------------------------+
| XP:             | 13,650                   |
+-----------------+--------------------------+

A dragon turtle's rough, deep green shell is much the same color as the deep water the monster favors, and the silver highlights that line the shell resemble light dancing on open water. The turtle’s legs, tail, and head are a lighter green, flecked with golden highlights. An adult dragon turtle can measure from 100 to 200 feet from snout to tail. They are occasionally mistaken for rocky outcroppings or even small islands.

Dragon turtles are fierce fighters and generally attack any creature that threatens their territory or looks like a potential meal. Though they are not true dragons, they do advance through the same sort of age categories as the true dragons do; however, each age category changes the dragon turtle's Hit Dice by 5.

Due to their massive size, dragon turtles are immune to virtually all poisons.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
