.. title: Golem*
.. slug: golem
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Golem*
.. type: text
.. image:

Golems are magically created automatons of great power. Constructing one involves the employment of mighty magic and elemental forces. The animating force for a golem is an elemental spirit. The process of creating the golem binds the spirit to the artificial body and subjects it to the will of the golem’s creator.

Being mindless, golems generally do nothing without orders from their creators. They follow instructions explicitly and are incapable of any strategy or tactics. A golem’s creator can command it if the golem is within 60 feet and can see and hear its creator. If not actively commanded, a golem usually follows its last instruction to the best of its ability, though if attacked it returns the attack. The creator can give the golem a simple command to govern its actions in his or her absence. The golem’s creator can order the golem to obey the commands of another person (who might in turn place the golem under someone else’s control, and so on), but the golem’s creator can always resume control over his creation by commanding the golem to obey him alone.

Golems have immunity to most magical and supernatural effects, except when otherwise noted. They can only be hit by magical weapons.
