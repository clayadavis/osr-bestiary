.. title: Elemental, Earth*
.. slug: elemental-earth
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental
.. description: Elemental, Earth*
.. type: text
.. image:

+-----------------+------------+-------------+-------------+
|                 | **Staff**  | **Device**  | **Spell**   |
+=================+============+=============+=============+
| Armor Class:    | 18 ‡       | 20 ‡        | 22 ‡        |
+-----------------+------------+-------------+-------------+
| Hit Dice:       | 8*         | 12* (+10)   | 16* (+12)   |
+-----------------+------------+-------------+-------------+
| No. of Attacks: | 1          | 1           | 1           |
+-----------------+------------+-------------+-------------+
| Damage:         | 1d12       | 2d8         | 3d6         |
+-----------------+------------+-------------+-------------+
| Movement:       | -- 20' (10') --                        |
+-----------------+----------------------------------------+
| No. Appearing:  | -- special --                          |
+-----------------+------------+-------------+-------------+
| Save As:        | Fighter: 8 | Fighter: 12 | Fighter: 16 |
+-----------------+------------+-------------+-------------+
| Morale:         | -- 10 --                               |
+-----------------+----------------------------------------+
| Treasure Type:  | -- None --                             |
+-----------------+------------+-------------+-------------+
| XP:             | 945        | 1,975       | 3,385       |
+-----------------+------------+-------------+-------------+

Earth elementals resemble crude, headless humanoid statues, with clublike hands and feet. They cannot cross a body of water wider than their own height. Earth elementals take double damage when attacked by fire (including fire elementals). They do an additional 1d8 points of damage against creatures, vehicles, or structures which rest on the ground.
