.. title: Gnoll
.. slug: gnoll
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid
.. description: Gnoll
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 (13)                 |
+-----------------+-------------------------+
| Hit Dice:       | 2                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon                |
+-----------------+-------------------------+
| Damage:         | 2d4 or by weapon +1     |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 3d6, Lair 3d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | Q, S each; D, K in lair |
+-----------------+-------------------------+
| XP:             | 75                      |
+-----------------+-------------------------+

Gnolls are hyena-headed, evil humanoids that wander in loose tribes. Most gnolls have dirty yellow or reddish-brown fur. An adult male gnoll is about 7½ feet tall and weighs 300 pounds.

Gnolls are nocturnal, and have Darkvision with a 30' range. They are cruel carnivores, preferring intelligent creatures for food because they scream more. They show little discipline when fighting unless they have a strong leader.

One out of every six gnolls will be a hardened warrior of 4 Hit Dice (240 XP) having a +1 bonus to damage due to strength. Gnolls gain a +1 bonus to their morale if they are led by such a warrior. In lairs of 12 or greater, there will be a pack leader of 6 Hit Dice (500 XP) having a +2 bonus to damage. In the lair, gnolls never fail a morale check as long as the pack leader is alive. In addition, a lair has a chance equal to 1-2 on 1d6 of a shaman being present, and 1 on 1d6 of a witch or warlock. A shaman is equivalent to a hardened warrior statistically, and in addition has Clerical abilities at level 1d4+1. A witch or warlock is equivalent to a regular gnoll, and has Magic-User abilities of level 1d4.
