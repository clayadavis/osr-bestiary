.. title: Bear, Black
.. slug: bear-black
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Bear, Black
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 4                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite + hug    |
+-----------------+-------------------------+
| Damage:         | 1d4/1d4/1d6 + 2d6 hug   |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d4, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 240                     |
+-----------------+-------------------------+

The black bear is a forest-dwelling omnivore that usually is not dangerous unless an interloper threatens its cubs or food supply.

Black bears may actually be pure black, blond, or cinnamon in color. They are rarely more than 5 feet long.
