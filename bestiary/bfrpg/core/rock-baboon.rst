.. title: Rock Baboon
.. slug: rock-baboon
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Rock Baboon
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 2                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 club/1 bite           |
+-----------------+-------------------------+
| Damage:         | 1d6/1d4                 |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 2d6, Wild 2d6, Lair 5d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 75                      |
+-----------------+-------------------------+

Rock baboons are a large, particularly intelligent variety of baboon. An adult male rock baboon is 4' to 5' tall and weighs 200 to 250 pounds, with females being a bit smaller and lighter.

Rock baboons are omnivorous, but prefer meat. They are aggressive, naturally cruel creatures. They will prepare ambushes in rocky or forested terrain and attack any party they outnumber.
