.. title: Whale, Narwhal
.. slug: whale-narwhal
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Whale, Narwhal
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 19         |
+-----------------+------------+
| Hit Dice:       | 12 (+10)   |
+-----------------+------------+
| No. of Attacks: | 1 horn     |
+-----------------+------------+
| Damage:         | 2d6        |
+-----------------+------------+
| Movement:       | Swim 60'   |
+-----------------+------------+
| No. Appearing:  | Wild 1d4   |
+-----------------+------------+
| Save As:        | Fighter: 6 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | Special    |
+-----------------+------------+
| XP:             | 1,875      |
+-----------------+------------+

Narwhals are aquatic mammals resembling large dolphins with a single (or rarely, double) tusk protruding straight forward from the mouth. The tusk is helical in shape, and they are sometimes cut short and sold as “unicorn horns.” However, they have no particular magical value. Narwhals are found in cold northern seas. They are not particularly aggressive.
