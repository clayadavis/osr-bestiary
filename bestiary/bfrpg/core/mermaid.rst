.. title: Mermaid
.. slug: mermaid
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, humanoid
.. description: Mermaids have the upper bodies of women and the lower bodies of dolphins. Also called “sirens,” mermaids often attempt to lure sailors or other men found near the sea.
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 12                          |
+-----------------+-----------------------------+
| Hit Dice:       | 1*                          |
+-----------------+-----------------------------+
| No. of Attacks: | 1 weapon                    |
+-----------------+-----------------------------+
| Damage:         | 1d6 or by weapon            |
+-----------------+-----------------------------+
| Movement:       | Swim 40'                    |
+-----------------+-----------------------------+
| No. Appearing:  | Wild 1d2 or 3d6 (see below) |
+-----------------+-----------------------------+
| Save As:        | Fighter: 1                  |
+-----------------+-----------------------------+
| Morale:         | 8                           |
+-----------------+-----------------------------+
| Treasure Type:  | A                           |
+-----------------+-----------------------------+
| XP:             | 37                          |
+-----------------+-----------------------------+

Mermaids have the upper bodies of women and the lower bodies of dolphins. Also called “sirens,” mermaids often attempt to lure sailors or other men found near the sea. They accomplish this by means of their enchanting songs.

**A mermaid's song will attract any man within 100 yards, but generally has no effect on women.** Men within the area of effect must save vs. Spells to resist, or else they will move toward the mermaid with amorous intent as directly as possible. If two mermaids are singing, apply a penalty of -4 to the save; more than two gives no extra benefit. Affected men will submit to anything the mermaid desires. When she tires of him, he might be freed or slain, depending on the mermaid's temperament.

Contrary to popular belief, mermaids are not fish (nor even half fish) and do not breathe water. They can hold their breath for up to an hour of light activity, or two turns (20 minutes) of strenuous action. However, being out of water more than two turns (20 minutes) causes the mermaid 1d4 points of damage per turn.

Mermaids can hear as well as dolphins, and can produce sounds ranging from the lowest frequency a normal human woman can produce up to the highest frequency of a dolphin. This means that mermaids can learn to communicate with dolphins and whales; at least 35% of mermaids will know the language of one or the other, and 10% can communicate with any such creature.

Three-quarters of mermaid births are female. Of the quarter which are male, most have legs rather than tails. Such will either be slain or put ashore to be adopted by humans, depending on the temperament of the mother. Mermen (those born with tails) are raised to be subservient to the females. A small mermaid community (3d6 including the male) will often form around such a merman and his mother, who becomes their leader. Such a group is called a pod.

One-third of female mermaids are infertile. Other mermaids can sense this, but non-mermaids cannot tell. Infertile mermaids usually remain with a fertile sister (or more rarely a close friend) to help her ensnare men. This explains the first number appearing given; in any group of 2, one will be infertile.

A mermaid with a child will not generally be encountered, as they remain in the deeper parts of the ocean and avoid the attention of men. Pods of mermaids do likewise, and in fact any pod includes 2d4-2 children/juveniles (over and above the number rolled for Number Appearing). Men generally meet mermaids only in groups of 1 or 2.

**Mermaids arm themselves with spears or daggers.** They hunt fish and harvest kelp for food. Mermaids sometimes possess more than 1 hit die, and about 3% have some Clerical abilities.
