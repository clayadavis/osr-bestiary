.. title: Hellhound
.. slug: hellhound
.. date: 2019-11-08 13:50:36.109736
.. tags: fiend
.. description: Hellhound
.. type: text
.. image:

+-----------------+------------------------------------+
| Armor Class:    | 14 to 18                           |
+-----------------+------------------------------------+
| Hit Dice:       | 3** to 7**                         |
+-----------------+------------------------------------+
| No. of Attacks: | 1 bite or 1 breath                 |
+-----------------+------------------------------------+
| Damage:         | 1d6 or 1d6 per Hit Die             |
+-----------------+------------------------------------+
| Movement:       | 40'                                |
+-----------------+------------------------------------+
| No. Appearing:  | 2d4, Wild 2d4, Lair 2d4            |
+-----------------+------------------------------------+
| Save As:        | Fighter: 3 to 7 (same as Hit Dice) |
+-----------------+------------------------------------+
| Morale:         | 9                                  |
+-----------------+------------------------------------+
| Treasure Type:  | C                                  |
+-----------------+------------------------------------+
| XP:             | 205 - 800                          |
+-----------------+------------------------------------+

Hellhounds are canine creatures sheathed in hellish flame. A typical hell hound stands 4½ feet high at the shoulder and weighs 120 pounds. They are native to another plane where they hunt in packs; sometimes powerful wizards or evil priests summon them for use as watchdogs. In addition to biting, each hellhound may breathe fire a number of times per day equal to its hit dice. In combat, one-third of the time (1-2 on 1d6) a hellhound will choose to breathe fire; otherwise it will attempt to bite. Roll each round to determine which attack form will be used.

A hellhound's breath weapon is a cone of flame 10' wide at the far end which is 10' long for those with 3 or 4 hit dice, 20' long for those with 5 or 6 hit dice, and 30' long for the largest hellhounds. This breath weapon does 1d6 points of damage per each hit die of the hellhound to all within the area of effect; a successful saving throw vs. Dragon Breath reduces damage to half normal.

Note that hellhounds vary with regard to the number of hit dice each has. If generating a group randomly, roll 1d6+1 for the hit dice of each, reading a total of 2 as 3. A hellhound has an Armor Class equal to 11 plus its hit dice.
