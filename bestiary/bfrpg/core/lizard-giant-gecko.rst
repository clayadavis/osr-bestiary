.. title: Lizard, Giant Gecko
.. slug: lizard-giant-gecko
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Lizard, Giant Gecko
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 15             |
+-----------------+----------------+
| Hit Dice:       | 3+1            |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 1d8            |
+-----------------+----------------+
| Movement:       | 40' (special)  |
+-----------------+----------------+
| No. Appearing:  | 1d6, Wild 1d10 |
+-----------------+----------------+
| Save As:        | Fighter: 2     |
+-----------------+----------------+
| Morale:         | 7              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 145            |
+-----------------+----------------+

Giant gecko lizards range from 4' to 6' in length, and are generally green in color, though grey or white versions can be found underground. They can climb walls and even walk across ceilings at full movement rate due to their specialized toe pads. They are carnivores, typically attacking weaker prey from above.
