.. title: Doppleganger
.. slug: doppleganger
.. date: 2019-11-08 13:50:36.109736
.. tags: monstrosity, shapechanger
.. description: Doppleganger
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 fist                  |
+-----------------+-------------------------+
| Damage:         | 1d12 or by weapon       |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | E                       |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

Dopplegangers are strange beings that are able to take on the shapes of those they encounter; they can also read minds (as the spell, **ESP**, but with no limit of duration). In its natural form, the creature looks more or less humanoid, but slender and frail, with gangly limbs and half-formed features. The flesh is pale and hairless. Its large, bulging eyes are yellow with slitted pupils. A doppleganger is hardy, with a natural agility not in keeping with its frail appearance. In its natural form a doppleganger is about 5½ feet tall and weighs about 150 pounds.

Dopplegangers make excellent use of their natural mimicry to stage ambushes, bait traps, and infiltrate humanoid society. Although not usually evil, they are interested only in themselves and regard all others as playthings to be manipulated and deceived.

When in its natural form, a doppleganger strikes with its powerful fists. In the shape of a warrior or some other armed person, it attacks with whatever weapon is appropriate. In such cases, it uses its mind reading power to employ the same tactics and strategies as the person it is impersonating.
