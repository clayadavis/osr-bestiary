.. title: Dinosaur, Triceratops
.. slug: dinosaur-triceratops
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Dinosaur, Triceratops
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 19                              |
+-----------------+---------------------------------+
| Hit Dice:       | 11 (+9)                         |
+-----------------+---------------------------------+
| No. of Attacks: | 1 gore or 1 trample             |
+-----------------+---------------------------------+
| Damage:         | 3d6 or 3d6 (special, see below) |
+-----------------+---------------------------------+
| Movement:       | 30' (15')                       |
+-----------------+---------------------------------+
| No. Appearing:  | Wild 1d4                        |
+-----------------+---------------------------------+
| Save As:        | Fighter: 7                      |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | None                            |
+-----------------+---------------------------------+
| XP:             | 1,575                           |
+-----------------+---------------------------------+

A triceratops is a three-horned herbivorous dinosaur. They are aggressive toward interlopers, attacking anyone who might appear to be a threat. These creatures are about 25 feet long and weigh about 20,000 pounds. A triceratops will usually attempt to trample smaller opponents. Up to two adjacent man-sized or up to four smaller opponents may be trampled simultaneously; the triceratops rolls a single attack roll which is compared to the Armor Class of each of the potential victims, and then rolls a separate damage roll for each one successfully hit. The gore attack may only be used against a single man-sized or larger creature, but may be used in the same round as the trample if the creature being gored is larger than man sized. Also note that a charging bonus may be applied to the gore attack.
