.. title: Snake, Spitting Cobra
.. slug: snake-spitting-cobra
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Snake, Spitting Cobra
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 13                        |
+-----------------+---------------------------+
| Hit Dice:       | 1*                        |
+-----------------+---------------------------+
| No. of Attacks: | 1 bite or 1 spit          |
+-----------------+---------------------------+
| Damage:         | 1d4 + poison or blindness |
+-----------------+---------------------------+
| Movement:       | 30'                       |
+-----------------+---------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1d6   |
+-----------------+---------------------------+
| Save As:        | Fighter: 1                |
+-----------------+---------------------------+
| Morale:         | 7                         |
+-----------------+---------------------------+
| Treasure Type:  | None                      |
+-----------------+---------------------------+
| XP:             | 37                        |
+-----------------+---------------------------+

Spitting cobras average about 7' in length at adulthood. They use their spreading hood to warn other creatures not to bother them, and generally refrain from attacking if possible to allow larger creatures time to retreat. Failure to retreat from the spitting cobra will likely result in the cobra spitting venom; the cobra can project its venom up to 5', and any creature hit must roll a save vs. Poison or be blinded permanently (though the **cure blindness** spell can be used to heal this injury). If the cobra cannot deter a creature by spitting, it will attack using its bite. In this case, those successfully hit must save vs. Poison or die.
