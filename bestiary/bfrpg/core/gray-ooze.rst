.. title: Gray Ooze
.. slug: gray-ooze
.. date: 2019-11-08 13:50:36.109736
.. tags: ooze
.. description: Gray Ooze
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 12          |
+-----------------+-------------+
| Hit Dice:       | 3*          |
+-----------------+-------------+
| No. of Attacks: | 1 pseudopod |
+-----------------+-------------+
| Damage:         | 2d8         |
+-----------------+-------------+
| Movement:       | 1'          |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 3  |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 175         |
+-----------------+-------------+

Gray oozes are amorphous creatures that live only to eat. They inhabit underground areas, scouring caverns, ruins, and dungeons in search of organic matter, living or dead. A gray ooze can grow to a diameter of up to 10 feet and a thickness of about 6 inches. A typical specimen weighs about 700 pounds.

A gray ooze secretes a digestive acid that quickly dissolves organic material and metal, but not stone. After a successful hit, the ooze will stick to the creature attacked, dealing 2d8 damage per round automatically. Normal (non-magical) armor or clothing dissolves and becomes useless immediately. A non-magical metal or wooden weapon that strikes a gray ooze also dissolves immediately. Magical weapons, armor, and clothing are allowed a saving throw (use the wearer's save vs. Death Ray, adding any magical “plus” value to the roll if applicable).
