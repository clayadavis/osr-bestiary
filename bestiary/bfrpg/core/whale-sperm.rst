.. title: Whale, Sperm
.. slug: whale-sperm
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Whale, Sperm
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 22                |
+-----------------+-------------------+
| Hit Dice:       | 36* (+16)         |
+-----------------+-------------------+
| No. of Attacks: | 1 bite or special |
+-----------------+-------------------+
| Damage:         | 3d20              |
+-----------------+-------------------+
| Movement:       | Swim 60' (20')    |
+-----------------+-------------------+
| No. Appearing:  | Wild 1d3          |
+-----------------+-------------------+
| Save As:        | Fighter: 8        |
+-----------------+-------------------+
| Morale:         | 7                 |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 17,850            |
+-----------------+-------------------+

These creatures can be up to 60 feet long. They prey on giant squid. Sperm whales can emit an invisible focused beam of sound 5' wide up to a 50' range underwater. This blast of sound disorients target creatures, leaving them effectively stunned for 1d4 rounds. A stunned character can neither move nor take action for the indicated duration. No attack roll is required, but a save vs. Death Ray is allowed to resist. A sperm whale can emit as many such blasts of sound as it desires, once per round, instead of biting.
