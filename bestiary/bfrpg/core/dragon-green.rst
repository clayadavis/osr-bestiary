.. title: Dragon, Green
.. slug: dragon-green
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying
.. description: Dragon, Green
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 19                              |
+-----------------+---------------------------------+
| Hit Dice:       | 8**                             |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 1d6/1d6/3d8 or breath/1d6       |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80' (15')               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4             |
+-----------------+---------------------------------+
| Save As:        | Fighter: 8 (as Hit Dice)        |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 1,015                           |
+-----------------+---------------------------------+

Green dragons initiate fights with little or no provocation, picking on creatures of any size. If the target is intriguing or seems formidable, the dragon stalks the creature to determine the best time to strike and the most appropriate tactics to use. If the target appears weak, the dragon makes its presence known quickly – it enjoys evoking terror.

Green dragons especially like to question adventurers to learn more about their society and abilities, what is going on in the countryside, and if there is treasure nearby. Adventurers may be allowed to live so long as they remain interesting… but woe to them when the dragon becomes bored.

Green dragons are immune to all poisons. Note that, despite their breath weapon being described as "poison gas," damage done by it is exactly the same as with other dragons. More specifically, those in the area of effect do not have to "save or die" as with ordinary poison, but rather save vs. Breath Weapon for half damage.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
