.. title: Ogre
.. slug: ogre
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Ogre
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 (12)                 |
+-----------------+-------------------------+
| Hit Dice:       | 4+1                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon                |
+-----------------+-------------------------+
| Damage:         | 2d6                     |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d6, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | C + 1d20x100 gp         |
+-----------------+-------------------------+
| XP:             | 240                     |
+-----------------+-------------------------+

Ogres appear as large, very ugly humans. Adult ogres stand 9 to 10 feet tall and weigh 600 to 650 pounds. Their skin color ranges from dull yellow to dull brown. Their clothing consists of poorly cured furs and hides, which add to their naturally repellent odor. Ogres are brutish and aggressive, but inherently lazy. They employ direct attacks in combat, typically using large clubs, axes, or pole arms, generally causing 2d6 damage. If normal weapons are employed, an ogre has a +3 bonus to damage due to strength. If an ogre fights bare-handed, it does 1d8 subduing damage per hit.

One out of every six ogres will be a pack leader of 6+1 Hit Dice (500 XP). Ogres gain a +1 bonus to their morale if they are led by a pack leader. In ogre lairs of 10 or greater, there will also be an ogre bully of 8+2 Hit Dice (875 XP), with an Armor Class of 17 (13) (movement 20') and having a +4 bonus to damage due to strength. Ogre bullies generally wire together pieces of chainmail to wear over their hides. Ogres gain +2 to morale so long as the ogre bully is present (and alive).
