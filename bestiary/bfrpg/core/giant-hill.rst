.. title: Giant, Hill
.. slug: giant-hill
.. date: 2019-11-08 13:50:36.109736
.. tags: giant
.. description: Giant, Hill
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 (13)                 |
+-----------------+-------------------------+
| Hit Dice:       | 8                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 giant weapon (club)   |
+-----------------+-------------------------+
| Damage:         | 2d8                     |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 8              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | E plus 1d8x1000 gp      |
+-----------------+-------------------------+
| XP:             | 875                     |
+-----------------+-------------------------+

The smallest of giants, adult hill giants stand between ten and twelve feet in height and weigh about 1,100 pounds. Hill giants can live to be 200 years old. Skin color among hill giants ranges from light tan to deep ruddy brown. They have brown or black hair and eyes the same color. They wear layers of crudely prepared hides, which they seldom wash or repair, preferring to simply add more hides as the old ones wear out.

Whether attacking with a weapon or st, hill giants deal 2d8 damage. Hill giants are brutish and aggressive. They are sometimes found leading groups of ogres or bugbears. Hill giants often keep **dire wolves** as pets.
