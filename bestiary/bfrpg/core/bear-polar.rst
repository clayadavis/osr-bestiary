.. title: Bear, Polar
.. slug: bear-polar
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Bear, Polar
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 14                     |
+-----------------+------------------------+
| Hit Dice:       | 6                      |
+-----------------+------------------------+
| No. of Attacks: | 2 claws/1 bite + hug   |
+-----------------+------------------------+
| Damage:         | 1d6/1d6/1d10 + 2d8 hug |
+-----------------+------------------------+
| Movement:       | 40'                    |
+-----------------+------------------------+
| No. Appearing:  | 1, Wild 1d2, Lair 1d2  |
+-----------------+------------------------+
| Save As:        | Fighter: 6             |
+-----------------+------------------------+
| Morale:         | 8                      |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 500                    |
+-----------------+------------------------+

These long, lean carnivores are slightly taller than grizzly bears, and just as hostile.
