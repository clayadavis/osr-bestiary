.. title: Living Statue
.. slug: living-statue
.. date: 2019-11-08 13:50:36.109736
.. tags: construct
.. description: Living Statue
.. type: text
.. image:

Living statues are magically animated. They are true automatons, unlike golems, which are animated by elemental spirits. While this means that living statues have no chance of going “berserk,” it also means that they may only perform simple programmed activities. They may not be commanded in any meaningful fashion. They make very effective guards for tombs, treasure rooms, and similar places.

Living statues can be crafted to resemble any sort of living creature, but most commonly are made to look like humans or demi-humans.
