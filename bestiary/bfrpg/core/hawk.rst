.. title: Hawk
.. slug: hawk
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying
.. description: Hawk
.. type: text
.. image:

+-----------------+--------------------+--------------------+
|                 | **Normal**         | **Giant**          |
+=================+====================+====================+
| Armor Class:    | 12                 | 14                 |
+-----------------+--------------------+--------------------+
| Hit Dice:       | 1d4 Hit Points     | 4                  |
+-----------------+--------------------+--------------------+
| No. of Attacks: | 1 claw or bite     | 1 claw or bite     |
+-----------------+--------------------+--------------------+
| Damage:         | 1d2                | 1d6                |
+-----------------+--------------------+--------------------+
| Movement:       | Fly 160'           | Fly 150' (10')     |
+-----------------+--------------------+--------------------+
| No. Appearing:  | Wild 1d6, Lair 1d6 | Wild 1d3, Lair 1d3 |
+-----------------+--------------------+--------------------+
| Save As:        | Fighter: 1         | Fighter: 4         |
+-----------------+--------------------+--------------------+
| Morale:         | 7                  | 8                  |
+-----------------+--------------------+--------------------+
| Treasure Type:  | None               | None               |
+-----------------+--------------------+--------------------+
| XP:             | 10                 | 240                |
+-----------------+--------------------+--------------------+

Hawks are similar to eagles but slightly smaller, being 1 to 2 feet long with wingspans of 6 feet or less.

Giant hawks are 4 to 6 feet long, with wingspans of 12 feet or more; they can carry off creatures of Halfling size or smaller.
