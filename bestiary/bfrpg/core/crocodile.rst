.. title: Crocodile
.. slug: crocodile
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, beast
.. description: Crocodile
.. type: text
.. image:

+-----------------+------------+------------+-------------+
|                 | **Normal** | **Large**  | **Giant**   |
+=================+============+============+=============+
| Armor Class:    | 15         | 17         | 19          |
+-----------------+------------+------------+-------------+
| Hit Dice:       | 2          | 6          | 15 (+11)    |
+-----------------+------------+------------+-------------+
| No. of Attacks: | 1 bite     | 1 bite     | 1 bite      |
+-----------------+------------+------------+-------------+
| Damage:         | 1d8        | 2d8        | 3d8         |
+-----------------+------------+------------+-------------+
| Movement:       | 30' (10') Swim 30' (10')              |
+-----------------+------------+------------+-------------+
| No. Appearing:  | Wild 1d8   | Wild 1d4   | Wild 1d3    |
+-----------------+------------+------------+-------------+
| Save As:        | Fighter: 2 | Fighter: 6 | Fighter: 15 |
+-----------------+------------+------------+-------------+
| Morale:         | 7          | 8          | 9           |
+-----------------+------------+------------+-------------+
| Treasure Type:  | None       | None       | None        |
+-----------------+------------+------------+-------------+
| XP:             | 75         | 500        | 2,850       |
+-----------------+------------+------------+-------------+

Crocodiles are aggressive predators 11 to 12 feet long. They lie mostly submerged in rivers or marshes, with only their eyes and nostrils showing, waiting for prey to come within reach; when in their natural element, they surprise on 1-4 on 1d6.

**Large Crocodiles:** These huge creatures are from 12-20 feet long. Large crocodiles fight and behave like their smaller cousins.

**Giant Crocodiles:** These huge creatures usually live in salt water and are generally more than 20 feet long. Giant crocodiles fight and behave like their smaller cousins.
