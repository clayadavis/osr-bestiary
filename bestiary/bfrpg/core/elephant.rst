.. title: Elephant
.. slug: elephant
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Elephant
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 18                   |
+-----------------+----------------------+
| Hit Dice:       | 9 (+8)               |
+-----------------+----------------------+
| No. of Attacks: | 2 tusks or 1 trample |
+-----------------+----------------------+
| Damage:         | 2d4/2d4 or 4d8       |
+-----------------+----------------------+
| Movement:       | 40' (15')            |
+-----------------+----------------------+
| No. Appearing:  | Wild 1d20            |
+-----------------+----------------------+
| Save As:        | Fighter: 9           |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | special              |
+-----------------+----------------------+
| XP:             | 1,075                |
+-----------------+----------------------+

Massive herbivores of tropical lands, elephants are unpredictable creatures but nevertheless are sometimes used as mounts or beasts of burden. This entry describes an African elephant. Indian elephants are slightly smaller and weaker, but more readily trained.

A light load for an African elephant is 7,500 pounds; a heavy load, up to 15,000 pounds. For an Indian elephant, a light load is up to 7,000 pounds, and a heavy load up to 14,000 pounds.

An elephant has no treasure as such, but the tusks of an elephant are worth 1d8 x 100 gp.
