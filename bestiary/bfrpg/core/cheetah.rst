.. title: Cheetah
.. slug: cheetah
.. date: 2019-11-08 13:50:36.109736
.. tags: beast
.. description: Cheetah
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 2                  |
+-----------------+--------------------+
| No. of Attacks: | 2 claws/1 bite     |
+-----------------+--------------------+
| Damage:         | 1d4/1d4/2d4        |
+-----------------+--------------------+
| Movement:       | 100'               |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d3, Lair 1d3 |
+-----------------+--------------------+
| Save As:        | Fighter: 2         |
+-----------------+--------------------+
| Morale:         | 7                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 75                 |
+-----------------+--------------------+

A Cheetah is one of the fastest land animals - a large (about 100 pounds) cat capable of reaching up to 75 miles per hour when running. It hunts alone or in small groups (usually composed of siblings). It will rarely attack humans unless compelled to do so, but a female will ferociously defend her young.
