.. title: Lycanthrope, Wererat*
.. slug: lycanthrope-wererat
.. date: 2019-11-08 13:50:36.109736
.. tags: humanoid, shapechanger
.. description: Lycanthrope, Wererat*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 13 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 3*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite or 1 weapon      |
+-----------------+-------------------------+
| Damage:         | 1d4 or 1d6 or by weapon |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d8, Wild 2d8, Lair 2d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 175                     |
+-----------------+-------------------------+

A wererat in human form tends to be a thin, wiry individual of shorter than average height, with eyes constantly darting around. A wererat's nose and mouth may twitch if he or she is excited. Males often wear thin, ragged mustaches.

In addition to assuming the form of a giant rat, wererats can assume an intermediate form (a “ratman”). The ratman form shares the animal form's immunity to normal weapons, and can deliver an identical bite, but in this form the wererat may use a normal weapon instead of biting. Note that the wererat in ratman form cannot bite and use a weapon in the same round.

Unlike most lycanthropes, wererats prefer to inhabit civilized areas, particularly cities. They frequently lair in sewers or other underground areas, coming out by night to steal from or kill city folk.
