.. title: Fish, Giant Bass
.. slug: fish-giant-bass
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast
.. description: Fish, Giant Bass
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 13             |
+-----------------+----------------+
| Hit Dice:       | 2              |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 1d6            |
+-----------------+----------------+
| Movement:       | Swim 40' (10') |
+-----------------+----------------+
| No. Appearing:  | Wild 1d6       |
+-----------------+----------------+
| Save As:        | Fighter: 2     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 75             |
+-----------------+----------------+

Giant bass are generally between 10' and 25' long. Most are greenish-grey, marked with dark lateral stripes, though some are almost completely black. They are generally found in lakes or rivers, as they are not adapted for salt water.

Giant bass are predatory, and on a natural attack roll of 20 a giant bass will swallow whole a dwarf-sized or smaller creature, which then takes 2d4 damage per round until it is dead. Swallowed characters can attack only with daggers or similar short weapons. Note that each giant bass can swallow at most one character, and a giant bass which has swallowed a character will attempt to retreat (having achieved its goal).
