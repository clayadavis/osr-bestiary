.. title: Resonated
.. slug: resonated
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Resonated
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 16                       |
+-----------------+--------------------------+
| Hit Dice:       | 3*                       |
+-----------------+--------------------------+
| No. of Attacks: | 1 weapon                 |
+-----------------+--------------------------+
| Damage:         | 1d8 or by weapon         |
+-----------------+--------------------------+
| Movement:       | 20'                      |
+-----------------+--------------------------+
| No. Appearing:  | See **Deadringer** entry |
+-----------------+--------------------------+
| Save As:        | Fighter: 3               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 150                      |
+-----------------+--------------------------+

A **Resonated** is an undead skeletal being created by a deadringer. It will seek anyone who bears the deadringer's curse. A resonated has such strong magic running through it that it regenerates 1d8 HP per hour. Upon reaching 0 HP it must wait until it reaches full HP before moving again. A resonated will continue to regenerate until the deadringer's curse is removed or the PCs kill the one who is cursed; after this the deadringer can be killed permanently. A resonated takes half damage from edged weapons and only 1 from bolts arrows or sling bullets. It is immune to control magic but can be Turned.
