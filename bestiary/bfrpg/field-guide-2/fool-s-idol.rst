.. title: Fool’s Idol
.. slug: fool-s-idol
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Fool’s Idol
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 15                |
+-----------------+-------------------+
| Hit Dice:       | 2*                |
+-----------------+-------------------+
| No. of Attacks: | 2 claws or 1 coin |
+-----------------+-------------------+
| Damage:         | 2d4/2d4 or 1d12   |
+-----------------+-------------------+
| Movement:       | 30'               |
+-----------------+-------------------+
| No. Appearing:  | 1d6               |
+-----------------+-------------------+
| Save As:        | Fighter: 2        |
+-----------------+-------------------+
| Morale:         | 10                |
+-----------------+-------------------+
| Treasure Type:  | H                 |
+-----------------+-------------------+
| XP:             | 100               |
+-----------------+-------------------+

A **Fool's Idol** appears as a golden, grotesque humanoid statue, roughly 2 to 4 feet in height. They appear to be constructs of some kind, or perhaps relatives of the gargoyle; though golden in appearance, they are actually made of fool's gold (iron pyrite). Though small, a fool's idol is very strong, and has a pair of vicious claws with which it attacks. A fool's idol may, alternately, choose to pick up and throw gold pieces (common in their "natural" habitat) at foes up to 40’ away, doing significant damage due to their great strength.

A fool's idol can be found in large concentrations of gold; for example, a large treasure hoard. They are often kept in the hoards of dragons or other powerful monsters as guardians for the treasure.
