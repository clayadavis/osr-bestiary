.. title: Wasp, Giant
.. slug: wasp-giant
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Wasp, Giant
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 15                          |
+-----------------+-----------------------------+
| Hit Dice:       | 4+1*                        |
+-----------------+-----------------------------+
| No. of Attacks: | 1 sting                     |
+-----------------+-----------------------------+
| Damage:         | 1d6 + poison                |
+-----------------+-----------------------------+
| Movement:       | 20' Fly 60'                 |
+-----------------+-----------------------------+
| No. Appearing:  | 1, Wild 1d4+1, Lair 1d10+10 |
+-----------------+-----------------------------+
| Save As:        | Fighter: 4                  |
+-----------------+-----------------------------+
| Morale:         | 10                          |
+-----------------+-----------------------------+
| Treasure Type:  | None                        |
+-----------------+-----------------------------+
| XP:             | 280                         |
+-----------------+-----------------------------+

A **Giant** **Wasp** attacks when hungry or threatened, stinging its prey. It takes dead or incapacitated victims back to its lair as food for its unhatched young. A victim stung by a giant wasp must make a saving throw vs. Poison or die.
