.. title: Loup
.. slug: loup
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Loup
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 16                       |
+-----------------+--------------------------+
| Hit Dice:       | 4                        |
+-----------------+--------------------------+
| No. of Attacks: | 2 claws/1 bite or weapon |
+-----------------+--------------------------+
| Damage:         | 1d6/1d6/1d8 or by weapon |
+-----------------+--------------------------+
| Movement:       | 40' Leap 60'             |
+-----------------+--------------------------+
| No. Appearing:  | 1d8                      |
+-----------------+--------------------------+
| Save As:        | Fighter: 3               |
+-----------------+--------------------------+
| Morale:         | 10                       |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 240                      |
+-----------------+--------------------------+

A **Loup** is a wolf-like humanoid, standing around 7' tall with thick black fur all over its body. It is a clever being, capable of utilizing cunning traps and often ambushing its prey. Loups are descended from wolves and are able to communicate with wolves as well as speak Common. A loup scarcely leaves its lair without being accompanied by at least 2d8 wolves. A loup enjoys the hunt and will usually chase after its prey for sport, often capturing humans and other creatures to release and hunt at its leisure. Small children are a particular favorite of loups for they provide the greatest entertainment.
