.. title: Bartleby's Wondrous Automaton
.. slug: bartleby-s-wondrous-automaton
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bartleby's Wondrous Automaton
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 18                     |
+-----------------+------------------------+
| Hit Dice:       | 4                      |
+-----------------+------------------------+
| No. of Attacks: | 1 steam blast/1 weapon |
+-----------------+------------------------+
| Damage:         | 1d12/by weapon         |
+-----------------+------------------------+
| Movement:       | 40'                    |
+-----------------+------------------------+
| No. Appearing:  | Wild 3d8, Lair 10d10   |
+-----------------+------------------------+
| Save As:        | Fighter: 4             |
+-----------------+------------------------+
| Morale:         | 12                     |
+-----------------+------------------------+
| Treasure Type:  | H                      |
+-----------------+------------------------+
| XP:             | 320                    |
+-----------------+------------------------+

A **Bartleby's Wondrous Automaton** is a seven-foot tall steel or brass humanoid skeleton. It is a mechanical contraption, animated by extremely intricate clockwork with hot steam in the place of blood. It is theorized that it was constructed by the mad wizard and famous author Bartleby the Bard, during his later years as paranoia overtook him before his untimely and unusually mysterious death. They can frequently be found guarding the enormous amount of books Bartleby wrote in strange but wonderful library fortresses.

A wondrous automaton can vary in appearance, from nothing more than a drab but intimidating gray steel, to resplendent gleaming brass and copper, with plates of engraved metal covering its inner workings and more vulnerable areas. It wields a wide variety of weapons with great competence but prefers crossbows and axes. Its components are highly valuable and sought after by many collectors and smiths.

In close quarters it can expel searingly hot steam from its face in a 10' cone, doing 1d12 damage. Those hit can save vs. Dragon Breath for half damage.

One in every 10 will be an **automaton alpha** (610 XP) which stands 9' tall, can breathe flame twice a day, has 6 HD, and are heavily-armored (AC 20). One in every 50 will be an **automaton primus** (1,480 XP) which stands 12' tall, has 10 HD, can breathe flame-infused steam 3d12 three times a day, and has an AC of 22.
