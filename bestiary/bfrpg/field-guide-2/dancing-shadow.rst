.. title: Dancing Shadow*
.. slug: dancing-shadow
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dancing Shadow*
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 19‡              |
+-----------------+------------------+
| Hit Dice:       | 3                |
+-----------------+------------------+
| No. of Attacks: | 1 claw + special |
+-----------------+------------------+
| Damage:         | 1d6 + special    |
+-----------------+------------------+
| Movement:       | 40’              |
+-----------------+------------------+
| No. Appearing:  | 1d6-1            |
+-----------------+------------------+
| Save As:        | Fighter: 3       |
+-----------------+------------------+
| Morale:         | 8                |
+-----------------+------------------+
| Treasure Type:  | A                |
+-----------------+------------------+
| XP:             | 175              |
+-----------------+------------------+

The **Dancing Shadow** is a rare and unusual monster that seemingly requires no sustenance and acts randomly and without reason. With prolonged observation however, patterns in its actions become apparent, often too late for those involved, that suggest some strange or nefarious agenda.

A dancing shadow appears to be a regular shadow at a cursory glance; upon contact with a humanoid creature it latches on, disguising itself as the person’s shadow almost flawlessly. The observant, however, may notice that it never flickers and sometimes is not in the direction that the local light source would cause. It will then remain dormant for a period of several days before striking, often at highly inopportune moments. With a successful attack roll the target must succeed a save vs. Spells. If the save is passed, the shadow will transfer to the nearest humanoid unless it is warded with a circle of salt. If the save fails, the target will lose control of his or her body which the shadow will puppet, causing them to dance uncontrollably. At this moment the shadow is vulnerable to attacks from magical weapons and spells. If not saved from its fate, the victim will die of exhaustion in 1d6 (+ Constitution modifier) hours.
