.. title: Fairy, Forest*
.. slug: fairy-forest
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Fairy, Forest*
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 18‡                            |
+-----------------+--------------------------------+
| Hit Dice:       | 10*                            |
+-----------------+--------------------------------+
| No. of Attacks: | 4 claws + poison               |
+-----------------+--------------------------------+
| Damage:         | 1d8/1d8/1d8/1d8 (all + poison) |
+-----------------+--------------------------------+
| Movement:       | 30’                            |
+-----------------+--------------------------------+
| No. Appearing:  | 1                              |
+-----------------+--------------------------------+
| Save As:        | Magic-user: 10                 |
+-----------------+--------------------------------+
| Morale:         | 12                             |
+-----------------+--------------------------------+
| Treasure Type:  | none                           |
+-----------------+--------------------------------+
| XP:             | 1,390                          |
+-----------------+--------------------------------+

A **Forest Fairy** is a manifestation of the consciousness of the forest, appearing with leaves, thorns, and a leaf green and wood brown motif as its attributes.

It attacks by slashing with its claws, which have the effect of a powerful hallucinogen, making every tree or humanoid creature (including party members) appear as the fairy itself.
