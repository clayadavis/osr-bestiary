.. title: Bloodshroud
.. slug: bloodshroud
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bloodshroud
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 12                  |
+-----------------+---------------------+
| Hit Dice:       | 4**                 |
+-----------------+---------------------+
| No. of Attacks: | 1 whip or 1 special |
+-----------------+---------------------+
| Damage:         | 1d4 or special      |
+-----------------+---------------------+
| Movement:       | Fly 60’             |
+-----------------+---------------------+
| No. Appearing:  | 1d2                 |
+-----------------+---------------------+
| Save As:        | Fighter: 4          |
+-----------------+---------------------+
| Morale:         | 9                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 320                 |
+-----------------+---------------------+

A **Bloodshroud** resembles a floating man covered in a bed sheet, soaked in dripping fresh blood. It takes half damage from bludgeoning weapons, and only 1 point from piercing weapons (plus any magical bonus).

A bloodshroud attacks by spraying blood up to 5’ at any opponent's eyes. An opponent hit must save vs. Poison or be blinded permanently. It can also attack by whipping with its cloak. If the bloodshroud hits and surpasses the victim's AC by 5 points, the victim will be pulled under the shroud, where they will be paralyzed and suffer 1 energy drain per round due to bloodsucking until death. The bloodshroud’s victim receives half of any damage suffered by the shroud, except for piercing or bludgeoning damage, which will be normal.

While bloodsucking a victim, the shroud may only move 20' per round, may not fly, and suffers a -2 penalty to Armor Class. Anytime the bloodshroud receives damage, the victim is entitled to a save vs. Paralysis to escape.

As with all undead monsters, a bloodshroud is immune to **sleep**, **charm**, and **hold** magic, as well as any spells affecting the mind. Bloodshrouds can be Turned by a Cleric, as if it were a Wraith.
