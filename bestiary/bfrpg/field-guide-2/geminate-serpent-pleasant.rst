.. title: Geminate Serpent, Pleasant
.. slug: geminate-serpent-pleasant
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Geminate Serpent, Pleasant
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 22                               |
+-----------------+----------------------------------+
| Hit Dice:       | 10**                             |
+-----------------+----------------------------------+
| No. of Attacks: | 2 bite or 1 breath or constrict* |
+-----------------+----------------------------------+
| Damage:         | 3d8/3d8 or breath or 1d6*        |
+-----------------+----------------------------------+
| Movement:       | 50’                              |
+-----------------+----------------------------------+
| No. Appearing:  | Lair 1d3+1                       |
+-----------------+----------------------------------+
| Save As:        | Fighter: 12                      |
+-----------------+----------------------------------+
| Morale:         | 10                               |
+-----------------+----------------------------------+
| Treasure Type:  | H+K                              |
+-----------------+----------------------------------+
| XP:             | 1,800                            |
+-----------------+----------------------------------+

A **Pleasant Geminate Serpent** is a cruel being with the unique ability to hide in plain sight; any victim that looks upon this creature can see it and are usually overcome with fear until the victim looks away. A victim that looks away will not remember seeing the creature, or that it even exists. The pleasant geminate serpent then simply slithers up to its prey and picks them up. The breath weapon has a similar effect and will wipe the memories of all creatures caught for the length of time specified in the table below; the breath also fills the minds of those affected with pleasant memories of time forgotten. Any victim who successfully saves against the breath weapon twice becomes immune to it.

**Pleasant Geminate Serpent Age Table**

+--------------+-----+------+-----+-----+-----+-----+------+
| Age Category | 1   | 2    | 3   | 4   | 5   | 6   | 7    |
+--------------+-----+------+-----+-----+-----+-----+------+
| Length       | 50’ | 60’  | 70’ | 80’ | 80’ | 90’ | 100’ |
+--------------+-----+------+-----+-----+-----+-----+------+
| Hit Dice     | 8   | 9    | 10  | 10  | 11  | 11  | 12   |
+--------------+-----+------+-----+-----+-----+-----+------+
| Attack Bonus | +8  | +8   | +9  | +9  | +10 | +10 | +11  |
+--------------+-----+------+-----+-----+-----+-----+------+
| Breath Type  | Forget                                    |
+--------------+-----+------+-----+-----+-----+-----+------+
| Length       | -   | 30’  | 40’ | 40’ | 50’ | 60’ | 60’  |
+--------------+-----+------+-----+-----+-----+-----+------+
| Width        | -   | 20’  | 20’ | 30’ | 30’ | 40’ | 50’  |
+--------------+-----+------+-----+-----+-----+-----+------+
| Time (Turns) | -   | ½    | 1   | 1   | 1½  | 2   | 2    |
+--------------+-----+------+-----+-----+-----+-----+------+
| Bite         | 2d8 | 2d10 | 5d4 | 3d8 | 3d8 | 4d6 | 5d6  |
+--------------+-----+------+-----+-----+-----+-----+------+
| Constrict    | 3d4 | 3d4  | 2d8 | 3d6 | 3d6 | 3d8 | 4d6  |
+--------------+-----+------+-----+-----+-----+-----+------+
| Talk         | 5%  | 30%  | 40% | 50% | 60% | 70% | 75%  |
+--------------+-----+------+-----+-----+-----+-----+------+
