.. title: Wind Wolf*
.. slug: wind-wolf
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Wind Wolf*
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 20‡                  |
+-----------------+----------------------+
| Hit Dice:       | 8*                   |
+-----------------+----------------------+
| No. of Attacks: | 2 claws or 1 howl    |
+-----------------+----------------------+
| Damage:         | 2d12/2d12 or special |
+-----------------+----------------------+
| Movement:       | 30’ Fly 120’         |
+-----------------+----------------------+
| No. Appearing:  | 1                    |
+-----------------+----------------------+
| Save As:        | Fighter: 8           |
+-----------------+----------------------+
| Morale:         | 11                   |
+-----------------+----------------------+
| Treasure Type:  | L                    |
+-----------------+----------------------+
| XP:             | 945                  |
+-----------------+----------------------+

A **Wind Wolf** is an ethereal wolf composed of intricate air currents. It has a set of tempestuous claws that it uses to rend through foes. Once every other round it can release a howl that summons a gale strong enough to push even the bulkiest adventurer back 5d10’.

A wind wolf is most commonly found in windy places, enjoying the tempest at the tops of tall towers and the squall at the summits of mountains.
