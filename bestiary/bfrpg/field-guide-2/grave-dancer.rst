.. title: Grave Dancer*
.. slug: grave-dancer
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Grave Dancer*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 16‡        |
+-----------------+------------+
| Hit Dice:       | Special    |
+-----------------+------------+
| No. of Attacks: | None       |
+-----------------+------------+
| Damage:         | None       |
+-----------------+------------+
| Movement:       | 20'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 8 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 360        |
+-----------------+------------+

A **Grave Dancer** is an undead monster appearing as a macabre figure dressed in brightly-colored but worn and tattered clothing. It can be found anywhere there are undead monsters having 4 or fewer hit dice, especially in cemeteries and tombs, and one grave dancer will be accompanied by at least 6 such creatures. When encountered, the grave dancer and its attendant undead will be dancing to strange dismal music which seems to have no source.

All undead monsters within 60' of the grave dancer will dance, receiving a +3 bonus to attack rolls and saving throws. Living creatures within the same 60' radius of the grave dancer suffer a penalty of -2 to attack rolls and saving throws, and a penalty of -1 to morale. Creatures which cannot hear the music will be unaffected.

As with all undead monsters, a grave dancer is immune to **sleep**, **charm**, and **hold** magic, as well as any spell affecting the mind. A grave dancer can only be hit by magical weapons, but when hit the grave dancer suffers no damage; instead it disappears (along with the accompanying music) for 1d6 rounds. Only successfully Turning one (as a vampire) or eliminating all other undead within the area will vanquish a grave dancer.
