.. title: Advi
.. slug: advi
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Advi
.. type: text
.. image:

The **Advi** are highly advanced machines far beyond the capability of mortals to understand. They appear as silvery spheres with faint green geometric patterns on their surfaces. They are all immune to damage from non-magical fire and take only half from magical fire. They each hold a single ‘core’, a perfect red sphere six inches in diameter. This can be used as a component in magical items and therefore can be easily sold for 1,000 gp. The advi are immune to **charm** and **fear** effects and always fight until destroyed.
