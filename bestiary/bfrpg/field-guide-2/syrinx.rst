.. title: Syrinx
.. slug: syrinx
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Syrinx
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 19                   |
+-----------------+----------------------+
| Hit Dice:       | 3                    |
+-----------------+----------------------+
| No. of Attacks: | 2 claws + bite       |
+-----------------+----------------------+
| Damage:         | 1d10/1d10 + poison   |
+-----------------+----------------------+
| Movement:       | 60'                  |
+-----------------+----------------------+
| No. Appearing:  | Wild 2d6, Lair 2d6x5 |
+-----------------+----------------------+
| Save As:        | Fighter: 3           |
+-----------------+----------------------+
| Morale:         | 10                   |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 105                  |
+-----------------+----------------------+

The **Syrinx** is a quadrupedal insect-like creature with four long segmented legs ending in long and sharp blade-like protrusions. It stands 9’ tall with its legs supporting a central torso, plated in shimmering black chitin, with two arms identical to its legs. The torso ends in a bulbous appendage covered in matte white eyes of various sizes as well as a pair of spiked and razor sharp mandibles.

Extremely aggressive, the syrinx commonly exhibits swarm behavior in combat and will attack intelligently, preferring opponents with little to no armor. The syrinx prefers to attack by clawing at its victims with its forelegs. If both attacks hit a single enemy, it will be impaled and then the syrinx will bite its enemy, injecting poison. This is oddly enough less poisonous than its flesh and saves at a -3.

If injured, its insides are spongy, porous, and matte white, with many smaller creatures existing within. Wounds will leak a viscous black liquid that will quickly solidify and then sublime into the air as a dark poisonous gas. The flesh and liquids are extremely poisonous and any attempt to consume them will require a save vs. Poison at a penalty of -5.

The syrinx is a nocturnal hunter and lives primarily underground. Its long, blade-like limbs are excellent at mining and it tends to carve its hive into solid rock, creating perfectly circular passages 15-20' across; it also secretes a hard black resin onto the walls. The passages created can extend for miles in all directions depending on the size of the hive, and will join together at several spherical central chambers.

The cry of the syrinx has been described as a single high-pitched and baleful note that fluctuates slightly and known to inflict gut-wrenching fear upon those who hear it, even if they do not know of the creature it belongs to. Any within earshot must roll beneath their Wisdom on 1d20 or flee in terror. Creatures and characters above 6 HD are immune to this effect unless extremely cowardly.
