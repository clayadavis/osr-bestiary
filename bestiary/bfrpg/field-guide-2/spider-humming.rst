.. title: Spider, Humming
.. slug: spider-humming
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Spider, Humming
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 12              |
+-----------------+-----------------+
| Hit Dice:       | 1               |
+-----------------+-----------------+
| No. of Attacks: | 4 slices        |
+-----------------+-----------------+
| Damage:         | 1d8/1d8/1d8/1d8 |
+-----------------+-----------------+
| Movement:       | 60’ Jump 15’    |
+-----------------+-----------------+
| No. Appearing:  | 2d4             |
+-----------------+-----------------+
| Save As:        | Fighter: 1      |
+-----------------+-----------------+
| Morale:         | 6               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 27              |
+-----------------+-----------------+

The **Humming Spider** is a dinner plate-sized arachnid shaped almost like a disk with an elongated head sporting two long prehensile probosces. Its rear four legs are thick and sturdy, easily able to bear its whole weight as well as being able to propel the spider into the air. The front four legs are long, thin, and extremely sharp. Humming spiders are nomadic and travel in small groups of two to eight.

The humming spider is cowardly and will flee if faced with large numbers or particularly able enemies. When it does attack, the results are fast and bloody. The humming spider will quickly jump at its target, flying through the air with its sharp forelegs slicing in a frenzied pattern. This produces the infamous humming that is normally the only warning a victim gets before being assaulted. The blades are so sharp that any non-magical armor is completely ignored. The spider feeds by blending the flesh of its victim, preferably human or Elven, into a thin red paste that it will then suck up through its proboscis.
