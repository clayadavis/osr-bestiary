.. title: Ghostcap Bloom
.. slug: ghostcap-bloom
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Ghostcap Bloom
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 12                               |
+-----------------+----------------------------------+
| Hit Dice:       | 4 (main bush), 2 (small bushes)  |
+-----------------+----------------------------------+
| No. of Attacks: | 1 + 1 per bush within 20’        |
+-----------------+----------------------------------+
| Damage:         | 1d4 + special                    |
+-----------------+----------------------------------+
| Movement:       | 0’                               |
+-----------------+----------------------------------+
| No. Appearing:  | 1 main bush + 1d8-1 small bushes |
+-----------------+----------------------------------+
| Save As:        | Fighter: 4                       |
+-----------------+----------------------------------+
| Morale:         | 12                               |
+-----------------+----------------------------------+
| Treasure Type:  | U                                |
+-----------------+----------------------------------+
| XP:             | 240                              |
+-----------------+----------------------------------+

The **Ghostcap Bloom** is a tall, undead plant usually found within abandoned graveyards, battlefields, and crypts. Smaller bushes, up to 3’ tall, branch off from the main one in a perimeter of up to 20’. These small bushes bear clusters of the flowers that give the plant its name. It collects flesh as fertilizer by grabbing and crushing any living being it gets hold of. Laying very still, the ghostcap bloom surprises on a roll of 1-3 on 1d6. A successful hit entangles the victim, suffering an additional 1d4 points of damage each round thereafter. A victim may attempt to escape by rolling a saving throw vs. Death Ray (with Strength bonus added).

In late autumn they produce a sickly sweet and musky aroma which attracts lesser undead to them, such as skeletons (1d8) and zombies (1d6-1). The ghostcap bloom does not appear to control these undead, as they do not defend the plant when it's attacked. But it has been observed that any undead within its area of pollination will drag their victim’s corpses to the plant so it can devour and break down the remains.

A ghostcap bloom is subject to Turning; the main plant is equivalent to a 4 HD creature, and the smaller bushes as 2 HD each. A smaller bush is outright destroyed by a successful Turn, while the main bush, if successfully Turned (but not Destroyed), receives a -3 penalty on all attack rolls for 2d4 rounds.
