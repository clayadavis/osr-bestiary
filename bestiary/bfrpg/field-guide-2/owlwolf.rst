.. title: Owlwolf
.. slug: owlwolf
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Owlwolf
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 17              |
+-----------------+-----------------+
| Hit Dice:       | 3               |
+-----------------+-----------------+
| No. of Attacks: | 2 claws + bite  |
+-----------------+-----------------+
| Damage:         | 1d6/1d6 + 1d8   |
+-----------------+-----------------+
| Movement:       | 60' and special |
+-----------------+-----------------+
| No. Appearing:  | 1d6+2           |
+-----------------+-----------------+
| Save As:        | Fighter: 3      |
+-----------------+-----------------+
| Morale:         | 9               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 175             |
+-----------------+-----------------+

The **Owlwolf** is a creature roughly the same size as a dire wolf, with a feathered pelt and a vicious curved beak lined with fangs. Its hindquarters are like those of a wolf, but its forelegs are more like wings tipped with claws.

The owlwolf is a nocturnal predator that hunts in a pack. It is aggressive and will attack almost any kind of creature, taking advantage of superior numbers and the cover of darkness to wear down its prey before using claws and teeth to rip it to shreds. An owlwolf favors attacking from behind when possible.

The owlwolf lacks true wings, but one can, with great effort, achieve considerable height when jumping and even glide for limited amounts of time. Once every three rounds an owlwolf can leap a distance of up to 100'; it may leap and then attack, or move normally, attack, and then leap away. They are thus dangerous combatants in wide open spaces. When leaping in partially-obstructed areas such as forest or brush, this distance is halved. When gliding they are completely silent, thus surprising on 1-4 on 1d6.

Owlwolves have almost perfect night vision and even in complete (but not magical) darkness can see up to 200'.
