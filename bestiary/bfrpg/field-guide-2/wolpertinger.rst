.. title: Wolpertinger
.. slug: wolpertinger
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Wolpertinger
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 16                       |
+-----------------+--------------------------+
| Hit Dice:       | 2*                       |
+-----------------+--------------------------+
| No. of Attacks: | 2 claws/1 bite or charge |
+-----------------+--------------------------+
| Damage:         | 1d3/1d3/1d4 or 2d4       |
+-----------------+--------------------------+
| Movement:       | 40' Fly 60'              |
+-----------------+--------------------------+
| No. Appearing:  | 1d4                      |
+-----------------+--------------------------+
| Save As:        | Fighter: 2               |
+-----------------+--------------------------+
| Morale:         | 9                        |
+-----------------+--------------------------+
| Treasure Type:  | Special                  |
+-----------------+--------------------------+
| XP:             | 75                       |
+-----------------+--------------------------+

A **Wolpertinger** is a winged hybrid creature, usually incorporating features of rabbits, deer, squirrels, and wolves. Sometimes small ones are encountered that pose little danger; however, larger specimens about the size of a large dog or wolf may be found, and these larger individuals are what is being described here. It has wickedly-sharp fangs, claws, and antlers that may be used to attack. The antlers are only used when charging (following normal charging rules for double damage), and then it will attack with claws and biting after such a charge.

Sometimes fey of the more wicked variety will utilize wolpertingers as mounts, otherwise groups of wolpertingers are usually encountered deep within densely-forested regions. A wolpertinger flies rather slow and clumsily, with flight being used primarily for short distances to get over impassable terrain; arial encounters would therefore be rather rare.

A wolpertinger's pelt is similar to high-quality rabbit or mink fur, but due to its rarity commands a price of over 100 gp just for the rough pelt. Worked fur garments are therefore even more valuable. Its antlers are often worked into objects such as knife handles or even as embellishments on bier steins.
