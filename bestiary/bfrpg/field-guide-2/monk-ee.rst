.. title: Monk-ee
.. slug: monk-ee
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Monk-ee
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 18                           |
+-----------------+------------------------------+
| Hit Dice:       | 4*                           |
+-----------------+------------------------------+
| No. of Attacks: | 4 punches or 2 weapons       |
+-----------------+------------------------------+
| Damage:         | 1d6/1d6/1d6/1d6 or by weapon |
+-----------------+------------------------------+
| Movement:       | 60'                          |
+-----------------+------------------------------+
| No. Appearing:  | Wild 2d6, Lair 6d6           |
+-----------------+------------------------------+
| Save As:        | Fighter: 3                   |
+-----------------+------------------------------+
| Morale:         | 10                           |
+-----------------+------------------------------+
| Treasure Type:  | S, I in lair                 |
+-----------------+------------------------------+
| XP:             | 280                          |
+-----------------+------------------------------+

A **Monk-ee** is a furry, tailed humanoid of short stature, clad in white or brown robes that allow for ease of movement. A monk-ee is adept at the arduous cultivation of the inner self that brings forth great speed and strength.

A monk-ee attacks with a very quick hail of blows and then retreats, limiting the harm it is exposed to. They are so swift that they can move after attacking instead of before; one will move into striking range, perform two attack routines (one at the end of the round when it moves, the other at the beginning of the next round) and then move away in that second round. A monk-ee will even choose to delay until after its opponent has acted in the hopes that it will get the initiative in the next round and thus face no attacks whatsoever from its opponent. The swiftness of a monk-ee is so great that it receives a bonus of +3 on all Initiative rolls.

A pack of monk-ees will be headed by a semi-enlightened monk-ee of 6 HD and AC 19 (555 XP). In a lair there will be a grand enlightened monk-ee of 8 HD, AC 20 (945 XP), 6 attacks per round, and 1d8 damage per attack.
