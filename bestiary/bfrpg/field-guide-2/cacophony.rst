.. title: Cacophony
.. slug: cacophony
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Cacophony
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 15                              |
+-----------------+---------------------------------+
| Hit Dice:       | 4*                              |
+-----------------+---------------------------------+
| No. of Attacks: | 1 toot/2 bang/1 clash or 1 ding |
+-----------------+---------------------------------+
| Damage:         | 1d6/1d4/1d4/2d8 or special      |
+-----------------+---------------------------------+
| Movement:       | Fly 30'                         |
+-----------------+---------------------------------+
| No. Appearing:  | 1                               |
+-----------------+---------------------------------+
| Save As:        | Fighter: 4                      |
+-----------------+---------------------------------+
| Morale:         | 12                              |
+-----------------+---------------------------------+
| Treasure Type:  | Special                         |
+-----------------+---------------------------------+
| XP:             | 280                             |
+-----------------+---------------------------------+

A **Cacophony** consists of a band of magically-animated instruments brought about by the lingering attachments of failed bards. It will attempt to find audiences to perform to, and upon finding one it will “perform” (i.e. attacks) until either the cacophony or the audience is defeated.

Once every three rounds the cacophony may use its “ding”, causing one target to save vs. Spells or run in fear for 2 turns, as the spell **cause fear**.

Instruments worth 2d12x100 gp can be obtained if the cacophony is defeated in a non-destructive manner.
