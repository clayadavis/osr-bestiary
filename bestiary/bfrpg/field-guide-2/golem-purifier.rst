.. title: Golem, Purifier*
.. slug: golem-purifier
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Purifier*
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 18‡                |
+-----------------+--------------------+
| Hit Dice:       | 8**                |
+-----------------+--------------------+
| No. of Attacks: | 2 fists or 1 flame |
+-----------------+--------------------+
| Damage:         | 2d10/2d10 or 3d6   |
+-----------------+--------------------+
| Movement:       | 20'                |
+-----------------+--------------------+
| No. Appearing:  | 1                  |
+-----------------+--------------------+
| Save As:        | Fighter: 8**       |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 1,015              |
+-----------------+--------------------+

A **Purifier Golem** is an 8’ tall construct of stone and brass, roughly humanoid but with swollen arms, at the end of which are numerous holes.

It attacks by punching, or by holding its arms out in front of it and belching fire from them in a 30’ long cone, 20’ wide at its far end. Victims may make a save vs. Dragon Breath for half damage.
