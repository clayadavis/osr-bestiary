.. title: Bear, Quill-Bear
.. slug: bear-quill-bear
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bear, Quill-Bear
.. type: text
.. image:

+-----------------+-------------------------------------------+---------------------------------------------+
|                 | Common                                    | Huge                                        |
+=================+===========================================+=============================================+
| Armor Class:    | 16                                        | 18                                          |
+-----------------+-------------------------------------------+---------------------------------------------+
| Hit Dice:       | 5                                         | 7                                           |
+-----------------+-------------------------------------------+---------------------------------------------+
| No. of Attacks: | 2 claws/1 bite + hug + special            | 2 claws/1 bite + hug + special              |
+-----------------+-------------------------------------------+---------------------------------------------+
| Damage:         | 1d6 claw, 1d8 bite, 2d8 hug, plus special | 1d8 claw, 1d10 bite, 2d10 hug, plus special |
+-----------------+-------------------------------------------+---------------------------------------------+
| Movement:       | 40'                                       | 40'                                         |
+-----------------+-------------------------------------------+---------------------------------------------+
| No. Appearing:  | 1d4                                       | 1d3                                         |
+-----------------+-------------------------------------------+---------------------------------------------+
| Save As:        | Fighter: 5                                | Fighter: 7                                  |
+-----------------+-------------------------------------------+---------------------------------------------+
| Morale:         | 5 / 8                                     | 5 / 8                                       |
+-----------------+-------------------------------------------+---------------------------------------------+
| Treasure Type:  | Quills 2d6x100 gp (if undamaged)          | Quills 2d8x100 gp (if undamaged)            |
+-----------------+-------------------------------------------+---------------------------------------------+
| XP:             | 360                                       | 780                                         |
+-----------------+-------------------------------------------+---------------------------------------------+

A **Quill-Bear** appears as a large bear with long, razor-sharp spikes covering its back and sides. This ursine creature is as massive as a typical brown bear, weighing more than 1,800 pounds and standing about 9’ when it rears up on its hind legs; a **Huge Quill-Bear** is even larger, standing up to 12' tall and weighing over 2,600 pounds.

As with other bears, a successful hit by both claws on the same opponent in a given round means that opponent has automatically been hugged and suffers the listed additional damage.

The quills of a quill-bear move as it moves, waving and shaking. Any creatures within 5 feet of a quill-bear as it walks or runs suffers 1d8 points of damage; a successful save vs. Death Ray is allowed to avoid this damage. However, anyone who attacks a quill-bear with a melee weapon (except for a pole arm or spear) must come within range of the spikes and thus suffer 1d8 points of damage automatically, with no saving throw allowed.

The quill-bear is normally a cowardly creature (hence the first given morale score) and avoids large groups of potential enemies. Most of the time one lives a solitary life, foraging for nuts and berries and sometimes carrion in the forest. During a full moon a quill-bear's behavior changes quite dramatically, becoming aggressive and bloodthirsty; use the second given morale score at such a time. During the full moon the creature's diet becomes entirely carnivorous, with a marked preference for larger prey such as horses and humanoid creatures, and the quill-bear will seek them out deliberately. This change in behavior applies both day and night during a full moon, even if the moon cannot be seen; however, as the quill-bear is nocturnal, one will not normally be encountered hunting in the daylight.

When under the light of a full moon, the quill-bear grows larger and gains a hit die; treat the extra hit points thus gained in a fashion similar to a **potion of heroism**. While exposed to moonlight a common quill-bear will regenerate damage at a rate of 2 hit points per round, while the huge variety regenerates 3 hit points per round. If the creature is killed but its body is left in moonlight, there is a 60% chance that the bear will arise again in 2d4 rounds with half of its hit points restored.
