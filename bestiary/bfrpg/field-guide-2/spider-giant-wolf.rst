.. title: Spider, Giant Wolf
.. slug: spider-giant-wolf
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Spider, Giant Wolf
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 18                 |
+-----------------+--------------------+
| Hit Dice:       | 9*                 |
+-----------------+--------------------+
| No. of Attacks: | 1 bite             |
+-----------------+--------------------+
| Damage:         | 1d12 bite + poison |
+-----------------+--------------------+
| Movement:       | 50' Jump 20'       |
+-----------------+--------------------+
| No. Appearing:  | 1                  |
+-----------------+--------------------+
| Save As:        | Fighter: 9         |
+-----------------+--------------------+
| Morale:         | 10                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 1,150              |
+-----------------+--------------------+

The **Giant Wolf Spider** is much like its normal-sized namesake. It is on average about 3’ tall and 5’ long, colored in patterns of light and dark brown. The giant wolf spider does not build a web, and doesn’t have the ability to produce webs. It normally makes its lair in caves, ruins, or simply burrowing into the ground.

It is a swift and merciless hunter, preferring to either wait in hiding for prey to come by, or in some cases to chase prey down. Due to its natural camouflage coloration the giant wolf spider is able to surprise prey on a roll of 1-3 on 1d6 when waiting motionless.

The giant wolf spider is a powerful jumper, able to jump up to almost half of its normal movement rate. When waiting in ambush, a giant wolf spider will wait until prey approaches within 20’ and leap on it to attack. When chasing prey, it will close to within 20’ and leap for the kill.

Anyone bitten by a giant wolf spider must save vs. Poison or be paralyzed for 2d6 turns. A **neutralize poison** spell will negate this effect.
