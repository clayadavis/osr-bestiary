.. title: Crockle
.. slug: crockle
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Crockle
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 17                |
+-----------------+-------------------+
| Hit Dice:       | 5                 |
+-----------------+-------------------+
| No. of Attacks: | 1 bite or special |
+-----------------+-------------------+
| Damage:         | 2d8 or special    |
+-----------------+-------------------+
| Movement:       | 5' Swim 50'       |
+-----------------+-------------------+
| No. Appearing:  | 1d4               |
+-----------------+-------------------+
| Save As:        | Fighter: 5        |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 750               |
+-----------------+-------------------+

A **Crockle** is a crocodile-like creature that lives near water and can grow up to 30' long. A crockle attacks creatures near the water by jumping out and grabbing it. If a creature strays within 20' of the edge of a crockle's body of water, it will leap out and grapple it, dealing 1d10 damage (save vs. Death Ray to avoid). On successive rounds a save must be made or the grappled creature takes an additional 1d6 damage and is dragged towards the water. A creature dragged into the water may still make a save, but at a -2 as it is harder to escape underwater. The victim also takes drowning damage until it dies or escapes.
