.. title: Swamp Monster
.. slug: swamp-monster
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Swamp Monster
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 16                 |
+-----------------+--------------------+
| Hit Dice:       | 5                  |
+-----------------+--------------------+
| No. of Attacks: | 2 strikes          |
+-----------------+--------------------+
| Damage:         | 1d6/1d6            |
+-----------------+--------------------+
| Movement:       | 40'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d8, Lair 4d8 |
+-----------------+--------------------+
| Save As:        | Fighter: 3         |
+-----------------+--------------------+
| Morale:         | 8                  |
+-----------------+--------------------+
| Treasure Type:  | L                  |
+-----------------+--------------------+
| XP:             | 100                |
+-----------------+--------------------+

A **Swamp Monster** is a humanoid creature that stands around 7’ tall. Moss, vines, and other small vegetation grow all over its body, giving its skin a greenish hue as it ages; when a swamp monster is born its skin is a dull blue color. Due to its natural camouflage, in swamps, marshes, and bogs swamp monsters surprise on a roll of 1-4 on a d6. Anyone encountering a swamp monster will flee immediately unless a save vs. Petrify is made; those surprised attempt the save at a -5. A swamp monster dares not stray from its habitat, as it believes that the sticky ground holds it down.

A swamp monster is a semi-intelligent being and lives in a small mud hut it builds; these require a secret door check to find. All treasure recovered from a swamp monster will be covered in mud; it will take 4 turns to find all the treasure in a swamp monster village. There are normally 4 swamp monsters living in a single hut. Creatures that stray into a swamp monster's hut must make a save vs. Death Ray or be stuck in the mud until a successful save is made on a successive round.
