.. title: Snake, Clockwork Razor
.. slug: snake-clockwork-razor
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Snake, Clockwork Razor
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 17                  |
+-----------------+---------------------+
| Hit Dice:       | 2                   |
+-----------------+---------------------+
| No. of Attacks: | 1 lash or constrict |
+-----------------+---------------------+
| Damage:         | 2d6 or 3d10         |
+-----------------+---------------------+
| Movement:       | 30'                 |
+-----------------+---------------------+
| No. Appearing:  | 1d3                 |
+-----------------+---------------------+
| Save As:        | Fighter: 2          |
+-----------------+---------------------+
| Morale:         | 8                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 75                  |
+-----------------+---------------------+

The **Clockwork Razor Snake** is an abomination of nature produced by idle Magic-Users. The snake appears to be a regular python that has been cut apart into short sections by magical means, with the sections reconnected with clockwork. This effectively increases the creature's length by 50% (24' to 36'). These clockwork sections are fully under the control of the snake and are lined with long, straight razor sharp blades that angle over the biological sections.

The clockwork razor snake attacks by lashing out with its tail; alternately it may attempt to coil around a victim. On a successful attack roll, if the victim fails a save vs. Dragon Breath it is coiled and takes 3d10 points of damage per round. If a target is killed while in the grip of the clockwork razor snake, its corpse is torn to shreds.

These sad creatures are in constant pain, and cannot remain still for any length of time; they roam constantly, attacking any creatures they meet in sheer blind rage.
