.. title: Ushac
.. slug: ushac
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Ushac
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 11 (or armor worn) |
+-----------------+--------------------+
| Hit Dice:       | 1-1*               |
+-----------------+--------------------+
| No. of Attacks: | 1 weapon           |
+-----------------+--------------------+
| Damage:         | By weapon          |
+-----------------+--------------------+
| Movement:       | 30'                |
+-----------------+--------------------+
| No. Appearing:  | 1d8                |
+-----------------+--------------------+
| Save As:        | Normal Man         |
+-----------------+--------------------+
| Morale:         | 9                  |
+-----------------+--------------------+
| Treasure Type:  | Q each; C in lair  |
+-----------------+--------------------+
| XP:             | 13                 |
+-----------------+--------------------+

Cheerful and willing to help, **Ushac** are sadly the perfect minion race. Dwarf-sized with purple skin, gray eyes, cat-like ears, and bushy dodger-blue hair and beards, ushac are not inherently bad creatures. However, an ushac is quite easy to **charm** or otherwise dominate, much to the delight of wicked spellcasters.

An ushac can cast **light** (or its reverse, **darkness**) and **purify food and water** once per day. These innate powers are cast at first-level ability. In addition to these spell-like powers, an ushac not in metal armor is as stealthy as a Thief, having a 25% chance to Move Silently and a 10% chance to Hide in Shadows.

Most ushac are unarmored and carry only a dagger for defense. Tribal protectors (about one-third of the population) wear leather or chain mail armor and will carry a medium-sized weapon such as a short sword, hammer, hand axe, or sometimes a sling. Their Dwarf-like stature likewise limits weapon choice to those a Dwarf can use (specifically excluding two-handed swords, polearms, and longbows).

Noted for their gullibility and lack of resistance to charms, an ushac saves against such related effects and spells at -4 penalty (**charm**, **suggestion**, illusions, siren's songs, among others). Such effects tend to have longer duration as well when used against an ushac (double any such duration). Note that illusions and the like do not last longer, only the ongoing or lingering effects upon the ushac itself.
