.. title: Skeletal Legs
.. slug: skeletal-legs
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Skeletal Legs
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 11         |
+-----------------+------------+
| Hit Dice:       | 1d4 HP     |
+-----------------+------------+
| No. of Attacks: | 2 kicks    |
+-----------------+------------+
| Damage:         | 1d3/1d3    |
+-----------------+------------+
| Movement:       | 50'        |
+-----------------+------------+
| No. Appearing:  | 3d4        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 13         |
+-----------------+------------+

When a whole corpse cannot be found, an aspiring necromancer must settle for less. A pair of **Skeletal Legs** is exactly how it sounds: an incomplete skeleton from the pelvic bone to the toes. While not perhaps as desirable or hardy as other undead, the skeletal legs alone turn out to be surprisingly versatile. Both fast and determined, a group of them can quite easily overwhelm small groups by kicking and trampling with frightening speed. Skeletal legs are also quite stealthy by merit of being rather small and skinny; if not actively attacking something an observer has only a 30% of noticing it.
