.. title: Golem, Lead
.. slug: golem-lead
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Lead
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 16          |
+-----------------+-------------+
| Hit Dice:       | 12* (+10)   |
+-----------------+-------------+
| No. of Attacks: | 1 club      |
+-----------------+-------------+
| Damage:         | 3d6         |
+-----------------+-------------+
| Movement:       | 30'         |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 12 |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 1,975       |
+-----------------+-------------+

The secret of making **Lead Golems** has been lost, but there are still a few ancient ones in the deepest dark places beneath the earth. They were sacred to the bat-god Camazotz and were made by his priests, who were leaders of the weird Cheiropteran bat-people. It is always man-shaped but with the heads and wings of colossal bats. The wings are non-functional as a lead golem is far too heavy to fly. It fights with lead clubs. In addition to its attack, each lead golem can cast **confusion** once every seven rounds.

Legend has it that Camazotz has a lost temple deep beneath the earth which is inhabited by seven lead golems and a family of bats the size of rocs.
