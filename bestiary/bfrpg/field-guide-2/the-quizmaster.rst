.. title: The Quizmaster
.. slug: the-quizmaster
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: The Quizmaster
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 20          |
+-----------------+-------------+
| Hit Dice:       | 10          |
+-----------------+-------------+
| No. of Attacks: | Special     |
+-----------------+-------------+
| Damage:         | 2d8         |
+-----------------+-------------+
| Movement:       | 20'         |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 10 |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 2,000       |
+-----------------+-------------+

The **Quizmaster** was created by the human wizard White in an attempt to get students to remember answers to questions. The quizmaster killed White after he failed to answer the questions given to him. The quizmaster is a slender humanoid with pale skin and stands almost 10 feet tall. It wears a gray suit at all times. The quizmaster can be found wandering the wilderness.

When the quizmaster is within 50 feet all intelligent living creatures must make a save vs. Paralysis (modified by Wisdom bonus or penalty, if any) or be unable to move and too afraid to speak unless answering questions. A new save is made every 5 rounds. Any creature which makes the save is free to move about, but a second saving throw is needed in order to speak freely.

The quizmaster will approach creatures and in a deep slow voice will ask up to 1d6 difficult questions; if a question is not answered within 1d4 rounds, the quizmaster will attack the questioned creature until the question is correctly answered or the questioned creature is dead. An incorrect answer will provoke an immediate psychic attack that cannot be avoided doing 2d8 points of damage. If the questioned creature tries to run away, the quizmaster will pursue it until the questions are answered or the creature is dead.
