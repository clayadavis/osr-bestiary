.. title: Deep One, Hybrid
.. slug: deep-one-hybrid
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Deep One, Hybrid
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 14                   |
+-----------------+----------------------+
| Hit Dice:       | 1+1                  |
+-----------------+----------------------+
| No. of Attacks: | 2 claws or by weapon |
+-----------------+----------------------+
| Damage:         | 1d2/1d2 or by weapon |
+-----------------+----------------------+
| Movement:       | 30' Swim 20'         |
+-----------------+----------------------+
| No. Appearing:  | 2d4, Lair 4d8        |
+-----------------+----------------------+
| Save As:        | Fighter: 1           |
+-----------------+----------------------+
| Morale:         | 7                    |
+-----------------+----------------------+
| Treasure Type:  | C                    |
+-----------------+----------------------+
| XP:             | 25                   |
+-----------------+----------------------+

A **Hybrid** **Deep One** starts off life as a normal member of its humanoid parent's race. In fact, in the beginning it is completely unaware of its connection to the dwellers of the deep. Over time the hybrid slowly transforms into a true deep one as it takes on traits such as glassy unblinking eyes, small or a complete lack of ears, webbing between fingers and toes, or folds along its neck that eventually become gills. As the traits develop the hybrid individual becomes reclusive and of an increasingly alien mindset. The full transformation takes a variable amount time, though not usually completing before the individual becomes middle-aged. When the traits become too obvious to conceal the hybrid departs society to join with the deep ones.

While still within its humanoid community, a hybrid deep one will continue with a traditional trade or profession while operating in secret cults with dark rituals, furthering the sect's vile goals. It is possible that such a hybrid has class-based abilities; as such, the individual may have substantially different stats than those listed above, which represent a basic mid-point in the hybrid's transformation.
