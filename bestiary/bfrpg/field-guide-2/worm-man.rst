.. title: Worm Man
.. slug: worm-man
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Worm Man
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 13               |
+-----------------+------------------+
| Hit Dice:       | 3**              |
+-----------------+------------------+
| No. of Attacks: | 1                |
+-----------------+------------------+
| Damage:         | 1d3 or by weapon |
+-----------------+------------------+
| Movement:       | 30’              |
+-----------------+------------------+
| No. Appearing:  | 1d10             |
+-----------------+------------------+
| Save As:        | Fighter: 3       |
+-----------------+------------------+
| Morale:         | 9                |
+-----------------+------------------+
| Treasure Type:  | J                |
+-----------------+------------------+
| XP:             | 205              |
+-----------------+------------------+

**Worm Men** are humanoid worms that live deep underground. Each is about 7’ long and has two arms near the top of its body. The worm man may either slither or stand up on its tail to move in a more humanoid fashion. It has large eyes that are mounted on either side of its head, giving it excellent peripheral vision, and can only be surprised on a roll of 1 on a 1d6. A worm man can exist on minerals for a time, though bands of worm men often make forays to the surface to acquire decaying organic matter to supplement their diet. A worm man is immune to the **sleep** spell.

Each worm man has a spell-like ability that functions like a **charm person** spell. However, it has the limitation that the effect will fade once the worm man loses eye contact with the victim. The worm man may use this ability once per day and often leads with it during combat.

Worm men live in large communities apart from the other underground civilizations. However, they are often willing to be hired as soldiers by other races. Recently, they have begun to take a more aggressive stance toward the surface, perhaps deciding the time is right for them to consider a campaign of conquest.
