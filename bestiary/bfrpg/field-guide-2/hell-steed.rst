.. title: Hell Steed
.. slug: hell-steed
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Hell Steed
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 14 to 16                    |
+-----------------+-----------------------------+
| Hit Dice:       | 3** to 5**                  |
+-----------------+-----------------------------+
| No. of Attacks: | 2 hooves/1 bite or 1 breath |
+-----------------+-----------------------------+
| Damage:         | 1d6/1d6/1d10 or 1d6 per HD  |
+-----------------+-----------------------------+
| Movement:       | 80' (10'), Fly 160' (10')   |
+-----------------+-----------------------------+
| No. Appearing:  | 1 (+1 Dread Horseman)       |
+-----------------+-----------------------------+
| Save As:        | Fighter: 2 to 4             |
+-----------------+-----------------------------+
| Morale:         | 12                          |
+-----------------+-----------------------------+
| Treasure Type:  | None                        |
+-----------------+-----------------------------+
| XP:             | 205 to 450                  |
+-----------------+-----------------------------+

**Hell Steeds** are horses born from Hell; these beasts are often used as war mounts for dread horsemen. This horse has a withered, skeletal look about it, and a hide colored blood red, bone white, or ash black. Its hooves, mane, and tail are all made out of hellish green fire and it breathes such flames. A hell steed can also fly as if running on the air itself, leaving a trail of flaming green hoof prints in its wake. As an undead, it is immune to **sleep**, **charm**, and **hold** magic, and can be Turned by a Cleric.

Aggressive and territorial, a hell steed will attack anything that it believes to be a threat or competitor. In combat it mainly attacks with its hooves and a bite. It can also breathe fire in a 15' long by 15' wide cone for 1d6 damage per HD; those in the area-of-effect may save vs. Dragon Breath for half damage. The hell steed may breathe fire a number of times per day equal to its Hit Dice.
