.. title: Gorophont
.. slug: gorophont
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Gorophont
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 18                            |
+-----------------+-------------------------------+
| Hit Dice:       | 9                             |
+-----------------+-------------------------------+
| No. of Attacks: | 1 punch/1 weapon or 1 trample |
+-----------------+-------------------------------+
| Damage:         | 1d6/by weapon or 3d6          |
+-----------------+-------------------------------+
| Movement:       | 40'                           |
+-----------------+-------------------------------+
| No. Appearing:  | Wild 2d4, Lair 5d4            |
+-----------------+-------------------------------+
| Save As:        | Fighter: 9                    |
+-----------------+-------------------------------+
| Morale:         | 10                            |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 1,075                         |
+-----------------+-------------------------------+

A **Gorophont** is a centaur-type creature, with the upper body of a gorilla and the lower body of a small elephant. It is a fierce territorial creature, and typically fights with a very large lance.

A gorophont is a great tracker and hunter. It only has a 1 in 6 chance of being surprised. A gorophont also has a 1 in 6 chance of casually observing tracks on the road, and a 3 in 6 chance to actively track a quarry; against known quarries, this increases to a 5 in 6 chance.

There is a 1 in 10 chance that a gorophont will be a silverback; these have an extra +2 to any damage roll, a morale score of 12, and are worth 1,150 XP. Any gorophont fighting alongside a silverback counts its morale as if it where 12; the loss of a silverback results in a morale check with a -6 penalty.
