.. title: Chupacabra
.. slug: chupacabra
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Chupacabra
.. type: text
.. image:

+-----------------+------------------------------------+
| Armor Class:    | 16                                 |
+-----------------+------------------------------------+
| Hit Dice:       | 2                                  |
+-----------------+------------------------------------+
| No. of Attacks: | 1 bite                             |
+-----------------+------------------------------------+
| Damage:         | 1d8                                |
+-----------------+------------------------------------+
| Movement:       | 30' Hop                            |
+-----------------+------------------------------------+
| No. Appearing:  | 3d6 (with Cheiropterans), Wild 1d2 |
+-----------------+------------------------------------+
| Save As:        | Fighter: 2                         |
+-----------------+------------------------------------+
| Morale:         | 9                                  |
+-----------------+------------------------------------+
| Treasure Type:  | None                               |
+-----------------+------------------------------------+
| XP:             | 75                                 |
+-----------------+------------------------------------+

A **Chupacabra** is a nocturnal reptile with a row of spines running down its back. It moves by hopping like a kangaroo. Its eyes glow red and its screech is awful to hear.

A chupacabra is feared in farming communities because it kills far more livestock than it needs to eat. It particularly likes the taste of goat.

Owing to a chubacabra's excellent sense of smell, cheiropterans train and use them as hounds.
