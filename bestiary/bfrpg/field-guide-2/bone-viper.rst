.. title: Bone Viper
.. slug: bone-viper
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bone Viper
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14 (special, see below) |
+-----------------+-------------------------+
| Hit Dice:       | 1*                      |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite or 1 spit        |
+-----------------+-------------------------+
| Damage:         | 1d4 + poison or special |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4                     |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 37                      |
+-----------------+-------------------------+

A **Bone Viper** is an **undead** creature created to be used as a defender of some wizard's tower or evil priest's temple. Its poison is magically potent, and it is able to spit its poison up to 5’. Anyone hit by a spit attack must make a save vs. Poison or become blinded, while anyone bitten must make a save vs. Poison or die in 1d3 rounds.

As they are undead, bone vipers are immune to **sleep**, **charm**, and **hold** magic, and like other animated skeletons they suffer half damage from edged weapons and one point only (plus any magical bonus) from small missile weapons like arrows or sling stones. They may be **Turned** as if a skeleton.
