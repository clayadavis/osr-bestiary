.. title: Deep Ones
.. slug: deep-ones
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Deep Ones
.. type: text
.. image:

Deep Ones are a race of picean beings, combining traits of fish, amphibians, and humans. Deep ones revere ancient elder beings of the deep believed to predate the arrival of the gods of Humans, Dwarves, and the like.

Deep ones will secretly trade and form pacts with humanoids in coastal communities. These pacts inevitably include dark rituals, sacrifices, and even inter-breeding with the humanoid populations. The hybrids from such unions often rise to power within secret cults of dark god-beings.

*“I think their predominant color was a grayish-green, though they had white bellies. They were mostly shiny and slippery, but the ridges of their backs were scaly. Their forms vaguely suggested the anthropoid, while their heads were the heads of fish, with prodigious bulging eyes that never closed. At the sides of their necks were palpitating gills, and their long paws were webbed. They hopped irregularly, sometimes on two legs and sometimes on four. I was somehow glad that they had no more than four limbs. Their croaking, baying voices, clearly used for articulate speech, held all the dark shades of expression which their staring faces lacked... They were the blasphemous fish-frogs of the nameless design – living and horrible…”*

*- H.P. Lovecraft, The Shadow Over Innsmouth*
