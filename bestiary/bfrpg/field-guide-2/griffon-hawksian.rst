.. title: Griffon, Hawksian
.. slug: griffon-hawksian
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Griffon, Hawksian
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 18                               |
+-----------------+----------------------------------+
| Hit Dice:       | 8                                |
+-----------------+----------------------------------+
| No. of Attacks: | 2 claws/1 bite or 2 bludgeons    |
+-----------------+----------------------------------+
| Damage:         | 2d6/2d6/2d10 or 1d12/1d12 + stun |
+-----------------+----------------------------------+
| Movement:       | 40’                              |
+-----------------+----------------------------------+
| No. Appearing:  | 1, Wild 1d2                      |
+-----------------+----------------------------------+
| Save As:        | Fighter: 8                       |
+-----------------+----------------------------------+
| Morale:         | 11                               |
+-----------------+----------------------------------+
| Treasure Type:  | F                                |
+-----------------+----------------------------------+
| XP:             | 945                              |
+-----------------+----------------------------------+

Much like a regular griffon, the **Hawksian Griffon** is a combination of lion and eagle. Unlike a regular griffon its front half is lion and the rear half is eagle. The result of a twisted magical experiment, it is an extremely aggressive and dangerous animal. Unlike its flying cousin the hawksian griffon does not possess wings that allow it to fly, instead having malformed bone stubs that it can use to bludgeon enemies in combat. They are also slightly larger, measuring about 12' from nose to tail and standing around 7' tall.

Hawksian griffons have a wide variety of coloration, however they tend towards a combination of dark gray and red, with a multicolored patch of fur and feathers marking the point where lion and eagle meet, with females of the species being slightly larger. It prefers grasslands and savanna, and preys upon pretty much anything that crosses its path. Unlike lions and griffons, hawksian griffons are mostly solitary creatures and any groups found will be mating pairs. It has been suggested that it seeks solitude as it is in constant pain due to deformities caused by the process that created the species. In addition, its life is frequently brutal, hard, and short. It targets Human Magic-Users over all others, perhaps in dim recognition as the beings who originally cursed it to a joyless existence.
