.. title: Amarok
.. slug: amarok
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Amarok
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 15 (13)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 2                               |
+-----------------+---------------------------------+
| No. of Attacks: | 1 weapon or 1 bite              |
+-----------------+---------------------------------+
| Damage:         | 2d4 or by weapon +1 or 1d6 bite |
+-----------------+---------------------------------+
| Movement:       | 30' Unarmored 40'               |
+-----------------+---------------------------------+
| No. Appearing:  | 1d6, Wild 3d6, Lair 3d6         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 2                      |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | Q, S each; D, K in lair         |
+-----------------+---------------------------------+
| XP:             | 75                              |
+-----------------+---------------------------------+

The **Amarok** is a lupine humanoid that forms packs and hunts wide ranges of wilderness areas. It speaks its own language of barks, growls, yaps, and howls. An amarok's attitude mirrors that of wolves and is often rather aggressive towards other races. An amarok is substantially larger than a human, reaching a height of 7 to 8 feet when fully upright and a weight of about 300 pounds. It can have a wide variety of coat colors and patterns consisting of brown, black, gray, and white tones. Amaroki are often called “wolfen” by other races.

The amarok is an active predator, operating equally in day or night but generally preferring night. It has Darkvision with a 30' range. It hunts effectively in packs, flanking and surrounding prey. Due to its strength, each has a +1 bonus on damage rolls due to strength.

One out of every six amarok will be a hardened warrior of 4 Hit Dice (240 XP) and have a +2 bonus to damage due to strength. An amarok gains a +1 bonus to its morale if it’s led by such a warrior. In lairs of 12 or greater, there will be a pack leader of 6 Hit Dice (500 XP) with a +3 bonus to damage. In the lair, and amarok never fails a morale check as long as the pack leader is alive. In addition, a lair has a chance equal to 1-2 on 1d6 of a shaman being present, and 1 on 1d6 of a witch or warlock. A shaman is equivalent to a hardened warrior statistically, but with Clerical abilities of level 1d4+1. A witch or warlock is equivalent to a regular amarok, but with Magic-User abilities of level 1d4.
