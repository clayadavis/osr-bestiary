.. title: Catoblepas
.. slug: catoblepas
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Catoblepas
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 13             |
+-----------------+----------------+
| Hit Dice:       | 7**            |
+-----------------+----------------+
| No. of Attacks: | 1 tail or gaze |
+-----------------+----------------+
| Damage:         | 1d6 or special |
+-----------------+----------------+
| Movement:       | 20'            |
+-----------------+----------------+
| No. Appearing:  | 1d3            |
+-----------------+----------------+
| Save As:        | Fighter: 7     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | C              |
+-----------------+----------------+
| XP:             | 800            |
+-----------------+----------------+

A **Catoblepas** is a fantastically ugly creature with a warthog-like face and body, a long neck, and a powerful tail ending in a bone club similar to an ankylosaurus. All told it is about 10’ long and weighs 700 pounds. A catoblepas prefers to be left alone to stew in its misery, but if threatened it will attempt to ward away attackers with its tail. Most dangerous is its gaze, which shines with a pale green beam of light; any creature touched by this ray must save vs. Death Ray or die instantly. Fortunately, its ungainly-balanced neck and general unwillingness to get involved means that it will only be able to bring its head to bear once every 1d4 rounds. However, if it should gain surprise a catoblepas will nearly always forget itself and glance upward long enough to use its gaze. Catoblepas are immune to any magical effects that cause instant death, including **disintegration**.
