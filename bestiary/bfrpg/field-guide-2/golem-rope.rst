.. title: Golem, Rope
.. slug: golem-rope
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Rope
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 4*         |
+-----------------+------------+
| No. of Attacks: | 1 grapple  |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 4 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 280        |
+-----------------+------------+

Designed for shipboard use, a **Rope Golem** is made from coils of ordinary rope. It typically wrestles and holds (as explained in the **Basic Fantasy RPG Core Rules** on page 48), but once the target is grappled, on the next round the rope golem will pass a loop around whichever appendage it believes to be its target's neck. The target then has one round to cut the rope with a dagger, or it will be throttled. For most creatures throttling is fatal, but anyone wearing plate mail armor and a helm is also presumed to have a gorget that provides protection from this attack.
