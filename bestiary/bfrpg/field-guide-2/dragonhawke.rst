.. title: Dragonhawke
.. slug: dragonhawke
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dragonhawke
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 16                           |
+-----------------+------------------------------+
| Hit Dice:       | 2                            |
+-----------------+------------------------------+
| No. of Attacks: | 1 charge or 2 claws + 1 bite |
+-----------------+------------------------------+
| Damage:         | Special or 1d3/1d3 + 1d4     |
+-----------------+------------------------------+
| Movement:       | 10' Fly 120'                 |
+-----------------+------------------------------+
| No. Appearing:  | 1d3                          |
+-----------------+------------------------------+
| Save As:        | Fighter: 2                   |
+-----------------+------------------------------+
| Morale:         | 8                            |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 75                           |
+-----------------+------------------------------+

**Dragonhawkes** are small dragon-like creatures with avian features. A dragonhawke is covered with feather-like scales in a pattern that mimics a bird-of-prey species such as an eagle, hawk, or owl. Those resembling eagles or hawks have very keen vision, while the owl-like ones are nocturnal and have Darkvision with a range of 120 feet.

Like an eagle, a dragonhawke typically attacks from a great height, diving earthward at tremendous speed. If it cannot dive, it will use its powerful talons and slashing beak to strike at its target’s head and eyes.

Dragonhawkes are highly prized by many cultures when trained for hunting. Some fey creatures use them as mounts.
