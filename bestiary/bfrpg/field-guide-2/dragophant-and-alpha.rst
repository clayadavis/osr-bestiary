.. title: Dragophant (and Alpha)
.. slug: dragophant-and-alpha
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dragophant (and Alpha)
.. type: text
.. image:

+-----------------+---------------------------+---------------------------+
|                 | **Dragophant**            | **Alpha Dragophant**      |
+=================+===========================+===========================+
| Armor Class:    | 18                        | 19                        |
+-----------------+---------------------------+---------------------------+
| Hit Dice:       | 8**                       | 9**                       |
+-----------------+---------------------------+---------------------------+
| No. of Attacks: | 2 tusks/1 stomp or breath (steam cloud)               |
+-----------------+---------------------------+---------------------------+
| Damage:         | 1d6/1d6/3d8 or 8d8 breath | 1d8/1d8/3d8 or 9d8 breath |
+-----------------+---------------------------+---------------------------+
| Movement:       | 30' Fly 60' (15')                                     |
+-----------------+---------------------------+---------------------------+
| No. Appearing:  | 1, Wild 1d4               | 1                         |
+-----------------+---------------------------+---------------------------+
| Save As:        | Fighter: 8                | Fighter: 9                |
+-----------------+---------------------------+---------------------------+
| Morale:         | 7 (see below)             | 8                         |
+-----------------+---------------------------+---------------------------+
| Treasure Type:  | F                         | H                         |
+-----------------+---------------------------+---------------------------+
| XP:             | 1,015                     | 1,225                     |
+-----------------+---------------------------+---------------------------+

The **Dragophant** is a foul and aggressive mixture of dragon and elephant. Legends of their origins vary, some speaking of a vile experiment, others of the anger of the gods. These rare beasts travel in small groups dominated by an alpha male. A group of dragophants will move to surround whatever the alpha is attacking and will not need to check morale unless the alpha is killed or incapacitated.

Like true dragons, dragophants will often have treasures, adorning themselves with shiny baubles and such. Treasure that can't be worn or carried will be stashed in a central location in an alpha dragophant’s territory.
