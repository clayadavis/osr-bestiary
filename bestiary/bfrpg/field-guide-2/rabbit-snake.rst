.. title: Rabbit Snake
.. slug: rabbit-snake
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Rabbit Snake
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 13            |
+-----------------+---------------+
| Hit Dice:       | 2             |
+-----------------+---------------+
| No. of Attacks: | 1 spit + bite |
+-----------------+---------------+
| Damage:         | 1d4           |
+-----------------+---------------+
| Movement:       | 50'           |
+-----------------+---------------+
| No. Appearing:  | 1d4 + 3       |
+-----------------+---------------+
| Save As:        | Fighter: 2    |
+-----------------+---------------+
| Morale:         | 8             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 125           |
+-----------------+---------------+

The **Rabbit Snake** is just as it sounds: a snake with the head of a rabbit and the tail of a rabbit. The average specimen is around 9’ long and is covered in patches of fur sticking out from underneath scales, which are shed when the snake sheds its skin. Its coloration can vary but is generally similar to that of rabbits that inhabit the same region. Its skin can be quite valuable as it is normally aesthetically pleasing and highly resistant to most kinds of acids. It can disguise itself as a rabbit, using the grass to cover its more snake-like features, bobbing up and down to give the appearance of jumping. It is normally found in mild climates and grasslands and lives underground in burrows preying on rabbits, deer, and similar creatures.

A rabbit snake is a vicious predator and will attempt to lure an unwary traveler or predator near its burrow, where it will then strike using its deadly venom (save vs. Poison or die) to incapacitate its victim. It can also spit acid up to 20 feet; a successful save vs. Death Ray is allowed to avoid it.
