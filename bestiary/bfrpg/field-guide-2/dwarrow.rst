.. title: Dwarrow
.. slug: dwarrow
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dwarrow
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 16 (11)                       |
+-----------------+-------------------------------+
| Hit Dice:       | 1+1                           |
+-----------------+-------------------------------+
| No. of Attacks: | 1 weapon                      |
+-----------------+-------------------------------+
| Damage:         | 1d6 or by weapon              |
+-----------------+-------------------------------+
| Movement:       | 20' Unarmored 40'             |
+-----------------+-------------------------------+
| No. Appearing:  | 1d8, Wild 2d6, Lair 5d10      |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1 (w/ Dwarf bonuses) |
+-----------------+-------------------------------+
| Morale:         | 8                             |
+-----------------+-------------------------------+
| Treasure Type:  | Q, S each; B, L in lair       |
+-----------------+-------------------------------+
| XP:             | 25                            |
+-----------------+-------------------------------+

**Dwarrow** are a grim, evil race who are cousins to the Dwarves. It is similar to a Dwarf in appearance but far less stocky. A dwarrow has ashen gray skin, black or white hair, large nose, drooping mustache, and short beard. A dwarrow prefers to wear drab, dark colors to better blend into the background of its stronghold. It is an expert craftsmen and its weapons and gear are always of the highest quality. A dwarrow is a courageous fighter and employs advanced tactics in battle. Dwarrows hate Dwarves and will attack them first unless ordered otherwise. A dwarrow suffers the same weapon restrictions as a Dwarf.

Dwarrow have Darkvision to a range of 80', and can speak both Dwarven and Common, though dwarrows rarely speak to non-dwarrows.

The statistics given above are for a standard dwarrow in chain mail with a shield. One out of every eight dwarrow will be a sergeant of 3+3 Hit Dice (145 XP). Regular dwarrow led by a sergeant gain a +1 bonus to their morale. In a lair or other settlement, one out of every 16 will be a dwarrow captain of 5+5 Hit Dice (360 XP) with a +1 bonus to damage due to strength. In lairs or other settlements of 30 or greater, there will be a dwarrow overlord of 7+7 Hit Dice (670 XP), with AC 18 (11) and having a +2 bonus to damage. In the lair, regular dwarrow gain a +2 bonus to their morale as long as the overlord is alive. In addition, a lair or other settlement has a chance equal to 1-3 on 1d6 of a dark priest being present (or 1-4 on 1d6 if a dwarrow overlord is present), and a 1-2 on 1d6 of a sorcerer. A dark priest is equivalent to a dwarrow sergeant statistically, but has Clerical abilities at level 1d6+1. A sorcerer is equivalent to a regular dwarrow, but has Magic-User abilities of level 1d4+2.
