.. title: Bear, Gummy
.. slug: bear-gummy
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bear, Gummy
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 6*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claw/1 bite or 1 jump |
+-----------------+-------------------------+
| Damage:         | 1d4/1d4/2d4 or 2d10     |
+-----------------+-------------------------+
| Movement:       | 40' Leap 30’            |
+-----------------+-------------------------+
| No. Appearing:  | 1d6                     |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 350                     |
+-----------------+-------------------------+

A **Gummy Bear** initially appears to be a normal brown bear (although vibrantly-colored ones are not unheard of). However, if an observer is close to the creature it appears to be translucent. Its flesh is rubbery and springs back to shape if pressed; it also tastes just like bear.

A gummy bear uses its springy legs to jump up to 30' and land on its opponent. The landing will always succeed, however the target can avoid the blow with a successful save vs. Death Ray. A gummy bear is so bouncy that blunt weapons do no damage and will bounce out of the attacker’s grasp unless a Strength test succeeds; if the save fails, the weapon will be launched 1d20+5’ away.
