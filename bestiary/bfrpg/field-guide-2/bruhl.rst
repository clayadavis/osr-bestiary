.. title: Bruhl
.. slug: bruhl
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bruhl
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 14 (11)                  |
+-----------------+--------------------------+
| Hit Dice:       | 1                        |
+-----------------+--------------------------+
| No. of Attacks: | 1 weapon                 |
+-----------------+--------------------------+
| Damage:         | 1d8 or by weapon         |
+-----------------+--------------------------+
| Movement:       | 30' Unarmored 40'        |
+-----------------+--------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6 |
+-----------------+--------------------------+
| Save As:        | Fighter: 1               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | Q, R each; D in lair     |
+-----------------+--------------------------+
| XP:             | 25                       |
+-----------------+--------------------------+

A **Bruhl** appears to be a feral proto-human similar to a Neanderthal (caveman), but slightly smaller and more simian-like. Despite its appearance, a bruhl is intelligent, wielding weaponry, wearing skins and leathers for clothing or armor, and even has an occasional shaman or witch doctor amongst its population. However, its tribal society is brutal and destructive, and bruhls nearly universally seek to subjugate other races to keep them as slaves.

A bruhl has Darkvision with a 60' range. It suffers a penalty of -1 on attack rolls in bright sunlight or within the radius of a **light** spell. Bruhl speak their own rough and simple language, but many also speak Common to some degree. A bruhl is densely-muscled, receiving a +1 bonus to hit and damage due to strength.

One out of every eight bruhl will be a warrior of 2 Hit Dice (75 XP). Regular bruhl gain a +1 bonus to their morale if they are led by such a warrior. In bruhl lairs, one out of every twelve will be a sub-chief of 4 Hit Dice (240 XP) with better armor and equipment, AC 15 (11), 20’ movement, and a +2 bonus to damage due to strength. In lairs of 30 or more, there will be a bruhl chieftain of 6 Hit Dice (500 XP), with an AC of 16 (11), 20’ movement, and a +3 bonus to damage. In the lair, bruhl never fail a morale check as long as the bruhl chieftain is alive. In addition, a lair has a chance equal to 1-2 on 1d6 of a shaman being present. A shaman is equivalent to a warrior bruhl statistically, but has Cleric abilities at level 1d4+1. In a similar fashion, there is a chance equal to 1 on 1d6 that a witch doctor is present, having Magic-User abilities at level 1d4.
