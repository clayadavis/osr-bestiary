.. title: Cheiropteran
.. slug: cheiropteran
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Cheiropteran
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 16                            |
+-----------------+-------------------------------+
| Hit Dice:       | 4                             |
+-----------------+-------------------------------+
| No. of Attacks: | 1 weapon                      |
+-----------------+-------------------------------+
| Damage:         | By weapon (with +2 STR bonus) |
+-----------------+-------------------------------+
| Movement:       | 30'                           |
+-----------------+-------------------------------+
| No. Appearing:  | Wild 2d6, Lair 10d6           |
+-----------------+-------------------------------+
| Save As:        | Fighter: 4                    |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | B, M (lair only)              |
+-----------------+-------------------------------+
| XP:             | 240                           |
+-----------------+-------------------------------+

**Cheiropterans** look like bugbears with the heads of great, misshapen bats. They are the bat-people of the deep underdark. They are born with eyes, but their priests sacrificed their eyes to Camazotz shortly after birth, so almost all cheiropterans encountered will be blind, their empty eye sockets sewn shut. It can still “see” through echolocation as a bat does. It is typically armored with a chain mail shirt and wears hard, heavy boots.

80% of a given force will be armed with halberds, and the other 20% with longbows. In addition to the treasure shown, an individual cheiropteran will carry 1d100 triangular bone coins. These are religious tokens, of value only to cheiropterans. It will also carry a strip of chewed rawhide. This is an ancestor-strip, bearing the teeth-marks of the preceding generations, and may be ransomed back to the cheiropteran leaders for as much as 10 gp. Priests will carry a silver holy symbol of Camazotz which is worth 25 gp on the open market, or 100 gp in ransom to the leaders.

For every 10 cheiropterans, one will be a corporal with AC 16 and 5 HD. For every 30, one will be a sergeant with AC 17 and 41-50 hit points, attacking as a 6 HD monster. If there are 50 or more, one will be a captain with AC 18 and 51-60 hit points, attacking as a 7 HD monster. There is a 50% chance that a priest will accompany a party of cheiropterans. A priest is a Level 4-7 Cleric with full spell-casting powers. If a priest is encountered there will also be 1-3 acolytes of level 1-3.Females are only encountered in their lair, and if they must fight they do so as gnolls; there will be females equal to 50% of the number of males. Where females are encountered there will be one whelp for every female. Whelps will typically flee but can fight as goblins if they must.

Cheiropterans are immune to any magic involving vision, including most illusions. Magical **silence** affects them as darkness would affect a sighted creature.

Wandering cheiropterans are sometimes (35%) found together with 3d6 chupacabras, which they train as hounds. A cheiropteran lair has a 90% chance of also containing 5d6 chupacabras.
