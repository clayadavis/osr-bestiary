.. title: Advi, Avenger*
.. slug: advi-avenger
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Advi, Avenger*
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 18                 |
+-----------------+--------------------+
| Hit Dice:       | 9*                 |
+-----------------+--------------------+
| No. of Attacks: | 2 blades or 1 beam |
+-----------------+--------------------+
| Damage:         | 2d8/2d8 or 4d6     |
+-----------------+--------------------+
| Movement:       | 30'                |
+-----------------+--------------------+
| No. Appearing:  | 1                  |
+-----------------+--------------------+
| Save As:        | Fighter: 9         |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 1,150              |
+-----------------+--------------------+

An **Avenger Advi** appears as a 1-foot diameter sphere with four knife-like legs and a single large ‘eye’. Its sharp legs also allow it to climb vertical surfaces such as walls.

It uses its sharp legs to slash at opponents. It can also fire a beam from its 'eye' at a target up to 50 feet away, and will set fire to any flammable material it hits. It cannot use the beam for the 2 rounds following the beam’s use.
