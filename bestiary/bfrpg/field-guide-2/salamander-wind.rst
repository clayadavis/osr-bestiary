.. title: Salamander, Wind*
.. slug: salamander-wind
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Salamander, Wind*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 24‡                     |
+-----------------+-------------------------+
| Hit Dice:       | 10* (+7)                |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite          |
+-----------------+-------------------------+
| Damage:         | 1d6/1d6/1d8             |
+-----------------+-------------------------+
| Movement:       | 30' Fly 60'             |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 2d3, Lair 1d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 10             |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | D                       |
+-----------------+-------------------------+
| XP:             | 1390                    |
+-----------------+-------------------------+

**Wind Salamanders** come from the Elemental Plane of Air. A wind salamander looks like a giant winged lizard, similar to a dragon but more sleek. It is nearly transparent, making them hard to spot and surprising on 1-4 on 1d6.

A wind salamander is constantly surrounded by strong winds, and all creatures within 20' must save vs. Petrify or be pushed 1d6×10' away. Those who succeed at this saving throw are reduced to half movement. This powerful wind deflects missile weapons in a fashion nearly identical to the spell **protection from normal missiles**.

Wind salamanders are intelligent and can speak the language of the Plane of Air, and many will also know Elvish, Common, or Dragon.
