.. title: Tick, Giant
.. slug: tick-giant
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Tick, Giant
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 14                            |
+-----------------+-------------------------------+
| Hit Dice:       | 1*                            |
+-----------------+-------------------------------+
| No. of Attacks: | 1 bite                        |
+-----------------+-------------------------------+
| Damage:         | 1d3 + blood drain and disease |
+-----------------+-------------------------------+
| Movement:       | 10'                           |
+-----------------+-------------------------------+
| No. Appearing:  | 1d6                           |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1                    |
+-----------------+-------------------------------+
| Morale:         | 7                             |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 37                            |
+-----------------+-------------------------------+

A **Giant Tick** is a blood-sucking insect about a foot long and is a more active hunter than its smaller kin. It typically first attacks by dropping out of a tree or other height when prey walks by, usually with complete surprise. It weighs little when not engorged, so usually the victim does not notice when a tic drops upon him. Adding to this, the tick's bite has an anesthetic quality, so the victim must save vs. Poison to notice the bite, which still causes 1d3 points of damage regardless. The GM should roll or otherwise keep results of these savings throws secret. Whether or not it is noticed, the tick drains blood for 1d4 points of damage every subsequent turn until either the victim dies or 20 such points of damage are drained. The hit points drained are not immediately noticed until the attached tick is realized. Each turn the victim may save again to notice the attached tick. After draining the mentioned 20 hit points from a target (or the target dies), the tick will drop off to finish its breeding cycle. An attached tick does not need to roll morale.

The tick is very durable and difficult to remove. Using brute force requires a Strength ability check (see p.153 of the **Basic Fantasy RPG Core Rules**), but this will cause an additional 1d4 points of damage to the attached victim. Even killing the tick will not cause its head to be removed, so even with extreme care and taking over ten minutes will still cause the victim 1 additional point of damage during the process. Applying fire to a still-live tick may cause it to release the character (the tick may save vs. Death Ray with failure causing the tic to drop off) but will often cause the afflicted character 1d3 points of damage in the process.

Making matters worse, the giant tick often carries a disease. The bitten character must save again vs. Poison or else be afflicted with a poison that causes 1 point of Constitution drain each day until death or a **cure disease** spell is applied. While such disease is active, the character may not heal this Constitution drain through rest (as described on p. 52 of the **Basic Fantasy RPG Core Rules**).

Giant ticks are fairly rare in most areas, as they decimate the fauna in a small region and subsequently starve themselves out, but may be more common in regions that have larger prey such as regions of prehistoric giant beasts.
