.. title: Bear, Ursine Behemoth
.. slug: bear-ursine-behemoth
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bear, Ursine Behemoth
.. type: text
.. image:

+-----------------+-----------------------------------+
| Armor Class:    | 21                                |
+-----------------+-----------------------------------+
| Hit Dice:       | 15                                |
+-----------------+-----------------------------------+
| No. of Attacks: | 2 claws/1 bite or hug (vs. Large) |
+-----------------+-----------------------------------+
| Damage:         | 2d6/2d6//2d8 or 4d6               |
+-----------------+-----------------------------------+
| Movement:       | 40'                               |
+-----------------+-----------------------------------+
| No. Appearing:  | 1                                 |
+-----------------+-----------------------------------+
| Save As:        | Fighter: 15                       |
+-----------------+-----------------------------------+
| Morale:         | 11                                |
+-----------------+-----------------------------------+
| Treasure Type:  | None                              |
+-----------------+-----------------------------------+
| XP:             | 2,850                             |
+-----------------+-----------------------------------+

An **Ursine Behemoth** is a rare bear of truly gargantuan size. Any particular breed of bear might produce an ursine behemoth, as they are born normal-looking but then grow to massive proportions. When standing upright on its hind legs, the ursine behemoth is 20’ to 30’ tall. It has a typical temperament for a bear, but due to its massive form it is continuously hungry.

An ursine behemoth attacks like others bears, but is not able to hug opponents who are not at least Large in size.
