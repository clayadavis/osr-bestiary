.. title: Serpent, Giant Devouring
.. slug: serpent-giant-devouring
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Serpent, Giant Devouring
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 24                           |
+-----------------+------------------------------+
| Hit Dice:       | 36**                         |
+-----------------+------------------------------+
| No. of Attacks: | 1 bite/1 tail or special     |
+-----------------+------------------------------+
| Damage:         | 5d8 + poison/5d12 or special |
+-----------------+------------------------------+
| Movement:       | 60’                          |
+-----------------+------------------------------+
| No. Appearing:  | 1                            |
+-----------------+------------------------------+
| Save As:        | Fighter: 20                  |
+-----------------+------------------------------+
| Morale:         | 10                           |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 18,450                       |
+-----------------+------------------------------+

A **Giant Devouring Serpent** is an enormous snake thousands of feet in length. It has jet-black and extraordinarily hard scales that are covered in faintly-purple pulsating runes. It is the scourge of cities, devouring them entirely and leaving only a featureless crater. Proportional to this, however, they are extraordinarily rare; it is common for there to be not a single sighting in a century.

It attacks by biting, with a successful hit pumping the victim with a potent toxin; he or she must save vs. Poison or die. While biting it may thrash its colossal body around, causing massive destruction to its surroundings and target alike. Once every 5 rounds it may choose to devour those in its surroundings. This affects all within 100’ of the mouth of the serpent, dealing 5d6 damage and healing the serpent by half of the total damage inflicted. Additionally this will annihilate any buildings or terrain within this 100’ radius.
