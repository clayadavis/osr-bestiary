.. title: Geminate Serpent, Bone
.. slug: geminate-serpent-bone
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Geminate Serpent, Bone
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 23                              |
+-----------------+---------------------------------+
| Hit Dice:       | 11**                            |
+-----------------+---------------------------------+
| No. of Attacks: | 2 bite or special or constrict* |
+-----------------+---------------------------------+
| Damage:         | 2d10/2d10 or special or 3d6*    |
+-----------------+---------------------------------+
| Movement:       | 50'                             |
+-----------------+---------------------------------+
| No. Appearing:  | Lair 1d3+1                      |
+-----------------+---------------------------------+
| Save As:        | Fighter: 12                     |
+-----------------+---------------------------------+
| Morale:         | 10                              |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 2,000                           |
+-----------------+---------------------------------+

A **Bone Geminate Serpent** is a skeletal being and doesn’t need food to live; it hunts for fun, and to train its young. This has lead to them being the most feared of all geminate serpents. Bone geminate serpents lie on their back and play dead, surprising on a roll of 1-3 on 1d6. Unlike most of its species, bone geminate serpents have no breath weapon, but rather may cast the spell **bones to blades** instead. This turns the bones on its body to blades, giving it +3 to armor class and damage; this also counts as a magical weapon. Once a day it can cast **raise dead** on all creature within a 30' radius, the effect lasting the same number of rounds as its age category; it does this to train its children. As a bone geminate serpent does not need its prey alive, constricting victims does real damage rather than subduing.

**Bone Geminate Serpent Age Table**

+--------------+-----+-----+-----+------+------+------+------+
| Age Category | 1   | 2   | 3   | 4    | 5    | 6    | 7    |
+--------------+-----+-----+-----+------+------+------+------+
| Length       | 60’ | 70’ | 80’ | 90’  | 90’  | 100’ | 110’ |
+--------------+-----+-----+-----+------+------+------+------+
| Hit Dice     | 9   | 10  | 10  | 11   | 11   | 12   | 13   |
+--------------+-----+-----+-----+------+------+------+------+
| Attack Bonus | +7  | +7  | +8  | +9   | +9   | +10  | +11  |
+--------------+-----+-----+-----+------+------+------+------+
| Spell        | Bones to Blades                             |
+--------------+-----+-----+-----+------+------+------+------+
| Armor Class  | 24  | 25  | 25  | 26   | 26   | 27   | 28   |
+--------------+-----+-----+-----+------+------+------+------+
| Attack Bonus | +10 | +11 | +11 | +12  | +12  | +13  | +14  |
+--------------+-----+-----+-----+------+------+------+------+
| Bite         | 2d8 | 2d8 | 4d4 | 2d10 | 2d10 | 2d12 | 4d6  |
+--------------+-----+-----+-----+------+------+------+------+
| Constrict    | 1d8 | 2d6 | 3d4 | 3d6  | 3d6  | 4d6  | 4d8  |
+--------------+-----+-----+-----+------+------+------+------+
| Talk         | -   | -   | -   | 10%  | 20%  | 20%  | 30%  |
+--------------+-----+-----+-----+------+------+------+------+
