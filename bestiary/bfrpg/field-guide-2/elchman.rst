.. title: Elchman
.. slug: elchman
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Elchman
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16 (14)                 |
+-----------------+-------------------------+
| Hit Dice:       | 9                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 gore or 1 weapon      |
+-----------------+-------------------------+
| Damage:         | 2d6 or by weapon +3     |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 9              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | B                       |
+-----------------+-------------------------+
| XP:             | 1,075                   |
+-----------------+-------------------------+

An **Elchman** resembles a bull-headed minotaur, only substantially larger and having the massive head and antlers of a northern moose. Each elchman stands greater than 15’ tall. Luckily they are largely solitary wanderers of northern marsh or lake-riddled lands. An elchman can be quite aggressive if one comes too near, but are otherwise fairly docile when left alone.

An elchman usually attacks with its massive antler rack, but may also use a weapon with a +3 damage bonus due to its great strength. An elchman sheds its antlers each year, so there are periods when one's antlers are too small to attack with. While each is completely furred, an elchman will wear additional hides or furs from creatures such as bears or wolves, but never from herbivores. Like most inhabitants of the northern climates, an elchman is essentially immune to cold environmental effects and even extreme or magical cold causes only half-damage. If a save is involved with such a cold-based effect, it receives a +4 bonus.
