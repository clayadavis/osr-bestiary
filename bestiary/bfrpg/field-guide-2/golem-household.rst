.. title: Golem, Household
.. slug: golem-household
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Household
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 11             |
+-----------------+----------------+
| Hit Dice:       | 1d4 hit points |
+-----------------+----------------+
| No. of Attacks: | 1              |
+-----------------+----------------+
| Damage:         | 1              |
+-----------------+----------------+
| Movement:       | 40'            |
+-----------------+----------------+
| No. Appearing:  | 2d4            |
+-----------------+----------------+
| Save As:        | Normal Man     |
+-----------------+----------------+
| Morale:         | 12             |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 10             |
+-----------------+----------------+

A **Household Golem** is made from animated standard household utensils such as mops and brushes. It is used as labor to avoid the cost and inconvenience of employing servants, and can perform most routine domestic chores, although for some reason asking it to fetch water always seems to go wrong.
