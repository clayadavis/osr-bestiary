.. title: Bear, Battle
.. slug: bear-battle
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bear, Battle
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 20                           |
+-----------------+------------------------------+
| Hit Dice:       | 8*                           |
+-----------------+------------------------------+
| No. of Attacks: | 2 claws + hug or 2 spears    |
+-----------------+------------------------------+
| Damage:         | 1d4+1/1d4+1 + 2d8 or 1d6/1d6 |
+-----------------+------------------------------+
| Movement:       | 40'                          |
+-----------------+------------------------------+
| No. Appearing:  | Wild 1d4, Patrol 2d4         |
+-----------------+------------------------------+
| Save As:        | Fighter: 8                   |
+-----------------+------------------------------+
| Morale:         | 9                            |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 940                          |
+-----------------+------------------------------+

A **Battle Bear** is a large bear covered in wiry black fur with smooth, hardened plates of bone covering vital areas. A pair of short spears are held by two additional stubby arms attached at its shoulders. It attacks by charging at a foe to skewer them on its spears before clawing and hugging, much like other bears. The hardened plates on its head cause it to be unable to effectively bite in combat.

A patrol of 2d4 battle bears may occasionally be found in areas of the forest with a high bear population, acting as guardians for other bear species.
