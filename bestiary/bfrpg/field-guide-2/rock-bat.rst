.. title: Rock Bat
.. slug: rock-bat
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Rock Bat
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 16         |
+-----------------+------------+
| Hit Dice:       | 2*         |
+-----------------+------------+
| No. of Attacks: | 1 stab     |
+-----------------+------------+
| Damage:         | 1d6        |
+-----------------+------------+
| Movement:       | 150' (50') |
+-----------------+------------+
| No. Appearing:  | 1d8        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 100        |
+-----------------+------------+

A **rock bat** is a silicate creature that resembles a large, irregular, generally rounded rock with a horn on the bottom of its body. It floats by emitting a glowing gravity ray from gem-like growths on the sides of its body. Its rocky hide and floating ability give it a respectable armor class.

A rock bat attacks by dropping directly down on its prey. On a natural 20 the victim is impaled by the rock bat, receiving 1d6 points of damage each round until either the victim or rock bat is slain.
