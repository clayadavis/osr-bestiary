.. title: Living Graveyard*
.. slug: living-graveyard
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Living Graveyard*
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 21‡                   |
+-----------------+-----------------------+
| Hit Dice:       | 40** (+15)            |
+-----------------+-----------------------+
| No. of Attacks: | 1 tombstone + special |
+-----------------+-----------------------+
| Damage:         | 2d6 + special         |
+-----------------+-----------------------+
| Movement:       | N/A                   |
+-----------------+-----------------------+
| No. Appearing:  | 1                     |
+-----------------+-----------------------+
| Save As:        | Magic-User: 16        |
+-----------------+-----------------------+
| Morale:         | 12                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 21,650                |
+-----------------+-----------------------+

A **Living Graveyard** resembles a giant mountain filled with tombstones, with a chapel-like building at its top. Whenever disturbed by robbers or any hostile force, the living graveyard will awaken and grow a face and arms, ready to attack. It attacks by shooting its gravestones; by doing this, the living graveyard will lose 1 HP per tombstone shot, and can shoot up to 1/4 of its HP. Each gravestone is a grenade-like attack, dealing 2d6 points of damage; in case of a miss, the intended victim must save vs. Dragon Breath or suffer half-damage due to the tombstone’s explosion on impact.

A living graveyard can release the remnants of those buried in it by also losing HP as above (and is also limited to 1/4 of this HP). The remnants will be skeletons (1-4 on 1d6) or zombies (5-6 on 1d6), who will attack anyone nearby.

At 0 HP the living graveyard will collapse and enter a regenerative slumber, regaining 1 HP per week or 1d6 HP per body buried in it. It can only be destroyed by entering its chapel during its slumber (via a 15th level magically-held door) and destroying its "heart", a glowing and disgusting glowing protuberance of 2d4 HP and located at the very center of the chapel. There is a percent chance, equal to half the living graveyard's full HP, that the heart is protected by (full HP/10) skeleton guardians (each with 2 HD+2 and +2 to attack and damage).
