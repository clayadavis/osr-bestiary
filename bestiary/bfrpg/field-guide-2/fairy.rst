.. title: Fairy
.. slug: fairy
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Fairy
.. type: text
.. image:

Fairies are sentient manifestations of a natural environment, such as a forest or a mountain. They appear in a form familiar to the viewer, coupled with attributes of their environment. They prefer taking on forms that are gentle and graceful, with female Elves being a particular favorite.

A fairy will respond harshly to anyone who disparages its environment and will be pleased if it is protected, but beyond that they care little about the actions of mortals, appearing apathetic in anything they do.
