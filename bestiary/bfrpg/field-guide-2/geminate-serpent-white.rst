.. title: Geminate Serpent, White
.. slug: geminate-serpent-white
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Geminate Serpent, White
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 20                               |
+-----------------+----------------------------------+
| Hit Dice:       | 8**                              |
+-----------------+----------------------------------+
| No. of Attacks: | 2 bite or 1 breath or constrict* |
+-----------------+----------------------------------+
| Damage:         | 2d8/2d8 or breath or 2d6*        |
+-----------------+----------------------------------+
| Movement:       | 40'                              |
+-----------------+----------------------------------+
| No. Appearing:  | Lair 1d3+1                       |
+-----------------+----------------------------------+
| Save As:        | Fighter: 9                       |
+-----------------+----------------------------------+
| Morale:         | 10                               |
+-----------------+----------------------------------+
| Treasure Type:  | H                                |
+-----------------+----------------------------------+
| XP:             | 1,500                            |
+-----------------+----------------------------------+

**White Geminate Serpents** are the shortest of their species, living near and on mountains where it snows a lot. They seek out humans to capture, as they provide the most entertainment and best training for their young. White serpents attack by burying themselves in snow and jumping at opponents, surprising on 1-4 on 1D6. The lairs of white geminate serpents are usually underground burrows spun with webs to ensnare any unwary travelers. The white geminate serpent uses its breath to ensnare opponents when surprised and drags them back to its lair in the web. A white geminate serpent’s web does not disappear like the spell, but will remain for up to a number of years equal to its age category. Just like the spell **web**, the web produced by a white geminate serpent is highly flammable.

**White Geminate Serpent Age Table**

+---------------+-----+-----+-----+-----+------+------+------+
| Age Category  | 1   | 2   | 3   | 4   | 5    | 6    | 7    |
+---------------+-----+-----+-----+-----+------+------+------+
| Length        | 20’ | 30’ | 40’ | 40’ | 50’  | 60’  | 60’  |
+---------------+-----+-----+-----+-----+------+------+------+
| Hit Dice      | 6   | 7   | 7   | 8   | 8    | 9    | 10   |
+---------------+-----+-----+-----+-----+------+------+------+
| Attack Bonus  | +6  | +7  | +8  | +8  | +8   | +9   | +9   |
+---------------+-----+-----+-----+-----+------+------+------+
| Breath Weapon | Web (Line)                                 |
+---------------+-----+-----+-----+-----+------+------+------+
| Length        | -   | 30' | 30' | 40' | 40'  | 50'  | 60'  |
+---------------+-----+-----+-----+-----+------+------+------+
| Width         | -   | 20' | 20' | 25' | 30'  | 30'  | 35'  |
+---------------+-----+-----+-----+-----+------+------+------+
| Bite          | 2d6 | 2d8 | 2d8 | 3d6 | 2d10 | 2d12 | 2d12 |
+---------------+-----+-----+-----+-----+------+------+------+
| Constrict     | 1d6 | 1d8 | 2d6 | 3d4 | 2d6  | 3d6  | 3d8  |
+---------------+-----+-----+-----+-----+------+------+------+
| Talk          | -   | -   | 20% | 30% | 50%  | 60%  | 65%  |
+---------------+-----+-----+-----+-----+------+------+------+
