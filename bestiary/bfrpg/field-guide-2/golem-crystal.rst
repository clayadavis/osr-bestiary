.. title: Golem, Crystal
.. slug: golem-crystal
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Crystal
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 6*         |
+-----------------+------------+
| No. of Attacks: | 2 fists    |
+-----------------+------------+
| Damage:         | 2d4/2d4    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 6 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 555        |
+-----------------+------------+

The **Crystal Golem** appears as a hairless androgynous humanoid, but is rarely seen as it can cast an **invisibility** spell upon itself as a full round action. They are typically used by wizards and liches who require very discreet workers or guards, and are often programmed to do their work in complete silence. A chain golem takes full damage from blunt weapons, but only half damage from cutting or piercing ones.
