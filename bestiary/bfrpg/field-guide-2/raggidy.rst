.. title: Raggidy
.. slug: raggidy
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Raggidy
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 14         |
+-----------------+------------+
| Hit Dice:       | 3          |
+-----------------+------------+
| No. of Attacks: | 2 fists    |
+-----------------+------------+
| Damage:         | 1d6/1d6    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1d10       |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | See below  |
+-----------------+------------+
| XP:             | 145        |
+-----------------+------------+

A **Raggidy** is a construct sewn together using magical thread. It is humanoid in appearance standing 7’ tall, weighing 300 pounds, and is made out of crudely-stitched cloth stuffed with sawdust. It has buttons for eyes and a twisted grin sewn onto its face. However, the magic thread that animates the raggidy gives it the barest hint of sentience, allowing it to act semi-independently and follow orders better than most constructs. It does not speak but it understands the language of its creator.

A raggidy is a fearless enemy and will engage in melee with little thought for tactics or strategy. It is highly flammable and takes double damage from fire-based attacks.

The magical fabric of a raggidy is valuable to both Magic-Users and tailors, and worth up to 1d6x100 gp. Note that, if destroyed by fire, acid, or other such attacks, the raggidy's material may be too damaged to sell.
