.. title: Mink (and Giant)
.. slug: mink-and-giant
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Mink (and Giant)
.. type: text
.. image:

+-----------------+--------------+----------------+
|                 | **Mink**     | **Giant Mink** |
+=================+==============+================+
| Armor Class:    | 15           | 15             |
+-----------------+--------------+----------------+
| Hit Dice:       | 1d4 HP       | 4+4            |
+-----------------+--------------+----------------+
| No. of Attacks: | 1 bite       | 1 bite         |
+-----------------+--------------+----------------+
| Damage:         | 1-2          | 1d8            |
+-----------------+--------------+----------------+
| Movement:       | 30' Swim 50' | 30' Swim 50'   |
+-----------------+--------------+----------------+
| No. Appearing:  | 1d6          | 1d4            |
+-----------------+--------------+----------------+
| Save As:        | Normal Man   | Fighter: 4     |
+-----------------+--------------+----------------+
| Morale:         | 7            | 8              |
+-----------------+--------------+----------------+
| Treasure Type:  | None*        | None*          |
+-----------------+--------------+----------------+
| XP:             | 10           | 240            |
+-----------------+--------------+----------------+

A **Mink** is a common name for an alert semi-aquatic carnivorous mammal of the mustelidae family. A mink falls somewhere between basically land-dwelling weasels and the even more aquatic otters. The furs of a mink are highly prized for its use in clothing. A mink uses quick darting attacks (+1 initiative), scoring vicious bites. Once a mink bites it can choose to hold on, causing automatic damage each round.

The **Giant Mink** is more commonly found in areas where other prehistoric (ice-age) creatures are found but otherwise conforms to typical mink behavior, being only larger and more dangerous.

* Minks and their giant kin have valuable furs which keeps their numbers down in regions near settlements.
