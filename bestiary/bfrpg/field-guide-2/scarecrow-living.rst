.. title: Scarecrow, Living
.. slug: scarecrow-living
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Scarecrow, Living
.. type: text
.. image:

+-----------------+--------------------+-------------+
|                 | **Normal**         | **Small**   |
+=================+====================+=============+
| Armor Class:    | 13                 | 12          |
+-----------------+--------------------+-------------+
| Hit Dice:       | 4 + 2* (+5)        | 2 + 2* (+3) |
+-----------------+--------------------+-------------+
| No. of Attacks: | 1 slam or 1 weapon | 1 slam      |
+-----------------+--------------------+-------------+
| Damage:         | 1d6+1 or by weapon | 1d4+1       |
+-----------------+--------------------+-------------+
| Movement:       | 30'                | 30'         |
+-----------------+--------------------+-------------+
| No. Appearing:  | 1, Wild 1d4        | 1d4         |
+-----------------+--------------------+-------------+
| Save As:        | Fighter: 4         | Fighter: 2  |
+-----------------+--------------------+-------------+
| Morale:         | 12                 | 12          |
+-----------------+--------------------+-------------+
| Treasure Type:  | None               | None        |
+-----------------+--------------------+-------------+
| XP:             | 280                | 100         |
+-----------------+--------------------+-------------+

**Living Scarecrows** are mindless and less sophisticated constructs. Commonly called harvest golems, they are created from a wooden frame with some old worn clothing, stuffed with hay, and topped with a carved pumpkin for a head. It is commonly seen in the fields during the harvest.

While most are unarmed, some living scarecrows are equipped with a threshing flail (treat as a mace), a pitchfork (treat as spear), or a harvesting scythe (treat as a pole arm). It has a +1 bonus to both attack and damage rolls in melee combat due to its magical strength. It also has Darkvision out to 60' and is immune to **fear**.

Unlike other golems, living scarecrows are not immune to mundane forms of attack and only has 25% resistance to magic spells. In addition, due to the materials in their construction the living scarecrow is weak against fire, taking 1 extra point per die of damage from fire attacks and suffers a -2 penalty to saves against fire-based attacks. It also has a -1 penalty to initiative rolls.

**Small Living Scarecrows** are just like their bigger cousins, but are about the size of a Halfling or a Human child.
