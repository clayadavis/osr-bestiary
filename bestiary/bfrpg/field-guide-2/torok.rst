.. title: Torok
.. slug: torok
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Torok
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 17 (14 unarmored)   |
+-----------------+---------------------+
| Hit Dice:       | 2                   |
+-----------------+---------------------+
| No. of Attacks: | 1 weapon            |
+-----------------+---------------------+
| Damage:         | As weapon           |
+-----------------+---------------------+
| Movement:       | 50' (60’ unarmored) |
+-----------------+---------------------+
| No. Appearing:  | 1d12+3              |
+-----------------+---------------------+
| Save As:        | Thief: 6            |
+-----------------+---------------------+
| Morale:         | 8                   |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 100                 |
+-----------------+---------------------+

Tall and slender, the **Torok** are a forest-dwelling race of bird-like humanoids who are almost unnaturally agile. Their heads are shaped like long narrow triangles with a hooked beak at the end. They vary from 6 to 8 feet tall. Their skin comes in a variety of different hues ranging from bright red to blue and yellow, with small tufts of feathers on the elbows and the back of the head. They are intelligent and tend towards evil, preying on animals and humanoids alike, often not bothering to kill their prey once they have incapacitated them.

Torok group together in tribes, normally led by the strongest warrior of the tribe. One out of every four will be a warrior of 3 HD and will normally be wearing armor. They prefer polearms, as a specific type unique to each warrior denotes states, and will be engraved with various feats the warrior has achieved; they will use any weapon that comes to hand in an emergency. One in twenty toroks will be a torok chief of 4 HD; these normally wear more ornate armor dyed a separate color to show status. In torok lairs of 50 or greater there will be a high chieftain of 7 HD, who wears plate armor (AC 19) and has a +2 bonus to damage. In addition, there is a 50% chance of a shaman being present who is equivalent to a warrior but has Magic-User abilities at level 1d4+3.

Toroks have a +2 bonus to initiative and are difficult to catch by surprise. Their tribal society revolves around an ingrained honor system, and will never turn down a challenge to ritualistic combat.
