.. title: Horseman, Dread
.. slug: horseman-dread
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Horseman, Dread
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 18 to 20              |
+-----------------+-----------------------+
| Hit Dice:       | 6** to 8** (+7 to +9) |
+-----------------+-----------------------+
| No. of Attacks: | 1 slam or 1 weapon    |
+-----------------+-----------------------+
| Damage:         | 1d6 or by weapon      |
+-----------------+-----------------------+
| Movement:       | 40'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1 (+ 1 Hell Steed)    |
+-----------------+-----------------------+
| Save As:        | Fighter: 6 to 8       |
+-----------------+-----------------------+
| Morale:         | 12                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 610 to 1,015          |
+-----------------+-----------------------+

**Dread Horsemen** are Hell's light cavalry and outriders. These skeletal warriors wear charred, blackened plate mail and prefer to run-down their foes and dispatch them with a **battleaxe +1**. A dread horseman also has the spell abilities of cleric of the 3rd to 5th level (based on its hit dice).

A dread horsemen takes one-half damage from non-magical weapons, and like all undead, it is immune to **sleep**, **charm**, and **hold** magic, and can be Turned by a Cleric according to its hit dice. It is also immune to **fear**, and has Darkvision out to 60'.

If a dread horsemen stands on hallowed ground, it loses its Cleric abilities and suffers a -4 penalty on all checks and saves. It can instantly tell when it enters a hallowed area and will leave immediately.

If reduced to 0 Hit Points, a dread horseman is not destroyed but disappears in a blaze of hellish green flames, banished back to the infernal realm, along with all equipment it carries. Once banished, it cannot come back to the mortal world for an entire year. To truly destroy one you must restraine it and then remove its skull, place it in a **blessed** oaken box, and then burn it.
