.. title: Boar, Sewer
.. slug: boar-sewer
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Boar, Sewer
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 18                 |
+-----------------+--------------------+
| Hit Dice:       | 7                  |
+-----------------+--------------------+
| No. of Attacks: | 1 bite or 1 charge |
+-----------------+--------------------+
| Damage:         | 1d10 or 1d12       |
+-----------------+--------------------+
| Movement:       | 30' Charge 90’     |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d6, Lair 2D8 |
+-----------------+--------------------+
| Save As:        | Fighter: 5         |
+-----------------+--------------------+
| Morale:         | 8                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 500                |
+-----------------+--------------------+

A **Sewer Boar** is a boar that lives in sewers, mud hollows, and marshes. It can grow up to 10’ long and 5’ tall. The sewer boar has thick brown fur and is covered in a layer of extraordinarily thick blubber; it is so thick that any non-magical weapon will be stuck in the hide unless a Strength check is made. The sewer boar takes half-damage from all weapons except spears, which are the only ones that can pierce the flesh deep enough to cause significant harm. A sewer boar can charge 90' and in doing so deals damage to all in its path unless a save vs. Death Ray is made to get out of the way.
