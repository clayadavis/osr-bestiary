.. title: Skeletal Flayer
.. slug: skeletal-flayer
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Skeletal Flayer
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 15                 |
+-----------------+--------------------+
| Hit Dice:       | 1                  |
+-----------------+--------------------+
| No. of Attacks: | 1 whip/1 claw      |
+-----------------+--------------------+
| Damage:         | 1d6 + 2/1d6        |
+-----------------+--------------------+
| Movement:       | 40'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d6, Lair 3d6 |
+-----------------+--------------------+
| Save As:        | Fighter: 1         |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | A                  |
+-----------------+--------------------+
| XP:             | 37                 |
+-----------------+--------------------+

The **Skeletal Flayer** appears to be a regular skeleton with its forearms and hands replaced with a long whip and a steel curved blade. It is disturbingly unique in its method of attack; it attempts to flay the skin off of its victim during combat, using the blade and whip to brutal and grisly effect. If successful it will proceed to wear the victim’s skin as a grotesque mockery of life, and is often mistaken for a zombie as a result of the skin’s progressive decomposing.

A skeletal flayer that has not recently killed and skinned a victim is mechanically similar to a regular skeleton, but when wearing the skin of a humanoid of roughly the same size it gains the following benefits: +2 to all damage, half damage from both bladed and blunt weapons, and a 10' movement bonus.
