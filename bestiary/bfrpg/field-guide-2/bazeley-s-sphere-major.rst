.. title: Bazeley’s Sphere Major
.. slug: bazeley-s-sphere-major
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bazeley’s Sphere Major
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 20                             |
+-----------------+--------------------------------+
| Hit Dice:       | 12*                            |
+-----------------+--------------------------------+
| No. of Attacks: | 2 bludgeon or 2d4 sphere shots |
+-----------------+--------------------------------+
| Damage:         | 1d6/1d6 or 1d4 each            |
+-----------------+--------------------------------+
| Movement:       | 60'                            |
+-----------------+--------------------------------+
| No. Appearing:  | 3d10 HD worth                  |
+-----------------+--------------------------------+
| Save As:        | Fighter: 3                     |
+-----------------+--------------------------------+
| Morale:         | 12                             |
+-----------------+--------------------------------+
| Treasure Type:  | 1,200 gp worth of crystals     |
+-----------------+--------------------------------+
| XP:             | 1,975                          |
+-----------------+--------------------------------+

The most powerful form is the **Sphere Major**, a towering monstrosity that stands 12’ high. It uses its club-like arms to bludgeon foes. Alternatively it can shoot out 2d4 clusters of spheres; each of these shots will remove a hit dice from the sphere major. The spheres shot out will then coalesce into a **Sphere Recombinant**.
