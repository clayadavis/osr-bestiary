.. title: Troll, Night
.. slug: troll-night
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Troll, Night
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 8*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite          |
+-----------------+-------------------------+
| Damage:         | 1d8/1d8/1d12            |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 6              |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | F                       |
+-----------------+-------------------------+
| XP:             | 945                     |
+-----------------+-------------------------+

**Night Trolls** are a shadowy relative of the common troll native to the planes of shadow. They are rarely found on the material plane except when summoned or when rifts form between these planes. They lurk in areas shrouded in perpetual darkness and are fearsome predators. They are similar to common trolls in size and stature, but their skin is a purplish black in hue and their long, lank hair is universally dark blue in color. They are incredibly quiet and stealthy and can hide and move silently as a Thief equal to their hit dice.

Like common trolls, night trolls have the power of regeneration. However, their regenerative power is far greater when in areas of darkness. Night trolls heal 3 HP each round after being injured when standing in dark or dim conditions. This rate is reduced to 1 HP when the night troll is in brightly-lit areas. A night troll reduced to 0 HP is not dead but only disabled for 2d6 rounds, at which point it will heal either 3 or 1 HP depending on the lighting conditions. Note that night trolls are far more cunning and sinister than common trolls and will often “play dead”. Unlike common trolls, fire and acid do not stop a night troll’s regeneration. However, if a **continual light** spell is cast upon the troll, its regeneration ceases completely for the duration of the spell.

Exposing a night troll to direct sunlight is also a method to destroy the creature. It can only act for three rounds and is destroyed completely on the fourth should it not escape the sun's rays. If a night troll loses a limb or body part, the lost portion regrows in one turn; or, the creature can regenerate the severed member instantly by holding it against the stump.

Night trolls are far more cunning and patient than normal trolls, and are willing to stalk potential prey for hours or even days before attacking.
