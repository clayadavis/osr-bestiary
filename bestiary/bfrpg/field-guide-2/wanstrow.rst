.. title: Wanstrow
.. slug: wanstrow
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Wanstrow
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 18         |
+-----------------+------------+
| Hit Dice:       | 4          |
+-----------------+------------+
| No. of Attacks: | 2          |
+-----------------+------------+
| Damage:         | 1d10/1d10  |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1d6        |
+-----------------+------------+
| Save As:        | Fighter: 4 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 240        |
+-----------------+------------+

**Wanstrows** resemble tall and hideously-mutated orcs with a single eye. They vary greatly in coloration and may possess an unusual number of limbs, some having their lower half shaped like that of a snake, others having limbs replaced by strange bundles of prehensile tentacles that function similarly to a regular limb. Many will appear to have been partially-melted with facial features being partially obscured underneath flaps of skin. They all share a few features however. All have a set of three mandibles instead of a mouth, all have small sections of lung protruding from the skin that will inflate and deflate as the creature breathes, and all possess only one eye.

Some believe that wanstrows are the unfortunately fertile result of a failed magical experiment attempting to change orc physiology, however none can say for sure. They seem to possess a base level of intelligence similar to that of a dog or wolf. Most wanstrows exhibit signs of being in physical pain such as infrequent muscles spasms and constant loud screaming. They are highly aggressive and inhumanly strong.
