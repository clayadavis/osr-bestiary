.. title: Water Leaper
.. slug: water-leaper
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Water Leaper
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 16                   |
+-----------------+----------------------+
| Hit Dice:       | 5                    |
+-----------------+----------------------+
| No. of Attacks: | 1 bite or 1 sting    |
+-----------------+----------------------+
| Damage:         | 2d6 or 1d3 + poison  |
+-----------------+----------------------+
| Movement:       | 10’ Fly 60’ Swim 40’ |
+-----------------+----------------------+
| No. Appearing:  | 1, Lair 1d4          |
+-----------------+----------------------+
| Save As:        | Fighter: 5           |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 360                  |
+-----------------+----------------------+

A strange creature likely to inhabit swamps and lakes, the **Water Leaper** is described by some as resembling a giant frog with a large pair of bat's wings in place of its forelegs and a complete lack of hind legs, as well as possessing a long scaly lizard-like tail with a barbed poison stinger at the end. It acquired its name from the way it jumps and leaps across the water using its wings. This movement is often startling; it ambushes its victim by leaping out of the water, latching on, and dragging them back into the watery depths. The water leaper is known to prey on livestock, primarily sheep, and sometimes even fishermen, often being blamed for disappearances in rural fishing communities.
