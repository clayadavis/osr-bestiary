.. title: Octopeople
.. slug: octopeople
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Octopeople
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 13                            |
+-----------------+-------------------------------+
| Hit Dice:       | 8                             |
+-----------------+-------------------------------+
| No. of Attacks: | 1 tentacle or by weapon       |
+-----------------+-------------------------------+
| Damage:         | 1d10 + paralysis or by weapon |
+-----------------+-------------------------------+
| Movement:       | 30' Unarmored 40'             |
+-----------------+-------------------------------+
| No. Appearing:  | 2d4, Wild 1d6                 |
+-----------------+-------------------------------+
| Save As:        | Fighter: 10                   |
+-----------------+-------------------------------+
| Morale:         | 12                            |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 1,000                         |
+-----------------+-------------------------------+

**Octopeople** are humanoids that resemble blue octopi from the waist up. They live in settlements, and under disguise replace royalty or other people of similar influence. An octoperson's blood is blue and due to the constant replacing of royalty many now assume that real royalty have blue blood. An octoperson is indistinguishable from normal people by most means, though **true seeing** and similar spells can see through the disguise.

An octoperson's disguise is comprised of the flesh of humans. Once an octoperson kills a human they skin and wear its skin; it can also absorb the human's memories by eating its brain.

An octoperson reproduces by laying eggs in a humanoid creature, usually ones that are captured by the octoperson's disguise prior to replacement. Over time octopeople will replace all people in a settlement. When this is completed the disguises are removed and the octopeople live in the captured settlement as their true selves. Octopeople do not need to eat humans after colonizing and will live in peace with local creatures, living off plants and domesticated creatures as a human might.

When in octopus form, an octoperson can attack using a tentacle that paralyzes on touch unless a save vs. Paralysis is made. This is used to capture victims and either eat and replace or to lay eggs in. No one really knows how the octopeople came about; many say it was a **polymorph** spell that was not quite right, while others will swear that they have always been here and have only recently revealed themselves.
