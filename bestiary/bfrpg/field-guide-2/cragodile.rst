.. title: Cragodile
.. slug: cragodile
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Cragodile
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 20                       |
+-----------------+--------------------------+
| Hit Dice:       | 8                        |
+-----------------+--------------------------+
| No. of Attacks: | 1 bite or 1 tail         |
+-----------------+--------------------------+
| Damage:         | 2d8 or 3d6               |
+-----------------+--------------------------+
| Movement:       | 30' (10') Swim 30' (10') |
+-----------------+--------------------------+
| No. Appearing:  | Wild 1d4                 |
+-----------------+--------------------------+
| Save As:        | Fighter: 8               |
+-----------------+--------------------------+
| Morale:         | 9                        |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 875                      |
+-----------------+--------------------------+

A **Cragodile** is akin to a normal crocodile but with a heavy stony hide. Unlike its relatives, a cragodile may be found in virtually any climate. It hunts the shallows and banks of waterways, remaining submerged until prey comes within reach and surprising on 1-4 on 1d6. Because of a cragodile's exceptionally hard skin it takes half damage from piercing or edged weapons. A cragodile is significantly heavier than its normal kin and does not actually swim. The swim speed listed is actually more of a “run” speed along the bottom of a waterway which it can navigate as easily as dry land.
