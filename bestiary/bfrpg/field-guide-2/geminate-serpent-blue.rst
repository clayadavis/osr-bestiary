.. title: Geminate Serpent, Blue
.. slug: geminate-serpent-blue
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Geminate Serpent, Blue
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 19                               |
+-----------------+----------------------------------+
| Hit Dice:       | 7**                              |
+-----------------+----------------------------------+
| No. of Attacks: | 2 bite or 1 breath or constrict* |
+-----------------+----------------------------------+
| Damage:         | 2d12/2d12 or breath or 3d4*      |
+-----------------+----------------------------------+
| Movement:       | 50'                              |
+-----------------+----------------------------------+
| No. Appearing:  | Lair 1d3+1                       |
+-----------------+----------------------------------+
| Save As:        | Fighter: 11                      |
+-----------------+----------------------------------+
| Morale:         | 11                               |
+-----------------+----------------------------------+
| Treasure Type:  | H+L                              |
+-----------------+----------------------------------+
| XP:             | 1,700                            |
+-----------------+----------------------------------+

**Blue Geminate Serpents** live in colder humid climates on top of hills and the like. Blue serpents feed off farmers and livestock captured from the outskirts of towns and cities, using its breath to freeze and drag its victims back to its lair. Creatures frozen are unable to do anything besides breathe for 1d8 hours. Once in a blue geminate serpent’s lair, the victim is frozen to the roof above where the serpent sleeps. The serpent lies under the victim with mouths agape waiting for the victim to try to escape and fall. Captured creatures that do not provide enough struggle or have died are simply left frozen to the roof. The limbs of such frozen creatures can withstand 150 lb of weight.

**Blue Geminate Serpent Age Table**

+---------------+-----+-----+------+------+------+------+------+
| Age Category  | 1   | 2   | 3    | 4    | 5    | 6    | 7    |
+---------------+-----+-----+------+------+------+------+------+
| Length        | 50’ | 60’ | 80’  | 80’  | 90’  | 100’ | 120’ |
+---------------+-----+-----+------+------+------+------+------+
| Hit Dice      | 6   | 6   | 7    | 7    | 8    | 8    | 9    |
+---------------+-----+-----+------+------+------+------+------+
| Attack Bonus  | +6  | +7  | +7   | +8   | +8   | +9   | +10  |
+---------------+-----+-----+------+------+------+------+------+
| Breath Weapon | Freeze (Line)                                |
+---------------+-----+-----+------+------+------+------+------+
| Length        | -   | 20' | 30'  | 40'  | 40'  | 50'  | 60'  |
+---------------+-----+-----+------+------+------+------+------+
| Width         | -   | 30' | 30'  | 35'  | 35'  | 40'  | 45'  |
+---------------+-----+-----+------+------+------+------+------+
| Bite          | 2d8 | 4d4 | 2d10 | 2d12 | 2d12 | 3d10 | 6d6  |
+---------------+-----+-----+------+------+------+------+------+
| Constrict     | 1d8 | 2d6 | 3d4  | 3d4  | 3d6  | 3d6  | 3d8  |
+---------------+-----+-----+------+------+------+------+------+
| Talk          | -   | 10% | 20%  | 30%  | 40%  | 40%  | 50%  |
+---------------+-----+-----+------+------+------+------+------+
