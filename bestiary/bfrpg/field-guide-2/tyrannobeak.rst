.. title: Tyrannobeak
.. slug: tyrannobeak
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Tyrannobeak
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 23                 |
+-----------------+--------------------+
| Hit Dice:       | 18 (AB +12)        |
+-----------------+--------------------+
| No. of Attacks: | 1 claw/1 beak      |
+-----------------+--------------------+
| Damage:         | 2d8/4d6            |
+-----------------+--------------------+
| Movement:       | 50' (10') Leap 10’ |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d3           |
+-----------------+--------------------+
| Save As:        | Fighter: 18        |
+-----------------+--------------------+
| Morale:         | 11                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 4,000              |
+-----------------+--------------------+

The **Tyrannobeak** is a massive member of the terror-bird family (Phorusrhacidae) of bipedal flightless birds. Despite its enormous size (20’ height and 5-ton weight), a tyrannobeak is a swift runner and can leap prodigiously. It is a top-level predator, able to take down the largest of prey. When possible, it typically first attacks by leaping (counts as a charge attack) with both claws and a bite, but once properly in melee can only make one claw attack per round in addition to a bite. If the tyrrannobeak is actively chasing a fleeing opponent, then it can only manage biting attacks.

When two are encountered it will be a mated pair. If a third is present, then it will be an immature individual (roll d% and apply to HD and attacks, rounding as necessary). While several eggs are laid, the young aggressively attack each other until only the dominant alpha remains. The adults protect and instruct this alpha youth until it reaches adulthood.
