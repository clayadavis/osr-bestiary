.. title: Dufflepud
.. slug: dufflepud
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dufflepud
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 16 (13)    |
+-----------------+------------+
| Hit Dice:       | 1          |
+-----------------+------------+
| No. of Attacks: | 1 spear    |
+-----------------+------------+
| Damage:         | 1d6        |
+-----------------+------------+
| Movement:       | 20'        |
+-----------------+------------+
| No. Appearing:  | 4d8        |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | C          |
+-----------------+------------+
| XP:             | 25         |
+-----------------+------------+

The **Dufflepud** is a strange dwarf-like creature whose primary means of locomotion is its enormous single foot that it uses to make long hops, enabling it to travel at a surprising speed. A dufflepud is of foul temperament, cursing frequently and bemoaning its unique condition. Despite this it is a highly courageous and capable warrior, wielding long spears and shields and wearing heavy chain into combat. A dufflepud is also an excellent miner and fisherman, even able to use its single foot as a canoe.

Once a day a dufflepud can turn itself invisible as the spell **invisibility**, although it is loud and clumsy enough that this does not help its ability to sneak around. One in every five dufflepuds is a veteran warrior of 2 HD, and one in every 20 is a dufflepud captain, armored in plate (AC 18) and wielding a notched great axe. The leader of the dufflepuds is known as the chief duffer, who wears gold gilt plate armor (AC 18) and wields a magical weapon with a bonus of 1d4-1.
