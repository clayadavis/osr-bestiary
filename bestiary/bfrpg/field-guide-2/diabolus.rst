.. title: Diabolus
.. slug: diabolus
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Diabolus
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 14          |
+-----------------+-------------+
| Hit Dice:       | 15**        |
+-----------------+-------------+
| No. of Attacks: | 1 bite      |
+-----------------+-------------+
| Damage:         | 2d6+4       |
+-----------------+-------------+
| Movement:       | 60’         |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 15 |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 3.100       |
+-----------------+-------------+

The final degenerative stage of a vampire that has been starved of blood for an extended duration, the **Diabolus** is a pallid and gaunt 6’ to 8’ tall humanoid with disproportionately long arms and legs. The creature is completely hairless and its face has morphed from something that once resembled human to something more beast-like. Its jaws visibly protrude from the rest of its face, and its fangs have lengthened to several inches, becoming jagged and uneven in the process.

A diabolus' eyes glow a deep and crazed crimson, the great cunning and intelligence that once existed behind them long vanished and replaced with an insatiable lust for blood and carnage, causing the creature to have no regard for its own safety. It will sense and seek out any living beings within its lair with extreme prejudice. No amount of blood or any other substances can revert a diabolus back to its previous vampiric state. Once fallen this far, the individual is forever lost; however it normally takes decades, if not centuries, to reach this point.

While it has fallen far from its former state, a diabolus is still in essence a vampire, if a depraved and horrendously mutated one, and shares the usual traits. It casts no shadows and throws no reflections in mirrors. It cannot **charm** others with its gaze; instead, those who meet it must make a save vs. Spells or be frozen to the spot in terror and be unable to act for 1d3 rounds. Creatures with 8 or more hit dice are immune to this effect. A diabolus will use its lengthened fangs to rip into enemies; this has a paralyzing effect similar to that of ghouls. Those killed by a diabolus will rise as ghouls on the next new moon; these ghouls will not attack the diabolus that created them.

Perhaps a result of its deprived state, a diabolus is immune to the effects of running water and is much more resilient to sunlight, being able to survive when exposed directly for 5 rounds, although it will burst into flames and lose 1/5th of its hit points for each round of exposure. The creature also demonstrates a limited ability to change its shape, being able to sprout wings from its back over the course of two rounds, granting it limited flight (20'). It can also conceal itself perfectly within deep shadows, essentially invisible. Its movement speed doubles when hiding.

A diabolus is immune to the effects of garlic and will not recoil at a strongly-presented mirror; however an exposed holy symbol within 10' of it will set parts of it alight and cause flesh to boil away, doing 1d6+1 damage per rounds. This effect does not stack and the character bearing the symbol will become its sole target until the symbol is removed from the radius. The creature can only be killed by exposing it to daylight, forcing it to drink holy water, or blood that has been blessed by a Cleric of 9th level or higher.

Powerful vampires have been known to deliberately starve rivals and disobedient thralls until they become a diabolus, using them as the equivalent of a guard dog. As a result, these creatures are often found inhabiting caves beneath a vampire's castle or lair.
