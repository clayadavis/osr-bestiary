.. title: Gila
.. slug: gila
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Gila
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 1-1                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite/1 weapon         |
+-----------------+-------------------------+
| Damage:         | 1d4/by weapon           |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | Q, R each; D, K in lair |
+-----------------+-------------------------+
| XP:             | 10                      |
+-----------------+-------------------------+

**Gila** (singular and plural) are a race of relatively small lizard men. Standing about 4’ in height, each has varying patterns of light and dark scales, and these scales tend to be thicker and denser than those found in other lizard folk. Gila tend towards slow and sluggish mannerisms, but are fully capable of normal movement rates and activities when there is a need. Gila are not inherently aggressive but are very territorial and defensive.

When using a small or medium-sized melee weapon a gila may also bite for 1d4 damage, which also delivers a painful but otherwise mild toxin. Those affected must save vs. Poison or suffer an additional 1d3 damage and a -1 attack/damage penalty for the next four hours. Multiple bites will cause additional damage and the penalties are cumulative. While the toxin is active rest is impossible, as are activities that require rest or inactivity (such as spell memorization). A **neutralize poison** spell will nullify this lingering effect.

One out of every five gila will be a warrior of 3-3 HD (145 XP) and even tougher skin (AC 16). Regular gila gain a +1 bonus to their morale if they are led by a warrior. In a gila village, one out of every ten will be a chieftain of 5-5 HD (360 XP) with AC 17 and a +1 bonus to damage due to strength.
