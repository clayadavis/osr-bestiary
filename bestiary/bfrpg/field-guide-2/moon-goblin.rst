.. title: Moon Goblin
.. slug: moon-goblin
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Moon Goblin
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 16                     |
+-----------------+------------------------+
| Hit Dice:       | 2                      |
+-----------------+------------------------+
| No. of Attacks: | 1 weapon               |
+-----------------+------------------------+
| Damage:         | 1d6 or by weapon       |
+-----------------+------------------------+
| Movement:       | 30'                    |
+-----------------+------------------------+
| No. Appearing:  | 2d10 Wild, 4d8x10 Lair |
+-----------------+------------------------+
| Save As:        | Fighter: 2             |
+-----------------+------------------------+
| Morale:         | 8                      |
+-----------------+------------------------+
| Treasure Type:  | E                      |
+-----------------+------------------------+
| XP:             | 75                     |
+-----------------+------------------------+

The **Moon Goblins** are a tribe of unusually well-organized mountain-dwelling goblinoids. Unlike other goblins, they show an affinity for metalwork and mining. In addition, they are more intelligent and cunning than their lowland kin. Featuring a more blue-green coloration, moon goblins stand a foot or so taller, with a stockier and comparatively more muscular frame.

Due to their better understanding of metal work and mining, moon goblins warriors are very well-equipped, normally wearing full chainmail hauberks covered by tabards emblazoned with an image of a lunar eclipse. They can and will wield any weapon available to Dwarves but show a particular fondness for heavy crossbows and throwing spears. Moon goblins share the Halfling ability to hide in outdoor areas. The largest war bands have even been reported to construct wood and stone fortresses.

For every group of eight moon goblin warriors there is a better-armed and equipped captain who wears platemail (AC 18) and has 4 HD. They tend to wield great weapons such as great axes and mauls. Sometimes accompanying a band of moon goblins are the trappers, who stand a good distance away from the rest of the group, setting devious traps and scouting for the captains. Trappers can set traps very quickly using tripwire-activated nets, hidden spikes, and many other devious contraptions to hinder those in the path of the main group. They are less robust than the warriors of the tribe (1 HD) and have a lower AC (14), but move faster (40') and exclusively attack from range with short bows, occasionally using poisoned arrows. Trappers save as Fighter: 2.

In groups of 20 or more moon goblins there will be either a moon goblin champion or shaman. The champion (5 HD, AC 20) is an experienced warrior respected by the rest of the group. They are highly skilled warriors but are prone to foolishness, such as attempting to imitate other races' chivalry or honor, on rare occasions challenging enemies to single combat in order to settle disputes. Shamans are the spiritual leaders of the moon goblins, but are less sturdy than the champions (3 HD, AC 16). They make up for this with their ability to cast spells as a 6th-level Cleric. The shamans wear ornate armor often made of the bones of enemies and decorated with bloody trophies.

One in every 50-100 moon goblins is a wyvern rider. The bravest of the champions journey up high into the mountain peaks to attempt to tame a wyvern. These powerful moon goblins are perhaps the sturdiest examples of their kind (8 HD, AC 22). They wear piecemeal plate armor, made with sections of their enemies' equipment, most commonly the superior craftsmanship of the Dwarves. Moon goblins led by a wyvern rider gain a +2 bonus to morale checks. The wyvern rider's trusty if unpredictable mounts allow them to surprise unwary travelers and ambush enemies at a moment's notice.

The leaders of the moon goblins are known to be their mysterious shaman-kings. Legendary warriors amongst their people and wielders of shamanistic power, they are a very formidable opponent to all but the most prepared (12 HD, AC 22). They appear leading bands of no less than 200 moon goblins, and can cast spells as a 10th-level Cleric. The shaman-kings are always accompanied by an honor guard of 1d4+1 moon goblin champions, and when led by one all moon goblins gain a morale of 12 until the shaman-king is killed or routed.
