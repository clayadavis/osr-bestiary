.. title: Cockhorse
.. slug: cockhorse
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Cockhorse
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 15                 |
+-----------------+--------------------+
| Hit Dice:       | 4                  |
+-----------------+--------------------+
| No. of Attacks: | 2 claw/1 peck      |
+-----------------+--------------------+
| Damage:         | 1d6/1d6/1d8        |
+-----------------+--------------------+
| Movement:       | 60' (10')          |
+-----------------+--------------------+
| No. Appearing:  | Wild 3d6, Lair 4d8 |
+-----------------+--------------------+
| Save As:        | Fighter: 3         |
+-----------------+--------------------+
| Morale:         | 10                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 280                |
+-----------------+--------------------+

A **Cockhorse** is a domesticated creature that is a magical combination of a horse and chicken. It has the body and front legs of a horse, with the back legs, tail, and head of a chicken. Due to being half-chicken these creatures are not very bright and overly trusting. It will only harm others if attacked. It can carry 400 pounds as a light load and 750 as a heavy load.
