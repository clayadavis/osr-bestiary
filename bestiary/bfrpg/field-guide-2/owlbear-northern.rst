.. title: Owlbear, Northern
.. slug: owlbear-northern
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Owlbear, Northern
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 16                     |
+-----------------+------------------------+
| Hit Dice:       | 8                      |
+-----------------+------------------------+
| No. of Attacks: | 2 claws/1 bite + 1 hug |
+-----------------+------------------------+
| Damage:         | 1d8/1d8/1d10 + 2d8     |
+-----------------+------------------------+
| Movement:       | 40' Fly 60'            |
+-----------------+------------------------+
| No. Appearing:  | 1, Wild 1d2, Lair 1d4  |
+-----------------+------------------------+
| Save As:        | Fighter: 8             |
+-----------------+------------------------+
| Morale:         | 9                      |
+-----------------+------------------------+
| Treasure Type:  | C                      |
+-----------------+------------------------+
| XP:             | 875                    |
+-----------------+------------------------+

In a similar fashion to the standard owlbear, a **Northern Owlbear** mixes the qualities of a polar bear and snow owl. It tends to be substantially larger than the southern climate owlbear. On rare occasions a northern owlbear is hatched with large feathered wings.

A northern owlbear fights much like a bear but is more aggressive. Also like a normal bear a northern owlbear must hit with both claws in order to deal the listed “hug” damage. As a polar climate inhabitant it is essentially immune to cold environmental effects and even extreme or magical cold causes half-damage. If a save is involved with such a cold-based effect, it receives a +4 bonus.
