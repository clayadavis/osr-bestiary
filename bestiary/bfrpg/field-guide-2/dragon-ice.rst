.. title: Dragon, Ice
.. slug: dragon-ice
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dragon, Ice
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 20                            |
+-----------------+-------------------------------+
| Hit Dice:       | 14 (+10)                      |
+-----------------+-------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/tail |
+-----------------+-------------------------------+
| Damage:         | 2d4/2d4/6d6 or 14d8/2d6       |
+-----------------+-------------------------------+
| Movement:       | 30' Fly 80'                   |
+-----------------+-------------------------------+
| No. Appearing:  | 1d2                           |
+-----------------+-------------------------------+
| Save As:        | Fighter: 12                   |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | G, H                          |
+-----------------+-------------------------------+
| XP:             | 5,650                         |
+-----------------+-------------------------------+

An exceedingly rare breed of dragon from the frozen north, the **Ice Dragon** is clad in extremely thick and almost transparent scales, colored like lightly-frosted glass that is paler on the stomach and chest. It is around 15' to 20' tall and 80' to 120' long, with a wingspan of 200'. An ice dragon is notoriously spiky, developing clear, horn-like growths across the length of its body as well as long, serrated claws. An ice dragon is deviously intelligent and is usually neutral in outlook, although prone to disdain of the lesser races, having been described as cold and calculating by those that have survived encounters with it.

An ice dragon prefers to stalk its quarry for lengthy amounts of time, hunting humanoids for entertainment and often deliberately leaving dead beasts such as griffons and bears in its path. It subconsciously alters the local weather, causing freak snowstorms and blizzards wherever it ranges. While walking on snow, it leaves no footprints. It can also disguise itself completely by burrowing underneath, often using this to ambush its prey, surprising on 1-5 on 1d6.

An ice dragon is also an adept spell caster, able to cast spells of up to 5th level, similar to an age category 6 gold dragon. In addition to this, once per encounter, it can project an illusion of itself to a distance up to 400'; it can also replicate sound, smell, and touch. The illusion will disappear after its target takes over 30 points of damage.

A young ice dragon has never been sighted, leading some to believe that they are extremely ancient white dragons or perhaps a dying species, which would explain the rarity of sightings. It is also believed that it hibernates for very large amounts of time, sometimes even hundreds of years, under ice sheets and inside of glaciers.
