.. title: Infernal, Hezrou*
.. slug: infernal-hezrou
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Infernal, Hezrou*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 22‡                     |
+-----------------+-------------------------+
| Hit Dice:       | 10** (AB +9)            |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws/1 bite or spell |
+-----------------+-------------------------+
| Damage:         | 1d4/1d4/4d4             |
+-----------------+-------------------------+
| Movement:       | 30'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4                     |
+-----------------+-------------------------+
| Save As:        | Fighter: 10             |
+-----------------+-------------------------+
| Morale:         | 11                      |
+-----------------+-------------------------+
| Treasure Type:  | C                       |
+-----------------+-------------------------+
| XP:             | 1,480                   |
+-----------------+-------------------------+

Cruel and somewhat stupid, a **Hezrou** is a demonic troglodyte-like humanoid slightly more powerful than a vrock (see **Basic Fantasy Field Guide volume 1**). A hezrou enjoys melee combat and will eagerly press an attack deep into the heart of enemy forces.

Like troglodytes, a hezrou secretes a smelly oil that keeps its scaly skin supple. All mammals (including the standard character races) find the scent repulsive, and those within 10’ of the hezrou must make a savings throw vs. Poison. Those failing the save suffer a -2 penalty to attack rolls while they remain within range of the hezrou. Getting out of range negates the penalty, but renewed exposure reinstates it. The result of the original save lasts a full 24 hours, after which a new save must be rolled.

Hezrou can freely communicate telepathically, and have Darkvision to 60’. Each can **teleport** at-will (as a 10th-level Magic-User). Additionally, a hezrou can **cause fear** (this effect is identical to that produced by a **Wand of Fear**) as its action for a round of combat. Once per day a hezrou has a 25% chance of summoning another hezrou.
