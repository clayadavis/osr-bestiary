.. title: Dracomander
.. slug: dracomander
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dracomander
.. type: text
.. image:

Dracomanders are weird monsters combining features of dragons and salamanders. They conform to the rules provided for true dragons in most cases (i.e. they have a breath weapon, age categories, may or may not speak, may or may not cast spells, and so on); exceptions will be noted below. Despite its dragon-like appearance, a dracomander also possesses the elemental powers of salamanders, in particular their area effect damage and resistance to non-magical weapons.

In general, a dracomander will resemble the "normal" salamander of the same type with the addition of dragon wings. It is not known whether dracomanders are actually natural creatures from the Elemental Planes, or if they are in fact the result of magical hybridization.

Dracomanders are known to associate with normal salamanders of the same sort, though they will not generally share lairs with them due to greed (i.e. allowing another intelligent monster in the lair is risking the theft of some of the dracomander's treasure). Likewise, dracomanders are known to hate both salamanders and dracomanders of other types.
