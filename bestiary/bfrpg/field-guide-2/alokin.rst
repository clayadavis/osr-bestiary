.. title: Alokin
.. slug: alokin
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Alokin
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 13                    |
+-----------------+-----------------------+
| Hit Dice:       | 2*                    |
+-----------------+-----------------------+
| No. of Attacks: | 1 fist or beam weapon |
+-----------------+-----------------------+
| Damage:         | 1d3 or 1d8            |
+-----------------+-----------------------+
| Movement:       | 90' (30')             |
+-----------------+-----------------------+
| No. Appearing:  | 1d6                   |
+-----------------+-----------------------+
| Save As:        | Fighter: 2            |
+-----------------+-----------------------+
| Morale:         | 7                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 100                   |
+-----------------+-----------------------+

An **Alokin** is an alien creature that stands about 4 feet tall and has rubbery reddish skin. Its thick skin helps absorb blows, which gives it a natural armor class of 13. It prefers to use a beam weapon that does 1d8 points of damage. If captured, this weapon may be used by player characters, but with a -2 penalty to a ranged attack roll. The captured weapon will have 2d6 charges remaining.

An alokin has the ability to stretch one of its arms to a length of 10 feet; this counts as its movement action for that round. It grants a +2 bonus to hit on a single melee attack the first time it uses this ability during an encounter.
