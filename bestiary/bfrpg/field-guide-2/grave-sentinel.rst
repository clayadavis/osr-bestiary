.. title: Grave Sentinel
.. slug: grave-sentinel
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Grave Sentinel
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 20                  |
+-----------------+---------------------+
| Hit Dice:       | 3**                 |
+-----------------+---------------------+
| No. of Attacks: | 1 weapon            |
+-----------------+---------------------+
| Damage:         | By weapon           |
+-----------------+---------------------+
| Movement:       | 30'                 |
+-----------------+---------------------+
| No. Appearing:  | Wild 3d8, Lair 8d10 |
+-----------------+---------------------+
| Save As:        | Fighter: 3          |
+-----------------+---------------------+
| Morale:         | 12                  |
+-----------------+---------------------+
| Treasure Type:  | U, Lair G and H     |
+-----------------+---------------------+
| XP:             | 235                 |
+-----------------+---------------------+

A grim presence in thick mist and on stormy nights, a **Grave Sentinel** prowls in the darkness around its final resting place, dutiful in its eternal watch, eerily silent, and implacable in combat. It is the personal retinues of mighty warlords and great kings from times long-forgotten. It is a warrior who fell in battle alongside its leader and now stands watch over its charge. As a result of this it inhabits ancient battlefields, graveyards, tombs, burial mounds, and catacombs, rarely seen elsewhere. It appears in smooth black plate armor with subtle gold and silver etchings, denoting the rank and deeds of the wearer, and a nearly featureless helmet that has a completely smooth face plate, as the dead do not require eyes to see. It most commonly wields a sword and shield, but can theoretically wield any weapon reasonably available. It possesses the ability to pass through any material less than 40' thick, with the exception of lead and copper. They are Turned as a 5 HD undead creature, and can also become incorporeal for one round, preventing it from causing and receiving damage, and causing it to appear as a being of smoke, with flashes of lightning swirling inside. Once per encounter it can **teleport** 10' in any direction.

One in every ten grave sentinels is a grave watcher (610 XP) with 6 HD and high Intelligence. A Grave Watcher is accompanied by a group of ten grave sentinels and directs them in combat to attack the weakest or most dangerous enemies, such as Magic-Users and Clerics, before surrounding others. If the grave watcher is killed, the morale of the remaining sentinels suffers a penalty of -3, and if the check is failed they will retreat to the nearest watcher within 200' or become mindless and attack the nearest available target, be it friend or foe.

One in every 50 grave sentinels is a grave guardian (2,730 XP) with 14 HD and 22 AC. This towering warrior, normally between 7’ and 8’ tall, wears scratched and tarnished but otherwise featureless armor of burnished gold that glows a subtle white and emits a mist that carpets the floor of any room it occupies. It is often the corpse of a fallen champion or king, a warrior of unparalleled ability in its time, now long past. It is a cunning and capable commander and will make short work of an unprepared or unorganized force. A grave guardian can cast **haste** as an 8th-level Magic-User once per day, and is always accompanied by a personal guard of two grave watchers. It normally wields a magical great sword or great axe and can deal 4d6 damage per hit at a rate of 3/2 attacks per round. It can also create new grave sentinels from Fighters of level 3 or above. This process takes four days and can be used to return slain sentinels to activity.
