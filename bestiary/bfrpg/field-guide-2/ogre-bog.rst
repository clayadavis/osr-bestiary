.. title: Ogre, Bog
.. slug: ogre-bog
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Ogre, Bog
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 14                |
+-----------------+-------------------+
| Hit Dice:       | 4+1               |
+-----------------+-------------------+
| No. of Attacks: | 2 fists + swallow |
+-----------------+-------------------+
| Damage:         | 1d6/1d6 + special |
+-----------------+-------------------+
| Movement:       | 30’               |
+-----------------+-------------------+
| No. Appearing:  | 1d6, Lair 2d6     |
+-----------------+-------------------+
| Save As:        | Fighter: 4        |
+-----------------+-------------------+
| Morale:         | 10                |
+-----------------+-------------------+
| Treasure Type:  | D + 2d10x100 gp   |
+-----------------+-------------------+
| XP:             | 240               |
+-----------------+-------------------+

A **Bog Ogre** is a magical crossbreed between an ogre and a wug (see the **Basic Fantasy Field Guide volume 1**). Believed to have been created by some demented wizard in ages past, a bog ogre looks like a standard ogre in most respects, but has warty green skin, a grotesquely-wide mouth, and generally frog-like features. Each is 8’ to 10’ tall and weighs anywhere from 400 to 500 pounds. Like a common ogre, a bog ogre tends to carry large sacks containing acquired treasures.

Due to its powerful stature, a bog ogre attacks with its massive fists. If both attacks hit and the target is Halfling-sized or smaller, the bog ogre will attempt to stuff the hapless victim into its frog-like maw and swallow it whole. The target may make a saving throw vs. Death Ray. If successful, the victim has managed to break free of the bog ogre's grip and has avoided being eaten. If the save fails, the target is swallowed whole and suffers 1d6 points of damage each round until killed or cut free of the bog ogre's stomach (the stomach is sufficiently cramped that swallowed characters may not attack from within).
