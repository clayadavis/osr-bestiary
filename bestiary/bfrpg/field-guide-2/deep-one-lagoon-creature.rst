.. title: Deep One, Lagoon Creature
.. slug: deep-one-lagoon-creature
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Deep One, Lagoon Creature
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 18             |
+-----------------+----------------+
| Hit Dice:       | 5+5            |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 2d4/2d4/1d6    |
+-----------------+----------------+
| Movement:       | 20' Swim 30'   |
+-----------------+----------------+
| No. Appearing:  | 1d4            |
+-----------------+----------------+
| Save As:        | Fighter: 5     |
+-----------------+----------------+
| Morale:         | 9              |
+-----------------+----------------+
| Treasure Type:  | A              |
+-----------------+----------------+
| XP:             | 360            |
+-----------------+----------------+

A **Lagoon Creature** is a form of the hybrid deep one that has branched away from the other coastal water-dwelling deep ones, instead preferring deep, dark, and stagnant swampy regions. It is a large humanoid with a scaly body, almost like carapace. Its extremities are broad and fin-like with sharp clawed ends, and its mouth contains razor-sharp gar-like teeth. Like other deep ones, its huge unblinking eyes gives it Darkvision to 60' and superior eyesight while underwater. A lagoon creature is less intelligent than most other deep ones.

While lagoon creatures are usually solitary or found in very small groups, deep one masters may call upon them as bodyguards or shock troops when necessary.
