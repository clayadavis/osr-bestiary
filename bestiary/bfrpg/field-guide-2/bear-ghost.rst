.. title: Bear, Ghost*
.. slug: bear-ghost
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bear, Ghost*
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 16‡                   |
+-----------------+-----------------------+
| Hit Dice:       | 9*                    |
+-----------------+-----------------------+
| No. of Attacks: | 2 claws/1 bite + hug  |
+-----------------+-----------------------+
| Damage:         | 1d6/1d6/1d6 + 2d6     |
+-----------------+-----------------------+
| Movement:       | 40'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1, Wild 1d4, Lair 1d4 |
+-----------------+-----------------------+
| Save As:        | Fighter: 9            |
+-----------------+-----------------------+
| Morale:         | 8                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 1,150                 |
+-----------------+-----------------------+

While the term ghost-bear (or spirit-bear) usually refers to rare black bears with white coats, the **Ghost Bear** is actually an undead semi-corporeal bear. When a ghost bear roars it **causes fear** in similar fashion as the reverse of the spell **remove fear**, except that it causes all creatures within 120' to become frightened; those that fail to save vs. Spells will flee for 2 turns. Creatures with 6 or more hit dice are immune to this effect. The ghost bear will roar every 1d4 rounds in addition to any standard attacks.

The ghost bear confronts opponents in normal bear fashion with claws and bites. Upon scoring a hug attack the target is also drained of 1d3 points of Constitution. Elves and Dwarves (and other long-lived creatures such as dragons) are allowed a savings throw vs. Death Ray to resist this effect, which must be rolled each time a hug attack occurs. Characters who lose Constitution appear to have aged. If a ghost bear is fighting a living creature which does not have a Constitution score, the GM should assign whatever score he or she sees fit.

Unlike the Constitution loss caused by an actual ghost, the lost points are temporary and can be slowly healed in the manner described in the **Encounter** section of the **Basic Fantasy RPG Core Rules**. However, if a victim's Constitution is reduced to zero, he or she dies and returns as a ghost by the next nightfall.

A ghost bear can only be hit by magical weapons or spells. As with all undead, they can be Turned by a Cleric (as a vampire), and are immune to **sleep**, **charm** or **hold** spells.
