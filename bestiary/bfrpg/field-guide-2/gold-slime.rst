.. title: Gold Slime
.. slug: gold-slime
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Gold Slime
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 12         |
+-----------------+------------+
| Hit Dice:       | 3*         |
+-----------------+------------+
| No. of Attacks: | 1 dissolve |
+-----------------+------------+
| Damage:         | 3d4        |
+-----------------+------------+
| Movement:       | 1'         |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | Special    |
+-----------------+------------+
| XP:             | 175        |
+-----------------+------------+

A **Gold Slime** is an oozing blob of gelatinous golden slime, about 3’ in diameter, which lives by devouring gold. It can sense the presence and general direction of any gold within a mile of itself.

A gold slime will attack anyone carrying gold and will attempt to envelope their limbs, dissolving the victim with a strong acid. Once it neutralizes the victim, the gold slime will devour any gold the victim had.

While a gold slime does not carry any treasure, all of the gold it has devoured is still present, dissolved in the slime’s body. This gold can be extracted by boiling off the slime, which can be done over a hot fire. The amount of gold is left up to the GM, but should include any gold that the party knows to have been dissolved.
