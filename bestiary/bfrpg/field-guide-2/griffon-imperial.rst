.. title: Griffon, Imperial
.. slug: griffon-imperial
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Griffon, Imperial
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 19                       |
+-----------------+--------------------------+
| Hit Dice:       | 9                        |
+-----------------+--------------------------+
| No. of Attacks: | 2 claws/1 bite           |
+-----------------+--------------------------+
| Damage:         | 2d4/2d4/3d6              |
+-----------------+--------------------------+
| Movement:       | 40' (10') Fly 120' (10') |
+-----------------+--------------------------+
| No. Appearing:  | 1, Lair 1d4              |
+-----------------+--------------------------+
| Save As:        | Fighter: 9               |
+-----------------+--------------------------+
| Morale:         | 10                       |
+-----------------+--------------------------+
| Treasure Type:  | F                        |
+-----------------+--------------------------+
| XP:             | 1,075                    |
+-----------------+--------------------------+

The **Imperial Griffon** is a rare and majestic creature resembling a tiger with the head, foreclaws, and wings of an eagle. From nose to tail an adult imperial griffon can measure as much as 15’ long and is a fearsome sight to behold. Depending on the climate it is found in, an imperial griffon can be colored anywhere from lighter browns to white as snow. Broad, muscular, feathered wings emerge from the creature’s back and can span around 40’ or more. A mature imperial griffon weighs around 900 pounds.

An imperial griffon will nest in caves, cliff faces, and mountains, either in extremely cold climates or very humid ones. It swoops from the skies to prey on pretty much anything smaller or weaker than itself. Imperial griffons have a particular taste for horses, favoring them over all other prey. While small family groups may nest near each other, they are otherwise solitary animals, tending to hunt alone.

Despite the difficulty of keeping such dangerous creatures with voracious appetites, some have trained imperial griffons as loyal and hardy mounts. As a mount, an imperial griffon can carry up to 700 pounds normally, or 1,400 pounds when heavily loaded.
