.. title: Thunderbird
.. slug: thunderbird
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Thunderbird
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 17                         |
+-----------------+----------------------------+
| Hit Dice:       | 5*                         |
+-----------------+----------------------------+
| No. of Attacks: | 2 claws/1 bite + lightning |
+-----------------+----------------------------+
| Damage:         | 1d6/1d6/1d8 + 5d6          |
+-----------------+----------------------------+
| Movement:       | 10' Fly 90'                |
+-----------------+----------------------------+
| No. Appearing:  | 1                          |
+-----------------+----------------------------+
| Save As:        | Fighter: 5                 |
+-----------------+----------------------------+
| Morale:         | 9                          |
+-----------------+----------------------------+
| Treasure Type:  | None                       |
+-----------------+----------------------------+
| XP:             | 405                        |
+-----------------+----------------------------+

As rare as giant eagles are, a **Thunderbird** is even more rare. Equally large as giant eagles, it is about 10’ tall with a 20’ wingspan and has coloration that includes various hues of electric-blue. The very air around a thunderbird crackles and sparks with static electricity. A thunderbird is intelligent and often speaks Common along with any local languages.

A thunderbird typically attacks from a great height, diving earthward at tremendous speed (use charging rules). In addition to the normal attack routine, while diving a bolt of lightning trails the thunderbird, striking its target for 5d6 points of electrical damage. Those within 10’ of the target take half that damage. The target and those nearby get a save vs. Dragon Breath for half the received damage (i.e. half or one-quarter damage respectively). When it cannot dive, it uses its powerful talons and slashing beak to strike at its target’s head and eyes. Anyone striking a thunderbird with a metallic weapon will take 1d6 points of electrical damage in return (no save). A thunderbird is completely immune to any electrical-based damage.
