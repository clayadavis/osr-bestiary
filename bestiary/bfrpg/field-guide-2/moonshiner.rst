.. title: Moonshiner
.. slug: moonshiner
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Moonshiner
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 14             |
+-----------------+----------------+
| Hit Dice:       | 2*             |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d4/1d4/1d8    |
+-----------------+----------------+
| Movement:       | 60'            |
+-----------------+----------------+
| No. Appearing:  | 3d4, Lair 8d6  |
+-----------------+----------------+
| Save As:        | Fighter: 2     |
+-----------------+----------------+
| Morale:         | 6              |
+-----------------+----------------+
| Treasure Type:  | K in lair      |
+-----------------+----------------+
| XP:             | 100            |
+-----------------+----------------+

A **Moonshiner** is a six-legged excessively-muscled hound with an over-sized jaw and a penchant for a stiff drink, often found roaming Dwarven distilleries.

Its saliva has powerful intoxicating effects, such that anyone bitten suffers a penalty of -2 on attacks and saving throws, while also receiving a +2 bonus to morale checks.

The moonshiners will attack when they have the numerical advantage, attempting to rush at their foes with little regard to personal safety. A morale check is appropriate when they no longer outnumber their foes.
