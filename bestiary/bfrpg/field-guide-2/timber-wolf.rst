.. title: Timber-Wolf*
.. slug: timber-wolf
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Timber-Wolf*
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 24‡                 |
+-----------------+---------------------+
| Hit Dice:       | 18* (AB +12)        |
+-----------------+---------------------+
| No. of Attacks: | 1 bite or 1 trample |
+-----------------+---------------------+
| Damage:         | 3d8 or 4d10         |
+-----------------+---------------------+
| Movement:       | 40' (10')           |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Fighter: 18         |
+-----------------+---------------------+
| Morale:         | 11                  |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 4,160               |
+-----------------+---------------------+

A **Timber-Wolf** is a manifestation of nature in a state of rage. It is composed of rough-cut or broken logs, branches, and leaves in the shape of a gigantic canine. A timber-wolf attacks by biting or trampling; in order to trample it must first move at least 10’. A timber-wolf can even attack structures like a battering ram, inflicting damage equal to its trample attack.

Magical weapons or spells are required to damage a timber-wolf. As it consists entirely of wood, it takes double damage from magical fire attacks; ordinary fire, however, does only normal damage. A timber-wolf has a kind of basic intelligence, albeit in a state of rage. Exactly what brings forth a timber-wolf is not known, but attacks on a forest (such as excessive logging or similar devastation) may be the cause. When one appears, it's primary motive is to destroy or drive out whoever (or whatever) threatens its forest.
