.. title: Tusker (Eyvique)
.. slug: tusker-eyvique
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Tusker (Eyvique)
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 14                         |
+-----------------+----------------------------+
| Hit Dice:       | 2+2                        |
+-----------------+----------------------------+
| No. of Attacks: | 1 weapon                   |
+-----------------+----------------------------+
| Damage:         | 1d6 or by weapon           |
+-----------------+----------------------------+
| Movement:       | 30' Unarmored 40' Swim 30' |
+-----------------+----------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8    |
+-----------------+----------------------------+
| Save As:        | Fighter: 2                 |
+-----------------+----------------------------+
| Morale:         | 8                          |
+-----------------+----------------------------+
| Treasure Type:  | Q, R each; D, K in lair    |
+-----------------+----------------------------+
| XP:             | 75                         |
+-----------------+----------------------------+

Known as **Eyvique** (eye-vick) in their native tongue, **Tuskers** are stout humanoids living in coastal regions. Similar in stature to Dwarves, tuskers live simple lives fishing and whaling. As its name suggests it has a pair of long downward-pointing tusks much like a walrus. A tusker is thick-skinned and has ample fat reserves, giving it protection from the effects of a cold environment. Even extreme or magical cold causes only half-damage. If a save is involved with such a cold-based effect, a tusker receives a +4 bonus. Any tusker involved in grappling attacks does +2 damage due to its tusks, but otherwise a tusker does not attack directly with the tusks.

One out of every five tuskers will be a warrior of 4+4 hit dice (240 XP) and even tougher skin (AC 16). Regular tuskers gain a +1 bonus to their morale if they are led by a warrior. In tusker villages, one will be a chieftain of 8+8 hit dice (875 XP) with AC 18 and a +2 bonus to damage due to strength.
