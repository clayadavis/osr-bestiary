.. title: Geminate Serpent, Green
.. slug: geminate-serpent-green
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Geminate Serpent, Green
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 21                               |
+-----------------+----------------------------------+
| Hit Dice:       | 10**                             |
+-----------------+----------------------------------+
| No. of Attacks: | 2 bite or 1 breath or constrict* |
+-----------------+----------------------------------+
| Damage:         | 2d10/2d10 or breath or 2d8*      |
+-----------------+----------------------------------+
| Movement:       | 30'                              |
+-----------------+----------------------------------+
| No. Appearing:  | Lair 1d3+1                       |
+-----------------+----------------------------------+
| Save As:        | Fighter: 10                      |
+-----------------+----------------------------------+
| Morale:         | 8                                |
+-----------------+----------------------------------+
| Treasure Type:  | H                                |
+-----------------+----------------------------------+
| XP:             | 1,300                            |
+-----------------+----------------------------------+

A **Green Geminate Serpent** is considered the most cowardly of all the geminate serpents. They live in swamps, marshes, and dense forests. Eating mostly deer and other similar-sized creatures, they are scarcely seen by people. If a green geminate serpent comes across or is attacked by a group of adventurers it will throw up a smokescreen and imitate a dragon until it has agreed upon a deal to get rid of the adventurers, scared the group away, or put the group to sleep. If the green geminate serpent thinks that the adventurers will reveal the location of the lair, the group will be attacked.

**Green Geminate Serpent Age Table**

+---------------+-----+-----+-----+------+------+------+------+
| Age Category  | 1   | 2   | 3   | 4    | 5    | 6    | 7    |
+---------------+-----+-----+-----+------+------+------+------+
| Length        | 30’ | 40’ | 50’ | 60’  | 80’  | 100’ | 130’ |
+---------------+-----+-----+-----+------+------+------+------+
| Hit Dice      | 6   | 7   | 8   | 9    | 10   | 11   | 12   |
+---------------+-----+-----+-----+------+------+------+------+
| Attack Bonus  | +6  | +7  | +8  | +8   | +9   | +9   | +10  |
+---------------+-----+-----+-----+------+------+------+------+
| Breath Weapon | Sleep Gas (Cloud)                           |
+---------------+-----+-----+-----+------+------+------+------+
| Length        | -   | 80' | 90' | 100' | 110' | 120' | 130' |
+---------------+-----+-----+-----+------+------+------+------+
| Width         | -   | 30' | 30' | 35'  | 40'  | 50'  | 60'  |
+---------------+-----+-----+-----+------+------+------+------+
| Bite          | 2d6 | 2d8 | 3d6 | 2d10 | 2d10 | 2d12 | 2d12 |
+---------------+-----+-----+-----+------+------+------+------+
| Constrict     | 1d6 | 1d8 | 2d4 | 2d6  | 2d8  | 3d6  | 5d4  |
+---------------+-----+-----+-----+------+------+------+------+
| Talk          | -   | 10% | 20% | 40%  | 60%  | 70%  | 75%  |
+---------------+-----+-----+-----+------+------+------+------+
