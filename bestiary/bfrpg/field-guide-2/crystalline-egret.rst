.. title: Crystalline Egret
.. slug: crystalline-egret
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Crystalline Egret
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 17                         |
+-----------------+----------------------------+
| Hit Dice:       | 5**                        |
+-----------------+----------------------------+
| No. of Attacks: | 2 claws/1 bite or 1 breath |
+-----------------+----------------------------+
| Damage:         | 1d6/1d6/1d8, or 2d8        |
+-----------------+----------------------------+
| Movement:       | 10' Fly 70'                |
+-----------------+----------------------------+
| No. Appearing:  | 1d2                        |
+-----------------+----------------------------+
| Save As:        | Fighter: 5                 |
+-----------------+----------------------------+
| Morale:         | 12                         |
+-----------------+----------------------------+
| Treasure Type:  | 1 diamond (see below)      |
+-----------------+----------------------------+
| XP:             | 450                        |
+-----------------+----------------------------+

A **Crystalline Egret** is a 5’ tall white egret made entirely of ice. Due to the its pure white color it can surprise on 1-4 on 1d6 in snowy or foggy conditions. Each crystalline egret has a diamond in its head as a focus for the animating magic; this gem is worth 300 gp. Once every 1d4+1 rounds the crystalline egret is capable of “breathing” shards of ice at a single opponent up to 10’ away, instead of performing its normal attacks. These icy shards do 2d8 points of damage, with a successful save vs. Dragon Breath reducing damage by half. This is physical damage from the sharp shards of ice, rather than cold damage, so resistance to cold of any sort provides no protection.

The egret will begin to melt in temperatures above freezing. The egret takes an extra 1d6 damage from attacks involving fire or heat.
