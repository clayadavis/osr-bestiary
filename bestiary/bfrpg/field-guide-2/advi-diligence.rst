.. title: Advi, Diligence*
.. slug: advi-diligence
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Advi, Diligence*
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 26          |
+-----------------+-------------+
| Hit Dice:       | 12          |
+-----------------+-------------+
| No. of Attacks: | 4 tools     |
+-----------------+-------------+
| Damage:         | 1d4 each    |
+-----------------+-------------+
| Movement:       | 30'         |
+-----------------+-------------+
| No. Appearing:  | 1d4         |
+-----------------+-------------+
| Save As:        | Fighter: 12 |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 1,875       |
+-----------------+-------------+

A **Diligence Advi** is a 1-foot diameter sphere of silvery metal with eight appendages, four of them carrying small work tools (hammer, saw, etc.). It will use these tools to slowly chip away at its foes.
