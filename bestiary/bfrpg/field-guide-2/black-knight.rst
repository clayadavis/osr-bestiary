.. title: Black Knight
.. slug: black-knight
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Black Knight
.. type: text
.. image:

+-----------------+-----------------------------------------+
| Armor Class:    | 20                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 14** (+11, plus sword's magic bonus)    |
+-----------------+-----------------------------------------+
| No. of Attacks: | 1 weapon                                |
+-----------------+-----------------------------------------+
| Damage:         | by weapon + 3, plus sword's magic bonus |
+-----------------+-----------------------------------------+
| Movement:       | 30'                                     |
+-----------------+-----------------------------------------+
| No. Appearing:  | 1                                       |
+-----------------+-----------------------------------------+
| Save As:        | Fighter: 14                             |
+-----------------+-----------------------------------------+
| Morale:         | 11                                      |
+-----------------+-----------------------------------------+
| Treasure Type:  | F + Black Knight's sword                |
+-----------------+-----------------------------------------+
| XP:             | 1,480                                   |
+-----------------+-----------------------------------------+

A **Black Knight** was once a heroic warrior, but has been cursed by the gods for committing some unforgivable crime such as murdering a loved one or betraying a close friend for selfish purposes. It resembles a charred skeleton with burning coals in place of eyes. A black knight is dressed in the same armor it wore in life, which appears to have been blackened by fire. It will sometimes don tattered cloaks with deep hoods to disguise its appearance. It is sometimes served by other undead who act as retainers and aides. A black knight's voice is chilling and echoes hollowly. It speaks the languages it knew in life.

A black knight is as capable a fighter in death as it was in life, and retains the honor and pride that it once held. A black knight will never ambush an enemy or attack from behind, and will refuse to attack until an enemy has readied his or her weapon. It occasionally shows mercy to honorable opponents and individuals who remind it of people it knew in life. A black knight will be found wielding a magic sword 80% of the time. To determine what type of sword the knight is wielding, roll 1d6 and refer to the table below.

**Black Knight's Sword Table**

+---------+---------------------------------+
| **1d6** | **Type of Sword**               |
+---------+---------------------------------+
| 1       | Longsword +3                    |
+---------+---------------------------------+
| 2       | Two-handed Sword +3             |
+---------+---------------------------------+
| 3       | Shortsword +2, Charm Person     |
+---------+---------------------------------+
| 4       | Two-handed Sword +4             |
+---------+---------------------------------+
| 5       | Longsword +2, Flames on Command |
+---------+---------------------------------+
| 6       | Shortsword +1, Energy Drain*    |
+---------+---------------------------------+

** While the black knight is wielding this sword, the weapon has no limit to the number of levels it can drain.*

Black knights are very strong, having a Strength bonus of +3 on damage die rolls; this is in addition to the magic bonus of the knight's sword, as given above.

A black knight has a number of special abilities. Creatures of 5 HD or less that look upon the knight's grim visage must make a save vs. Spells or be shaken for the duration of the encounter, taking a -2 penalty on attack rolls and Armor Class. The black knight has a 75% chance to reflect any harmful spell that targets it, with reflected spells being targeted back at the caster. A black knight can cast **detect magic** and **detect invisibility** at will. In addition, the knight can also cast **fireball** and **wall of fire** each 3 times per day as a 10th level Magic-User.

A black knight only takes half damage from non-magical weapons, and like all undead is immune to **sleep**, **charm**, and **hold** magic. A black knight cannot be Turned by a Cleric.
