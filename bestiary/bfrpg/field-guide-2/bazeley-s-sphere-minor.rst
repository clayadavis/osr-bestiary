.. title: Bazeley’s Sphere Minor
.. slug: bazeley-s-sphere-minor
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bazeley’s Sphere Minor
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 23                       |
+-----------------+--------------------------+
| Hit Dice:       | 3*                       |
+-----------------+--------------------------+
| No. of Attacks: | 2 bludgeon               |
+-----------------+--------------------------+
| Damage:         | 1d6/1d6                  |
+-----------------+--------------------------+
| Movement:       | 60'                      |
+-----------------+--------------------------+
| No. Appearing:  | 3d10 HD worth            |
+-----------------+--------------------------+
| Save As:        | Fighter: 3               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | 300 gp worth of crystals |
+-----------------+--------------------------+
| XP:             | 175                      |
+-----------------+--------------------------+

The most common form that the spheres take is the **Sphere Minor**. It is a roughly-humanoid form that uses its arm-like appendages to bludgeon its foes. It can move startlingly fast; once per day it can double its movement rate for 3 rounds.

Upon its defeat, the sphere minor will explode into a hail of ordinary spheres, doing 4d6 points of damage to all within 10’ who fail to save vs. Death Ray.
