.. title: Spirit of Vengeance
.. slug: spirit-of-vengeance
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Spirit of Vengeance
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 20                      |
+-----------------+-------------------------+
| Hit Dice:       | 10**                    |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon                |
+-----------------+-------------------------+
| Damage:         | By weapon + 3           |
+-----------------+-------------------------+
| Movement:       | 20’                     |
+-----------------+-------------------------+
| No. Appearing:  | 1                       |
+-----------------+-------------------------+
| Save As:        | Fighter: 10             |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | Weapon + 3, 1d10×100 pp |
+-----------------+-------------------------+
| XP:             | 1,480                   |
+-----------------+-------------------------+

The **Spirit of Vengeance** is a creature made entirely of a dark and shadowy yet tangible mist. It takes the form of a tall heavily-armored and ornate figure wielding a great weapon in its spectral hands. It is wreathed in blue spectral flames and its eyes glow a bright blood red. It can appear differently to the betrayer, playing off of its fears and memories to take a form most terrible to the beholder. An aura of unease surrounds the spirit and those in proximity can experience cold sweats and an unexplained feeling of dread. Other signs of its presence include a sudden drastic drop in temperature and a massive increase in storms and lightning in the area. All creatures 4 HD or under (including constructs and undead) must make a save vs. Death Ray or become afraid and act as if Turned for 1d4+1 rounds.

A spirit of vengeance comes into being in places where a grave betrayal was committed, sometimes even resembling someone who was killed in the event. It then relentlessly pursues the betrayer or their descendant and attempts to enact its vengeance upon them, which is normally related to the manner of betrayal if possible. It is highly intelligent and can sense the thoughts of humanoids in close proximity, as well as being able to possess them just as a ghost does.

Its touch can drain the life force from others and it gains half of the hit points drained as health but otherwise this ability functions just as energy drain.

The spirit of vengeance can be killed in combat but if so it shall rise again upon the next full moon and continue the pursuit of its quarry. The only ways for it to die permanently are either for the pursued to die by its hand or for it to be tricked onto consecrated land (which it will normally avoid at all costs), doused in holy water, and then slain under a full moon.
