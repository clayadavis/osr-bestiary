.. title: Syrinx Juggernaut
.. slug: syrinx-juggernaut
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Syrinx Juggernaut
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 24                   |
+-----------------+----------------------+
| Hit Dice:       | 12                   |
+-----------------+----------------------+
| No. of Attacks: | 6 claws or acid      |
+-----------------+----------------------+
| Damage:         | 2d4 per claw or 6d10 |
+-----------------+----------------------+
| Movement:       | 40'                  |
+-----------------+----------------------+
| No. Appearing:  | Lair 1               |
+-----------------+----------------------+
| Save As:        | Fighter: 12          |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 2,075                |
+-----------------+----------------------+

Somewhat different in appearance to a regular syrinx, a **Syrinx Juggernaut** stands 24’ above the ground, most of that due to its twelve extremely long-bladed limbs. Its body is made up of six interlocking segmented sections plated in thick black and reflective chitin, covered in hundreds of milky white eyes.

Its insides are virtually identical to a regular syrinx, but larger and more spread out, with the exception of an organ that stretches almost the entire 25' long body, constructed from the same substance that coats the walls of its lair. This organ seemingly produces and stores a viscous and sticky acid that can be expelled from its mouth in a 60' long cone. The syrinx juggernaut can do this once every three rounds. If a victim is caught in the acid, in addition to the damage he or she must spend a round removing the substance (taking 1d6 points of damage in the process) or suffer 2d6 points of damage for the next five rounds.

The syrinx juggernaut has been observed to construct syrinxes from various secretions after feeding for lengthy amounts of time on corpses dragged into the central chamber, picking over the corpses quite thoroughly. Syrinx juggernauts have even been observed drinking any potions it comes into contact with, resulting in strange permanent effects that vary too much to document.
