.. title: Norker
.. slug: norker
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Norker
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 1+2                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon + 1 bite       |
+-----------------+-------------------------+
| Damage:         | 1d6 or by weapon + 1d4  |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 9                       |
+-----------------+-------------------------+
| Treasure Type:  | Q, R each; D, K in lair |
+-----------------+-------------------------+
| XP:             | 25                      |
+-----------------+-------------------------+

**Norkers** are fully subterranean members of the goblin races. A norker most closely resembles a hobgoblin but tends towards a more primitive demeanor. Each has long protruding fangs which sets it apart from its hobgoblin kin. A norker is hairless with tough leathery skin. It does not normally utilize armor or even clothing, wearing a belt with a loincloth at most. When using a small- or medium-sized melee weapon, a norker may also bite for 1d4 damage with its long fangs. It has Darkvision with a 90' range.

One out of every five norkers will be a warrior of 3+6 HD (145 XP) and even tougher skin (AC 15). Regular norkers gain a +1 bonus to their morale if they are led by a warrior. In norker lairs, one out of every ten will be a chieftain of 5+10 HD (360 XP) with AC 16 and a +1 bonus to damage due to strength.
