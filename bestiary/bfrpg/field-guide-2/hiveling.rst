.. title: Hiveling
.. slug: hiveling
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Hiveling
.. type: text
.. image:

+-----------------+--------------------------------------------+
| Armor Class:    | 15                                         |
+-----------------+--------------------------------------------+
| Hit Dice:       | 1d4+2 HP                                   |
+-----------------+--------------------------------------------+
| No. of Attacks: | 2 claws, 2 weapons, or 1 claw and 1 weapon |
+-----------------+--------------------------------------------+
| Damage:         | 1d4/1d4, by weapon, or 1d4/by weapon       |
+-----------------+--------------------------------------------+
| Movement:       | 50'                                        |
+-----------------+--------------------------------------------+
| No. Appearing:  | 3d6, Wild 3d6, Lair 8d10                   |
+-----------------+--------------------------------------------+
| Save As:        | Magic-User: 1                              |
+-----------------+--------------------------------------------+
| Morale:         | Special                                    |
+-----------------+--------------------------------------------+
| Treasure Type:  | A                                          |
+-----------------+--------------------------------------------+
| XP:             | 10                                         |
+-----------------+--------------------------------------------+

A **Hiveling** is a horrid little humanoid monster about the size of a goblin; they are almost totally covered with dark bristles like those of a wild boar. Only a hiveling's hideously pale, grinning face is exposed. All hivelings in a group are indistinguishable, and they do not have any obvious gender; how they reproduce is a mystery.

Hivelings are magically connected together into a hive-mind. Its morale is equal to 2 plus the number of hivelings in the group (maximum 12). The interconnection between them is so powerful that, if one suffers damage, one point of the damage is transferred to each allied hiveling within 40' until all other hivelings have taken a point of damage or there is just one point left; the remainder is all the damage the original hiveling will suffer. If a hiveling falls, any other hiveling having 2 or more HP can spend one round in contact with the fallen one and give it half (rounded down) of its own current HP. Similarly, healing magic will be divided point-by-point just as if it were damage, although undamaged hivelings will be passed over without receiving a point.

A hiveling has a +4 bonus to Dexterity checks, which accounts for its armor class and also applies to all attacks with ranged weapons. Due to its slight stature, a hiveling can only use small weapons such as short swords, daggers, slings, and such, but it can attack freely with two melee weapons at no penalty. It also has the ability to Pick Pockets with a 65% chance of success, which it uses in concert with its most significant ability: detection and identification of magic items.

A hiveling can detect the presence of magic items in a 30' radius, and can identify any magic item within 10'. As it is not particularly smart, a hiveling will avoid items that are complex or have multiple abilities. Because it is linked with a hive mind, a hiveling does not speak (though it may gibber madly when it attacks, there is no meaning to the sounds one makes), and thus will avoid items related to speaking languages. Neither does it read or write, so it will not be interested in scrolls or spell books.

On the other hand, a hiveling can activate any magic item, even one that requires command words (a hiveling is entirely capable of using a **Wand of Fireballs**, for example).

Any time a hiveling detects an "interesting" magic item, all hivelings in its group will try to acquire the item. If two or more hivelings can engage the holder of the item in combat, any who can flank that character will attempt to steal the item. If the hivelings are badly injured, they may seek out any character carrying a healing item, such as a **Potion of Healing**, and attempt to steal it or slay the one carrying it.

Even if a group of hivelings has morale of 12, they may choose to abandon combat when they believe they have acquired all the interesting items held by their enemies.
