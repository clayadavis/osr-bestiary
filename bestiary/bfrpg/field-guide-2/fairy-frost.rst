.. title: Fairy, Frost*
.. slug: fairy-frost
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Fairy, Frost*
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 18‡                          |
+-----------------+------------------------------+
| Hit Dice:       | 10*                          |
+-----------------+------------------------------+
| No. of Attacks: | 4 icy shards + slow          |
+-----------------+------------------------------+
| Damage:         | 1d8/1d8/1d8/1d8 (all + slow) |
+-----------------+------------------------------+
| Movement:       | 30’                          |
+-----------------+------------------------------+
| No. Appearing:  | 1                            |
+-----------------+------------------------------+
| Save As:        | Magic-user: 10               |
+-----------------+------------------------------+
| Morale:         | 12                           |
+-----------------+------------------------------+
| Treasure Type:  | none                         |
+-----------------+------------------------------+
| XP:             | 1,390                        |
+-----------------+------------------------------+

A **Frost Fairy** is a manifestation of the consciousness of a frozen tundra, appearing with translucent crystalline shards, a halo of snow, and a blue-white motif as its attributes.

It attacks by throwing shards of ice. The extreme cold of the shards reduces movements similar to a **slow** spell.
