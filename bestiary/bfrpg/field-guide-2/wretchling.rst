.. title: Wretchling
.. slug: wretchling
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Wretchling
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 17 (or as host) |
+-----------------+-----------------+
| Hit Dice:       | 5               |
+-----------------+-----------------+
| No. of Attacks: | 1               |
+-----------------+-----------------+
| Damage:         | Special         |
+-----------------+-----------------+
| Movement:       | 40’             |
+-----------------+-----------------+
| No. Appearing:  | 1               |
+-----------------+-----------------+
| Save As:        | Magic-User: 5   |
+-----------------+-----------------+
| Morale:         | 8               |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 360             |
+-----------------+-----------------+

A **Wretchling** dwells in the dark corners of the world, hiding in shadows and psychically feeding off of the feelings of those nearby, draining hope and joy and replacing them with fear and crushing depression. In its rarely-seen natural state, it resembles an upside-down humanoid torso with two spider-like appendages attached to each limb joint and where the neck would be. A head, vaguely arachnid in appearance with six large glossy differently-colored eyes, is situated directly in the middle of its chest and can turn entirely around. While its natural habitats, deep caves and areas that most would fear to tread, are rarely intruded upon by humanoids, if a wretchling should sense intelligent beings nearby it will pick one at random and begin to stalk them. If it manages to get within 40' of the chosen being without detection it will attack. If it succeeds its attack roll, the victim must make a save vs. Spells. If successful, the victim immediately becomes aware of the wretchling and is stunned for a single round. If the save fails, the creature disappears and slips into the mind of the new host.

A host in the first stage appears to be completely normal. However, terrible nightmares will begin to manifest for all those within 100'; first just while asleep, and then later even during waking hours. The nightmares begin with the victim seeing long spider-like appendages creeping from the corner of his or her vision, accompanied by a feeling of dread. It is not uncommon for the recipient of these visions to randomly break out in cold sweats.

After several days the visions will increase in frequency and severity to include faint whispering that leaves the victim uneasy and feeling physically ill. The wretchling will manifest itself at this time as a small child that only the host character can see. The victim will talk to this apparition frequently, and others in the area may take notice. If the wretchling is attacked in this state with a magical weapon or spell, it will be forced out of the host's body and will attempt to possess the nearest being. This can be prevented by encircling the host in salt before-hand, as wretchlings despise the substance. If this does not happen within a few days, soon all those near the host will start to see visions of violence targeting friends and allies; the whispering, now much louder, encourages them to attack those nearby.

At this point the host enters the second stage. The character must succeed a second save vs. Spells or die, their eyes turning a glossy black. In 1d6 hours they will arise, now undead (can be Turned as a zombie). The host will begin to sow the seeds of dissent among his or her allies, attacking companions in the night and feasting upon their flesh. Finally, the wretchling will shed the now paper-thin shell that is the remnants of its host. A wretchling in this state is gorged and gains two extra hit dice, but its movement speed is decreased by half. All those killed by a wretchling have a 20% chance to rise as a zombie within three days.
