.. title: Bazeley’s Sphere Recombinant
.. slug: bazeley-s-sphere-recombinant
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bazeley’s Sphere Recombinant
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 16                       |
+-----------------+--------------------------+
| Hit Dice:       | 1*                       |
+-----------------+--------------------------+
| No. of Attacks: | 2 sphere shots           |
+-----------------+--------------------------+
| Damage:         | 1d4+1/1d4+1              |
+-----------------+--------------------------+
| Movement:       | 80'                      |
+-----------------+--------------------------+
| No. Appearing:  | 3d10 HD worth            |
+-----------------+--------------------------+
| Save As:        | Fighter: 1               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | 100 gp worth of crystals |
+-----------------+--------------------------+
| XP:             | 37                       |
+-----------------+--------------------------+

The rarest form, the **Sphere Recombinant**, stands 6’ tall atop wheels constructed entirely from rigidly-linked spheres. The recombinant is much more streamlined than the minor form, and superficially resembles a skeleton. It has one large arm that can fire out spheres quite rapidly, allowing it to attack twice per round. The fired spheres will quietly return to the recombinant after being fired. Although larger than the minor, the recombinant is less durable due to sacrificing durability for speed.
