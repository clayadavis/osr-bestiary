.. title: Amalgam*
.. slug: amalgam
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Amalgam*
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 18‡                          |
+-----------------+------------------------------+
| Hit Dice:       | 8*                           |
+-----------------+------------------------------+
| No. of Attacks: | 1 engulf                     |
+-----------------+------------------------------+
| Damage:         | 1d6 + energy drain (1 level) |
+-----------------+------------------------------+
| Movement:       | Fly 90’                      |
+-----------------+------------------------------+
| No. Appearing:  | 1                            |
+-----------------+------------------------------+
| Save As:        | Fighter: 8                   |
+-----------------+------------------------------+
| Morale:         | 12                           |
+-----------------+------------------------------+
| Treasure Type:  | none                         |
+-----------------+------------------------------+
| XP:             | 1,085                        |
+-----------------+------------------------------+

An **Amalgam** is a type of undead that is formed when the spirits of numerous creatures who died in close proximity mingle together. Some amalgams are as hateful towards the living as other undead, while others are passive and seem to take no interest in the living that intrude upon their rest.

An amalgam is roughly 50' in diameter, although it can alter its general shape to fit into a similar volume. They vary widely in appearance; for example: a cloud of white mist in which vague humanoid figures can be seen forming and dispersing slowly; hundreds of unblinking eyes that hover in the air; groups of shadowy figures which flit about as one; a large cluster of small blue flames that float about.

Regardless of appearance, all amalgams share the same abilities. An amalgam attacks by engulfing a target with its “body.” Creatures caught suffer 1d6 points of damage due to cold each round, and also suffer one level of energy drain. Those resistant to cold or naturally-adapted to cold conditions may make a save vs. Spells every round to ignore the cold damage, but no saving throw applies to the energy drain.

An amalgam can cast the following spells at-will: **hold person**, **invisibility**, and **silence 15' radius**. It can also cast **cloudkill** once per day.

Like all undead, an amalgam may be Turned by a Cleric (as a ghost) and is immune to **sleep**, **charm**, and **hold** magics. Due to their incorporeal nature, they cannot be harmed by non-magical weapons. They are also immune to the effects of cold.
