.. title: Corpse Abomination
.. slug: corpse-abomination
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Corpse Abomination
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 14, 17, or 20         |
+-----------------+-----------------------+
| Hit Dice:       | 6, 12, or 18          |
+-----------------+-----------------------+
| No. of Attacks: | 3 bludgeon            |
+-----------------+-----------------------+
| Damage:         | 1d6, 1d8, or 2d8 each |
+-----------------+-----------------------+
| Movement:       | 10' Jump 60’          |
+-----------------+-----------------------+
| No. Appearing:  | 1d3                   |
+-----------------+-----------------------+
| Save As:        | Fighter: 6, 12, or 18 |
+-----------------+-----------------------+
| Morale:         | 12                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 610, 2,075, or 4,320  |
+-----------------+-----------------------+

A **Corpse Abomination** is the result of a mass grave being animated without any effort to separate the remains. It is a shambling amalgamation of rotting flesh and desiccated bones, packed with clay, soil, and dripping maggots. It stands anywhere from 8' to 20' tall, possessing many limbs and attacking bare-handed. It produces a constant chorus of wailing as it moves due to the torment suffered by the trapped souls that power the abomination. It exudes such a powerful stench that anything within 40’ must make a save vs. Poison to avoid a -2 on all rolls. The corpse abomination moves rather slowly due to its considerable bulk; however, to the surprise of many late adventurers, it can leap 60' by spreading itself out in the air to achieve maximum reach. It can also protrude the torsos of its attacking sections, which gives it a reach of 10'.
