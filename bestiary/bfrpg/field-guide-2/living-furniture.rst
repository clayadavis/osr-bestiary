.. title: Living Furniture
.. slug: living-furniture
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Living Furniture
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 13                             |
+-----------------+--------------------------------+
| Hit Dice:       | 2 to 4                         |
+-----------------+--------------------------------+
| No. of Attacks: | 1                              |
+-----------------+--------------------------------+
| Damage:         | 1d6                            |
+-----------------+--------------------------------+
| Movement:       | Small 10’ Medium 20’ Large 30’ |
+-----------------+--------------------------------+
| No. Appearing:  | Wild 1d6 Lair 3d10             |
+-----------------+--------------------------------+
| Save As:        | Fighter: 2                     |
+-----------------+--------------------------------+
| Morale:         | 10                             |
+-----------------+--------------------------------+
| Treasure Type:  | None                           |
+-----------------+--------------------------------+
| XP:             | 75 to 240                      |
+-----------------+--------------------------------+

**Living Furniture** is furniture that has been animated by a Magic-User to guard his or her tower or lair. A Magic-User will usually animate all the furniture in an attempt to swarm any intruders and make enough noise to alert others to the intrusion. Living furniture is semi-intelligent and will protect its own group of furniture; tables will protect chairs, for example. Living furniture does not need to eat but can consume material it is made from to re-gain hit points. Wooden living furniture takes half damage from sharp objects, and textile-based living furniture takes double; arrows do 1 damage regardless. Metal and stone furniture only take damage from blunt weapons.

The longer living furniture lives the more intelligent it becomes; after about a decade of animation, it will be clever enough to escape the caster's lair and roam the wild. If enough furniture escapes at once they will usually go on to create small settlements to live.

Living furniture attacks using its legs and flinging itself at the attacker. Complex furniture such as clocks have a 10% chance to cast two level-one spells from its animator's book per day. All candles can project a small firebolt once per two rounds, dealing 1d4 damage and setting the target on fire unless a save vs. Spells is made. **Dispel** has no affect on these creatures.

Small furniture such as stools has 2 HD, medium such as chairs have 3 HD, and large such as tables and shelves have 4 HD. All ceramics have 1 HP and will always hit its target when attacking, destroying itself in the process. Ceramics will always go last in a round of combat.
