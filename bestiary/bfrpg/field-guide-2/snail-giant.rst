.. title: Snail, Giant
.. slug: snail-giant
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Snail, Giant
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 17                       |
+-----------------+--------------------------+
| Hit Dice:       | 6*                       |
+-----------------+--------------------------+
| No. of Attacks: | 1 slam                   |
+-----------------+--------------------------+
| Damage:         | 1d8 + poison (paralysis) |
+-----------------+--------------------------+
| Movement:       | 10'                      |
+-----------------+--------------------------+
| No. Appearing:  | 1                        |
+-----------------+--------------------------+
| Save As:        | Fighter: 6               |
+-----------------+--------------------------+
| Morale:         | 10                       |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 555                      |
+-----------------+--------------------------+

A **Giant Snail** is a great molluscoid creature with a hard shell covering most of its body. It attacks by slamming its body into its opponents, covering them in its mucus (a powerful poison). The victim of this poison must save vs. Poison or be paralyzed for 1 turn.

It leaves a trail of this mucus which is also slippery, and any attempting to sprint through it must save vs. Death Ray or fall and be subject to the poison’s effects.

If surrounded the giant snail may retreat into its shell, giving it an amour class of 20 and a hardness of 4 (see the **Stronghold** section in the **Basic Fantasy RPG Core Rules**).
