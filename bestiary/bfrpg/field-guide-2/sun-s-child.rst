.. title: Sun’s Child*
.. slug: sun-s-child
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Sun’s Child*
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 16‡         |
+-----------------+-------------+
| Hit Dice:       | 10*         |
+-----------------+-------------+
| No. of Attacks: | Special     |
+-----------------+-------------+
| Damage:         | 3d6         |
+-----------------+-------------+
| Movement:       | Fly 40'     |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 10 |
+-----------------+-------------+
| Morale:         | 9           |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 1,390       |
+-----------------+-------------+

A **Sun’s Child** initially appears as a sphere of flame, essentially a miniature sun 20 feet in diameter. This however is not its true form, which is that of a humanoid baby, with a glowing white hot surface, located in the center of the ‘sun’.

It attacks by charging, bringing foes within its flames. These flames will burn any flammable objects and can melt most mundane metals (iron, etc.). Magically-enchanted items will be spared however.

To hit the sun’s child the attacker must have a reach or range of at least 10’ or else will fall within the sphere of flames and become engulfed in it.
