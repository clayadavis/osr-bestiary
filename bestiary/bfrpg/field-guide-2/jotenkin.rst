.. title: Jotenkin
.. slug: jotenkin
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Jotenkin
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15 (11)                 |
+-----------------+-------------------------+
| Hit Dice:       | 3+3                     |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon                |
+-----------------+-------------------------+
| Damage:         | 1d8 or by weapon +1     |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | Q, R each; D, K in lair |
+-----------------+-------------------------+
| XP:             | 145                     |
+-----------------+-------------------------+

Hailing from regions of rugged northern coastlines, **Jotenkin** are large humanoids with kinship to giants. Many simply call them Northmen. A jotenkin is less brutish and slightly smaller than an ogre, and may even be considered just a large race of mankind. Each is an expert seaman in the dragon-headed longboat that is customarily used. Jotenkin society is warrior-oriented and thrives by raiding and pillaging coastal communities. Other than such raiding, jotenkin do not generally mix with the normal humans that share the same regions, though they may take human slaves. Where the humans revere certain deities of their culture, the jotenkin revere the antithesis giants of those pantheons. They speak their own language that sounds similar to that of frost giants.

A jotenkin arms him- or herself with large axes, swords, spears, and the like, and utilize wooden shields. Even the smallest jotenkin receives a +1 damage bonus when using a weapon due to strength. Jotenkin are essentially immune to cold environmental effects and even extreme or magical cold causes only half-damage. If a save is involved with such a cold-based effect, a jotenkin receives a +4 bonus.

One out of every five jotenkin will be a warrior of 5+5 HD and +2 damage bonus. Regular jotenkin gain a +1 bonus to their morale if such warriors are around to keep order. Each jotenkin longboat will be led by a chieftain of 8+8 hit dice, having a +3 bonus to damage due to strength.
