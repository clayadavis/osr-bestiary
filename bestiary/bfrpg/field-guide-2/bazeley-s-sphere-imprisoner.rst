.. title: Bazeley’s Sphere Imprisoner
.. slug: bazeley-s-sphere-imprisoner
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bazeley’s Sphere Imprisoner
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 20                       |
+-----------------+--------------------------+
| Hit Dice:       | 8                        |
+-----------------+--------------------------+
| No. of Attacks: | None                     |
+-----------------+--------------------------+
| Damage:         | None                     |
+-----------------+--------------------------+
| Movement:       | 20'                      |
+-----------------+--------------------------+
| No. Appearing:  | 1                        |
+-----------------+--------------------------+
| Save As:        | Fighter: 8               |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | 800 gp worth of crystals |
+-----------------+--------------------------+
| XP:             | 875                      |
+-----------------+--------------------------+

A rarely-seen form of Bazeley's Marvelous Spheres, the **Sphere Imprisoner** will only assemble if there is a single intruder. Standing 8' tall, it lacks arms and appears to be a large sphere made of smaller spheres on legs. Once triggered, the imprisoner will assemble around the intruder. On a failed save vs. Dragon Breath, all spheres in the room will fly towards the target simultaneously, encasing them in a cage of moving metal. The imprisoner will then become dormant until either attacked or the correct command word is given. Most victims of the imprisoner die of starvation or thirst.
