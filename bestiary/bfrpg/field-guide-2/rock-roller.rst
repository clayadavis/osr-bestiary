.. title: Rock Roller
.. slug: rock-roller
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Rock Roller
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 16               |
+-----------------+------------------+
| Hit Dice:       | 4*               |
+-----------------+------------------+
| No. of Attacks: | 1 bite + special |
+-----------------+------------------+
| Damage:         | 1d8              |
+-----------------+------------------+
| Movement:       | 180' (60')       |
+-----------------+------------------+
| No. Appearing:  | 1d6              |
+-----------------+------------------+
| Save As:        | Fighter: 2       |
+-----------------+------------------+
| Morale:         | 7                |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 280              |
+-----------------+------------------+

**Rock Rollers** are creatures that resemble living boulders. They are mostly mouth with jewel-like eyes and are about the size of a full-grown human.

A rock roller hunts by hiding itself high on a cliff or wall and then rolling down at its intended prey. After a successful hit the victim must make a Dexterity check; on a failed check the victim is knocked to the ground and winded. The victim takes no damage, but must spend the next round to stand up and catch their breath. The victim cannot move nor attack that round.

A rock roller is immune to any form of **charm** magic.
