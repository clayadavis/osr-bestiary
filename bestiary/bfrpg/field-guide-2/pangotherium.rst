.. title: Pangotherium
.. slug: pangotherium
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Pangotherium
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 16         |
+-----------------+------------+
| Hit Dice:       | 7          |
+-----------------+------------+
| No. of Attacks: | 2 claws    |
+-----------------+------------+
| Damage:         | 2d4/2d4    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1d6        |
+-----------------+------------+
| Save As:        | Fighter: 7 |
+-----------------+------------+
| Morale:         | 9          |
+-----------------+------------+
| Treasure Type:  | J, K       |
+-----------------+------------+
| XP:             | 670        |
+-----------------+------------+

A **Pangotherium** looks like a grizzly bear with the head, hide, and rather-enlarged claws of a scaly anteater. Its place of origin is uncertain but it is not in this universe, and it is one of several alien creatures whose appearance indicates that a **conjuration** or **summoning** spell has gone wrong.

Pangotheria are intelligent animals, with similar cognitive ability to a gorilla. They can operate doors and other simple mechanisms, but they have no language, at least as humans would understand it. They are, however, extremely chaotic and unpredictable. Determine how each one reacts to the party by rolling 3d6. A score of 3-6 means it charges; a 7-8 means that it attacks; on a 9-12 it approaches without attacking; on a 13-14 it retreats; on a 15-17 it flees, and on an 18 it takes an immediate liking to the party and will roll on its back waiting for its belly to be scratched. In any case, if attacked it will certainly retaliate!
