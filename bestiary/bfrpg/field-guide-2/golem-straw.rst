.. title: Golem, Straw
.. slug: golem-straw
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Straw
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 11         |
+-----------------+------------+
| Hit Dice:       | 3*         |
+-----------------+------------+
| No. of Attacks: | 1 hug      |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 175        |
+-----------------+------------+

Certain druids can make a **Straw Golem**, which typically has an agricultural role and often used as a scarecrow. It takes half damage from blunt weapons and no damage from piercing ones. It is particularly vulnerable to fire, and any fire attack will set it alight, destroying it in 3 rounds. During these 3 rounds the golem will move at maximum speed towards whoever set it alight, seeking to hug them and hold them in the fire to join the straw golem in oblivion. Anyone held in a hug by a burning straw golem will take 1d6 points of damage in the first round, 2d6 in the second round, and 3d6 in the third round before it burns away.
