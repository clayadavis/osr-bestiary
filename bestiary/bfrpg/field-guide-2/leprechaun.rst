.. title: Leprechaun
.. slug: leprechaun
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Leprechaun
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 19                        |
+-----------------+---------------------------+
| Hit Dice:       | 1**                       |
+-----------------+---------------------------+
| No. of Attacks: | 1 miniature weapon        |
+-----------------+---------------------------+
| Damage:         | 1d2                       |
+-----------------+---------------------------+
| Movement:       | 30'                       |
+-----------------+---------------------------+
| No. Appearing:  | 1                         |
+-----------------+---------------------------+
| Save As:        | Magic-User: 1 (+ bonuses) |
+-----------------+---------------------------+
| Morale:         | 5                         |
+-----------------+---------------------------+
| Treasure Type:  | M (5,000 – 40,000 gp)     |
+-----------------+---------------------------+
| XP:             | 49                        |
+-----------------+---------------------------+

The "wee folk" are related to the other fey races of brownies, pixies, and sprites. A **Leprechaun** stands about 2’ tall and appears as a miniature human. Its hair color can be blond, brown, or red with a predominant tendency toward a red shade of all the colors. A male leprechaun usually prefers to have a beard. Leprechaun clothing tends toward tans and greens with gold buttons and buckles. A leprechaun can speak various Fey languages and invariably speaks Common with a heavy accent called a “brogue”. Leprechauns have Darkvision out to 60'. Because the wee folk are very suspicious of other races, a leprechaun is also very observant and will only be surprised by a roll of 1 on 1d6.

A leprechaun prefers to live alone or in small family clans. Leprechauns can be found in forests, usually in unexplored or rarely-used glades. A leprechaun will have a lair that only a Dwarf or Halfling character can access. It will not willingly lead anyone to this lair. Leprechauns have a deep desire for gold and will have treasure that reflects this. A leprechaun is very fearful of humans and Dwarves, because it believes that these races desire their gold. If there is an Elf in the party, the leprechaun will ignore any human or Dwarf leader and will address the Elf, since Elves are kindred fey spirits.

Leprechauns use tools and will have tiny swords that are treated like daggers. It will rarely use small bows and cannot use pole weapons. Like a brownie, a leprechaun will get a +4 bonus to any saves versus magic.

A leprechaun has some Thief-like abilities including Pick Pockets, Move Silently, and Hide, all as a 15th-level Thief. It also has spell-like abilities that can be used at-will, including **teleport**, **confusion**, and **bane**. A leprechaun will use the Hide and Move Silently skills to try and avoid a party. If a party does happen to catch one, the leprechaun will use Pick Pocket to steal any gold on the person that is holding it. The leprechaun will then cast **confusion** on the party, followed by **teleport**. Since it stays within a range of about 50 miles of its lair, it will generally be on target. If prevented from teleporting the leprechaun will try to buy its freedom with treasure or three wishes. Note that the treasure may have a **bane** curse (see the **bless** spell in the **Basic Fantasy RPG Core Rules**). The wishes will be very limited. The leprechaun will fulfill the wish, but will put a twist on it that could severely hurt the wisher; i.e. the leprechaun will tell the wisher where a large treasure is, but fail to mention the two level-7 red dragons guarding it.

There is rumor of a leprechaun king, an exceptionally wealthy leprechaun, with a very twisted idea of how to make a wish turn back on the person making the wish; i.e. “I wish for a mighty sword” results in a +3 sword made from the tooth of a demon, but for every enemy you kill with the sword there is a chance that the wisher will become a vampire.
