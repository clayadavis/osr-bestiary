.. title: Dreamless*
.. slug: dreamless
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Dreamless*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13‡        |
+-----------------+------------+
| Hit Dice:       | 2*         |
+-----------------+------------+
| No. of Attacks: | 1 touch    |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 2 |
+-----------------+------------+
| Morale:         | 10         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 100        |
+-----------------+------------+

A **Dreamless** appears as an indefinite humanoid shade, pale blue in color. It stalks the wilderness at night, targeting weary travelers and novice adventurers.

It attacks by touching its opponents; those touched by a dreamless must save vs. Spells or be unable to sleep until the effect is dispelled with a casting of **remove curse**.

Being unable to sleep does not cause immediate detriment. Spell-casters may still memorize their spells, though it takes twice the normal time, and others are unaffected. That is for the first day; after that a d20 must be rolled everyday, with the character’s Constitution bonus added and the number of days they have been awake subtracted, with a 1 or less indicating that the character has gone insane due to the sleep deprivation. Such a character may be cured with a casting of **remove fear**, but in the interim he or she may try and take their own life.
