.. title: Tymera
.. slug: tymera
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Tymera
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 17                            |
+-----------------+-------------------------------+
| Hit Dice:       | 11** (AB +9)                  |
+-----------------+-------------------------------+
| No. of Attacks: | 2 claws, 3 heads + special    |
+-----------------+-------------------------------+
| Damage:         | 1d6/1d6/2d4/2d4/3d4 + special |
+-----------------+-------------------------------+
| Movement:       | 40' (10') Fly 60' (15')       |
+-----------------+-------------------------------+
| No. Appearing:  | 1d2, Wild 1d4, Lair 1d4       |
+-----------------+-------------------------------+
| Save As:        | Fighter: 11                   |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | F                             |
+-----------------+-------------------------------+
| XP:             | 1,765                         |
+-----------------+-------------------------------+

**Tymeras** are a colder climate variant of the fierce three-headed chimera. Where the chimera has lion portions, the tymera consists of black- and white-striped arctic tiger parts. The goat head is more akin to a mountain ram, and the dragon portion is that of a white dragon. A tymera weighs substantially more than its chimera counterpart at about 5,000 pounds. In addition to physical attacks the white dragon head breathes forth a 50' long cone with a 10' wide end that causes 4d6 cold; victims may save vs. Dragon Breath for one-half damage. This attack is usable up to 10 times in one day but no more than every other round. The tymera is immune to cold-based attacks.

A tymera is intelligent but exceptionally ill-tempered. It can speak Dragon and may form pacts with other powerful creatures. There are rumors of a more benevolent variant with a silver dragon's head.
