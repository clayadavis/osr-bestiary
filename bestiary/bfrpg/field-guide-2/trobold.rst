.. title: Trobold
.. slug: trobold
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Trobold
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 14             |
+-----------------+----------------+
| Hit Dice:       | 1d4 HP **      |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d2/1d2/1d4    |
+-----------------+----------------+
| Movement:       | 30'            |
+-----------------+----------------+
| No. Appearing:  | 4d4, Lair 6d10 |
+-----------------+----------------+
| Save As:        | Normal Man     |
+-----------------+----------------+
| Morale:         | 9              |
+-----------------+----------------+
| Treasure Type:  | P, Lair J      |
+-----------------+----------------+
| XP:             | 16             |
+-----------------+----------------+

A **Trobold** is a magical hybrid of a troll and a kobold. Each is a short (3’ tall) hunched creature with rubbery gangly limbs and sharp clawed hands. A trobold retains the main physical features of a kobold, though its mouth is filled with rows of extremely sharp teeth in the manner of a troll.

Like a troll, a trobold will regenerate wounds at the rate of 2 HP per round. Unlike a troll, a slain trobold regenerates so swiftly it will rise up to fight again a mere two rounds after it was killed. Only fire and acid will inflict permanent injury and death upon the hideous little creature. Furthermore, if a slashing weapon is used to slay a trobold there is a 50% chance it will result in two trobolds rising from the corpse.

Trobolds attack in packs, using ambush, fiendish traps, and tricks combined with overwhelming numbers and savagery to send many a band of adventurers fleeing for their lives.
