.. title: Grawl
.. slug: grawl
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Grawl
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 14                          |
+-----------------+-----------------------------+
| Hit Dice:       | 2+2*                        |
+-----------------+-----------------------------+
| No. of Attacks: | 2 claws/1 bite or 1 weapon  |
+-----------------+-----------------------------+
| Damage:         | 1d4/1d4/1d6 or by weapon +1 |
+-----------------+-----------------------------+
| Movement:       | 40' Climb 30'               |
+-----------------+-----------------------------+
| No. Appearing:  | 2d6, Lair 8d6               |
+-----------------+-----------------------------+
| Save As:        | Fighter: 2                  |
+-----------------+-----------------------------+
| Morale:         | 9                           |
+-----------------+-----------------------------+
| Treasure Type:  | B, L in lair                |
+-----------------+-----------------------------+
| XP:             | 100                         |
+-----------------+-----------------------------+

**Grawls** are a savage race of subterranean dwelling humanoids. They have pale gray fur, sharp claws, a wide panther-like head, and a short bobbed tail. An adult male stands on average 6'5'' tall and weighs around 260 pounds; females are slightly smaller than males. A grawl typically goes about naked but sometimes paints itself to better blend into the darkness of its tunnel homes. It constantly prowls its territory, and will attack any creature that strays into the area. Most grawls do not carry weapons and fight only with their teeth and claws. The remainder carry stone clubs and axes and occasionally can be found carrying higher-quality weapons taken from prior victims.

A grawl has Darkvision to a range of 120'. It suffers a -2 attack penalty in bright sunlight or within the radius of a **light** spell. A grawl can speak a pidgin of Orc, Goblin, Dwarf, and Common, although its speech sounds like animal growls to most humanoids.

Grawls are vicious and wild in battle, showing little discipline or forethought. A tool-using grawl is more likely to use rudimentary tactics, like feigned retreats into waiting ambushes. If a grawl is reduced to 25% or less of its original hit points, it enters a berserk fury and gains a +2 bonus to its attack and damage rolls, but suffers a -2 penalty to its armor class. This rage lasts until the grawl is dead or all enemies are dead or out of sight.

One out of every 12 grawls will be a leader with 3+3* HD (175 XP), 16 (11) AC, and a +2 bonus to damage. In lairs of 30 or more, there will be 1d3 grawl shamans present. A shaman is equivalent to a grawl leader statistically, but has Clerical abilities at level 1d4+2.
