.. title: Advi, Charity*
.. slug: advi-charity
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Advi, Charity*
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 16              |
+-----------------+-----------------+
| Hit Dice:       | 6*              |
+-----------------+-----------------+
| No. of Attacks: | 4 fireballs     |
+-----------------+-----------------+
| Damage:         | 1d6/1d6/1d6/1d6 |
+-----------------+-----------------+
| Movement:       | 10'             |
+-----------------+-----------------+
| No. Appearing:  | 1d4             |
+-----------------+-----------------+
| Save As:        | Fighter: 6      |
+-----------------+-----------------+
| Morale:         | 12              |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 555             |
+-----------------+-----------------+

A **Charity Advi** is a sphere of silvery metal with dozens of small protrusions that glow a faint orange all over its body.

It attacks by firing miniature fireballs out of these protrusions, targeting up to four foes within 80 feet. The fireballs can set fire to flammable materials such as wood or cloth.
