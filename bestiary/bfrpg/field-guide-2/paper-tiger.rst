.. title: Paper Tiger
.. slug: paper-tiger
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Paper Tiger
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 15                   |
+-----------------+----------------------+
| Hit Dice:       | 7*, 9*, or 11*       |
+-----------------+----------------------+
| No. of Attacks: | 2 claws/1 bite       |
+-----------------+----------------------+
| Damage:         | 1d8/1d8/2d6          |
+-----------------+----------------------+
| Movement:       | 60’                  |
+-----------------+----------------------+
| No. Appearing:  | 1                    |
+-----------------+----------------------+
| Save As:        | Fighter: 7, 9, or 11 |
+-----------------+----------------------+
| Morale:         | 12                   |
+-----------------+----------------------+
| Treasure Type:  | None                 |
+-----------------+----------------------+
| XP:             | 635, 1,150, or 1,665 |
+-----------------+----------------------+

A **Paper Tiger** initially appears to be an ordinary scroll or book, and will almost always be found in a library or other large cache of similar materials. If opened and perused, the paper will expand and refold into the form of a life-sized origami tiger; this transformation requires a full round, during which time the paper tiger has AC 11, but as the creature surprises on a roll of 1-5 on 1d6 it is likely to complete its transformation unscathed. Paper tigers are white or cream-colored, with stripes made of closely-packed letters, numbers, runes, or other symbols.

As soon as the paper tiger's transformation is complete it will attack. As a construct created to protect the library in which it is found, it will prefer to attack the character who activated it but it will continue attacking until it is destroyed or all possible opponents have left the area it protects. Note that it will not normally leave the vicinity of the library it wards, though if more than half of the books and/or scrolls are removed from the library it may choose to pursue the creatures who took them. Also note that if the paper tiger is taken away in book/scroll form without being opened, as soon as it is opened it will attempt to return to its library, attacking anyone who gets in its way.

As a construct, a paper tiger is immune to **sleep**, **charm**, and **hold** magic, and has no mind which may be read using **ESP**. It is immune to poison and any other effects that specifically harm living creatures. It is especially vulnerable to fire, taking double damage from any fire-based attacks.

If all interlopers leave the library and remain out of the paper tiger's sight for at least a turn, the creature will resume its original form, hiding itself among the books or scrolls of the library. In this form the paper tiger is very nondescript; even those who know such a creature is present will not usually be able to identify it, though anyone who previously activated it has a chance equal to 1 on 1d6 to do so. Adjust this roll using the creature's Intelligence bonus (i.e. a 13 Intelligence gives a 1-2 on 1d6 chance, etc.). While in its book or scroll form, the paper tiger heals 1d6 points of damage each hour.

If a library contains the sort of knowledge that its curator believes is too dangerous to ever be revealed it may be warded by a **Flash Paper Tiger**. These creatures look and behave exactly as a normal paper tiger, but if slain or successfully attacked with fire a flash paper tiger explodes into a **fireball** (as the spell), dealing a number of dice of damage equal to the creature's HD. This fireball has the usual chance to ignite flammable materials in the area of effect, but as a special feature will instantly ignite any and all unprotected paper items the flames touch (a spellbook inside a backpack counts as "protected" and thus receives a saving throw, but a scroll held open by a character will instantly burn to ash).
