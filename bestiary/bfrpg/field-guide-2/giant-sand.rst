.. title: Giant, Sand
.. slug: giant-sand
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Giant, Sand
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 15 (13)           |
+-----------------+-------------------+
| Hit Dice:       | 13                |
+-----------------+-------------------+
| No. of Attacks: | 1 weapon or fists |
+-----------------+-------------------+
| Damage:         | 3d6               |
+-----------------+-------------------+
| Movement:       | 50'               |
+-----------------+-------------------+
| No. Appearing:  | Wild 2d10         |
+-----------------+-------------------+
| Save As:        | Fighter: 13       |
+-----------------+-------------------+
| Morale:         | 8                 |
+-----------------+-------------------+
| Treasure Type:  | E + 1d12x1,000 gp |
+-----------------+-------------------+
| XP:             | 2,175             |
+-----------------+-------------------+

A **Sand Giant** has dark swarthy skin and is particularly hairy. A sand giant's hair is always black, which only helps its shining blue eyes stand out. A sand giant dresses in flowing white robes and wears a veil over its face to keep out the blowing sand. A sand giant warrior typically wears thick leather armor under its robes. They scorn most decoration, excluding a few pieces of finely-crafted jewelry. An adult male is about 17’ tall and weighs around 3,500 pounds. Females are the same height and only slightly lighter. A sand giant can live for up to 400 years.

In the past they once ruled over their own desert kingdoms, but they were driven into the wastes by encroaching humanoids centuries ago. A sand giant lives as a nomad, grazing its herd of cattle upon whatever sparse vegetation it comes across. In some particularly fertile places a tribe of giants will settle down and establish a village where they grow date palms. A sand giant is belligerent and haughty, and sees no problem grazing its cattle on another farmer's crops. In lean times young male giants will often hire themselves out as mercenaries to help support their families.

A sand giant is a skilled warrior and often fights with well-crafted scimitars and enormous bows; its bow has double the range of a standard longbow. Whether attacking with weapons or its own powerful fists, a sand giant deals 3d6 points of damage.
