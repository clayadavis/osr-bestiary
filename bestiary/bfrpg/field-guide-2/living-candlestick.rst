.. title: Living Candlestick
.. slug: living-candlestick
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Living Candlestick
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 13                       |
+-----------------+--------------------------+
| Hit Dice:       | 1*                       |
+-----------------+--------------------------+
| No. of Attacks: | 1 firedart or 1 fireball |
+-----------------+--------------------------+
| Damage:         | 1d3 or 2d6               |
+-----------------+--------------------------+
| Movement:       | 10’                      |
+-----------------+--------------------------+
| No. Appearing:  | 1d6                      |
+-----------------+--------------------------+
| Save As:        | Fighter: 1               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 37                       |
+-----------------+--------------------------+

A **Living Candlestick** is an animated candelabra that will fight intruders. It does this by shooting tiny darts of fire from its candles, and by firing a small fireball every 1d6 rounds. This fireball covers a 10' radius, with a save vs. Spells for half-damage.

The light given off by a living candlestick is enough to illuminate twice the area of a standard lantern (60’ radius, dim light extending 40’ further).
