.. title: Vihm (Ratling)
.. slug: vihm-ratling
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Vihm (Ratling)
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 13 (11)                   |
+-----------------+---------------------------+
| Hit Dice:       | 1d4 HP                    |
+-----------------+---------------------------+
| No. of Attacks: | 1 weapon                  |
+-----------------+---------------------------+
| Damage:         | 1d4 or by weapon          |
+-----------------+---------------------------+
| Movement:       | 20' Unarmored 30'         |
+-----------------+---------------------------+
| No. Appearing:  | 4d4, Wild 6d10, Lair 6d10 |
+-----------------+---------------------------+
| Save As:        | Normal Man                |
+-----------------+---------------------------+
| Morale:         | 6                         |
+-----------------+---------------------------+
| Treasure Type:  | P, Q each; C in lair      |
+-----------------+---------------------------+
| XP:             | 10                        |
+-----------------+---------------------------+

**Vihm** are small, nearly hairless rodent-faced humanoids often referred to as ratlings. A vihm is 2’ to 2.5’ feet tall and weighs 35 to 45 pounds. Vihm are quite cowardly but very cunning and view nearly all larger races as enemies. Strangely the vihm prefer to live in proximity to these larger races, keeping to the nooks and crannies under the very noses of the larger races. The actual dens and burrows that vihm inhabit are extensively trapped.

Whenever possible, vihm set up ambushes near trapped areas with the goal of driving enemies into the traps, where other ratlings wait to utilize flaming oil, drop poisonous vermin, or simply shoot the victims. Preferring to stay out of melee, vihm receive a bonus of +1 to hit and damage with ranged attacks. Vihm have Darkvision to 60' and suffer a -1 penalty to attack rolls in bright sunlight or within the radius of **light** spells. Vihm typically wear various hides (equivalent to leather armor) in battle.

One out of every six vihm will be a warrior with 1 HD (25 XP). The vihm gain a +1 bonus to their morale if they are led by such a warrior. In vihm lairs one out of every twelve will be a chieftain of 2 HD (75 XP) with AC 14 (11) and a +1 bonus to damage due to strength. In lairs of 30 or greater there will be a vihm lord of 3 HD (145 XP) who wears heavier armor with AC 15 (11), has a movement of 10', and a +1 bonus to damage. In the lair, vihm never fail a morale check as long as the vihm lord is alive. In addition, a lair has a chance equal to 1 on 1d6 of a shaman being present (or 1-2 on 1d6 if a vihm lord is present). A shaman is equivalent to a regular vihm statistically, but has Clerical abilities at level 1d4+1.
