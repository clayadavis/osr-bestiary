.. title: Snake, Uktena
.. slug: snake-uktena
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Snake, Uktena
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 15           |
+-----------------+--------------+
| Hit Dice:       | 9*           |
+-----------------+--------------+
| No. of Attacks: | 1 bite       |
+-----------------+--------------+
| Damage:         | 2d6 + poison |
+-----------------+--------------+
| Movement:       | 40' Swim 30' |
+-----------------+--------------+
| No. Appearing:  | 1            |
+-----------------+--------------+
| Save As:        | Fighter: 9   |
+-----------------+--------------+
| Morale:         | 9            |
+-----------------+--------------+
| Treasure Type:  | J, L         |
+-----------------+--------------+
| XP:             | 1,150        |
+-----------------+--------------+

The **Uktena** is a great horned water serpent as large around as a tree trunk with a bright blazing crest on its forehead and scales that glow along the edges like some inner fire. Some have described the horns as ram-like and others as moose or stag antlers. It does not use the horns for attack. Uktena fear thunderbirds and will usually retreat to watery depths when one is near. An uktena keeps some treasures in its lair, generally acquired from victims.

The uktena's bite is powerful and those bitten must also save vs. Poison with a -2 penalty or fall comatose immediately and die within 2d4 turns. Making matters worse, the uktena's brilliant crest allows it to **charm monster** once per turn; those affected will simply move towards the horned serpent and remain charmed so that the uktena may consume at leisure. An opponent that averts its gaze, is blinded, or otherwise cannot see the jewel-like crest will not be affected by the charm attack.
