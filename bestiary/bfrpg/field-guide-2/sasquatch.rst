.. title: Sasquatch
.. slug: sasquatch
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Sasquatch
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 15                      |
+-----------------+-------------------------+
| Hit Dice:       | 5+5                     |
+-----------------+-------------------------+
| No. of Attacks: | 2 fists                 |
+-----------------+-------------------------+
| Damage:         | 1d6/1d6                 |
+-----------------+-------------------------+
| Movement:       | Unarmored 30' Climb 20' |
+-----------------+-------------------------+
| No. Appearing:  | 1d3, Lair 2d6           |
+-----------------+-------------------------+
| Save As:        | Fighter: 5              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 360                     |
+-----------------+-------------------------+

A **Sasquatch** is a seldom-seen large humanoid. It is clearly related in some distant way to apes and neanderthals but may be an entirely separate branch of humankind's kin. Each is over 7’ tall with long arms and walks upright most of the time. A sasquatch is covered in shaggy hair befitting its environment so that it can hide effectively when it does not want to be seen. When more than one is spotted it is usually a family group with young. A sasquatch can communicate with others of its kind, but its actual language is a mystery. That said, it also understands complex concepts and its ability to mimic sounds is extraordinary. It does not keep treasures that other races seek, though one might find various crude objects and trinkets where it lairs.

The sasquatch has nocturnal vision that is superior to human sight, though it does not have actual Darkvision. A sasquatch climbs nearly as easily as moving about by foot. Even for its size, the sasquatch hides in its home environment as easily as a Halfling can in forested areas (with only 10% chance of detection), however they do not have this ability in other environments such as indoors or underground. A sasquatch is essentially immune to cold environmental effects and even extreme or magical cold causes half-damage. If a save is involved with such a cold-based effect it receives a +4 bonus. It fights effectively with its fists but if a weapon is used the sasquatch gains a +2 bonus on its attacks due to its tremendous strength.

Most sasquatch encounters have been largely benign meetings where both parties have simply surprised each other. Many sasquatch are generally peaceful denizens of their forest, though local legends speak of brutal hunters among their kind.
