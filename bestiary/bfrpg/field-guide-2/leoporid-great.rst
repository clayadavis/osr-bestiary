.. title: Leoporid, Great
.. slug: leoporid-great
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Leoporid, Great
.. type: text
.. image:

+-----------------+-----------------------------------------+
| Armor Class:    | 14                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 3                                       |
+-----------------+-----------------------------------------+
| No. of Attacks: | 1 antler or 1 rake (against large only) |
+-----------------+-----------------------------------------+
| Damage:         | 2d4 or 2d6                              |
+-----------------+-----------------------------------------+
| Movement:       | 80’ (5’)                                |
+-----------------+-----------------------------------------+
| No. Appearing:  | Wild 1d4                                |
+-----------------+-----------------------------------------+
| Save As:        | Fighter: 3                              |
+-----------------+-----------------------------------------+
| Morale:         | 9                                       |
+-----------------+-----------------------------------------+
| Treasure Type:  | None                                    |
+-----------------+-----------------------------------------+
| XP:             | 175                                     |
+-----------------+-----------------------------------------+

A **Great Leoporid**, sometimes known as a** jackalope king**, is essentially a massive jackrabbit species found in plains regions. Each has elk-like antlers which may be used in charging attacks (following normal charging rules). Against large opponents the great leoporid will leap and use its rear clawed feet to rake for considerable damage.

Domestication of a leoporid is a very long and difficult process. Riding one requires considerable skill and is a rough ride, but the great speeds make up for the discomfort. Unlike other horse-sized creatures, a great leoporid can turn quickly (having a 5' maneuver rating). Domesticated great leoporids are highly prized as courier mounts in plains regions. A light load for a great leoporid is up to 250 pounds; a heavy load is up to 500 pounds. However, in either case specially-designed saddles and bags are necessary (those designed for equines will not fit).
