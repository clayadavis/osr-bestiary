.. title: Bazeley's Clockwork Armature
.. slug: bazeley-s-clockwork-armature
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bazeley's Clockwork Armature
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 19                       |
+-----------------+--------------------------+
| Hit Dice:       | 10*                      |
+-----------------+--------------------------+
| No. of Attacks: | 2 slashes/1 ray or steam |
+-----------------+--------------------------+
| Damage:         | 1d10/1d10/2d8 or 5d8     |
+-----------------+--------------------------+
| Movement:       | 40'                      |
+-----------------+--------------------------+
| No. Appearing:  | 1                        |
+-----------------+--------------------------+
| Save As:        | Fighter: 10              |
+-----------------+--------------------------+
| Morale:         | 12                       |
+-----------------+--------------------------+
| Treasure Type:  | G                        |
+-----------------+--------------------------+
| XP:             | 1,390                    |
+-----------------+--------------------------+

**Bazeley's Clockwork Armature** is a massive forty-foot tall bronze clockwork contraption that resembles an oblate armored disc, with four long segmented, blade-tipped legs extending out from a recessed band around the middle of the creature, through which cogs and various mechanical components can be seen.

A long brontosaurus-like neck extends from the front upwards, ending in a single flat glowing “eye” surrounded by three armored flaps that can close around it when in danger. This eye has the ability to fire a red ray of energy at a single target, dealing 2d8 damage. It can also expel super-heated steam from nozzles surrounding the eye in a 60’ cone. It can do this once every five rounds, doing 5d8 points of damage. Upon being hit by the steam, a save vs. Death Ray must be made to avoid being knocked prone. The armature can slash with its front legs, with each doing 1d10 points of damage.

The armature takes only a quarter of the damage from fire-based attacks. Conversely, electrical-based attacks (such as **lightning bolt**) meddle with the delicate interior components, causing double damage.

If the head can successfully be retrieved there is a 50% chance to recover an engraved bronze cylinder that can fire a ray of energy 120' which deals 2d8 points of damage. The cylinder will have 2d12 charges remaining, and 2d6 charges may be regained by striking it with a **lightning bolt**.
