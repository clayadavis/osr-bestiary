.. title: Infernal, Shadow Fiend*
.. slug: infernal-shadow-fiend
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Infernal, Shadow Fiend*
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 18‡                 |
+-----------------+---------------------+
| Hit Dice:       | 6*, 8*, 10*, or 12* |
+-----------------+---------------------+
| No. of Attacks: | 1 touch or 1 spell  |
+-----------------+---------------------+
| Damage:         | 1d6 or 2d6 (10 HD+) |
+-----------------+---------------------+
| Movement:       | Fly 180’            |
+-----------------+---------------------+
| No. Appearing:  | 1d4                 |
+-----------------+---------------------+
| Save As:        | Thief: 14           |
+-----------------+---------------------+
| Morale:         | 9                   |
+-----------------+---------------------+
| Treasure Type:  | E                   |
+-----------------+---------------------+
| XP:             | 665                 |
+-----------------+---------------------+

A **Shadow Fiend** is an insubstantial infernal that feeds off the fears, doubts, and nightmares of living creatures. When seen in well-lit areas it appears as a horned and winged humanoid figure whose lower body trails off into nothing. It lacks any facial features, and its body appears to be composed of dense smoke. It is subtle for an infernal, and can remain hidden among populations of humanoids or monsters for years without being detected. It prefers abandoned homes, old ruins, dark sewers, and tunnels as lairs. A shadow fiend grows stronger the more it feeds, as reflected in the variable HD amount above. When in darkness a shadow fiend is effectively **invisible** as per the spell. Regardless of whether it is in darkness or not, it moves in complete silence; only the stirring of the air is a hint as to a shadow fiend's passage.

A shadow fiend that hits a living target with its touch attack deals the listed amount of damage and at the same time regenerates the shadow fiend. A shadow fiend has the power to move objects up to 10 pounds up to 50’ away from itself via magic. It can move said objects about 5’ a round. It will typically use this power to open and close doors, knock over objects, or throw small objects about. The goal is to terrorize and demoralize creatures it is “haunting”, to induce fear and paranoia. In addition to this ability a shadow fiend can cast **darkness** and **phantasmal force** each once per day. However, a shadow fiend caught in areas of bright light (such as the area of a **light** spell or a torch) takes 1d6 damage per round as the light burns away its shadowy form.

Due to its insubstantial nature, a shadow fiend is immune to poison, acid, and cold. It only takes half damage from lightning or fire-based attacks. Magical weapons are required to hit a shadow fiend in combat.
