.. title: Bazeley's Marvelous Spheres
.. slug: bazeley-s-marvelous-spheres
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bazeley's Marvelous Spheres
.. type: text
.. image:

Another invention of the alchemist Bazeley, **The Marvelous Spheres** are small bronze spheres, each around an inch in diameter, almost perfectly smooth, and made of burnished bronze with small runes etched in intricate patterns on the surface.

These spheres are generally found dormant in long-forgotten rooms and dusty corners, guarding old and abandoned treasures. They may be covered with a cloth or otherwise concealed; to an uninformed observer they will appear as useless, if potentially valuable, curiosities. When the object they guard is removed from a specific area, the spheres activate as denoted by a thin line of runes that will gently glow. The runes carved upon them will glow a variety of colors and pulse at differing speeds. The spheres will roll together into a larger construct, which will then attack. This will surprise on 1-4 on a d6.

The spheres stick perfectly to one another and even when part of a larger whole, they move independently of one another, flowing almost like water around obstacles or through small gaps. This allows them to avoid any projectile smaller than a ballista bolt. The spheres are also immune to fire-based damage and take only half from any magical source such as a **fireball**. The larger the form, the stronger the bond between the spheres, translating to a more durable foe. Individually the spheres can move 60’ per round.

When defeated the spheres lose their bonds and collapse to the floor. Every hit dice worth of spheres contains enchanted sapphires worth 100 gp.
