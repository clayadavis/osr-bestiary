.. title: Curse*
.. slug: curse
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Curse*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15‡        |
+-----------------+------------+
| Hit Dice:       | 7*         |
+-----------------+------------+
| No. of Attacks: | 1 devour   |
+-----------------+------------+
| Damage:         | 2d6        |
+-----------------+------------+
| Movement:       | 40’        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 7 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 735        |
+-----------------+------------+

A **Curse** is a reddish brown mass of a viscous liquid about 10’ across; as a liquid it can fit through a space as small as 5” wide. Created by mortals and cursed by an unknown god, it lives in isolation in hatred and fear. They are mainly found in uninhabited areas such as deep in forests or high on mountains.

A curse attacks by jumping at and onto its targets from up to 30’ away and devouring them. The victim may attempt to escape on his or her Initiative with a save vs. Death Ray. Those killed by a curse become curses themselves, transforming the round immediately following death. As such, those slain by a curse may not be raised by anything short of a **wish**.
