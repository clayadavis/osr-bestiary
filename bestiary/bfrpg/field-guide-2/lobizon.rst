.. title: Lobizon*
.. slug: lobizon
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Lobizon*
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 14†            |
+-----------------+----------------+
| Hit Dice:       | 3**            |
+-----------------+----------------+
| No. of Attacks: | 2 grabs/1 lick |
+-----------------+----------------+
| Damage:         | Special        |
+-----------------+----------------+
| Movement:       | 60'            |
+-----------------+----------------+
| No. Appearing:  | 1d4            |
+-----------------+----------------+
| Save As:        | Fighter: 3     |
+-----------------+----------------+
| Morale:         | 10             |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 205            |
+-----------------+----------------+

A **Lobizon** is a person cursed to become a malnourished, tailless wolf-like creature of human size; it has humanoid ears, hands, and feet and is covered in very short fur. A lobizon is nocturnal and has Darkvision to 30'.

A lobizon lives by feeding on the offerings to the dead (from flowers to candles and even non-edible things); if none are found it will feed on the remains of the dead; thus they are commonly sighted in graveyards where there can be as many as 3d6 lobizons.

In combat a lobizon seeks to grab its opponent with both hands, and if successful will automatically lick it. Those licked by a lobizon must save vs. Spells or be cursed to become a lobizon in 2d6 days.
