.. title: Giant Flying Brain
.. slug: giant-flying-brain
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Giant Flying Brain
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 12            |
+-----------------+---------------+
| Hit Dice:       | 5*+1          |
+-----------------+---------------+
| No. of Attacks: | 4 spells      |
+-----------------+---------------+
| Damage:         | By spell      |
+-----------------+---------------+
| Movement:       | Fly 50'       |
+-----------------+---------------+
| No. Appearing:  | Lair 1d4      |
+-----------------+---------------+
| Save As:        | Magic-User: 5 |
+-----------------+---------------+
| Morale:         | 8             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 405           |
+-----------------+---------------+

Created as a unique form of a homunculus, a **Giant Flying Brain** is a giant disembodied brain that is 6' tall and 6' wide, and hovers approximately 5' to 10' in the air. Despite not having limbs it is highly intelligent, able to cast spells to protect itself from potential harm. It also has the ability to read the thoughts of sentient creatures within a 100' range. Without appendages, it cannot hold any type of treasure, but can be found guarding places or things containing great knowledge, such as libraries or magical repositories.

A giant flying brain can cast spells as a 4th level Magic-User. When first encountering a giant flying brain, PCs must save vs. Spells or be paralyzed for 2d8 turns as if targeted by a **hold person** spell.
