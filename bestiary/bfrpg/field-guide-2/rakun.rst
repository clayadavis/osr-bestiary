.. title: Rakun
.. slug: rakun
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Rakun
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 12                      |
+-----------------+-------------------------+
| Hit Dice:       | 1d6 HP                  |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon or one bite    |
+-----------------+-------------------------+
| Damage:         | 1d4 or by weapon or 1d3 |
+-----------------+-------------------------+
| Movement:       | 30' Unarmored 40'       |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 1              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | Q, R each; D, K in lair |
+-----------------+-------------------------+
| XP:             | 10                      |
+-----------------+-------------------------+

The **Rakun** (pronounced as Raccoon but also known as Vaskebjørn) are a race of what can best be described as talking raccoons, speaking their own unique language. Unlike their less-evolved kin, rakun will readily use tools and weapons, having a particular fondness for crossbows of various types (or fire-arms if the setting allows), receiving a +1 bonus to attacks with such device-propelled weaponry. It stands slightly taller than a Halfling but tends to be leaner. Its eyesight is keen at night (double that of a human's nightvision), and even in total darkness has Darkvision out 30’.

When cornered it will bite at opponents. A rakun shares the Halfling ability to hide in natural surroundings, so that outdoors (its preferred forest terrain) there is only a 10% chance of being detected. Even indoors or in non-preferred terrain it can hide such that there is only a 30% chance of detection. A rakun's ability to climb is extraordinary, receiving a substantial bonus to any such attempt, and when climbing trees the rakun almost never fails such a check under normal circumstances (effectively 100% unless circumstances carry substantial penalties).

Rakun do not often mix with other races, as each tends to have a wild or fierce streak to its personality. Within communities of rakun one can find members with additional (class-based) abilities. When classed a rakun uses one HD type smaller than normally utilized (d8 becomes d6, d6 becomes d4, and d4 becomes d3).
