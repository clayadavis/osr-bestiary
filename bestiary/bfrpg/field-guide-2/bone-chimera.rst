.. title: Bone Chimera
.. slug: bone-chimera
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bone Chimera
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 17 (special, see below)       |
+-----------------+-------------------------------+
| Hit Dice:       | 5                             |
+-----------------+-------------------------------+
| No. of Attacks: | 2 bites/2 claws/2 tails       |
+-----------------+-------------------------------+
| Damage:         | 2d6 bite, 1d6 claw, 1d10 tail |
+-----------------+-------------------------------+
| Movement:       | 60’                           |
+-----------------+-------------------------------+
| No. Appearing:  | 1d6                           |
+-----------------+-------------------------------+
| Save As:        | Fighter: 5                    |
+-----------------+-------------------------------+
| Morale:         | 12                            |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 240                           |
+-----------------+-------------------------------+

A **Bone Chimera** is an abominable **undead** monster composed of the skeletons of several different species. It has two heads, five legs (all of different lengths), and a pair of tails. In spite of this odd arrangement it is an effective combatant, able to swiftly move about the battlefield and fight fiercely. One can attack opponents in front with its bites and claws, while assaulting opponents behind with its tails; however, it cannot attack a single opponent with its bite and claws and its tails.

Bone chimeras are immune to **sleep**, **charm**, and **hold** magic (as are all undead monsters), and like normal animated skeletons they suffer half damage from edged weapons and one point only (plus any magical bonus) from small missile weapons like arrows or sling stones. They may be **Turned** as if ghouls.
