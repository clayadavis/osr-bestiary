.. title: Maggot-man*
.. slug: maggot-man
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Maggot-man*
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 17‡                           |
+-----------------+-------------------------------+
| Hit Dice:       | 4* (or more)                  |
+-----------------+-------------------------------+
| No. of Attacks: | 1 fist + special              |
+-----------------+-------------------------------+
| Damage:         | 1d6 + special                 |
+-----------------+-------------------------------+
| Movement:       | 30'                           |
+-----------------+-------------------------------+
| No. Appearing:  | 1                             |
+-----------------+-------------------------------+
| Save As:        | Magic-User: 8                 |
+-----------------+-------------------------------+
| Morale:         | 9                             |
+-----------------+-------------------------------+
| Treasure Type:  | U, V in lair                  |
+-----------------+-------------------------------+
| XP:             | 1,015 (MU-8 equivalent level) |
+-----------------+-------------------------------+

Usually encountered wearing voluminous hooded robes, the **Maggot-man** is a strange form of the semi-undead remains of a powerful necromancer. Should the hood be removed or otherwise its true form revealed, it appears as a terrifying writhing mass of maggots in human form. A character that sees the true form must save vs. Spells (at -2 penalty) or else be affected as if struck by a **cause fear** spell, forcing the affected to flee for 2 turns. Anyone affected that is unable to flee will cower in place, unable to act otherwise. Even those who successfully save are unnerved by the sight, and will have a -2 penalty to attacks or actions.

Even in undeath a maggot-man remains very intelligent and can cast spells as a Magic-User equivalent to twice its hit dice. If the GM utilizes the optional Necromancer supplement, then the maggot-man will usually be a Necromancer instead. In addition to spells the maggot-man attacks by striking its opponent. Anyone struck by a maggot-man has a 2 in 6 chance (1-2 on 1d6) of being affected by a special form of rot grub (see the **Basic Fantasy RPG Core Rules**). Anyone that subsequently dies from the rot grub will rise as a zombie under the control of the maggot-man for a short time as maggots strip the flesh off of the bones (2d4 days). If the maggot-man is defeated while such a rot-grub zombie endures, then the maggot-man will inhabit that body. The maggot-man will usually have one such infected zombie hidden away somewhere nearby as a contingency, though this requires a fairly regular supply of fresh corpses to maintain.

A maggot-man can only be hit by magical weapons or spells. As with all undead, it can be Turned by a Cleric (as a vampire), and is immune to **sleep**, **charm**, or **hold** spells.
