.. title: Bear
.. slug: bear
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Bear
.. type: text
.. image:

**Bears** attack by rending their opponent with their claws, dragging them in and biting them. A successful hit with both paws indicates a hug attack for additional damage (as given for each specific bear type). All bears are very tough to kill, and are able to move and attack for one round after losing all hit points.
