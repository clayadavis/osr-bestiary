.. title: Salamander
.. slug: salamander
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Salamander
.. type: text
.. image:

Salamanders are large, lizard-like creatures from the elemental planes. They are sometimes found on the material plane, arriving through naturally-occurring dimensional rifts or summoned by high-level Magic-Users. Due to their highly magical nature, they cannot be harmed by non-magical weapons.
