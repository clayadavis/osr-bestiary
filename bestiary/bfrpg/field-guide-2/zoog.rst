.. title: Zoog
.. slug: zoog
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Zoog
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 14               |
+-----------------+------------------+
| Hit Dice:       | 1d4 HP           |
+-----------------+------------------+
| No. of Attacks: | 1 dagger or bite |
+-----------------+------------------+
| Damage:         | 1d4              |
+-----------------+------------------+
| Movement:       | 30'              |
+-----------------+------------------+
| No. Appearing:  | 3d4              |
+-----------------+------------------+
| Save As:        | Magic-User: 1    |
+-----------------+------------------+
| Morale:         | 7                |
+-----------------+------------------+
| Treasure Type:  | P, J in lair     |
+-----------------+------------------+
| XP:             | 10               |
+-----------------+------------------+

A **Zoog** is a small rodent-like being with sharp teeth and small tentacles in its mouth. They despise cats and will work diligently to eliminate any feline creatures or beings in the area. Zoogs congregate in family units and are quite intelligent. They use tools, and one can often find zoogs that have magical ability (up to 1d6 levels of Magic-User with +1 hit point per level added). A zoog climbs trees much like a squirrel may.

*"In the tunnels of that twisted wood, whose low prodigious oaks twine groping boughs and shine dim with the phosphorescence of strange fungi, dwell the furtive and secretive zoogs;"*

*-H.P. Lovecraft, Dream-Quest of Unknown Kadath*
