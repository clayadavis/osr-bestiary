.. title: Deadringer
.. slug: deadringer
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Deadringer
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 16                 |
+-----------------+--------------------+
| Hit Dice:       | 5*                 |
+-----------------+--------------------+
| No. of Attacks: | 1 touch or special |
+-----------------+--------------------+
| Damage:         | Special            |
+-----------------+--------------------+
| Movement:       | 30’                |
+-----------------+--------------------+
| No. Appearing:  | 1                  |
+-----------------+--------------------+
| Save As:        | Fighter: 4         |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 400                |
+-----------------+--------------------+

A **Deadringer** is a form of skeletal undead which appears in almost all respects like the more common form of an animated skeleton. A deadringer carries a small wooden-handled brass bell which it rings constantly as soon as it sights possible opponents. Deadringers are often hidden within a group of other undead monsters.

All undead creatures within 20' of a deadringer receive a +1 bonus to attack and damage rolls so long as it rings its bell. Further, any corpses within 50' of the deadringer when the bell is rung will be animated as **Resonated**.

In melee a deadringer will try to touch an opponent with its free hand while ringing the bell. If its attack roll succeeds, the one touched must make a save vs. Spells or be deafened for 1d4 rounds. Such a victim is additionally cursed to attract all undead within 10 miles; all such monsters will treat the accursed as their preferred enemy, though this does not mean that they will ignore attacks by other characters. Undead under the control of spell-casters will cease to obey commands issued by their master, so long as the accursed is within the 10 mile range. The curse can be removed as normal with a **remove curse** spell, or otherwise ends with the death of the afflicted.

Deadringers take half damage from edged weapons and only 1 point from bolts, arrows, or sling bullets. They are immune to **sleep**, **charm**, and **hold**, and can be Turned by a Cleric (as a mummy).
