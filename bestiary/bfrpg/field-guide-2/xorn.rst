.. title: Xorn
.. slug: xorn
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Xorn
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 21             |
+-----------------+----------------+
| Hit Dice:       | 7+1**          |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 4d6            |
+-----------------+----------------+
| Movement:       | 20' Burrow 20' |
+-----------------+----------------+
| No. Appearing:  | 1d6            |
+-----------------+----------------+
| Save As:        | Fighter: 7     |
+-----------------+----------------+
| Morale:         | 12             |
+-----------------+----------------+
| Treasure Type:  | I, J, K, M     |
+-----------------+----------------+
| XP:             | 800            |
+-----------------+----------------+

A **Xorn** is a native to the Elemental Plane of Earth. Xorns are about 5’ tall and wide and weigh about 600 pounds. It speaks Common along with its own elemental language. Because a xorn’s symmetrically-placed eyes allow it to look in any direction, it cannot be flanked. A xorn does not attack fleshy beings except to defend itself or its property since it cannot digest meat. A xorn is indifferent to creatures of the material plane – with the sole exception of anyone carrying a significant amount of precious metals or minerals, which a xorn eats. A xorn can smell food up to 20’ away. It can be quite aggressive when seeking food, especially on the material plane, where such sustenance is harder to find than on its native plane.

A xorn’s favorite mode of attack is to wait just beneath a stone surface until a foe comes within reach, then emerge suddenly. Groups of xorns often send one of their number to the surface to negotiate for food while the remainder position themselves for a surprise attack. A xorn can glide through stone, dirt, or almost any other sort of earth (except metal) as easily as a fish swims through water. Its burrowing leaves behind no tunnel or hole, nor does it create any ripple or other signs of its presence. A **move earth** spell cast on an area containing a burrowing xorn flings the xorn back 30’, stunning the creature for 1 round unless it succeeds on a savings throw vs. Spells.
