.. title: Mithridatium Bush
.. slug: mithridatium-bush
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Mithridatium Bush
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 11†        |
+-----------------+------------+
| Hit Dice:       | 3*         |
+-----------------+------------+
| No. of Attacks: | Special    |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 0'         |
+-----------------+------------+
| No. Appearing:  | 1d4+1      |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 175        |
+-----------------+------------+

The **Mithridatium Bush** is found in the form of a shrub or hedge with numerous tightly-closed pods. When any living creature approaches within 5’ the pods open, revealing exotic deep crimson flowers edged with black. These blossoms exude soporific pollen which can affect all living creatures within a 5’ radius. Those within the radius must make a saving throw vs. Poison or fall into a dreamless sleep, with grogginess lasting a full hour after waking (-2 on all attack rolls and saving throws, and -10% for Thief abilities). The bush is not harmed by blunt or piercing weapons or by most missile weapons, but suffers full damage from slashing or chopping weapons.

Folklore hints at danger if the leaves or branches of this bush are burned, but any actual effects are left for the GM to decide.
