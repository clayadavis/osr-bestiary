.. title: Golem, Tin
.. slug: golem-tin
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Golem, Tin
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 19         |
+-----------------+------------+
| Hit Dice:       | 7*         |
+-----------------+------------+
| No. of Attacks: | 2 hands    |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 30'        |
+-----------------+------------+
| No. Appearing:  | 1d6        |
+-----------------+------------+
| Save As:        | Fighter: 7 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | J, K       |
+-----------------+------------+
| XP:             | 800        |
+-----------------+------------+

A **Tin Golem** is a man-shaped golem made for heavy labor. It is typically about 8’ tall, stocky, and as strong as a hill giant. When encountered, a tin golem will generally be operating a heavy work tool such as a sawmill, mangle, or forge press. It is normally passive, but when malfunctioning will tend to see intruders as workpieces or raw materials. In this case the tin golem will attempt to (for example) saw unfamiliar characters into planks. The tin golem will need to score a hit with both hands in melee; if one hand hits then the target is unable to flee, and will effectively have AC 11 against the other hand's attack. Once both hands have hit the golem will place its target into the heavy work tool, inflicting 3d6 damage if it succeeds. Very strong characters have a chance, calculated as 10% per point of Strength over 15, to resist being forced into the tool.
