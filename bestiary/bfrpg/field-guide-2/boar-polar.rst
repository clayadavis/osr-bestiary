.. title: Boar, Polar
.. slug: boar-polar
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Boar, Polar
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 5          |
+-----------------+------------+
| No. of Attacks: | 1 tusk     |
+-----------------+------------+
| Damage:         | 2d6        |
+-----------------+------------+
| Movement:       | 50' (10')  |
+-----------------+------------+
| No. Appearing:  | Wild 1d6   |
+-----------------+------------+
| Save As:        | Fighter: 5 |
+-----------------+------------+
| Morale:         | 9          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 360        |
+-----------------+------------+

A **Polar** **Boar** is covered in thick mottled white fur and an even thicker layer of fatty blubber. Adults are about 6’ long and 4’ high at the shoulder. While quite rare due to the difficulty in domesticating, a polar boar is large enough to be ridden or to pull a sled. They are used by some northern Dwarf clans.

Due to the extremes of its habitat, a polar boar has an even nastier temperament than a regular boar. It will typically charge and gore its opponent with its sharp tusks. Females and males are equally dangerous. A polar boar is essentially immune to cold environmental effects and even extreme or magical cold causes only half-damage. If a save is involved with such a cold-based effect, it receives a +4 bonus.
