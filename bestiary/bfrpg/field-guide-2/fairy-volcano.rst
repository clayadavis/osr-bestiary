.. title: Fairy, Volcano*
.. slug: fairy-volcano
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Fairy, Volcano*
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 18‡                    |
+-----------------+------------------------+
| Hit Dice:       | 10*                    |
+-----------------+------------------------+
| No. of Attacks: | 4 bludgeon or eruption |
+-----------------+------------------------+
| Damage:         | 1d8/1d8/1d8/1d8 or 4d6 |
+-----------------+------------------------+
| Movement:       | 30’                    |
+-----------------+------------------------+
| No. Appearing:  | 1                      |
+-----------------+------------------------+
| Save As:        | Magic-user: 10         |
+-----------------+------------------------+
| Morale:         | 12                     |
+-----------------+------------------------+
| Treasure Type:  | none                   |
+-----------------+------------------------+
| XP:             | 1,390                  |
+-----------------+------------------------+

A **Volcano Fairy** is a manifestation of the consciousness of a volcano, appearing with a flaming head, glowing rivulets of phantasmal lava running down its skin, and a lava red and basalt black motif as its attributes.

A volcano fairy is able to erupt a stream of lava at its foes. This stream is 5’ wide and 40’ long. It will also set fire to anything flammable. The volcano fairy can only erupt every other round.
