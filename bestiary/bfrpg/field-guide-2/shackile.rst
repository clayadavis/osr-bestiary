.. title: Shackile
.. slug: shackile
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Shackile
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 3                       |
+-----------------+-------------------------+
| No. of Attacks: | 1 weapon/1 beak         |
+-----------------+-------------------------+
| Damage:         | 1d6 or by weapon/1d4    |
+-----------------+-------------------------+
| Movement:       | 30' Fly 10’             |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 3d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | S each, D lair          |
+-----------------+-------------------------+
| XP:             | 100                     |
+-----------------+-------------------------+

A **Shackile** is a humanoid creature with the head, neck, and wings of a goose; on its humanoid torso is the face of a human. This human face has control over the legs; the goose head controls its arms and wings. The two function separately and each have their own separate HD value. If either is destroyed the other may still use its own body parts. A shackile’s goose head attacks anything that gets close to it apart from its own kind. A shackile cannot fly well and will only do so to escape an opponent. Shackiles are shunned by mankind and live in tribes far from civilization. In each tribe of shackiles there will be a leader with 4 HD and AC 18. A shackile speaks Common and will attempt to negotiate before getting too close to enemies. If a shackile surprises an opponent, on its first attack it may break an arm in its beak unless a save vs. Death Ray is succeeded.
