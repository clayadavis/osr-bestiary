.. title: Telethia
.. slug: telethia
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Telethia
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 20                         |
+-----------------+----------------------------+
| Hit Dice:       | 7**                        |
+-----------------+----------------------------+
| No. of Attacks: | 2 claws/2 bite or special  |
+-----------------+----------------------------+
| Damage:         | 2d4/2d4/3d6/3d6 or special |
+-----------------+----------------------------+
| Movement:       | 30' Fly 120'               |
+-----------------+----------------------------+
| No. Appearing:  | Wild 1d4, Lair 3d4         |
+-----------------+----------------------------+
| Save As:        | Fighter: 9                 |
+-----------------+----------------------------+
| Morale:         | 9                          |
+-----------------+----------------------------+
| Treasure Type:  | D                          |
+-----------------+----------------------------+
| XP:             | 800                        |
+-----------------+----------------------------+

A **Telethia** is a large dual-headed beast of magical nature with two pairs of blue glistening wings on a powerful, iridescent body. It ranges from 10’ to 20’ in length (plus a tail of about 10’) and has a wingspan of 50’. It has vicious claws on all four legs and a muscular jaw set in each head. A telethia can live for up to 400 years.

What makes a telethia truly frightening is not its vicious claws or multiple jaws but rather its ability to read the mind of its foes. It can use **ESP** (as the spell, but continuously), and once per day can copy the memorized spells of a chosen target; those spells remain available to the telethia for the remainder of the day.

Although a telethia is intelligent it is not sapient. It usually hunts in the day before returning to its lair as the sun sets. They form communities around an alpha (AC 22 and 12 HD). Normal telethia fight with +2 morale when near the alpha.
