.. title: Pipe Beast
.. slug: pipe-beast
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Pipe Beast
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 18                           |
+-----------------+------------------------------+
| Hit Dice:       | 6+6*                         |
+-----------------+------------------------------+
| No. of Attacks: | 2 claws/1 tail or acid spray |
+-----------------+------------------------------+
| Damage:         | 2d6/2d6/1d10 or 4d8          |
+-----------------+------------------------------+
| Movement:       | 30'                          |
+-----------------+------------------------------+
| No. Appearing:  | 1d3                          |
+-----------------+------------------------------+
| Save As:        | Fighter: 6                   |
+-----------------+------------------------------+
| Morale:         | 12                           |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 555                          |
+-----------------+------------------------------+

**Pipe Beasts** are unnatural arcane constructs and are generally created to guard and protect specific rooms or items. Because of this singular focus it will attack anyone who enters the room or guarded area, with the exception of the person who created it, until the threat is destroyed or the pipe beast is.

A pipe beast is well-equipped for this type of duty with powerful pincer-like claws and a slashing tail. It can also spew a cone of pure acid every 4 rounds, dealing damage to everthing in its path. A successful save vs. Dragon Breath reduces this damage by half. The acid cone affects an area 60' long and is 20' wide at its furthest point. The pipe beast can only use this attack 3 times a day before it must recharge its supply.

Pipe beasts have a very hard chitinous exoskeleton which provides ample protection against most weapons; further, because of their construction fire seems to have no effect on them.
