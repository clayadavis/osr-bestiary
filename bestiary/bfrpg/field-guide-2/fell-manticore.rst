.. title: Fell Manticore
.. slug: fell-manticore
.. date: 2019-11-08 13:50:52.588217
.. tags:
.. description: Fell Manticore
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 16                         |
+-----------------+----------------------------+
| Hit Dice:       | 6*                         |
+-----------------+----------------------------+
| No. of Attacks: | 2 claws/1 bite or 6 spikes |
+-----------------+----------------------------+
| Damage:         | 1d4/1d4/2d4 or 1d6         |
+-----------------+----------------------------+
| Movement:       | 40' Fly 90’                |
+-----------------+----------------------------+
| No. Appearing:  | 1d2, Lair 1d3              |
+-----------------+----------------------------+
| Save As:        | Fighter: 6                 |
+-----------------+----------------------------+
| Morale:         | 9                          |
+-----------------+----------------------------+
| Treasure Type:  | D                          |
+-----------------+----------------------------+
| XP:             | 555                        |
+-----------------+----------------------------+

A **Fell Manticore** is a crossbreed of the common manticore and a displacer beast. Slightly smaller and leaner than a normal manticore, it is otherwise similar in appearance, except that its skin and fur are jet black, like a displacer beast. If three are encountered in a lair, the third will be a juvenile (roll % and adjust accordingly), as the juveniles fight amongst themselves until only the strongest of the litter remains.

A fell manticore has all of the usual abilities of the common manticore, including the ability to shoot spikes from its tail (6 per round, 24 spikes total; it can regrow two spikes per day). Each also possesses the light-warping qualities of a displacer beast, causing it to appear to be 3’ from its actual position, imposing a -2 penalty on opponents’ attack rolls and granting it a +2 bonus to saving throws.
