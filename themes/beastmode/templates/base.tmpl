{#  -*- coding: utf-8 -*- #}
{% import 'base_helper.tmpl' as base with context %}
{{ set_locale(lang) }}
{{ base.html_headstart() }}
{% block extra_head %}
{#  Leave this block alone. #}
{% endblock %}
{{ template_hooks['extra_head']() }}
</head>
<body>
<a href="#content" class="sr-only sr-only-focusable">{{ messages("Skip to main content") }}</a>

<!-- Header and menu bar -->
<div class="container">
    <header class="blog-header py-3">
        <div class="row nbb-header align-items-center">
            {# <div class="col-md-3 col-xs-2 col-sm-2" style="width: auto;">
                <div class="collapse bs-nav-collapsible bootblog4-search-form-holder">
                    {{ search_form }}
                </div>
            </div> #}
            <div class="col bootblog4-brand text-center" style="width: auto;">
                <a class="navbar-brand blog-header-logo text-dark" href="{{ abs_link(_link("root", None, lang)) }}">
                {% if logo_url %}
                    <img src="{{ logo_url }}" alt="{{ blog_title|e }}" id="logo" class="d-inline-block align-top">
                {% endif %}

                {% if show_blog_title %}
                    <span id="blog-title">{{ blog_title|e }}</span>
                {% endif %}
                </a>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-light bg-white static-top">
        <ul class="navbar-nav nav-fill d-flex flex-row w-100">
            {{ base.html_navigation_links_entries(navigation_links) }}
            {{ template_hooks['menu']() }}
        </ul>
    </nav>
{% block before_content %}{% endblock %}
</div>

<div class="container" id="content" role="main">
    <div class="body-content">
        {% if theme_config.get('sidebar') %}
            <div class="row"><div class="col-md-8 blog-main">
        {% endif %}
        <!--Body content-->
        {{ template_hooks['page_header']() }}
        {% block extra_header %}{% endblock %}
        {% block content %}{% endblock %}
        <!--End of body content-->
        {% if theme_config.get('sidebar') %}
        </div><aside class="col-md-4 blog-sidebar">{{ theme_config.get('sidebar') }}</aside></div>
        {% endif %}

        <footer id="footer">
            {{ content_footer }}
            {{ template_hooks['page_footer']() }}
            {% block extra_footer %}{% endblock %}
        </footer>
    </div>
</div>

{{ base.late_load_js() }}
    {% if date_fanciness != 0 %}
        <!-- fancy dates -->
        <script>
        moment.locale("{{ momentjs_locales[lang] }}");
        fancydates({{ date_fanciness }}, {{ js_date_format }});
        </script>
        <!-- end fancy dates -->
    {% endif %}
    {% block extra_js %}{% endblock %}
{{ body_end }}
{{ template_hooks['body_end']() }}
</body>
</html>
