.. title: The Free Bestiary
.. slug: index
.. date: 2019-11-04 18:35:23 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: true

Welcome to *The Free Bestiary* for old-school, OSR, and old-school-inspired
RPGs. This project is open-source, mobile-friendly, and free of ads and
trackers. Check out the Bestiary_ section to get started.

This site including all of the monster data can be found on our our GitLab_
repository. You can help out by adding tags, correcting typos, or just raising
issues on the repo.

This project owes a huge debt to `Basic Fantasy Role-Playing Game`_ (BFRPG) for
providing an amazing and open-source collection of monsters to build upon.

.. _bestiary: link://category_index
.. _GitLab: https://gitlab.com/clayadavis/osr-bestiary
.. _`Basic Fantasy Role-Playing Game`: https://www.basicfantasy.org/
